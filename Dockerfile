FROM alpine:3.11

LABEL MAINTAINER KiyanjaLabs.org

RUN  apk update \
  && apk upgrade --available\
  && apk add ca-certificates \
  && update-ca-certificates \
  && apk add --update coreutils && rm -rf /var/cache/apk/*   \ 
  && apk add --update openjdk11 tzdata curl unzip bash \
  && apk add --no-cache nss \
  && rm -rf /var/cache/apk/* \
  && apk add --no-cache tini openrc busybox-initscripts

RUN mkdir -p /srv/asac/app/hubasac/extract-svc/lib /srv/asac/app/hubasac/extract-svc/extract_hubasac /srv/asac/app/hubasac/extract-svc/config \
    mkdir -p /var/log/hubasac

ADD ./scripts* /srv/asac/app/hubasac/extract-svc/scripts
ADD ./config* /srv/asac/app/hubasac/extract-svc/config
ADD ./lib* /srv/asac/app/hubasac/extract-svc/lib
ADD ./extract_hubasac* /srv/asac/app/hubasac/extract-svc/extract_hubasac
COPY ./scripts/initJob.sh /srv/asac/app/hubasac/extract-svc/scripts/initJob.sh
COPY ./jobInfo.properties /srv/asac/app/hubasac/extract-svc/jobInfo.properties
COPY ./extract_hubasac/extract_hubasac_run.sh /srv/asac/app/hubasac/extract-svc/extract_hubasac/extract_hubasac_run.sh

CMD ["chmod", "+x", "/srv/asac/app/hubasac/extract-svc/scripts/initJob.sh"]

WORKDIR /srv/asac/app/hubasac/extract-svc/extract_hubasac

ENTRYPOINT ["/bin/sh","/srv/asac/app/hubasac/extract-svc/scripts/initJob.sh"]