// ============================================================================
//
// Copyright (c) 2006-2015, Talend SA
//
// Ce code source a été automatiquement généré par_Talend Open Studio for ESB
// / Soumis à la Licence Apache, Version 2.0 (la "Licence") ;
// votre utilisation de ce fichier doit respecter les termes de la Licence.
// Vous pouvez obtenir une copie de la Licence sur
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Sauf lorsqu'explicitement prévu par la loi en vigueur ou accepté par écrit, le logiciel
// distribué sous la Licence est distribué "TEL QUEL",
// SANS GARANTIE OU CONDITION D'AUCUNE SORTE, expresse ou implicite.
// Consultez la Licence pour connaître la terminologie spécifique régissant les autorisations et
// les limites prévues par la Licence.


package extracteur_hubasac_backup.extract_assure_1_5;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 





@SuppressWarnings("unused")

/**
 * Job: extract_assure Purpose: <br>
 * Description:  <br>
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status 
 */
public class extract_assure implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(assure_FieldSeparator != null){
				
					this.setProperty("assure_FieldSeparator", assure_FieldSeparator.toString());
				
			}
			
			if(assure_RowSeparator != null){
				
					this.setProperty("assure_RowSeparator", assure_RowSeparator.toString());
				
			}
			
			if(assure_Encoding != null){
				
					this.setProperty("assure_Encoding", assure_Encoding.toString());
				
			}
			
			if(assure_Header != null){
				
					this.setProperty("assure_Header", assure_Header.toString());
				
			}
			
			if(assure_File != null){
				
					this.setProperty("assure_File", assure_File.toString());
				
			}
			
			if(pg_connexion_Port != null){
				
					this.setProperty("pg_connexion_Port", pg_connexion_Port.toString());
				
			}
			
			if(pg_connexion_Login != null){
				
					this.setProperty("pg_connexion_Login", pg_connexion_Login.toString());
				
			}
			
			if(pg_connexion_AdditionalParams != null){
				
					this.setProperty("pg_connexion_AdditionalParams", pg_connexion_AdditionalParams.toString());
				
			}
			
			if(pg_connexion_Schema != null){
				
					this.setProperty("pg_connexion_Schema", pg_connexion_Schema.toString());
				
			}
			
			if(pg_connexion_Server != null){
				
					this.setProperty("pg_connexion_Server", pg_connexion_Server.toString());
				
			}
			
			if(pg_connexion_Password != null){
				
					this.setProperty("pg_connexion_Password", pg_connexion_Password.toString());
				
			}
			
			if(pg_connexion_Database != null){
				
					this.setProperty("pg_connexion_Database", pg_connexion_Database.toString());
				
			}
			
			if(assure_rejects_structure_check_FieldSeparator != null){
				
					this.setProperty("assure_rejects_structure_check_FieldSeparator", assure_rejects_structure_check_FieldSeparator.toString());
				
			}
			
			if(assure_rejects_structure_check_File != null){
				
					this.setProperty("assure_rejects_structure_check_File", assure_rejects_structure_check_File.toString());
				
			}
			
			if(assure_rejects_structure_check_Encoding != null){
				
					this.setProperty("assure_rejects_structure_check_Encoding", assure_rejects_structure_check_Encoding.toString());
				
			}
			
			if(assure_rejects_structure_check_RowSeparator != null){
				
					this.setProperty("assure_rejects_structure_check_RowSeparator", assure_rejects_structure_check_RowSeparator.toString());
				
			}
			
			if(assure_rejet_rg_RowSeparator != null){
				
					this.setProperty("assure_rejet_rg_RowSeparator", assure_rejet_rg_RowSeparator.toString());
				
			}
			
			if(assure_rejet_rg_File != null){
				
					this.setProperty("assure_rejet_rg_File", assure_rejet_rg_File.toString());
				
			}
			
			if(assure_rejet_rg_FieldSeparator != null){
				
					this.setProperty("assure_rejet_rg_FieldSeparator", assure_rejet_rg_FieldSeparator.toString());
				
			}
			
			if(assure_rejet_rg_Encoding != null){
				
					this.setProperty("assure_rejet_rg_Encoding", assure_rejet_rg_Encoding.toString());
				
			}
			
			if(assure_rejet_schema_File != null){
				
					this.setProperty("assure_rejet_schema_File", assure_rejet_schema_File.toString());
				
			}
			
			if(assure_rejet_schema_Encoding != null){
				
					this.setProperty("assure_rejet_schema_Encoding", assure_rejet_schema_Encoding.toString());
				
			}
			
			if(assure_rejet_schema_FieldSeparator != null){
				
					this.setProperty("assure_rejet_schema_FieldSeparator", assure_rejet_schema_FieldSeparator.toString());
				
			}
			
			if(assure_rejet_schema_RowSeparator != null){
				
					this.setProperty("assure_rejet_schema_RowSeparator", assure_rejet_schema_RowSeparator.toString());
				
			}
			
		}
		
		//if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if(NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

public String assure_FieldSeparator;
public String getAssure_FieldSeparator(){
	return this.assure_FieldSeparator;
}
public String assure_RowSeparator;
public String getAssure_RowSeparator(){
	return this.assure_RowSeparator;
}
public String assure_Encoding;
public String getAssure_Encoding(){
	return this.assure_Encoding;
}
public Integer assure_Header;
public Integer getAssure_Header(){
	return this.assure_Header;
}
		public String assure_File;
		public String getAssure_File(){
			return this.assure_File;
		}
		
public String pg_connexion_Port;
public String getPg_connexion_Port(){
	return this.pg_connexion_Port;
}
public String pg_connexion_Login;
public String getPg_connexion_Login(){
	return this.pg_connexion_Login;
}
public String pg_connexion_AdditionalParams;
public String getPg_connexion_AdditionalParams(){
	return this.pg_connexion_AdditionalParams;
}
public String pg_connexion_Schema;
public String getPg_connexion_Schema(){
	return this.pg_connexion_Schema;
}
public String pg_connexion_Server;
public String getPg_connexion_Server(){
	return this.pg_connexion_Server;
}
public java.lang.String pg_connexion_Password;
public java.lang.String getPg_connexion_Password(){
	return this.pg_connexion_Password;
}
public String pg_connexion_Database;
public String getPg_connexion_Database(){
	return this.pg_connexion_Database;
}
public String assure_rejects_structure_check_FieldSeparator;
public String getAssure_rejects_structure_check_FieldSeparator(){
	return this.assure_rejects_structure_check_FieldSeparator;
}
		public String assure_rejects_structure_check_File;
		public String getAssure_rejects_structure_check_File(){
			return this.assure_rejects_structure_check_File;
		}
		
public String assure_rejects_structure_check_Encoding;
public String getAssure_rejects_structure_check_Encoding(){
	return this.assure_rejects_structure_check_Encoding;
}
public String assure_rejects_structure_check_RowSeparator;
public String getAssure_rejects_structure_check_RowSeparator(){
	return this.assure_rejects_structure_check_RowSeparator;
}
public String assure_rejet_rg_RowSeparator;
public String getAssure_rejet_rg_RowSeparator(){
	return this.assure_rejet_rg_RowSeparator;
}
		public String assure_rejet_rg_File;
		public String getAssure_rejet_rg_File(){
			return this.assure_rejet_rg_File;
		}
		
public String assure_rejet_rg_FieldSeparator;
public String getAssure_rejet_rg_FieldSeparator(){
	return this.assure_rejet_rg_FieldSeparator;
}
public String assure_rejet_rg_Encoding;
public String getAssure_rejet_rg_Encoding(){
	return this.assure_rejet_rg_Encoding;
}
		public String assure_rejet_schema_File;
		public String getAssure_rejet_schema_File(){
			return this.assure_rejet_schema_File;
		}
		
public String assure_rejet_schema_Encoding;
public String getAssure_rejet_schema_Encoding(){
	return this.assure_rejet_schema_Encoding;
}
public String assure_rejet_schema_FieldSeparator;
public String getAssure_rejet_schema_FieldSeparator(){
	return this.assure_rejet_schema_FieldSeparator;
}
public String assure_rejet_schema_RowSeparator;
public String getAssure_rejet_schema_RowSeparator(){
	return this.assure_rejet_schema_RowSeparator;
}
	}
	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "1.5";
	private final String jobName = "extract_assure";
	private final String projectName = "EXTRACTEUR_HUBASAC_BACKUP";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}
	
	public void setDataSourceReferences(List serviceReferences) throws Exception{
		
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();
		
		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils.getServices(serviceReferences,  javax.sql.DataSource.class).entrySet()) {
                    dataSources.put(entry.getKey(), entry.getValue());
                    talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}


private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				extract_assure.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(extract_assure.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
			}
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void tFileList_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileInputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSchemaComplianceCheck_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileCopy_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileCopy_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_5_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileList_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tFileCopy_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
	

	public static class ContextBean {
		static String evaluate(String context, String contextExpression)
				throws IOException, javax.script.ScriptException {
			boolean isExpression = contextExpression.contains("+") || contextExpression.contains("(");
			final String prefix = isExpression ? "\"" : "";
			java.util.Properties defaultProps = new java.util.Properties();
			java.io.InputStream inContext = extract_assure.class.getClassLoader()
					.getResourceAsStream("extracteur_hubasac_backup/extract_assure_1_5/contexts/" + context + ".properties");
			if (inContext == null) {
				inContext = extract_assure.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + context + ".properties");
			}
			try {
			    defaultProps.load(inContext);
			} finally {
			    inContext.close();
			}
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("context.([\\w]+)");
			java.util.regex.Matcher matcher = pattern.matcher(contextExpression);

			while (matcher.find()) {
				contextExpression = contextExpression.replaceAll(matcher.group(0),
						prefix + defaultProps.getProperty(matcher.group(1)) + prefix);
			}
			if (contextExpression.startsWith("/services")) {
				contextExpression = contextExpression.replaceFirst("/services","");
            }
			return isExpression ? evaluateContextExpression(contextExpression) : contextExpression;
		}

		public static String evaluateContextExpression(String expression) throws javax.script.ScriptException {
			javax.script.ScriptEngineManager manager = new javax.script.ScriptEngineManager();
			javax.script.ScriptEngine engine = manager.getEngineByName("nashorn");
			// Add some import for Java
			expression = expression.replaceAll("System.getProperty", "java.lang.System.getProperty");
			return engine.eval(expression).toString();
		}

        public static String getContext(String context, String contextName, String jobName) throws Exception {

            String currentContext = null;
            String jobNameStripped = jobName.substring(jobName.lastIndexOf(".") + 1);

            boolean inOSGi = routines.system.BundleUtils.inOSGi();

            if (inOSGi) {
                java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobNameStripped);

                if (jobProperties != null) {
                    currentContext = (String)jobProperties.get("context");
                }
            }

            return contextName.contains("context.") ? evaluate(currentContext == null ? context : currentContext, contextName) : contextName;
        }
    }








public static class row11Struct implements routines.system.IPersistableRow<row11Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row11Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row6Struct implements routines.system.IPersistableRow<row6Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row6Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row5Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class validesStruct implements routines.system.IPersistableRow<validesStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(validesStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class rejetsStruct implements routines.system.IPersistableRow<rejetsStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(rejetsStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row4Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row12Struct implements routines.system.IPersistableRow<row12Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row12Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class rejet_SchStruct implements routines.system.IPersistableRow<rejet_SchStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(rejet_SchStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row7Struct implements routines.system.IPersistableRow<row7Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row7Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row10Struct implements routines.system.IPersistableRow<row10Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row10Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class rejet_strucStruct implements routines.system.IPersistableRow<rejet_strucStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(rejet_strucStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row9Struct implements routines.system.IPersistableRow<row9Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row9Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row8Struct implements routines.system.IPersistableRow<row8Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[0];

	
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String qualite;

				public String getQualite () {
					return this.qualite;
				}
				
			    public String nom;

				public String getNom () {
					return this.nom;
				}
				
			    public String prenom;

				public String getPrenom () {
					return this.prenom;
				}
				
			    public String profession;

				public String getProfession () {
					return this.profession;
				}
				
			    public java.util.Date date_naissance;

				public java.util.Date getDate_naissance () {
					return this.date_naissance;
				}
				
			    public String num_contribuable;

				public String getNum_contribuable () {
					return this.num_contribuable;
				}
				
			    public String ville;

				public String getVille () {
					return this.ville;
				}
				
			    public String rue;

				public String getRue () {
					return this.rue;
				}
				
			    public String boite_postale;

				public String getBoite_postale () {
					return this.boite_postale;
				}
				
			    public String telephone;

				public String getTelephone () {
					return this.telephone;
				}
				
			    public String email;

				public String getEmail () {
					return this.email;
				}
				
			    public String numero_permis;

				public String getNumero_permis () {
					return this.numero_permis;
				}
				
			    public String categorie_permis;

				public String getCategorie_permis () {
					return this.categorie_permis;
				}
				
			    public java.util.Date date_delivrance;

				public java.util.Date getDate_delivrance () {
					return this.date_delivrance;
				}
				
			    public String permis_delivre_par;

				public String getPermis_delivre_par () {
					return this.permis_delivre_par;
				}
				
			    public String nom_prenom_conducteur;

				public String getNom_prenom_conducteur () {
					return this.nom_prenom_conducteur;
				}
				
			    public java.util.Date date_naissance_conducteur;

				public java.util.Date getDate_naissance_conducteur () {
					return this.date_naissance_conducteur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_assure, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_assure) {

        	try {

        		int length = 0;
		
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.qualite = readString(dis);
					
					this.nom = readString(dis);
					
					this.prenom = readString(dis);
					
					this.profession = readString(dis);
					
					this.date_naissance = readDate(dis);
					
					this.num_contribuable = readString(dis);
					
					this.ville = readString(dis);
					
					this.rue = readString(dis);
					
					this.boite_postale = readString(dis);
					
					this.telephone = readString(dis);
					
					this.email = readString(dis);
					
					this.numero_permis = readString(dis);
					
					this.categorie_permis = readString(dis);
					
					this.date_delivrance = readDate(dis);
					
					this.permis_delivre_par = readString(dis);
					
					this.nom_prenom_conducteur = readString(dis);
					
					this.date_naissance_conducteur = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.qualite,dos);
					
					// String
				
						writeString(this.nom,dos);
					
					// String
				
						writeString(this.prenom,dos);
					
					// String
				
						writeString(this.profession,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance,dos);
					
					// String
				
						writeString(this.num_contribuable,dos);
					
					// String
				
						writeString(this.ville,dos);
					
					// String
				
						writeString(this.rue,dos);
					
					// String
				
						writeString(this.boite_postale,dos);
					
					// String
				
						writeString(this.telephone,dos);
					
					// String
				
						writeString(this.email,dos);
					
					// String
				
						writeString(this.numero_permis,dos);
					
					// String
				
						writeString(this.categorie_permis,dos);
					
					// java.util.Date
				
						writeDate(this.date_delivrance,dos);
					
					// String
				
						writeString(this.permis_delivre_par,dos);
					
					// String
				
						writeString(this.nom_prenom_conducteur,dos);
					
					// java.util.Date
				
						writeDate(this.date_naissance_conducteur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",qualite="+qualite);
		sb.append(",nom="+nom);
		sb.append(",prenom="+prenom);
		sb.append(",profession="+profession);
		sb.append(",date_naissance="+String.valueOf(date_naissance));
		sb.append(",num_contribuable="+num_contribuable);
		sb.append(",ville="+ville);
		sb.append(",rue="+rue);
		sb.append(",boite_postale="+boite_postale);
		sb.append(",telephone="+telephone);
		sb.append(",email="+email);
		sb.append(",numero_permis="+numero_permis);
		sb.append(",categorie_permis="+categorie_permis);
		sb.append(",date_delivrance="+String.valueOf(date_delivrance));
		sb.append(",permis_delivre_par="+permis_delivre_par);
		sb.append(",nom_prenom_conducteur="+nom_prenom_conducteur);
		sb.append(",date_naissance_conducteur="+String.valueOf(date_naissance_conducteur));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row8Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tFileList_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileList_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row1Struct row1 = new row1Struct();
row2Struct row2 = new row2Struct();
row2Struct row4 = row2;
validesStruct valides = new validesStruct();
validesStruct row5 = valides;
rejetsStruct rejets = new rejetsStruct();
rejetsStruct row6 = rejets;
row11Struct row11 = new row11Struct();
row3Struct row3 = new row3Struct();
row3Struct row7 = row3;
rejet_SchStruct rejet_Sch = new rejet_SchStruct();
row12Struct row12 = new row12Struct();
row8Struct row8 = new row8Struct();
row8Struct row9 = row8;
rejet_strucStruct rejet_struc = new rejet_strucStruct();
row10Struct row10 = new row10Struct();



	
	/**
	 * [tFileList_1 begin ] start
	 */

				
			int NB_ITERATE_tFileInputDelimited_1 = 0; //for statistics
			

	
		
		ok_Hash.put("tFileList_1", false);
		start_Hash.put("tFileList_1", System.currentTimeMillis());
		
	
	currentComponent="tFileList_1";

	
		int tos_count_tFileList_1 = 0;
		
	
 
     
    
  String directory_tFileList_1 = "/var/hubasac/inputfile/assure";
  final java.util.List<String> maskList_tFileList_1 = new java.util.ArrayList<String>();
  final java.util.List<java.util.regex.Pattern> patternList_tFileList_1 = new java.util.ArrayList<java.util.regex.Pattern>(); 
    maskList_tFileList_1.add("assure*.txt");  
  for (final String filemask_tFileList_1 : maskList_tFileList_1) {
	String filemask_compile_tFileList_1 = filemask_tFileList_1;
	
		filemask_compile_tFileList_1 = org.apache.oro.text.GlobCompiler.globToPerl5(filemask_tFileList_1.toCharArray(), org.apache.oro.text.GlobCompiler.DEFAULT_MASK);
	
		java.util.regex.Pattern fileNamePattern_tFileList_1 = java.util.regex.Pattern.compile(filemask_compile_tFileList_1, java.util.regex.Pattern.CASE_INSENSITIVE);
	
	patternList_tFileList_1.add(fileNamePattern_tFileList_1);
  }
  int NB_FILEtFileList_1 = 0;

  final boolean case_sensitive_tFileList_1 = false;
	
	
	
    final java.util.List<java.io.File> list_tFileList_1 = new java.util.ArrayList<java.io.File>();
    final java.util.Set<String> filePath_tFileList_1 = new java.util.HashSet<String>();
	java.io.File file_tFileList_1 = new java.io.File(directory_tFileList_1);
    
		file_tFileList_1.listFiles(new java.io.FilenameFilter() {
			public boolean accept(java.io.File dir, String name) {
				java.io.File file = new java.io.File(dir, name);
				
	                if (!file.isDirectory()) {
						
    	String fileName_tFileList_1 = file.getName();
		for (final java.util.regex.Pattern fileNamePattern_tFileList_1 : patternList_tFileList_1) {
          	if (fileNamePattern_tFileList_1.matcher(fileName_tFileList_1).matches()){
					if(!filePath_tFileList_1.contains(file.getAbsolutePath())) {
			          list_tFileList_1.add(file);
			          filePath_tFileList_1.add(file.getAbsolutePath());
			        }
			}
		}
	                	return true;
	                } else {
	                  file.listFiles(this);
	                }
				
				return false;
			}
		}
		); 
      java.util.Collections.sort(list_tFileList_1);
    
    for (int i_tFileList_1 = 0; i_tFileList_1 < list_tFileList_1.size(); i_tFileList_1++){
      java.io.File files_tFileList_1 = list_tFileList_1.get(i_tFileList_1);
      String fileName_tFileList_1 = files_tFileList_1.getName();
      
      String currentFileName_tFileList_1 = files_tFileList_1.getName(); 
      String currentFilePath_tFileList_1 = files_tFileList_1.getAbsolutePath();
      String currentFileDirectory_tFileList_1 = files_tFileList_1.getParent();
      String currentFileExtension_tFileList_1 = null;
      
      if (files_tFileList_1.getName().contains(".") && files_tFileList_1.isFile()){
        currentFileExtension_tFileList_1 = files_tFileList_1.getName().substring(files_tFileList_1.getName().lastIndexOf(".") + 1);
      } else{
        currentFileExtension_tFileList_1 = "";
      }
      
      NB_FILEtFileList_1 ++;
      globalMap.put("tFileList_1_CURRENT_FILE", currentFileName_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEPATH", currentFilePath_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEDIRECTORY", currentFileDirectory_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEEXTENSION", currentFileExtension_tFileList_1);
      globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
      
 



/**
 * [tFileList_1 begin ] stop
 */
	
	/**
	 * [tFileList_1 main ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 


	tos_count_tFileList_1++;

/**
 * [tFileList_1 main ] stop
 */
	
	/**
	 * [tFileList_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_begin ] stop
 */
	NB_ITERATE_tFileInputDelimited_1++;
	
	
					if(execStat){				
	       				runStat.updateStatOnConnection("row2", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row5", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("rejet_struc", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row4", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row12", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row11", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("valides", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row6", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row7", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("OnComponentOk1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("rejet_Sch", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row8", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row9", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("rejets", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row10", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row3", 3, 0);
					}           			
				
				if(execStat){
					runStat.updateStatOnConnection("iterate1", 1, "exec" + NB_ITERATE_tFileInputDelimited_1);
					//Thread.sleep(1000);
				}				
			






	
	/**
	 * [tDBOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_1", false);
		start_Hash.put("tDBOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row5");
					}
				
		int tos_count_tDBOutput_1 = 0;
		





String dbschema_tDBOutput_1 = null;
	dbschema_tDBOutput_1 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_1 = null;
if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
	tableName_tDBOutput_1 = ("assure");
} else {
	tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "\".\"" + ("assure");
}

        int updateKeyCount_tDBOutput_1 = 2;
        if(updateKeyCount_tDBOutput_1 < 1) {
            throw new RuntimeException("For update, Schema must have a key");
        } else if (updateKeyCount_tDBOutput_1 == 23 && true) {
                    System.err.println("For update, every Schema column can not be a key");
        }

int nb_line_tDBOutput_1 = 0;
int nb_line_update_tDBOutput_1 = 0;
int nb_line_inserted_tDBOutput_1 = 0;
int nb_line_deleted_tDBOutput_1 = 0;
int nb_line_rejected_tDBOutput_1 = 0;

int deletedCount_tDBOutput_1=0;
int updatedCount_tDBOutput_1=0;
int insertedCount_tDBOutput_1=0;
int rowsToCommitCount_tDBOutput_1=0;
int rejectedCount_tDBOutput_1=0;

boolean whetherReject_tDBOutput_1 = false;

java.sql.Connection conn_tDBOutput_1 = null;
String dbUser_tDBOutput_1 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_1 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_1 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_1 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;

    conn_tDBOutput_1 = java.sql.DriverManager.getConnection(url_tDBOutput_1,dbUser_tDBOutput_1,dbPwd_tDBOutput_1);
	
	resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
        conn_tDBOutput_1.setAutoCommit(false);
        int commitEvery_tDBOutput_1 = 10000;
        int commitCounter_tDBOutput_1 = 0;



int count_tDBOutput_1=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_1 = conn_tDBOutput_1.getMetaData();
                                boolean whetherExist_tDBOutput_1 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_1 = dbMetaData_tDBOutput_1.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_1 = "public";
                                    if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_1 = stmtSchema_tDBOutput_1.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_1.next()){
                                                defaultSchema_tDBOutput_1 = rsSchema_tDBOutput_1.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_1.next()) {
                                        String table_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_NAME");
                                        String schema_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_1.equals(("assure"))
                                            && (schema_tDBOutput_1.equals(dbschema_tDBOutput_1) || ((dbschema_tDBOutput_1 ==null || dbschema_tDBOutput_1.trim().length() ==0) && defaultSchema_tDBOutput_1.equals(schema_tDBOutput_1)))) {
                                            whetherExist_tDBOutput_1 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_1) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1.createStatement()) {
                                        stmtCreate_tDBOutput_1.execute("CREATE TABLE \"" + tableName_tDBOutput_1 + "\"(\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"qualite\" VARCHAR(128)   not null ,\"nom\" VARCHAR(128)   not null ,\"prenom\" VARCHAR(128)  ,\"profession\" VARCHAR(128)   not null ,\"date_naissance\" TIMESTAMP ,\"num_contribuable\" VARCHAR(128)   not null ,\"ville\" VARCHAR(128)  ,\"rue\" VARCHAR(128)  ,\"boite_postale\" VARCHAR(128)  ,\"telephone\" VARCHAR(128)  ,\"email\" VARCHAR(128)  ,\"numero_permis\" VARCHAR(128)  ,\"categorie_permis\" VARCHAR(128)  ,\"date_delivrance\" TIMESTAMP ,\"permis_delivre_par\" VARCHAR(128)  ,\"nom_prenom_conducteur\" VARCHAR(128)  ,\"date_naissance_conducteur\" TIMESTAMP ,\"c_status\" INT4 default 1  not null ,\"c_date_mis_a_jour\" TIMESTAMP ,\"c_date_transfer\" TIMESTAMP ,\"commentaires\" VARCHAR(128)  ,primary key(\"code_assure\",\"code_assureur\"))");
                                    }
                                }
	    java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement("SELECT COUNT(1) FROM \"" + tableName_tDBOutput_1 + "\" WHERE \"code_assure\" = ? AND \"code_assureur\" = ?");
	    resourceMap.put("pstmt_tDBOutput_1", pstmt_tDBOutput_1);
	    String insert_tDBOutput_1 = "INSERT INTO \"" + tableName_tDBOutput_1 + "\" (\"code_assure\",\"code_assureur\",\"qualite\",\"nom\",\"prenom\",\"profession\",\"date_naissance\",\"num_contribuable\",\"ville\",\"rue\",\"boite_postale\",\"telephone\",\"email\",\"numero_permis\",\"categorie_permis\",\"date_delivrance\",\"permis_delivre_par\",\"nom_prenom_conducteur\",\"date_naissance_conducteur\",\"c_status\",\"c_date_mis_a_jour\",\"c_date_transfer\",\"commentaires\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    java.sql.PreparedStatement pstmtInsert_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(insert_tDBOutput_1);
	    resourceMap.put("pstmtInsert_tDBOutput_1", pstmtInsert_tDBOutput_1);
	    String update_tDBOutput_1 = "UPDATE \"" + tableName_tDBOutput_1 + "\" SET \"qualite\" = ?,\"nom\" = ?,\"prenom\" = ?,\"profession\" = ?,\"date_naissance\" = ?,\"num_contribuable\" = ?,\"ville\" = ?,\"rue\" = ?,\"boite_postale\" = ?,\"telephone\" = ?,\"email\" = ?,\"numero_permis\" = ?,\"categorie_permis\" = ?,\"date_delivrance\" = ?,\"permis_delivre_par\" = ?,\"nom_prenom_conducteur\" = ?,\"date_naissance_conducteur\" = ?,\"c_status\" = ?,\"c_date_mis_a_jour\" = ?,\"c_date_transfer\" = ?,\"commentaires\" = ? WHERE \"code_assure\" = ? AND \"code_assureur\" = ?";
	    java.sql.PreparedStatement pstmtUpdate_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(update_tDBOutput_1);
	    resourceMap.put("pstmtUpdate_tDBOutput_1", pstmtUpdate_tDBOutput_1);
	    

 



/**
 * [tDBOutput_1 begin ] stop
 */



	
	/**
	 * [tLogRow_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_3", false);
		start_Hash.put("tLogRow_3", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"valides");
					}
				
		int tos_count_tLogRow_3 = 0;
		

	///////////////////////
	
         class Util_tLogRow_3 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[23];

        public void addRow(String[] row) {

            for (int i = 0; i < 23; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 22 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 22 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%23$-");
        			        sbformat.append(colLengths[22]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[21] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[22] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_3 util_tLogRow_3 = new Util_tLogRow_3();
        util_tLogRow_3.setTableName("tLogRow_3");
        util_tLogRow_3.addRow(new String[]{"code_assure","code_assureur","qualite","nom","prenom","profession","date_naissance","num_contribuable","ville","rue","boite_postale","telephone","email","numero_permis","categorie_permis","date_delivrance","permis_delivre_par","nom_prenom_conducteur","date_naissance_conducteur","c_status","c_date_mis_a_jour","c_date_transfer","commentaires",});        
 		StringBuilder strBuffer_tLogRow_3 = null;
		int nb_line_tLogRow_3 = 0;
///////////////////////    			



 



/**
 * [tLogRow_3 begin ] stop
 */






	
	/**
	 * [tFileOutputDelimited_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_3", false);
		start_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row11");
					}
				
		int tos_count_tFileOutputDelimited_3 = 0;
		

String fileName_tFileOutputDelimited_3 = "";
    fileName_tFileOutputDelimited_3 = (new java.io.File(context.assure_rejet_rg_File)).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_3 = null;
    String extension_tFileOutputDelimited_3 = null;
    String directory_tFileOutputDelimited_3 = null;
    if((fileName_tFileOutputDelimited_3.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") < fileName_tFileOutputDelimited_3.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        }
        directory_tFileOutputDelimited_3 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_3 = true;
    java.io.File filetFileOutputDelimited_3 = new java.io.File(fileName_tFileOutputDelimited_3);
    globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
        if(filetFileOutputDelimited_3.exists()){
            isFileGenerated_tFileOutputDelimited_3 = false;
        }
            int nb_line_tFileOutputDelimited_3 = 0;
            int splitedFileNo_tFileOutputDelimited_3 = 0;
            int currentRow_tFileOutputDelimited_3 = 0;

            final String OUT_DELIM_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:FIELDSEPARATOR */context.assure_rejet_rg_FieldSeparator/** End field tFileOutputDelimited_3:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:ROWSEPARATOR */context.assure_rejet_rg_RowSeparator/** End field tFileOutputDelimited_3:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_3 != null && directory_tFileOutputDelimited_3.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_3 = new java.io.File(directory_tFileOutputDelimited_3);
                        if(!dir_tFileOutputDelimited_3.exists()) {
                            dir_tFileOutputDelimited_3.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_3 = null;

                        outtFileOutputDelimited_3 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_3, true),context.assure_rejet_rg_Encoding));
                                    if(filetFileOutputDelimited_3.length()==0){
                                        outtFileOutputDelimited_3.write("code_assure");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("code_assureur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("qualite");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("nom");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("prenom");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("profession");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_naissance");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("num_contribuable");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("ville");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("rue");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("boite_postale");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("telephone");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("email");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("numero_permis");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("categorie_permis");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_delivrance");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("permis_delivre_par");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("nom_prenom_conducteur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_naissance_conducteur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_extraction");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_depot");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("Error_Message");
                                        outtFileOutputDelimited_3.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_3", outtFileOutputDelimited_3);
resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

 



/**
 * [tFileOutputDelimited_3 begin ] stop
 */



	
	/**
	 * [tDBOutput_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_3", false);
		start_Hash.put("tDBOutput_3", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row6");
					}
				
		int tos_count_tDBOutput_3 = 0;
		





String dbschema_tDBOutput_3 = null;
	dbschema_tDBOutput_3 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_3 = null;
if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
	tableName_tDBOutput_3 = ("assure_rejects_RG_check");
} else {
	tableName_tDBOutput_3 = dbschema_tDBOutput_3 + "\".\"" + ("assure_rejects_RG_check");
}


int nb_line_tDBOutput_3 = 0;
int nb_line_update_tDBOutput_3 = 0;
int nb_line_inserted_tDBOutput_3 = 0;
int nb_line_deleted_tDBOutput_3 = 0;
int nb_line_rejected_tDBOutput_3 = 0;

int deletedCount_tDBOutput_3=0;
int updatedCount_tDBOutput_3=0;
int insertedCount_tDBOutput_3=0;
int rowsToCommitCount_tDBOutput_3=0;
int rejectedCount_tDBOutput_3=0;

boolean whetherReject_tDBOutput_3 = false;

java.sql.Connection conn_tDBOutput_3 = null;
String dbUser_tDBOutput_3 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_3 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_3 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_3 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;

    conn_tDBOutput_3 = java.sql.DriverManager.getConnection(url_tDBOutput_3,dbUser_tDBOutput_3,dbPwd_tDBOutput_3);
	
	resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);
        conn_tDBOutput_3.setAutoCommit(false);
        int commitEvery_tDBOutput_3 = 10000;
        int commitCounter_tDBOutput_3 = 0;


   int batchSize_tDBOutput_3 = 10000;
   int batchSizeCounter_tDBOutput_3=0;

int count_tDBOutput_3=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_3 = conn_tDBOutput_3.getMetaData();
                                boolean whetherExist_tDBOutput_3 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_3 = dbMetaData_tDBOutput_3.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_3 = "public";
                                    if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_3 = conn_tDBOutput_3.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_3 = stmtSchema_tDBOutput_3.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_3.next()){
                                                defaultSchema_tDBOutput_3 = rsSchema_tDBOutput_3.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_3.next()) {
                                        String table_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_NAME");
                                        String schema_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_3.equals(("assure_rejects_RG_check"))
                                            && (schema_tDBOutput_3.equals(dbschema_tDBOutput_3) || ((dbschema_tDBOutput_3 ==null || dbschema_tDBOutput_3.trim().length() ==0) && defaultSchema_tDBOutput_3.equals(schema_tDBOutput_3)))) {
                                            whetherExist_tDBOutput_3 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_3) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_3 = conn_tDBOutput_3.createStatement()) {
                                        stmtCreate_tDBOutput_3.execute("CREATE TABLE \"" + tableName_tDBOutput_3 + "\"(\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"qualite\" VARCHAR(128)   not null ,\"nom\" VARCHAR(128)   not null ,\"prenom\" VARCHAR(128)  ,\"profession\" VARCHAR(128)   not null ,\"date_naissance\" TIMESTAMP ,\"num_contribuable\" VARCHAR(128)   not null ,\"ville\" VARCHAR(128)  ,\"rue\" VARCHAR(128)  ,\"boite_postal\" VARCHAR(128)  ,\"telephone\" VARCHAR(128)  ,\"email\" VARCHAR(128)  ,\"numero_permis\" VARCHAR(128)  ,\"categorie_permis\" VARCHAR(128)  ,\"date_delivrance\" TIMESTAMP ,\"permis_delivre_par\" VARCHAR(128)  ,\"nom_prenom_conducteur\" VARCHAR(128)  ,\"date_naissance_conducteur\" TIMESTAMP ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"Error_Message\" VARCHAR(1024)  )");
                                    }
                                }
	    String insert_tDBOutput_3 = "INSERT INTO \"" + tableName_tDBOutput_3 + "\" (\"code_assure\",\"code_assureur\",\"qualite\",\"nom\",\"prenom\",\"profession\",\"date_naissance\",\"num_contribuable\",\"ville\",\"rue\",\"boite_postal\",\"telephone\",\"email\",\"numero_permis\",\"categorie_permis\",\"date_delivrance\",\"permis_delivre_par\",\"nom_prenom_conducteur\",\"date_naissance_conducteur\",\"date_extraction\",\"date_depot\",\"Error_Message\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3.prepareStatement(insert_tDBOutput_3);
	    resourceMap.put("pstmt_tDBOutput_3", pstmt_tDBOutput_3);
	    

 



/**
 * [tDBOutput_3 begin ] stop
 */



	
	/**
	 * [tLogRow_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_4", false);
		start_Hash.put("tLogRow_4", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"rejets");
					}
				
		int tos_count_tLogRow_4 = 0;
		

	///////////////////////
	
         class Util_tLogRow_4 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[22];

        public void addRow(String[] row) {

            for (int i = 0; i < 22; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 21 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 21 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%22$-");
        			        sbformat.append(colLengths[21]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[20] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[21] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_4 util_tLogRow_4 = new Util_tLogRow_4();
        util_tLogRow_4.setTableName("tLogRow_4");
        util_tLogRow_4.addRow(new String[]{"code_assure","code_assureur","qualite","nom","prenom","profession","date_naissance","num_contribuable","ville","rue","boite_postale","telephone","email","numero_permis","categorie_permis","date_delivrance","permis_delivre_par","nom_prenom_conducteur","date_naissance_conducteur","date_extraction","date_depot","Error_Message",});        
 		StringBuilder strBuffer_tLogRow_4 = null;
		int nb_line_tLogRow_4 = 0;
///////////////////////    			



 



/**
 * [tLogRow_4 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row4");
					}
				
		int tos_count_tMap_1 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
	String Dnaissance;
	String NumPermis;
	String Ddelivrance;
	String CatPermis;
	String NomPConducteur;
	String DnaissanceConducteur;
	String Qualite;
	String NumContribuableVal;
	String ProfessionVal;
	String CatPermisVal;
	String ContribuableVal;
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
validesStruct valides_tmp = new validesStruct();
rejetsStruct rejets_tmp = new rejetsStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row2");
					}
				
		int tos_count_tLogRow_1 = 0;
		

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[19];

        public void addRow(String[] row) {

            for (int i = 0; i < 19; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 18 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 18 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[18] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"code_assure","code_assureur","qualite","nom","prenom","profession","date_naissance","num_contribuable","ville","rue","boite_postale","telephone","email","numero_permis","categorie_permis","date_delivrance","permis_delivre_par","nom_prenom_conducteur","date_naissance_conducteur",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_4", false);
		start_Hash.put("tFileOutputDelimited_4", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row12");
					}
				
		int tos_count_tFileOutputDelimited_4 = 0;
		

String fileName_tFileOutputDelimited_4 = "";
    fileName_tFileOutputDelimited_4 = (new java.io.File(context.assure_rejet_schema_File)).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_4 = null;
    String extension_tFileOutputDelimited_4 = null;
    String directory_tFileOutputDelimited_4 = null;
    if((fileName_tFileOutputDelimited_4.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_4.lastIndexOf(".") < fileName_tFileOutputDelimited_4.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4;
            extension_tFileOutputDelimited_4 = "";
        } else {
            fullName_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4.substring(0, fileName_tFileOutputDelimited_4.lastIndexOf("."));
            extension_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4.substring(fileName_tFileOutputDelimited_4.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4.substring(0, fileName_tFileOutputDelimited_4.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_4.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4.substring(0, fileName_tFileOutputDelimited_4.lastIndexOf("."));
            extension_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4.substring(fileName_tFileOutputDelimited_4.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_4 = fileName_tFileOutputDelimited_4;
            extension_tFileOutputDelimited_4 = "";
        }
        directory_tFileOutputDelimited_4 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_4 = true;
    java.io.File filetFileOutputDelimited_4 = new java.io.File(fileName_tFileOutputDelimited_4);
    globalMap.put("tFileOutputDelimited_4_FILE_NAME",fileName_tFileOutputDelimited_4);
        if(filetFileOutputDelimited_4.exists()){
            isFileGenerated_tFileOutputDelimited_4 = false;
        }
            int nb_line_tFileOutputDelimited_4 = 0;
            int splitedFileNo_tFileOutputDelimited_4 = 0;
            int currentRow_tFileOutputDelimited_4 = 0;

            final String OUT_DELIM_tFileOutputDelimited_4 = /** Start field tFileOutputDelimited_4:FIELDSEPARATOR */context.assure_rejet_schema_FieldSeparator/** End field tFileOutputDelimited_4:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_4 = /** Start field tFileOutputDelimited_4:ROWSEPARATOR */context.assure_rejet_schema_RowSeparator/** End field tFileOutputDelimited_4:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_4 != null && directory_tFileOutputDelimited_4.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_4 = new java.io.File(directory_tFileOutputDelimited_4);
                        if(!dir_tFileOutputDelimited_4.exists()) {
                            dir_tFileOutputDelimited_4.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_4 = null;

                        outtFileOutputDelimited_4 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_4, true),context.assure_rejet_schema_Encoding));
                                    if(filetFileOutputDelimited_4.length()==0){
                                        outtFileOutputDelimited_4.write("code_assure");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("code_assureur");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("qualite");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("nom");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("prenom");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("profession");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("date_naissance");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("num_contribuable");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("ville");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("rue");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("boite_postale");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("telephone");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("email");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("numero_permis");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("categorie_permis");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("date_delivrance");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("permis_delivre_par");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("nom_prenom_conducteur");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("date_naissance_conducteur");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("date_extraction");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("date_depot");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("errorCode");
                                            outtFileOutputDelimited_4.write(OUT_DELIM_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.write("errorMessage");
                                        outtFileOutputDelimited_4.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_4);
                                        outtFileOutputDelimited_4.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_4", outtFileOutputDelimited_4);
resourceMap.put("nb_line_tFileOutputDelimited_4", nb_line_tFileOutputDelimited_4);

 



/**
 * [tFileOutputDelimited_4 begin ] stop
 */



	
	/**
	 * [tDBOutput_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_4", false);
		start_Hash.put("tDBOutput_4", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"rejet_Sch");
					}
				
		int tos_count_tDBOutput_4 = 0;
		





String dbschema_tDBOutput_4 = null;
	dbschema_tDBOutput_4 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_4 = null;
if(dbschema_tDBOutput_4 == null || dbschema_tDBOutput_4.trim().length() == 0) {
	tableName_tDBOutput_4 = ("assure_rejects_schema_check");
} else {
	tableName_tDBOutput_4 = dbschema_tDBOutput_4 + "\".\"" + ("assure_rejects_schema_check");
}


int nb_line_tDBOutput_4 = 0;
int nb_line_update_tDBOutput_4 = 0;
int nb_line_inserted_tDBOutput_4 = 0;
int nb_line_deleted_tDBOutput_4 = 0;
int nb_line_rejected_tDBOutput_4 = 0;

int deletedCount_tDBOutput_4=0;
int updatedCount_tDBOutput_4=0;
int insertedCount_tDBOutput_4=0;
int rowsToCommitCount_tDBOutput_4=0;
int rejectedCount_tDBOutput_4=0;

boolean whetherReject_tDBOutput_4 = false;

java.sql.Connection conn_tDBOutput_4 = null;
String dbUser_tDBOutput_4 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_4 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_4 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_4 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_4 = decryptedPassword_tDBOutput_4;

    conn_tDBOutput_4 = java.sql.DriverManager.getConnection(url_tDBOutput_4,dbUser_tDBOutput_4,dbPwd_tDBOutput_4);
	
	resourceMap.put("conn_tDBOutput_4", conn_tDBOutput_4);
        conn_tDBOutput_4.setAutoCommit(false);
        int commitEvery_tDBOutput_4 = 10000;
        int commitCounter_tDBOutput_4 = 0;


   int batchSize_tDBOutput_4 = 10000;
   int batchSizeCounter_tDBOutput_4=0;

int count_tDBOutput_4=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_4 = conn_tDBOutput_4.getMetaData();
                                boolean whetherExist_tDBOutput_4 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_4 = dbMetaData_tDBOutput_4.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_4 = "public";
                                    if(dbschema_tDBOutput_4 == null || dbschema_tDBOutput_4.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_4 = conn_tDBOutput_4.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_4 = stmtSchema_tDBOutput_4.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_4.next()){
                                                defaultSchema_tDBOutput_4 = rsSchema_tDBOutput_4.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_4.next()) {
                                        String table_tDBOutput_4 = rsTable_tDBOutput_4.getString("TABLE_NAME");
                                        String schema_tDBOutput_4 = rsTable_tDBOutput_4.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_4.equals(("assure_rejects_schema_check"))
                                            && (schema_tDBOutput_4.equals(dbschema_tDBOutput_4) || ((dbschema_tDBOutput_4 ==null || dbschema_tDBOutput_4.trim().length() ==0) && defaultSchema_tDBOutput_4.equals(schema_tDBOutput_4)))) {
                                            whetherExist_tDBOutput_4 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_4) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_4 = conn_tDBOutput_4.createStatement()) {
                                        stmtCreate_tDBOutput_4.execute("CREATE TABLE \"" + tableName_tDBOutput_4 + "\"(\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"qualite\" VARCHAR(128)   not null ,\"nom\" VARCHAR(128)   not null ,\"prenom\" VARCHAR(128)  ,\"profession\" VARCHAR(128)   not null ,\"date_naissance\" TIMESTAMP ,\"num_contribuable\" VARCHAR(128)   not null ,\"ville\" VARCHAR(128)  ,\"rue\" VARCHAR(128)  ,\"boite_postale\" VARCHAR(128)  ,\"telephone\" VARCHAR(128)  ,\"email\" VARCHAR(128)  ,\"numero_permis\" VARCHAR(128)  ,\"categorie_permis\" VARCHAR(128)  ,\"date_delivrance\" TIMESTAMP ,\"permis_delivre_par\" VARCHAR(128)  ,\"nom_prenom_conducteur\" VARCHAR(128)  ,\"date_naissance_conducteur\" TIMESTAMP ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_4 = "INSERT INTO \"" + tableName_tDBOutput_4 + "\" (\"code_assure\",\"code_assureur\",\"qualite\",\"nom\",\"prenom\",\"profession\",\"date_naissance\",\"num_contribuable\",\"ville\",\"rue\",\"boite_postale\",\"telephone\",\"email\",\"numero_permis\",\"categorie_permis\",\"date_delivrance\",\"permis_delivre_par\",\"nom_prenom_conducteur\",\"date_naissance_conducteur\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_4 = conn_tDBOutput_4.prepareStatement(insert_tDBOutput_4);
	    resourceMap.put("pstmt_tDBOutput_4", pstmt_tDBOutput_4);
	    

 



/**
 * [tDBOutput_4 begin ] stop
 */



	
	/**
	 * [tMap_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_3", false);
		start_Hash.put("tMap_3", System.currentTimeMillis());
		
	
	currentComponent="tMap_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row7");
					}
				
		int tos_count_tMap_3 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_3__Struct  {
}
Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
// ###############################

// ###############################
// # Outputs initialization
rejet_SchStruct rejet_Sch_tmp = new rejet_SchStruct();
// ###############################

        
        



        









 



/**
 * [tMap_3 begin ] stop
 */



	
	/**
	 * [tLogRow_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_2", false);
		start_Hash.put("tLogRow_2", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row3");
					}
				
		int tos_count_tLogRow_2 = 0;
		

	///////////////////////
	
         class Util_tLogRow_2 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[21];

        public void addRow(String[] row) {

            for (int i = 0; i < 21; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 20 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 20 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[20] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_2 util_tLogRow_2 = new Util_tLogRow_2();
        util_tLogRow_2.setTableName("tLogRow_2");
        util_tLogRow_2.addRow(new String[]{"code_assure","code_assureur","qualite","nom","prenom","profession","date_naissance","num_contribuable","ville","rue","boite_postale","telephone","email","numero_permis","categorie_permis","date_delivrance","permis_delivre_par","nom_prenom_conducteur","date_naissance_conducteur","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_2 = null;
		int nb_line_tLogRow_2 = 0;
///////////////////////    			



 



/**
 * [tLogRow_2 begin ] stop
 */



	
	/**
	 * [tSchemaComplianceCheck_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSchemaComplianceCheck_1", false);
		start_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());
		
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row1");
					}
				
		int tos_count_tSchemaComplianceCheck_1 = 0;
		

    class RowSetValueUtil_tSchemaComplianceCheck_1 {

        boolean ifPassedThrough = true;
        int errorCodeThrough = 0;
        String errorMessageThrough = "";
        int resultErrorCodeThrough = 0;
        String resultErrorMessageThrough = "";
        String tmpContentThrough = null;

        boolean ifPassed = true;
        int errorCode = 0;
        String errorMessage = "";

        void handleBigdecimalPrecision(String data, int iPrecision, int maxLength){
            //number of digits before the decimal point(ignoring frontend zeroes)
            int len1 = 0;
            int len2 = 0;
            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
            if(data.startsWith("-")){
                data = data.substring(1);
            }
            data = org.apache.commons.lang.StringUtils.stripStart(data, "0");

            if(data.indexOf(".") >= 0){
                len1 = data.indexOf(".");
                data = org.apache.commons.lang.StringUtils.stripEnd(data, "0");
                len2 = data.length() - (len1 + 1);
            }else{
                len1 = data.length();
            }

            if (iPrecision < len2) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|precision Non-matches";
            } else if (maxLength < len1 + iPrecision) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|invalid Length setting is unsuitable for Precision";
            }
        }

        int handleErrorCode(int errorCode, int resultErrorCode){
            if (errorCode > 0) {
                if (resultErrorCode > 0) {
                    resultErrorCode = 16;
                } else {
                    resultErrorCode = errorCode;
                }
            }
            return resultErrorCode;
        }

        String handleErrorMessage(String errorMessage, String resultErrorMessage, String columnLabel){
            if (errorMessage.length() > 0) {
                if (resultErrorMessage.length() > 0) {
                    resultErrorMessage += ";"+ errorMessage.replaceFirst("\\|", columnLabel);
                } else {
                    resultErrorMessage = errorMessage.replaceFirst("\\|", columnLabel);
                }
            }
            return resultErrorMessage;
        }

        void reset(){
            ifPassedThrough = true;
            errorCodeThrough = 0;
            errorMessageThrough = "";
            resultErrorCodeThrough = 0;
            resultErrorMessageThrough = "";
            tmpContentThrough = null;

            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
        }

        void setRowValue_0(row1Struct row1) {
    // validate nullable (empty as null)
    if ((row1.code_assure == null) || ("".equals(row1.code_assure))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assure != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assure);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assure != null
    ) {
        if (row1.code_assure.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assure:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.code_assureur == null) || ("".equals(row1.code_assureur))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assureur != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assureur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assureur != null
    ) {
        if (row1.code_assureur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assureur:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.qualite == null) || ("".equals(row1.qualite))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.qualite != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.qualite);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.qualite != null
    ) {
        if (row1.qualite.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"qualite:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.nom == null) || ("".equals(row1.nom))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.nom != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.nom);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.nom != null
    ) {
        if (row1.nom.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"nom:");
            errorMessageThrough = "";    try {
        if(
        row1.prenom != null
        && (!"".equals(row1.prenom))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.prenom);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.prenom != null
    && (!"".equals(row1.prenom))
    ) {
        if (row1.prenom.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"prenom:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.profession == null) || ("".equals(row1.profession))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.profession != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.profession);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.profession != null
    ) {
        if (row1.profession.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"profession:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_naissance:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.num_contribuable == null) || ("".equals(row1.num_contribuable))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.num_contribuable != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.num_contribuable);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.num_contribuable != null
    ) {
        if (row1.num_contribuable.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"num_contribuable:");
            errorMessageThrough = "";    try {
        if(
        row1.ville != null
        && (!"".equals(row1.ville))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.ville);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.ville != null
    && (!"".equals(row1.ville))
    ) {
        if (row1.ville.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"ville:");
            errorMessageThrough = "";    try {
        if(
        row1.rue != null
        && (!"".equals(row1.rue))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.rue);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.rue != null
    && (!"".equals(row1.rue))
    ) {
        if (row1.rue.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"rue:");
            errorMessageThrough = "";    try {
        if(
        row1.boite_postale != null
        && (!"".equals(row1.boite_postale))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.boite_postale);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.boite_postale != null
    && (!"".equals(row1.boite_postale))
    ) {
        if (row1.boite_postale.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"boite_postale:");
            errorMessageThrough = "";    try {
        if(
        row1.telephone != null
        && (!"".equals(row1.telephone))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.telephone);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.telephone != null
    && (!"".equals(row1.telephone))
    ) {
        if (row1.telephone.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"telephone:");
            errorMessageThrough = "";    try {
        if(
        row1.email != null
        && (!"".equals(row1.email))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.email);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.email != null
    && (!"".equals(row1.email))
    ) {
        if (row1.email.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"email:");
            errorMessageThrough = "";    try {
        if(
        row1.numero_permis != null
        && (!"".equals(row1.numero_permis))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.numero_permis);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.numero_permis != null
    && (!"".equals(row1.numero_permis))
    ) {
        if (row1.numero_permis.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"numero_permis:");
            errorMessageThrough = "";    try {
        if(
        row1.categorie_permis != null
        && (!"".equals(row1.categorie_permis))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.categorie_permis);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.categorie_permis != null
    && (!"".equals(row1.categorie_permis))
    ) {
        if (row1.categorie_permis.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"categorie_permis:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_delivrance:");
            errorMessageThrough = "";    try {
        if(
        row1.permis_delivre_par != null
        && (!"".equals(row1.permis_delivre_par))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.permis_delivre_par);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.permis_delivre_par != null
    && (!"".equals(row1.permis_delivre_par))
    ) {
        if (row1.permis_delivre_par.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"permis_delivre_par:");
            errorMessageThrough = "";    try {
        if(
        row1.nom_prenom_conducteur != null
        && (!"".equals(row1.nom_prenom_conducteur))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.nom_prenom_conducteur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.nom_prenom_conducteur != null
    && (!"".equals(row1.nom_prenom_conducteur))
    ) {
        if (row1.nom_prenom_conducteur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"nom_prenom_conducteur:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_naissance_conducteur:");
            errorMessageThrough = "";
        }
    }
    RowSetValueUtil_tSchemaComplianceCheck_1 rsvUtil_tSchemaComplianceCheck_1 = new RowSetValueUtil_tSchemaComplianceCheck_1();

 



/**
 * [tSchemaComplianceCheck_1 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_1", false);
		start_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row10");
					}
				
		int tos_count_tFileOutputDelimited_1 = 0;
		

String fileName_tFileOutputDelimited_1 = "";
    fileName_tFileOutputDelimited_1 = (new java.io.File(context.assure_rejects_structure_check_File)).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_1 = null;
    String extension_tFileOutputDelimited_1 = null;
    String directory_tFileOutputDelimited_1 = null;
    if((fileName_tFileOutputDelimited_1.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") < fileName_tFileOutputDelimited_1.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        }
        directory_tFileOutputDelimited_1 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_1 = true;
    java.io.File filetFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
    globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
        if(filetFileOutputDelimited_1.exists()){
            isFileGenerated_tFileOutputDelimited_1 = false;
        }
            int nb_line_tFileOutputDelimited_1 = 0;
            int splitedFileNo_tFileOutputDelimited_1 = 0;
            int currentRow_tFileOutputDelimited_1 = 0;

            final String OUT_DELIM_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:FIELDSEPARATOR */context.assure_rejects_structure_check_FieldSeparator/** End field tFileOutputDelimited_1:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:ROWSEPARATOR */context.assure_rejects_structure_check_RowSeparator/** End field tFileOutputDelimited_1:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_1 != null && directory_tFileOutputDelimited_1.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_1 = new java.io.File(directory_tFileOutputDelimited_1);
                        if(!dir_tFileOutputDelimited_1.exists()) {
                            dir_tFileOutputDelimited_1.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_1 = null;

                        outtFileOutputDelimited_1 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_1, true),context.assure_rejects_structure_check_Encoding));
                                    if(filetFileOutputDelimited_1.length()==0){
                                        outtFileOutputDelimited_1.write("code_assure");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("code_assureur");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("qualite");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("nom");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("prenom");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("profession");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_naissance");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("num_contribuable");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("ville");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("rue");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("boite_postale");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("telephone");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("email");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("numero_permis");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("categorie_permis");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_delivrance");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("permis_delivre_par");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("nom_prenom_conducteur");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_naissance_conducteur");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_extraction");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_depot");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorCode");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorMessage");
                                        outtFileOutputDelimited_1.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_1", outtFileOutputDelimited_1);
resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

 



/**
 * [tFileOutputDelimited_1 begin ] stop
 */



	
	/**
	 * [tDBOutput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_2", false);
		start_Hash.put("tDBOutput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"rejet_struc");
					}
				
		int tos_count_tDBOutput_2 = 0;
		





String dbschema_tDBOutput_2 = null;
	dbschema_tDBOutput_2 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_2 = null;
if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
	tableName_tDBOutput_2 = ("assure_rejects_structure_check");
} else {
	tableName_tDBOutput_2 = dbschema_tDBOutput_2 + "\".\"" + ("assure_rejects_structure_check");
}


int nb_line_tDBOutput_2 = 0;
int nb_line_update_tDBOutput_2 = 0;
int nb_line_inserted_tDBOutput_2 = 0;
int nb_line_deleted_tDBOutput_2 = 0;
int nb_line_rejected_tDBOutput_2 = 0;

int deletedCount_tDBOutput_2=0;
int updatedCount_tDBOutput_2=0;
int insertedCount_tDBOutput_2=0;
int rowsToCommitCount_tDBOutput_2=0;
int rejectedCount_tDBOutput_2=0;

boolean whetherReject_tDBOutput_2 = false;

java.sql.Connection conn_tDBOutput_2 = null;
String dbUser_tDBOutput_2 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_2 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_2 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_2 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;

    conn_tDBOutput_2 = java.sql.DriverManager.getConnection(url_tDBOutput_2,dbUser_tDBOutput_2,dbPwd_tDBOutput_2);
	
	resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);
        conn_tDBOutput_2.setAutoCommit(false);
        int commitEvery_tDBOutput_2 = 10000;
        int commitCounter_tDBOutput_2 = 0;


   int batchSize_tDBOutput_2 = 10000;
   int batchSizeCounter_tDBOutput_2=0;

int count_tDBOutput_2=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_2 = conn_tDBOutput_2.getMetaData();
                                boolean whetherExist_tDBOutput_2 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_2 = dbMetaData_tDBOutput_2.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_2 = "public";
                                    if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_2 = conn_tDBOutput_2.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_2 = stmtSchema_tDBOutput_2.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_2.next()){
                                                defaultSchema_tDBOutput_2 = rsSchema_tDBOutput_2.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_2.next()) {
                                        String table_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_NAME");
                                        String schema_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_2.equals(("assure_rejects_structure_check"))
                                            && (schema_tDBOutput_2.equals(dbschema_tDBOutput_2) || ((dbschema_tDBOutput_2 ==null || dbschema_tDBOutput_2.trim().length() ==0) && defaultSchema_tDBOutput_2.equals(schema_tDBOutput_2)))) {
                                            whetherExist_tDBOutput_2 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_2) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_2 = conn_tDBOutput_2.createStatement()) {
                                        stmtCreate_tDBOutput_2.execute("CREATE TABLE \"" + tableName_tDBOutput_2 + "\"(\"code_assure\" VARCHAR(128)  ,\"code_assureur\" VARCHAR(128)  ,\"qualite\" VARCHAR(128)  ,\"nom\" VARCHAR(128)  ,\"prenom\" VARCHAR(128)  ,\"profession\" VARCHAR(128)  ,\"date_naissance\" TIMESTAMP ,\"num_contribuable\" VARCHAR(128)  ,\"ville\" VARCHAR(128)  ,\"rue\" VARCHAR(128)  ,\"boite_postale\" VARCHAR(128)  ,\"telephone\" VARCHAR(128)  ,\"email\" VARCHAR(128)  ,\"numero_permis\" VARCHAR(128)  ,\"categorie_permis\" VARCHAR(128)  ,\"date_delivrance\" TIMESTAMP ,\"permis_delivre_par\" VARCHAR(128)  ,\"nom_prenom_conducteur\" VARCHAR(128)  ,\"date_naissance_conducteur\" TIMESTAMP ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_2 = "INSERT INTO \"" + tableName_tDBOutput_2 + "\" (\"code_assure\",\"code_assureur\",\"qualite\",\"nom\",\"prenom\",\"profession\",\"date_naissance\",\"num_contribuable\",\"ville\",\"rue\",\"boite_postale\",\"telephone\",\"email\",\"numero_permis\",\"categorie_permis\",\"date_delivrance\",\"permis_delivre_par\",\"nom_prenom_conducteur\",\"date_naissance_conducteur\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2.prepareStatement(insert_tDBOutput_2);
	    resourceMap.put("pstmt_tDBOutput_2", pstmt_tDBOutput_2);
	    

 



/**
 * [tDBOutput_2 begin ] stop
 */



	
	/**
	 * [tMap_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_2", false);
		start_Hash.put("tMap_2", System.currentTimeMillis());
		
	
	currentComponent="tMap_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row9");
					}
				
		int tos_count_tMap_2 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_2__Struct  {
}
Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
rejet_strucStruct rejet_struc_tmp = new rejet_strucStruct();
// ###############################

        
        



        









 



/**
 * [tMap_2 begin ] stop
 */



	
	/**
	 * [tLogRow_5 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_5", false);
		start_Hash.put("tLogRow_5", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_5";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row8");
					}
				
		int tos_count_tLogRow_5 = 0;
		

	///////////////////////
	
         class Util_tLogRow_5 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[21];

        public void addRow(String[] row) {

            for (int i = 0; i < 21; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 20 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 20 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%15$-");
        			        sbformat.append(colLengths[14]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%16$-");
        			        sbformat.append(colLengths[15]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%17$-");
        			        sbformat.append(colLengths[16]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%18$-");
        			        sbformat.append(colLengths[17]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%19$-");
        			        sbformat.append(colLengths[18]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%20$-");
        			        sbformat.append(colLengths[19]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%21$-");
        			        sbformat.append(colLengths[20]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[13] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[14] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[15] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[16] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[17] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[18] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[19] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[20] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_5 util_tLogRow_5 = new Util_tLogRow_5();
        util_tLogRow_5.setTableName("tLogRow_5");
        util_tLogRow_5.addRow(new String[]{"code_assure","code_assureur","qualite","nom","prenom","profession","date_naissance","num_contribuable","ville","rue","boite_postale","telephone","email","numero_permis","categorie_permis","date_delivrance","permis_delivre_par","nom_prenom_conducteur","date_naissance_conducteur","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_5 = null;
		int nb_line_tLogRow_5 = 0;
///////////////////////    			



 



/**
 * [tLogRow_5 begin ] stop
 */



	
	/**
	 * [tFileInputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileInputDelimited_1", false);
		start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileInputDelimited_1";

	
		int tos_count_tFileInputDelimited_1 = 0;
		
	
	
	
 
	
	
	final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();
	
	
				int nb_line_tFileInputDelimited_1 = 0;
				int footer_tFileInputDelimited_1 = 0;
				int totalLinetFileInputDelimited_1 = 0;
				int limittFileInputDelimited_1 = -1;
				int lastLinetFileInputDelimited_1 = -1;	
				
				char fieldSeparator_tFileInputDelimited_1[] = null;
				
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.assure_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1 = ((String)context.assure_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
			
				char rowSeparator_tFileInputDelimited_1[] = null;
			
				//support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'. 
				if ( ((String)context.assure_RowSeparator).length() > 0 ){
					rowSeparator_tFileInputDelimited_1 = ((String)context.assure_RowSeparator).toCharArray();
				}else {
					throw new IllegalArgumentException("Row Separator must be assigned a char."); 
				}
			
				Object filename_tFileInputDelimited_1 = /** Start field tFileInputDelimited_1:FILENAME */((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"))/** End field tFileInputDelimited_1:FILENAME */;		
				com.talend.csv.CSVReader csvReadertFileInputDelimited_1 = null;
	
				try{
					
						String[] rowtFileInputDelimited_1=null;
						int currentLinetFileInputDelimited_1 = 0;
	        			int outputLinetFileInputDelimited_1 = 0;
						try {//TD110 begin
							if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
							
			int footer_value_tFileInputDelimited_1 = 0;
			if(footer_value_tFileInputDelimited_1 > 0){
				throw new java.lang.Exception("When the input source is a stream,footer shouldn't be bigger than 0.");
			}
		
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.assure_Encoding);
							}else{
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.assure_Encoding);
		        			}
					
					
					csvReadertFileInputDelimited_1.setTrimWhitespace(false);
					if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )
	        			csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
	        				csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	            				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							      
		
			
						if(footer_tFileInputDelimited_1 > 0){
						for(totalLinetFileInputDelimited_1=0;totalLinetFileInputDelimited_1 < context.assure_Header; totalLinetFileInputDelimited_1++){
							csvReadertFileInputDelimited_1.readNext();
						}
						csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
			            while (csvReadertFileInputDelimited_1.readNext()) {
							
	                
	                		totalLinetFileInputDelimited_1++;
	                
							
	                
			            }
	            		int lastLineTemptFileInputDelimited_1 = totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1   < 0? 0 : totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1 ;
	            		if(lastLinetFileInputDelimited_1 > 0){
	                		lastLinetFileInputDelimited_1 = lastLinetFileInputDelimited_1 < lastLineTemptFileInputDelimited_1 ? lastLinetFileInputDelimited_1 : lastLineTemptFileInputDelimited_1; 
	            		}else {
	                		lastLinetFileInputDelimited_1 = lastLineTemptFileInputDelimited_1;
	            		}
	         
			          	csvReadertFileInputDelimited_1.close();
				        if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.assure_Encoding);
		        		}else{
							csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.assure_Encoding);
						}
						csvReadertFileInputDelimited_1.setTrimWhitespace(false);
						if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )	
	        				csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
							csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	        				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							  
	        		}
	        
			        if(limittFileInputDelimited_1 != 0){
			        	for(currentLinetFileInputDelimited_1=0;currentLinetFileInputDelimited_1 < context.assure_Header;currentLinetFileInputDelimited_1++){
			        		csvReadertFileInputDelimited_1.readNext();
			        	}
			        }
			        csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
	        
	    		} catch(java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
					
						
						System.err.println(e.getMessage());
					
	    		}//TD110 end
	        
			    
	        	while ( limittFileInputDelimited_1 != 0 && csvReadertFileInputDelimited_1!=null && csvReadertFileInputDelimited_1.readNext() ) { 
	        		rowstate_tFileInputDelimited_1.reset();
	        
		        	rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
		        	
					
	        	
	        	
	        		currentLinetFileInputDelimited_1++;
	            
		            if(lastLinetFileInputDelimited_1 > -1 && currentLinetFileInputDelimited_1 > lastLinetFileInputDelimited_1) {
		                break;
	    	        }
	        	    outputLinetFileInputDelimited_1++;
	            	if (limittFileInputDelimited_1 > 0 && outputLinetFileInputDelimited_1 > limittFileInputDelimited_1) {
	                	break;
	            	}  
	                                                                      
					
	    							row1 = null;			
								
	    							row8 = null;			
								
								boolean whetherReject_tFileInputDelimited_1 = false;
								row1 = new row1Struct();
								try {			
									
				char fieldSeparator_tFileInputDelimited_1_ListType[] = null;
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.assure_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1_ListType = ((String)context.assure_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
				if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
					
							row1.code_assure = null;
					
							row1.code_assureur = null;
					
							row1.qualite = null;
					
							row1.nom = null;
					
							row1.prenom = null;
					
							row1.profession = null;
					
							row1.date_naissance = null;
					
							row1.num_contribuable = null;
					
							row1.ville = null;
					
							row1.rue = null;
					
							row1.boite_postale = null;
					
							row1.telephone = null;
					
							row1.email = null;
					
							row1.numero_permis = null;
					
							row1.categorie_permis = null;
					
							row1.date_delivrance = null;
					
							row1.permis_delivre_par = null;
					
							row1.nom_prenom_conducteur = null;
					
							row1.date_naissance_conducteur = null;
					
				}else{
					
					for(int i_tFileInputDelimited_1=0;i_tFileInputDelimited_1<rowtFileInputDelimited_1.length;i_tFileInputDelimited_1++){
						rowtFileInputDelimited_1[i_tFileInputDelimited_1]=rowtFileInputDelimited_1[i_tFileInputDelimited_1].trim();
					}
					
	                int columnIndexWithD_tFileInputDelimited_1 = 0; //Column Index 
	                
						columnIndexWithD_tFileInputDelimited_1 = 0;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assure = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assure = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 1;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assureur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assureur = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 2;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.qualite = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.qualite = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 3;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.nom = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.nom = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 4;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.prenom = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.prenom = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 5;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.profession = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.profession = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 6;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_naissance = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_naissance", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_naissance = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_naissance = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 7;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.num_contribuable = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.num_contribuable = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 8;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.ville = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.ville = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 9;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.rue = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.rue = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 10;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.boite_postale = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.boite_postale = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 11;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.telephone = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.telephone = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 12;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.email = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.email = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 13;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.numero_permis = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.numero_permis = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 14;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.categorie_permis = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.categorie_permis = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 15;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_delivrance = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_delivrance", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_delivrance = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_delivrance = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 16;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.permis_delivre_par = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.permis_delivre_par = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 17;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.nom_prenom_conducteur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.nom_prenom_conducteur = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 18;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_naissance_conducteur = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_naissance_conducteur", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_naissance_conducteur = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_naissance_conducteur = null;
							
						
						}
						
						
					
				}
				
 					int filedsum = rowtFileInputDelimited_1.length;
 					if(filedsum < (19 )){
 						throw new java.lang.Exception("Column(s) missing");
 					} else if(filedsum > (19 )) {
 						throw new RuntimeException("Too many columns");
 					}     
				
									
									if(rowstate_tFileInputDelimited_1.getException()!=null) {
										throw rowstate_tFileInputDelimited_1.getException();
									}
									
									
	    						} catch (java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
							        whetherReject_tFileInputDelimited_1 = true;
        							
						                    row8 = new row8Struct();
                							
    				row8.code_assure = row1.code_assure;
				
    				row8.code_assureur = row1.code_assureur;
				
    				row8.qualite = row1.qualite;
				
    				row8.nom = row1.nom;
				
    				row8.prenom = row1.prenom;
				
    				row8.profession = row1.profession;
				
    				row8.date_naissance = row1.date_naissance;
				
    				row8.num_contribuable = row1.num_contribuable;
				
    				row8.ville = row1.ville;
				
    				row8.rue = row1.rue;
				
    				row8.boite_postale = row1.boite_postale;
				
    				row8.telephone = row1.telephone;
				
    				row8.email = row1.email;
				
    				row8.numero_permis = row1.numero_permis;
				
    				row8.categorie_permis = row1.categorie_permis;
				
    				row8.date_delivrance = row1.date_delivrance;
				
    				row8.permis_delivre_par = row1.permis_delivre_par;
				
    				row8.nom_prenom_conducteur = row1.nom_prenom_conducteur;
				
    				row8.date_naissance_conducteur = row1.date_naissance_conducteur;
				
			
                							row8.errorMessage = e.getMessage() + " - Line: " + tos_count_tFileInputDelimited_1;
                							row1 = null;
                						
            							globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());
            							
	    						}
	
							

 



/**
 * [tFileInputDelimited_1 begin ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 


	tos_count_tFileInputDelimited_1++;

/**
 * [tFileInputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_begin ] stop
 */
// Start of branch "row1"
if(row1 != null) { 



	
	/**
	 * [tSchemaComplianceCheck_1 main ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row1"
						
						);
					}
					
	row2 = null;	row3 = null;
	rsvUtil_tSchemaComplianceCheck_1.setRowValue_0(row1);
	if (rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row2 = new row2Struct();
		row2.code_assure = row1.code_assure;
		row2.code_assureur = row1.code_assureur;
		row2.qualite = row1.qualite;
		row2.nom = row1.nom;
		row2.prenom = row1.prenom;
		row2.profession = row1.profession;
		row2.date_naissance = row1.date_naissance;
		row2.num_contribuable = row1.num_contribuable;
		row2.ville = row1.ville;
		row2.rue = row1.rue;
		row2.boite_postale = row1.boite_postale;
		row2.telephone = row1.telephone;
		row2.email = row1.email;
		row2.numero_permis = row1.numero_permis;
		row2.categorie_permis = row1.categorie_permis;
		row2.date_delivrance = row1.date_delivrance;
		row2.permis_delivre_par = row1.permis_delivre_par;
		row2.nom_prenom_conducteur = row1.nom_prenom_conducteur;
		row2.date_naissance_conducteur = row1.date_naissance_conducteur;
	}
	if (!rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row3 = new row3Struct();
		row3.code_assure = row1.code_assure;
		row3.code_assureur = row1.code_assureur;
		row3.qualite = row1.qualite;
		row3.nom = row1.nom;
		row3.prenom = row1.prenom;
		row3.profession = row1.profession;
		row3.date_naissance = row1.date_naissance;
		row3.num_contribuable = row1.num_contribuable;
		row3.ville = row1.ville;
		row3.rue = row1.rue;
		row3.boite_postale = row1.boite_postale;
		row3.telephone = row1.telephone;
		row3.email = row1.email;
		row3.numero_permis = row1.numero_permis;
		row3.categorie_permis = row1.categorie_permis;
		row3.date_delivrance = row1.date_delivrance;
		row3.permis_delivre_par = row1.permis_delivre_par;
		row3.nom_prenom_conducteur = row1.nom_prenom_conducteur;
		row3.date_naissance_conducteur = row1.date_naissance_conducteur;
		row3.errorCode = String.valueOf(rsvUtil_tSchemaComplianceCheck_1.resultErrorCodeThrough);
		row3.errorMessage = rsvUtil_tSchemaComplianceCheck_1.resultErrorMessageThrough;
	}
	rsvUtil_tSchemaComplianceCheck_1.reset();

 


	tos_count_tSchemaComplianceCheck_1++;

/**
 * [tSchemaComplianceCheck_1 main ] stop
 */
	
	/**
	 * [tSchemaComplianceCheck_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_begin ] stop
 */
// Start of branch "row2"
if(row2 != null) { 



	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row2"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[19];
   				
	    		if(row2.code_assure != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row2.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_assureur != null) { //              
                 row_tLogRow_1[1]=    						    
				                String.valueOf(row2.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.qualite != null) { //              
                 row_tLogRow_1[2]=    						    
				                String.valueOf(row2.qualite)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.nom != null) { //              
                 row_tLogRow_1[3]=    						    
				                String.valueOf(row2.nom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.prenom != null) { //              
                 row_tLogRow_1[4]=    						    
				                String.valueOf(row2.prenom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.profession != null) { //              
                 row_tLogRow_1[5]=    						    
				                String.valueOf(row2.profession)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_naissance != null) { //              
                 row_tLogRow_1[6]=    						
								FormatterUtils.format_Date(row2.date_naissance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row2.num_contribuable != null) { //              
                 row_tLogRow_1[7]=    						    
				                String.valueOf(row2.num_contribuable)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.ville != null) { //              
                 row_tLogRow_1[8]=    						    
				                String.valueOf(row2.ville)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.rue != null) { //              
                 row_tLogRow_1[9]=    						    
				                String.valueOf(row2.rue)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.boite_postale != null) { //              
                 row_tLogRow_1[10]=    						    
				                String.valueOf(row2.boite_postale)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.telephone != null) { //              
                 row_tLogRow_1[11]=    						    
				                String.valueOf(row2.telephone)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.email != null) { //              
                 row_tLogRow_1[12]=    						    
				                String.valueOf(row2.email)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.numero_permis != null) { //              
                 row_tLogRow_1[13]=    						    
				                String.valueOf(row2.numero_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.categorie_permis != null) { //              
                 row_tLogRow_1[14]=    						    
				                String.valueOf(row2.categorie_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_delivrance != null) { //              
                 row_tLogRow_1[15]=    						
								FormatterUtils.format_Date(row2.date_delivrance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row2.permis_delivre_par != null) { //              
                 row_tLogRow_1[16]=    						    
				                String.valueOf(row2.permis_delivre_par)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.nom_prenom_conducteur != null) { //              
                 row_tLogRow_1[17]=    						    
				                String.valueOf(row2.nom_prenom_conducteur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_naissance_conducteur != null) { //              
                 row_tLogRow_1[18]=    						
								FormatterUtils.format_Date(row2.date_naissance_conducteur, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 
     row4 = row2;


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row4"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;
Var.Dnaissance = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance==null?"date_naissance_vide_avec_Qualité_PersonnePhysique   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance!=null && TalendDate.compareDate(row4.date_naissance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1?"date_naissance_erronee   ":"" ;
Var.NumPermis = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.numero_permis)?"numero_permis_vide_avec_Qualité_PP   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.numero_permis) && StringHandling.LEN(row4.numero_permis)<6?"numero_permis_invalide   "   :"" ;
Var.Ddelivrance = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_delivrance==null?"Date_delivrance_permis_vide_avec_Qualité_PP   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_delivrance!=null && TalendDate.compareDate(row4.date_delivrance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1?"Date_delivrance_permis_erronee   ":"" ;
Var.CatPermis = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.categorie_permis)?"categorie_permis_vide_avec_Qualité_PP   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && StringHandling.DOWNCASE(row4.categorie_permis).equals("1")?"categorie_permis_invalide   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && !"A1_A_B_BE_C_CE_D_DE_FA1_FA_FB_G".contains(row4.categorie_permis)?"categorie_permis_invalide   ": "" ;
Var.NomPConducteur = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.nom_prenom_conducteur)?"Nom_prenom_conducteur_vide_avec_Qualité_PP   ":"" ;
Var.DnaissanceConducteur = StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance_conducteur==null?"Date_naissance_conducteur_vide_avec_Qualité_PP   ":StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance_conducteur!=null && TalendDate.compareDate(row4.date_naissance_conducteur,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1?"Date_naissance_conducteur_erronee   ":"" ;
Var.Qualite = !StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && !StringHandling.DOWNCASE(row4.qualite).equals("personne morale")?"Qualite_est_mal_renseignee   ":"" ;
Var.NumContribuableVal = StringHandling.LEN(row4.num_contribuable)<10?"numero_contribuable_invalide   ":"pxxxxxxxxxxxxxxxxxxxx_........................_niu00000000000000000000_pr000000000000000_p000000000000000000000000000000000000_po0000000000000000000_pn0000000000000000000000000000000001_pnbb000000000000000000000000000_no00000000000000_PN000000000000000000000000001_ousmanou adamou".contains(StringHandling.DOWNCASE(row4.num_contribuable)) || !StringHandling.IS_ALPHA(StringHandling.LEFT(row4.num_contribuable,1))?"numero_contribuable_invalide   ":"" ;
Var.ProfessionVal = StringHandling.LEN(row4.profession)<6?"profession_invalide   ":"" ;
Var.CatPermisVal = "A1_A2_A_B_B1_B2_BE_C_C1_CE_C1E_D_D1_D2_DE_E_FA1_FA_FB_G" ;
Var.ContribuableVal = "pxxxxxxxxxxxxxxxxxxxx_........................_niu00000000000000000000_pr000000000000000_p000000000000000000000000000000000000_po0000000000000000000_pn0000000000000000000000000000000001_pnbb000000000000000000000000000_no00000000000000_PN000000000000000000000000001_ousmanou adamou" ;// ###############################
        // ###############################
        // # Output tables

valides = null;
rejets = null;

boolean rejected_tMap_1 = true;

// # Output table : 'valides'
// # Filter conditions 
if( 

!(StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance==null || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.numero_permis) || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_delivrance==null|| StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.categorie_permis)|| StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && ("").equals(row4.nom_prenom_conducteur)|| StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance_conducteur==null || !StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && !StringHandling.DOWNCASE(row4.qualite).equals("personne morale") || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && !Var.CatPermisVal.contains(row4.categorie_permis)|| StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && StringHandling.DOWNCASE(row4.categorie_permis).equals("1") || StringHandling.LEN(row4.num_contribuable)<10 || Var.ContribuableVal.contains(StringHandling.DOWNCASE(row4.num_contribuable)) || !StringHandling.IS_ALPHA(StringHandling.LEFT(row4.num_contribuable,1)) || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && StringHandling.LEN(row4.numero_permis)<6 || StringHandling.LEN(row4.profession)<6 || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_delivrance!=null && TalendDate.compareDate(row4.date_delivrance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1 || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance!=null && TalendDate.compareDate(row4.date_naissance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1 || StringHandling.DOWNCASE(row4.qualite).equals("personne physique") && row4.date_naissance_conducteur!=null && TalendDate.compareDate(row4.date_naissance_conducteur,TalendDate.parseDate("dd/MM/yyyy", "01/01/1910"))==-1)

 ) {
rejected_tMap_1 = false;
valides_tmp.code_assure = row4.code_assure ;
valides_tmp.code_assureur = row4.code_assureur ;
valides_tmp.qualite = row4.qualite ;
valides_tmp.nom = row4.nom ;
valides_tmp.prenom = row4.prenom ;
valides_tmp.profession = row4.profession ;
valides_tmp.date_naissance = row4.date_naissance ;
valides_tmp.num_contribuable = row4.num_contribuable ;
valides_tmp.ville = row4.ville ;
valides_tmp.rue = row4.rue ;
valides_tmp.boite_postale = row4.boite_postale ;
valides_tmp.telephone = row4.telephone ;
valides_tmp.email = row4.email ;
valides_tmp.numero_permis = row4.numero_permis ;
valides_tmp.categorie_permis = row4.categorie_permis ;
valides_tmp.date_delivrance = row4.date_delivrance ;
valides_tmp.permis_delivre_par = row4.permis_delivre_par ;
valides_tmp.nom_prenom_conducteur = row4.nom_prenom_conducteur ;
valides_tmp.date_naissance_conducteur = row4.date_naissance_conducteur ;
valides_tmp.c_status = 1;
valides_tmp.c_date_mis_a_jour = TalendDate.getCurrentDate() ;
valides_tmp.c_date_transfer = null;
valides_tmp.commentaires = null;
valides = valides_tmp;
} // closing filter/reject
// ###### START REJECTS ##### 

// # Output reject table : 'rejets'
// # Filter conditions 
if( rejected_tMap_1 ) {
rejets_tmp.code_assure = row4.code_assure ;
rejets_tmp.code_assureur = row4.code_assureur ;
rejets_tmp.qualite = row4.qualite ;
rejets_tmp.nom = row4.nom ;
rejets_tmp.prenom = row4.prenom ;
rejets_tmp.profession = row4.profession ;
rejets_tmp.date_naissance = row4.date_naissance ;
rejets_tmp.num_contribuable = row4.num_contribuable ;
rejets_tmp.ville = row4.ville ;
rejets_tmp.rue = row4.rue ;
rejets_tmp.boite_postale = row4.boite_postale ;
rejets_tmp.telephone = row4.telephone ;
rejets_tmp.email = row4.email ;
rejets_tmp.numero_permis = row4.numero_permis ;
rejets_tmp.categorie_permis = row4.categorie_permis ;
rejets_tmp.date_delivrance = row4.date_delivrance ;
rejets_tmp.permis_delivre_par = row4.permis_delivre_par ;
rejets_tmp.nom_prenom_conducteur = row4.nom_prenom_conducteur ;
rejets_tmp.date_naissance_conducteur = row4.date_naissance_conducteur ;
rejets_tmp.date_extraction = TalendDate.getCurrentDate() ;
rejets_tmp.date_depot = null;
rejets_tmp.Error_Message = Var.Dnaissance+Var.NumPermis+Var.Ddelivrance+Var.CatPermis+Var.NomPConducteur+Var.DnaissanceConducteur+Var.Qualite+Var.NumContribuableVal+Var.ProfessionVal;
rejets = rejets_tmp;
} // closing filter/reject
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "valides"
if(valides != null) { 



	
	/**
	 * [tLogRow_3 main ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"valides"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_3 = new String[23];
   				
	    		if(valides.code_assure != null) { //              
                 row_tLogRow_3[0]=    						    
				                String.valueOf(valides.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.code_assureur != null) { //              
                 row_tLogRow_3[1]=    						    
				                String.valueOf(valides.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.qualite != null) { //              
                 row_tLogRow_3[2]=    						    
				                String.valueOf(valides.qualite)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.nom != null) { //              
                 row_tLogRow_3[3]=    						    
				                String.valueOf(valides.nom)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.prenom != null) { //              
                 row_tLogRow_3[4]=    						    
				                String.valueOf(valides.prenom)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.profession != null) { //              
                 row_tLogRow_3[5]=    						    
				                String.valueOf(valides.profession)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.date_naissance != null) { //              
                 row_tLogRow_3[6]=    						
								FormatterUtils.format_Date(valides.date_naissance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(valides.num_contribuable != null) { //              
                 row_tLogRow_3[7]=    						    
				                String.valueOf(valides.num_contribuable)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.ville != null) { //              
                 row_tLogRow_3[8]=    						    
				                String.valueOf(valides.ville)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.rue != null) { //              
                 row_tLogRow_3[9]=    						    
				                String.valueOf(valides.rue)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.boite_postale != null) { //              
                 row_tLogRow_3[10]=    						    
				                String.valueOf(valides.boite_postale)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.telephone != null) { //              
                 row_tLogRow_3[11]=    						    
				                String.valueOf(valides.telephone)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.email != null) { //              
                 row_tLogRow_3[12]=    						    
				                String.valueOf(valides.email)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.numero_permis != null) { //              
                 row_tLogRow_3[13]=    						    
				                String.valueOf(valides.numero_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.categorie_permis != null) { //              
                 row_tLogRow_3[14]=    						    
				                String.valueOf(valides.categorie_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.date_delivrance != null) { //              
                 row_tLogRow_3[15]=    						
								FormatterUtils.format_Date(valides.date_delivrance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(valides.permis_delivre_par != null) { //              
                 row_tLogRow_3[16]=    						    
				                String.valueOf(valides.permis_delivre_par)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.nom_prenom_conducteur != null) { //              
                 row_tLogRow_3[17]=    						    
				                String.valueOf(valides.nom_prenom_conducteur)			
					          ;	
							
	    		} //			
    			   				
	    		if(valides.date_naissance_conducteur != null) { //              
                 row_tLogRow_3[18]=    						
								FormatterUtils.format_Date(valides.date_naissance_conducteur, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_3[19]=    						    
				                String.valueOf(valides.c_status)			
					          ;	
										
    			   				
	    		if(valides.c_date_mis_a_jour != null) { //              
                 row_tLogRow_3[20]=    						
								FormatterUtils.format_Date(valides.c_date_mis_a_jour, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(valides.c_date_transfer != null) { //              
                 row_tLogRow_3[21]=    						
								FormatterUtils.format_Date(valides.c_date_transfer, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(valides.commentaires != null) { //              
                 row_tLogRow_3[22]=    						    
				                String.valueOf(valides.commentaires)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_3.addRow(row_tLogRow_3);	
				nb_line_tLogRow_3++;
//////

//////                    
                    
///////////////////////    			

 
     row5 = valides;


	tos_count_tLogRow_3++;

/**
 * [tLogRow_3 main ] stop
 */
	
	/**
	 * [tLogRow_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_begin ] stop
 */

	
	/**
	 * [tDBOutput_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row5"
						
						);
					}
					



        whetherReject_tDBOutput_1 = false;
                    if(row5.code_assure == null) {
pstmt_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(1, row5.code_assure);
}

                    if(row5.code_assureur == null) {
pstmt_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(2, row5.code_assureur);
}

            int checkCount_tDBOutput_1 = -1;
            try (java.sql.ResultSet rs_tDBOutput_1 = pstmt_tDBOutput_1.executeQuery()) {
                while(rs_tDBOutput_1.next()) {
                    checkCount_tDBOutput_1 = rs_tDBOutput_1.getInt(1);
                }
            }
            if(checkCount_tDBOutput_1 > 0) {
                        if(row5.qualite == null) {
pstmtUpdate_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(1, row5.qualite);
}

                        if(row5.nom == null) {
pstmtUpdate_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(2, row5.nom);
}

                        if(row5.prenom == null) {
pstmtUpdate_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(3, row5.prenom);
}

                        if(row5.profession == null) {
pstmtUpdate_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(4, row5.profession);
}

                        if(row5.date_naissance != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(5, new java.sql.Timestamp(row5.date_naissance.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(5, java.sql.Types.TIMESTAMP);
}

                        if(row5.num_contribuable == null) {
pstmtUpdate_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(6, row5.num_contribuable);
}

                        if(row5.ville == null) {
pstmtUpdate_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(7, row5.ville);
}

                        if(row5.rue == null) {
pstmtUpdate_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(8, row5.rue);
}

                        if(row5.boite_postale == null) {
pstmtUpdate_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(9, row5.boite_postale);
}

                        if(row5.telephone == null) {
pstmtUpdate_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(10, row5.telephone);
}

                        if(row5.email == null) {
pstmtUpdate_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(11, row5.email);
}

                        if(row5.numero_permis == null) {
pstmtUpdate_tDBOutput_1.setNull(12, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(12, row5.numero_permis);
}

                        if(row5.categorie_permis == null) {
pstmtUpdate_tDBOutput_1.setNull(13, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(13, row5.categorie_permis);
}

                        if(row5.date_delivrance != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(14, new java.sql.Timestamp(row5.date_delivrance.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(14, java.sql.Types.TIMESTAMP);
}

                        if(row5.permis_delivre_par == null) {
pstmtUpdate_tDBOutput_1.setNull(15, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(15, row5.permis_delivre_par);
}

                        if(row5.nom_prenom_conducteur == null) {
pstmtUpdate_tDBOutput_1.setNull(16, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(16, row5.nom_prenom_conducteur);
}

                        if(row5.date_naissance_conducteur != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(17, new java.sql.Timestamp(row5.date_naissance_conducteur.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(17, java.sql.Types.TIMESTAMP);
}

                        pstmtUpdate_tDBOutput_1.setInt(18, row5.c_status);

                        if(row5.c_date_mis_a_jour != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(19, new java.sql.Timestamp(row5.c_date_mis_a_jour.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(19, java.sql.Types.TIMESTAMP);
}

                        if(row5.c_date_transfer != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(20, new java.sql.Timestamp(row5.c_date_transfer.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(20, java.sql.Types.TIMESTAMP);
}

                        if(row5.commentaires == null) {
pstmtUpdate_tDBOutput_1.setNull(21, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(21, row5.commentaires);
}

                        if(row5.code_assure == null) {
pstmtUpdate_tDBOutput_1.setNull(22 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(22 + count_tDBOutput_1, row5.code_assure);
}

                        if(row5.code_assureur == null) {
pstmtUpdate_tDBOutput_1.setNull(23 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(23 + count_tDBOutput_1, row5.code_assureur);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtUpdate_tDBOutput_1.executeUpdate();
                    updatedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            } else {
                        if(row5.code_assure == null) {
pstmtInsert_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(1, row5.code_assure);
}

                        if(row5.code_assureur == null) {
pstmtInsert_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(2, row5.code_assureur);
}

                        if(row5.qualite == null) {
pstmtInsert_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(3, row5.qualite);
}

                        if(row5.nom == null) {
pstmtInsert_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(4, row5.nom);
}

                        if(row5.prenom == null) {
pstmtInsert_tDBOutput_1.setNull(5, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(5, row5.prenom);
}

                        if(row5.profession == null) {
pstmtInsert_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(6, row5.profession);
}

                        if(row5.date_naissance != null) {
pstmtInsert_tDBOutput_1.setTimestamp(7, new java.sql.Timestamp(row5.date_naissance.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(7, java.sql.Types.TIMESTAMP);
}

                        if(row5.num_contribuable == null) {
pstmtInsert_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(8, row5.num_contribuable);
}

                        if(row5.ville == null) {
pstmtInsert_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(9, row5.ville);
}

                        if(row5.rue == null) {
pstmtInsert_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(10, row5.rue);
}

                        if(row5.boite_postale == null) {
pstmtInsert_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(11, row5.boite_postale);
}

                        if(row5.telephone == null) {
pstmtInsert_tDBOutput_1.setNull(12, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(12, row5.telephone);
}

                        if(row5.email == null) {
pstmtInsert_tDBOutput_1.setNull(13, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(13, row5.email);
}

                        if(row5.numero_permis == null) {
pstmtInsert_tDBOutput_1.setNull(14, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(14, row5.numero_permis);
}

                        if(row5.categorie_permis == null) {
pstmtInsert_tDBOutput_1.setNull(15, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(15, row5.categorie_permis);
}

                        if(row5.date_delivrance != null) {
pstmtInsert_tDBOutput_1.setTimestamp(16, new java.sql.Timestamp(row5.date_delivrance.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(16, java.sql.Types.TIMESTAMP);
}

                        if(row5.permis_delivre_par == null) {
pstmtInsert_tDBOutput_1.setNull(17, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(17, row5.permis_delivre_par);
}

                        if(row5.nom_prenom_conducteur == null) {
pstmtInsert_tDBOutput_1.setNull(18, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(18, row5.nom_prenom_conducteur);
}

                        if(row5.date_naissance_conducteur != null) {
pstmtInsert_tDBOutput_1.setTimestamp(19, new java.sql.Timestamp(row5.date_naissance_conducteur.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(19, java.sql.Types.TIMESTAMP);
}

                        pstmtInsert_tDBOutput_1.setInt(20, row5.c_status);

                        if(row5.c_date_mis_a_jour != null) {
pstmtInsert_tDBOutput_1.setTimestamp(21, new java.sql.Timestamp(row5.c_date_mis_a_jour.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(21, java.sql.Types.TIMESTAMP);
}

                        if(row5.c_date_transfer != null) {
pstmtInsert_tDBOutput_1.setTimestamp(22, new java.sql.Timestamp(row5.c_date_transfer.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(22, java.sql.Types.TIMESTAMP);
}

                        if(row5.commentaires == null) {
pstmtInsert_tDBOutput_1.setNull(23, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(23, row5.commentaires);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtInsert_tDBOutput_1.executeUpdate();
                    insertedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            }
            if(!whetherReject_tDBOutput_1) {
            }
    		    commitCounter_tDBOutput_1++;
                if(commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    }
                    conn_tDBOutput_1.commit();
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_1 = 0;
                    }
                    commitCounter_tDBOutput_1=0;
                }

 


	tos_count_tDBOutput_1++;

/**
 * [tDBOutput_1 main ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_end ] stop
 */

} // End of branch "valides"




// Start of branch "rejets"
if(rejets != null) { 



	
	/**
	 * [tLogRow_4 main ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"rejets"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_4 = new String[22];
   				
	    		if(rejets.code_assure != null) { //              
                 row_tLogRow_4[0]=    						    
				                String.valueOf(rejets.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.code_assureur != null) { //              
                 row_tLogRow_4[1]=    						    
				                String.valueOf(rejets.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.qualite != null) { //              
                 row_tLogRow_4[2]=    						    
				                String.valueOf(rejets.qualite)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.nom != null) { //              
                 row_tLogRow_4[3]=    						    
				                String.valueOf(rejets.nom)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.prenom != null) { //              
                 row_tLogRow_4[4]=    						    
				                String.valueOf(rejets.prenom)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.profession != null) { //              
                 row_tLogRow_4[5]=    						    
				                String.valueOf(rejets.profession)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.date_naissance != null) { //              
                 row_tLogRow_4[6]=    						
								FormatterUtils.format_Date(rejets.date_naissance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.num_contribuable != null) { //              
                 row_tLogRow_4[7]=    						    
				                String.valueOf(rejets.num_contribuable)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.ville != null) { //              
                 row_tLogRow_4[8]=    						    
				                String.valueOf(rejets.ville)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.rue != null) { //              
                 row_tLogRow_4[9]=    						    
				                String.valueOf(rejets.rue)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.boite_postale != null) { //              
                 row_tLogRow_4[10]=    						    
				                String.valueOf(rejets.boite_postale)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.telephone != null) { //              
                 row_tLogRow_4[11]=    						    
				                String.valueOf(rejets.telephone)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.email != null) { //              
                 row_tLogRow_4[12]=    						    
				                String.valueOf(rejets.email)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.numero_permis != null) { //              
                 row_tLogRow_4[13]=    						    
				                String.valueOf(rejets.numero_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.categorie_permis != null) { //              
                 row_tLogRow_4[14]=    						    
				                String.valueOf(rejets.categorie_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.date_delivrance != null) { //              
                 row_tLogRow_4[15]=    						
								FormatterUtils.format_Date(rejets.date_delivrance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.permis_delivre_par != null) { //              
                 row_tLogRow_4[16]=    						    
				                String.valueOf(rejets.permis_delivre_par)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.nom_prenom_conducteur != null) { //              
                 row_tLogRow_4[17]=    						    
				                String.valueOf(rejets.nom_prenom_conducteur)			
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.date_naissance_conducteur != null) { //              
                 row_tLogRow_4[18]=    						
								FormatterUtils.format_Date(rejets.date_naissance_conducteur, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.date_extraction != null) { //              
                 row_tLogRow_4[19]=    						
								FormatterUtils.format_Date(rejets.date_extraction, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.date_depot != null) { //              
                 row_tLogRow_4[20]=    						
								FormatterUtils.format_Date(rejets.date_depot, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(rejets.Error_Message != null) { //              
                 row_tLogRow_4[21]=    						    
				                String.valueOf(rejets.Error_Message)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_4.addRow(row_tLogRow_4);	
				nb_line_tLogRow_4++;
//////

//////                    
                    
///////////////////////    			

 
     row6 = rejets;


	tos_count_tLogRow_4++;

/**
 * [tLogRow_4 main ] stop
 */
	
	/**
	 * [tLogRow_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_begin ] stop
 */

	
	/**
	 * [tDBOutput_3 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row6"
						
						);
					}
					



            row11 = null;
        whetherReject_tDBOutput_3 = false;
                    if(row6.code_assure == null) {
pstmt_tDBOutput_3.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(1, row6.code_assure);
}

                    if(row6.code_assureur == null) {
pstmt_tDBOutput_3.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(2, row6.code_assureur);
}

                    if(row6.qualite == null) {
pstmt_tDBOutput_3.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(3, row6.qualite);
}

                    if(row6.nom == null) {
pstmt_tDBOutput_3.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(4, row6.nom);
}

                    if(row6.prenom == null) {
pstmt_tDBOutput_3.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(5, row6.prenom);
}

                    if(row6.profession == null) {
pstmt_tDBOutput_3.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(6, row6.profession);
}

                    if(row6.date_naissance != null) {
pstmt_tDBOutput_3.setTimestamp(7, new java.sql.Timestamp(row6.date_naissance.getTime()));
} else {
pstmt_tDBOutput_3.setNull(7, java.sql.Types.TIMESTAMP);
}

                    if(row6.num_contribuable == null) {
pstmt_tDBOutput_3.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(8, row6.num_contribuable);
}

                    if(row6.ville == null) {
pstmt_tDBOutput_3.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(9, row6.ville);
}

                    if(row6.rue == null) {
pstmt_tDBOutput_3.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(10, row6.rue);
}

                    if(row6.boite_postale == null) {
pstmt_tDBOutput_3.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(11, row6.boite_postale);
}

                    if(row6.telephone == null) {
pstmt_tDBOutput_3.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(12, row6.telephone);
}

                    if(row6.email == null) {
pstmt_tDBOutput_3.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(13, row6.email);
}

                    if(row6.numero_permis == null) {
pstmt_tDBOutput_3.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(14, row6.numero_permis);
}

                    if(row6.categorie_permis == null) {
pstmt_tDBOutput_3.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(15, row6.categorie_permis);
}

                    if(row6.date_delivrance != null) {
pstmt_tDBOutput_3.setTimestamp(16, new java.sql.Timestamp(row6.date_delivrance.getTime()));
} else {
pstmt_tDBOutput_3.setNull(16, java.sql.Types.TIMESTAMP);
}

                    if(row6.permis_delivre_par == null) {
pstmt_tDBOutput_3.setNull(17, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(17, row6.permis_delivre_par);
}

                    if(row6.nom_prenom_conducteur == null) {
pstmt_tDBOutput_3.setNull(18, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(18, row6.nom_prenom_conducteur);
}

                    if(row6.date_naissance_conducteur != null) {
pstmt_tDBOutput_3.setTimestamp(19, new java.sql.Timestamp(row6.date_naissance_conducteur.getTime()));
} else {
pstmt_tDBOutput_3.setNull(19, java.sql.Types.TIMESTAMP);
}

                    if(row6.date_extraction != null) {
pstmt_tDBOutput_3.setTimestamp(20, new java.sql.Timestamp(row6.date_extraction.getTime()));
} else {
pstmt_tDBOutput_3.setNull(20, java.sql.Types.TIMESTAMP);
}

                    if(row6.date_depot != null) {
pstmt_tDBOutput_3.setTimestamp(21, new java.sql.Timestamp(row6.date_depot.getTime()));
} else {
pstmt_tDBOutput_3.setNull(21, java.sql.Types.TIMESTAMP);
}

                    if(row6.Error_Message == null) {
pstmt_tDBOutput_3.setNull(22, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(22, row6.Error_Message);
}

			
    		pstmt_tDBOutput_3.addBatch();
    		nb_line_tDBOutput_3++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_3++;
    		  
            if(!whetherReject_tDBOutput_3) {
                            row11 = new row11Struct();
                                row11.code_assure = row6.code_assure;
                                row11.code_assureur = row6.code_assureur;
                                row11.qualite = row6.qualite;
                                row11.nom = row6.nom;
                                row11.prenom = row6.prenom;
                                row11.profession = row6.profession;
                                row11.date_naissance = row6.date_naissance;
                                row11.num_contribuable = row6.num_contribuable;
                                row11.ville = row6.ville;
                                row11.rue = row6.rue;
                                row11.boite_postale = row6.boite_postale;
                                row11.telephone = row6.telephone;
                                row11.email = row6.email;
                                row11.numero_permis = row6.numero_permis;
                                row11.categorie_permis = row6.categorie_permis;
                                row11.date_delivrance = row6.date_delivrance;
                                row11.permis_delivre_par = row6.permis_delivre_par;
                                row11.nom_prenom_conducteur = row6.nom_prenom_conducteur;
                                row11.date_naissance_conducteur = row6.date_naissance_conducteur;
                                row11.date_extraction = row6.date_extraction;
                                row11.date_depot = row6.date_depot;
                                row11.Error_Message = row6.Error_Message;
            }
    			if ((batchSize_tDBOutput_3 > 0) && (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3)) {
                try {
						int countSum_tDBOutput_3 = 0;
						    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
				    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
            	    	batchSizeCounter_tDBOutput_3 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
				    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
				    	String errormessage_tDBOutput_3;
						if (ne_tDBOutput_3 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
							errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
						}else{
							errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
						}
				    	
				    	int countSum_tDBOutput_3 = 0;
						for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
						rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    	System.err.println(errormessage_tDBOutput_3);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_3++;
                if(commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {
                if ((batchSize_tDBOutput_3 > 0) && (batchSizeCounter_tDBOutput_3 > 0)) {
                try {
                		int countSum_tDBOutput_3 = 0;
                		    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
            	    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
            	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
                batchSizeCounter_tDBOutput_3 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
			    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
			    	String errormessage_tDBOutput_3;
					if (ne_tDBOutput_3 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
						errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
					}else{
						errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
					}
			    	
			    	int countSum_tDBOutput_3 = 0;
					for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
					
			    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
			    	
			    	System.err.println(errormessage_tDBOutput_3);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    }
                    conn_tDBOutput_3.commit();
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_3 = 0;
                    }
                    commitCounter_tDBOutput_3=0;
                }

 


	tos_count_tDBOutput_3++;

/**
 * [tDBOutput_3 main ] stop
 */
	
	/**
	 * [tDBOutput_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_begin ] stop
 */
// Start of branch "row11"
if(row11 != null) { 



	
	/**
	 * [tFileOutputDelimited_3 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row11"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_3 = new StringBuilder();
                            if(row11.code_assure != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.code_assureur != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.qualite != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.qualite
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.nom != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.nom
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.prenom != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.prenom
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.profession != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.profession
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.date_naissance != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row11.date_naissance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.num_contribuable != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.num_contribuable
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.ville != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.ville
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.rue != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.rue
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.boite_postale != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.boite_postale
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.telephone != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.telephone
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.email != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.email
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.numero_permis != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.numero_permis
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.categorie_permis != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.categorie_permis
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.date_delivrance != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row11.date_delivrance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.permis_delivre_par != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.permis_delivre_par
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.nom_prenom_conducteur != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.nom_prenom_conducteur
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.date_naissance_conducteur != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row11.date_naissance_conducteur, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.date_extraction != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row11.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.date_depot != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row11.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row11.Error_Message != null) {
                        sb_tFileOutputDelimited_3.append(
                            row11.Error_Message
                        );
                            }
                    sb_tFileOutputDelimited_3.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);


                    nb_line_tFileOutputDelimited_3++;
                    resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

                        outtFileOutputDelimited_3.write(sb_tFileOutputDelimited_3.toString());




 


	tos_count_tFileOutputDelimited_3++;

/**
 * [tFileOutputDelimited_3 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_end ] stop
 */

} // End of branch "row11"




	
	/**
	 * [tDBOutput_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_end ] stop
 */

} // End of branch "rejets"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */

} // End of branch "row2"




// Start of branch "row3"
if(row3 != null) { 



	
	/**
	 * [tLogRow_2 main ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row3"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_2 = new String[21];
   				
	    		if(row3.code_assure != null) { //              
                 row_tLogRow_2[0]=    						    
				                String.valueOf(row3.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.code_assureur != null) { //              
                 row_tLogRow_2[1]=    						    
				                String.valueOf(row3.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.qualite != null) { //              
                 row_tLogRow_2[2]=    						    
				                String.valueOf(row3.qualite)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.nom != null) { //              
                 row_tLogRow_2[3]=    						    
				                String.valueOf(row3.nom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.prenom != null) { //              
                 row_tLogRow_2[4]=    						    
				                String.valueOf(row3.prenom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.profession != null) { //              
                 row_tLogRow_2[5]=    						    
				                String.valueOf(row3.profession)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.date_naissance != null) { //              
                 row_tLogRow_2[6]=    						
								FormatterUtils.format_Date(row3.date_naissance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row3.num_contribuable != null) { //              
                 row_tLogRow_2[7]=    						    
				                String.valueOf(row3.num_contribuable)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.ville != null) { //              
                 row_tLogRow_2[8]=    						    
				                String.valueOf(row3.ville)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.rue != null) { //              
                 row_tLogRow_2[9]=    						    
				                String.valueOf(row3.rue)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.boite_postale != null) { //              
                 row_tLogRow_2[10]=    						    
				                String.valueOf(row3.boite_postale)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.telephone != null) { //              
                 row_tLogRow_2[11]=    						    
				                String.valueOf(row3.telephone)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.email != null) { //              
                 row_tLogRow_2[12]=    						    
				                String.valueOf(row3.email)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.numero_permis != null) { //              
                 row_tLogRow_2[13]=    						    
				                String.valueOf(row3.numero_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.categorie_permis != null) { //              
                 row_tLogRow_2[14]=    						    
				                String.valueOf(row3.categorie_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.date_delivrance != null) { //              
                 row_tLogRow_2[15]=    						
								FormatterUtils.format_Date(row3.date_delivrance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row3.permis_delivre_par != null) { //              
                 row_tLogRow_2[16]=    						    
				                String.valueOf(row3.permis_delivre_par)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.nom_prenom_conducteur != null) { //              
                 row_tLogRow_2[17]=    						    
				                String.valueOf(row3.nom_prenom_conducteur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.date_naissance_conducteur != null) { //              
                 row_tLogRow_2[18]=    						
								FormatterUtils.format_Date(row3.date_naissance_conducteur, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row3.errorCode != null) { //              
                 row_tLogRow_2[19]=    						    
				                String.valueOf(row3.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.errorMessage != null) { //              
                 row_tLogRow_2[20]=    						    
				                String.valueOf(row3.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_2.addRow(row_tLogRow_2);	
				nb_line_tLogRow_2++;
//////

//////                    
                    
///////////////////////    			

 
     row7 = row3;


	tos_count_tLogRow_2++;

/**
 * [tLogRow_2 main ] stop
 */
	
	/**
	 * [tLogRow_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_3 main ] start
	 */

	

	
	
	currentComponent="tMap_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row7"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_3 = false;
		  boolean mainRowRejected_tMap_3 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
        // ###############################
        // # Output tables

rejet_Sch = null;


// # Output table : 'rejet_Sch'
rejet_Sch_tmp.code_assure = row7.code_assure ;
rejet_Sch_tmp.code_assureur = row7.code_assureur ;
rejet_Sch_tmp.qualite = row7.qualite ;
rejet_Sch_tmp.nom = row7.nom ;
rejet_Sch_tmp.prenom = row7.prenom ;
rejet_Sch_tmp.profession = row7.profession ;
rejet_Sch_tmp.date_naissance = row7.date_naissance ;
rejet_Sch_tmp.num_contribuable = row7.num_contribuable ;
rejet_Sch_tmp.ville = row7.ville ;
rejet_Sch_tmp.rue = row7.rue ;
rejet_Sch_tmp.boite_postale = row7.boite_postale;
rejet_Sch_tmp.telephone = row7.telephone ;
rejet_Sch_tmp.email = row7.email ;
rejet_Sch_tmp.numero_permis = row7.numero_permis ;
rejet_Sch_tmp.categorie_permis = row7.categorie_permis ;
rejet_Sch_tmp.date_delivrance = row7.date_delivrance ;
rejet_Sch_tmp.permis_delivre_par = row7.permis_delivre_par ;
rejet_Sch_tmp.nom_prenom_conducteur = row7.nom_prenom_conducteur ;
rejet_Sch_tmp.date_naissance_conducteur = row7.date_naissance_conducteur ;
rejet_Sch_tmp.date_extraction = TalendDate.getCurrentDate() ;
rejet_Sch_tmp.date_depot = null;
rejet_Sch_tmp.errorCode = row7.errorCode ;
rejet_Sch_tmp.errorMessage = row7.errorMessage ;
rejet_Sch = rejet_Sch_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_3 = false;










 


	tos_count_tMap_3++;

/**
 * [tMap_3 main ] stop
 */
	
	/**
	 * [tMap_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_begin ] stop
 */
// Start of branch "rejet_Sch"
if(rejet_Sch != null) { 



	
	/**
	 * [tDBOutput_4 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"rejet_Sch"
						
						);
					}
					



            row12 = null;
        whetherReject_tDBOutput_4 = false;
                    if(rejet_Sch.code_assure == null) {
pstmt_tDBOutput_4.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(1, rejet_Sch.code_assure);
}

                    if(rejet_Sch.code_assureur == null) {
pstmt_tDBOutput_4.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(2, rejet_Sch.code_assureur);
}

                    if(rejet_Sch.qualite == null) {
pstmt_tDBOutput_4.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(3, rejet_Sch.qualite);
}

                    if(rejet_Sch.nom == null) {
pstmt_tDBOutput_4.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(4, rejet_Sch.nom);
}

                    if(rejet_Sch.prenom == null) {
pstmt_tDBOutput_4.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(5, rejet_Sch.prenom);
}

                    if(rejet_Sch.profession == null) {
pstmt_tDBOutput_4.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(6, rejet_Sch.profession);
}

                    if(rejet_Sch.date_naissance != null) {
pstmt_tDBOutput_4.setTimestamp(7, new java.sql.Timestamp(rejet_Sch.date_naissance.getTime()));
} else {
pstmt_tDBOutput_4.setNull(7, java.sql.Types.TIMESTAMP);
}

                    if(rejet_Sch.num_contribuable == null) {
pstmt_tDBOutput_4.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(8, rejet_Sch.num_contribuable);
}

                    if(rejet_Sch.ville == null) {
pstmt_tDBOutput_4.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(9, rejet_Sch.ville);
}

                    if(rejet_Sch.rue == null) {
pstmt_tDBOutput_4.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(10, rejet_Sch.rue);
}

                    if(rejet_Sch.boite_postale == null) {
pstmt_tDBOutput_4.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(11, rejet_Sch.boite_postale);
}

                    if(rejet_Sch.telephone == null) {
pstmt_tDBOutput_4.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(12, rejet_Sch.telephone);
}

                    if(rejet_Sch.email == null) {
pstmt_tDBOutput_4.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(13, rejet_Sch.email);
}

                    if(rejet_Sch.numero_permis == null) {
pstmt_tDBOutput_4.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(14, rejet_Sch.numero_permis);
}

                    if(rejet_Sch.categorie_permis == null) {
pstmt_tDBOutput_4.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(15, rejet_Sch.categorie_permis);
}

                    if(rejet_Sch.date_delivrance != null) {
pstmt_tDBOutput_4.setTimestamp(16, new java.sql.Timestamp(rejet_Sch.date_delivrance.getTime()));
} else {
pstmt_tDBOutput_4.setNull(16, java.sql.Types.TIMESTAMP);
}

                    if(rejet_Sch.permis_delivre_par == null) {
pstmt_tDBOutput_4.setNull(17, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(17, rejet_Sch.permis_delivre_par);
}

                    if(rejet_Sch.nom_prenom_conducteur == null) {
pstmt_tDBOutput_4.setNull(18, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(18, rejet_Sch.nom_prenom_conducteur);
}

                    if(rejet_Sch.date_naissance_conducteur != null) {
pstmt_tDBOutput_4.setTimestamp(19, new java.sql.Timestamp(rejet_Sch.date_naissance_conducteur.getTime()));
} else {
pstmt_tDBOutput_4.setNull(19, java.sql.Types.TIMESTAMP);
}

                    if(rejet_Sch.date_extraction != null) {
pstmt_tDBOutput_4.setTimestamp(20, new java.sql.Timestamp(rejet_Sch.date_extraction.getTime()));
} else {
pstmt_tDBOutput_4.setNull(20, java.sql.Types.TIMESTAMP);
}

                    if(rejet_Sch.date_depot != null) {
pstmt_tDBOutput_4.setTimestamp(21, new java.sql.Timestamp(rejet_Sch.date_depot.getTime()));
} else {
pstmt_tDBOutput_4.setNull(21, java.sql.Types.TIMESTAMP);
}

                    if(rejet_Sch.errorCode == null) {
pstmt_tDBOutput_4.setNull(22, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(22, rejet_Sch.errorCode);
}

                    if(rejet_Sch.errorMessage == null) {
pstmt_tDBOutput_4.setNull(23, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(23, rejet_Sch.errorMessage);
}

			
    		pstmt_tDBOutput_4.addBatch();
    		nb_line_tDBOutput_4++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_4++;
    		  
            if(!whetherReject_tDBOutput_4) {
                            row12 = new row12Struct();
                                row12.code_assure = rejet_Sch.code_assure;
                                row12.code_assureur = rejet_Sch.code_assureur;
                                row12.qualite = rejet_Sch.qualite;
                                row12.nom = rejet_Sch.nom;
                                row12.prenom = rejet_Sch.prenom;
                                row12.profession = rejet_Sch.profession;
                                row12.date_naissance = rejet_Sch.date_naissance;
                                row12.num_contribuable = rejet_Sch.num_contribuable;
                                row12.ville = rejet_Sch.ville;
                                row12.rue = rejet_Sch.rue;
                                row12.boite_postale = rejet_Sch.boite_postale;
                                row12.telephone = rejet_Sch.telephone;
                                row12.email = rejet_Sch.email;
                                row12.numero_permis = rejet_Sch.numero_permis;
                                row12.categorie_permis = rejet_Sch.categorie_permis;
                                row12.date_delivrance = rejet_Sch.date_delivrance;
                                row12.permis_delivre_par = rejet_Sch.permis_delivre_par;
                                row12.nom_prenom_conducteur = rejet_Sch.nom_prenom_conducteur;
                                row12.date_naissance_conducteur = rejet_Sch.date_naissance_conducteur;
                                row12.date_extraction = rejet_Sch.date_extraction;
                                row12.date_depot = rejet_Sch.date_depot;
                                row12.errorCode = rejet_Sch.errorCode;
                                row12.errorMessage = rejet_Sch.errorMessage;
            }
    			if ((batchSize_tDBOutput_4 > 0) && (batchSize_tDBOutput_4 <= batchSizeCounter_tDBOutput_4)) {
                try {
						int countSum_tDBOutput_4 = 0;
						    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
				    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
            	    	batchSizeCounter_tDBOutput_4 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
				    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
				    	String errormessage_tDBOutput_4;
						if (ne_tDBOutput_4 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
							errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
						}else{
							errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
						}
				    	
				    	int countSum_tDBOutput_4 = 0;
						for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
						rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    	System.err.println(errormessage_tDBOutput_4);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_4++;
                if(commitEvery_tDBOutput_4 <= commitCounter_tDBOutput_4) {
                if ((batchSize_tDBOutput_4 > 0) && (batchSizeCounter_tDBOutput_4 > 0)) {
                try {
                		int countSum_tDBOutput_4 = 0;
                		    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
            	    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
            	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
                batchSizeCounter_tDBOutput_4 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
			    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
			    	String errormessage_tDBOutput_4;
					if (ne_tDBOutput_4 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
						errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
					}else{
						errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
					}
			    	
			    	int countSum_tDBOutput_4 = 0;
					for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
					
			    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
			    	
			    	System.err.println(errormessage_tDBOutput_4);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    }
                    conn_tDBOutput_4.commit();
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_4 = 0;
                    }
                    commitCounter_tDBOutput_4=0;
                }

 


	tos_count_tDBOutput_4++;

/**
 * [tDBOutput_4 main ] stop
 */
	
	/**
	 * [tDBOutput_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_begin ] stop
 */
// Start of branch "row12"
if(row12 != null) { 



	
	/**
	 * [tFileOutputDelimited_4 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row12"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_4 = new StringBuilder();
                            if(row12.code_assure != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.code_assureur != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.qualite != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.qualite
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.nom != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.nom
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.prenom != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.prenom
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.profession != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.profession
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.date_naissance != null) {
                        sb_tFileOutputDelimited_4.append(
                            FormatterUtils.format_Date(row12.date_naissance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.num_contribuable != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.num_contribuable
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.ville != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.ville
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.rue != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.rue
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.boite_postale != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.boite_postale
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.telephone != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.telephone
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.email != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.email
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.numero_permis != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.numero_permis
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.categorie_permis != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.categorie_permis
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.date_delivrance != null) {
                        sb_tFileOutputDelimited_4.append(
                            FormatterUtils.format_Date(row12.date_delivrance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.permis_delivre_par != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.permis_delivre_par
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.nom_prenom_conducteur != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.nom_prenom_conducteur
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.date_naissance_conducteur != null) {
                        sb_tFileOutputDelimited_4.append(
                            FormatterUtils.format_Date(row12.date_naissance_conducteur, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.date_extraction != null) {
                        sb_tFileOutputDelimited_4.append(
                            FormatterUtils.format_Date(row12.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.date_depot != null) {
                        sb_tFileOutputDelimited_4.append(
                            FormatterUtils.format_Date(row12.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.errorCode != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_4.append(OUT_DELIM_tFileOutputDelimited_4);
                            if(row12.errorMessage != null) {
                        sb_tFileOutputDelimited_4.append(
                            row12.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_4.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_4);


                    nb_line_tFileOutputDelimited_4++;
                    resourceMap.put("nb_line_tFileOutputDelimited_4", nb_line_tFileOutputDelimited_4);

                        outtFileOutputDelimited_4.write(sb_tFileOutputDelimited_4.toString());




 


	tos_count_tFileOutputDelimited_4++;

/**
 * [tFileOutputDelimited_4 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_4";

	

 



/**
 * [tFileOutputDelimited_4 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_4";

	

 



/**
 * [tFileOutputDelimited_4 process_data_end ] stop
 */

} // End of branch "row12"




	
	/**
	 * [tDBOutput_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_end ] stop
 */

} // End of branch "rejet_Sch"




	
	/**
	 * [tMap_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_end ] stop
 */

} // End of branch "row3"




	
	/**
	 * [tSchemaComplianceCheck_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_end ] stop
 */

} // End of branch "row1"




// Start of branch "row8"
if(row8 != null) { 



	
	/**
	 * [tLogRow_5 main ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row8"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_5 = new String[21];
   				
	    		if(row8.code_assure != null) { //              
                 row_tLogRow_5[0]=    						    
				                String.valueOf(row8.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.code_assureur != null) { //              
                 row_tLogRow_5[1]=    						    
				                String.valueOf(row8.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.qualite != null) { //              
                 row_tLogRow_5[2]=    						    
				                String.valueOf(row8.qualite)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.nom != null) { //              
                 row_tLogRow_5[3]=    						    
				                String.valueOf(row8.nom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.prenom != null) { //              
                 row_tLogRow_5[4]=    						    
				                String.valueOf(row8.prenom)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.profession != null) { //              
                 row_tLogRow_5[5]=    						    
				                String.valueOf(row8.profession)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.date_naissance != null) { //              
                 row_tLogRow_5[6]=    						
								FormatterUtils.format_Date(row8.date_naissance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row8.num_contribuable != null) { //              
                 row_tLogRow_5[7]=    						    
				                String.valueOf(row8.num_contribuable)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.ville != null) { //              
                 row_tLogRow_5[8]=    						    
				                String.valueOf(row8.ville)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.rue != null) { //              
                 row_tLogRow_5[9]=    						    
				                String.valueOf(row8.rue)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.boite_postale != null) { //              
                 row_tLogRow_5[10]=    						    
				                String.valueOf(row8.boite_postale)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.telephone != null) { //              
                 row_tLogRow_5[11]=    						    
				                String.valueOf(row8.telephone)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.email != null) { //              
                 row_tLogRow_5[12]=    						    
				                String.valueOf(row8.email)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.numero_permis != null) { //              
                 row_tLogRow_5[13]=    						    
				                String.valueOf(row8.numero_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.categorie_permis != null) { //              
                 row_tLogRow_5[14]=    						    
				                String.valueOf(row8.categorie_permis)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.date_delivrance != null) { //              
                 row_tLogRow_5[15]=    						
								FormatterUtils.format_Date(row8.date_delivrance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row8.permis_delivre_par != null) { //              
                 row_tLogRow_5[16]=    						    
				                String.valueOf(row8.permis_delivre_par)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.nom_prenom_conducteur != null) { //              
                 row_tLogRow_5[17]=    						    
				                String.valueOf(row8.nom_prenom_conducteur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.date_naissance_conducteur != null) { //              
                 row_tLogRow_5[18]=    						
								FormatterUtils.format_Date(row8.date_naissance_conducteur, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row8.errorCode != null) { //              
                 row_tLogRow_5[19]=    						    
				                String.valueOf(row8.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row8.errorMessage != null) { //              
                 row_tLogRow_5[20]=    						    
				                String.valueOf(row8.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_5.addRow(row_tLogRow_5);	
				nb_line_tLogRow_5++;
//////

//////                    
                    
///////////////////////    			

 
     row9 = row8;


	tos_count_tLogRow_5++;

/**
 * [tLogRow_5 main ] stop
 */
	
	/**
	 * [tLogRow_5 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 process_data_begin ] stop
 */

	
	/**
	 * [tMap_2 main ] start
	 */

	

	
	
	currentComponent="tMap_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row9"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_2 = false;
		  boolean mainRowRejected_tMap_2 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
        // ###############################
        // # Output tables

rejet_struc = null;


// # Output table : 'rejet_struc'
rejet_struc_tmp.code_assure = row9.code_assure ;
rejet_struc_tmp.code_assureur = row9.code_assureur ;
rejet_struc_tmp.qualite = row9.qualite ;
rejet_struc_tmp.nom = row9.nom ;
rejet_struc_tmp.prenom = row9.prenom ;
rejet_struc_tmp.profession = row9.profession ;
rejet_struc_tmp.date_naissance = row9.date_naissance ;
rejet_struc_tmp.num_contribuable = row9.num_contribuable ;
rejet_struc_tmp.ville = row9.ville ;
rejet_struc_tmp.rue = row9.rue ;
rejet_struc_tmp.boite_postale = row9.boite_postale;
rejet_struc_tmp.telephone = row9.telephone ;
rejet_struc_tmp.email = row9.email ;
rejet_struc_tmp.numero_permis = row9.numero_permis ;
rejet_struc_tmp.categorie_permis = row9.categorie_permis ;
rejet_struc_tmp.date_delivrance = row9.date_delivrance ;
rejet_struc_tmp.permis_delivre_par = row9.permis_delivre_par ;
rejet_struc_tmp.nom_prenom_conducteur = row9.nom_prenom_conducteur ;
rejet_struc_tmp.date_naissance_conducteur = row9.date_naissance_conducteur ;
rejet_struc_tmp.date_extraction = TalendDate.getCurrentDate() ;
rejet_struc_tmp.date_depot = null;
rejet_struc_tmp.errorCode = row9.errorCode ;
rejet_struc_tmp.errorMessage = row9.errorMessage ;
rejet_struc = rejet_struc_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_2 = false;










 


	tos_count_tMap_2++;

/**
 * [tMap_2 main ] stop
 */
	
	/**
	 * [tMap_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_begin ] stop
 */
// Start of branch "rejet_struc"
if(rejet_struc != null) { 



	
	/**
	 * [tDBOutput_2 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"rejet_struc"
						
						);
					}
					



            row10 = null;
        whetherReject_tDBOutput_2 = false;
                    if(rejet_struc.code_assure == null) {
pstmt_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(1, rejet_struc.code_assure);
}

                    if(rejet_struc.code_assureur == null) {
pstmt_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(2, rejet_struc.code_assureur);
}

                    if(rejet_struc.qualite == null) {
pstmt_tDBOutput_2.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(3, rejet_struc.qualite);
}

                    if(rejet_struc.nom == null) {
pstmt_tDBOutput_2.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(4, rejet_struc.nom);
}

                    if(rejet_struc.prenom == null) {
pstmt_tDBOutput_2.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(5, rejet_struc.prenom);
}

                    if(rejet_struc.profession == null) {
pstmt_tDBOutput_2.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(6, rejet_struc.profession);
}

                    if(rejet_struc.date_naissance != null) {
pstmt_tDBOutput_2.setTimestamp(7, new java.sql.Timestamp(rejet_struc.date_naissance.getTime()));
} else {
pstmt_tDBOutput_2.setNull(7, java.sql.Types.TIMESTAMP);
}

                    if(rejet_struc.num_contribuable == null) {
pstmt_tDBOutput_2.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(8, rejet_struc.num_contribuable);
}

                    if(rejet_struc.ville == null) {
pstmt_tDBOutput_2.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(9, rejet_struc.ville);
}

                    if(rejet_struc.rue == null) {
pstmt_tDBOutput_2.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(10, rejet_struc.rue);
}

                    if(rejet_struc.boite_postale == null) {
pstmt_tDBOutput_2.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(11, rejet_struc.boite_postale);
}

                    if(rejet_struc.telephone == null) {
pstmt_tDBOutput_2.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(12, rejet_struc.telephone);
}

                    if(rejet_struc.email == null) {
pstmt_tDBOutput_2.setNull(13, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(13, rejet_struc.email);
}

                    if(rejet_struc.numero_permis == null) {
pstmt_tDBOutput_2.setNull(14, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(14, rejet_struc.numero_permis);
}

                    if(rejet_struc.categorie_permis == null) {
pstmt_tDBOutput_2.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(15, rejet_struc.categorie_permis);
}

                    if(rejet_struc.date_delivrance != null) {
pstmt_tDBOutput_2.setTimestamp(16, new java.sql.Timestamp(rejet_struc.date_delivrance.getTime()));
} else {
pstmt_tDBOutput_2.setNull(16, java.sql.Types.TIMESTAMP);
}

                    if(rejet_struc.permis_delivre_par == null) {
pstmt_tDBOutput_2.setNull(17, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(17, rejet_struc.permis_delivre_par);
}

                    if(rejet_struc.nom_prenom_conducteur == null) {
pstmt_tDBOutput_2.setNull(18, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(18, rejet_struc.nom_prenom_conducteur);
}

                    if(rejet_struc.date_naissance_conducteur != null) {
pstmt_tDBOutput_2.setTimestamp(19, new java.sql.Timestamp(rejet_struc.date_naissance_conducteur.getTime()));
} else {
pstmt_tDBOutput_2.setNull(19, java.sql.Types.TIMESTAMP);
}

                    if(rejet_struc.date_extraction != null) {
pstmt_tDBOutput_2.setTimestamp(20, new java.sql.Timestamp(rejet_struc.date_extraction.getTime()));
} else {
pstmt_tDBOutput_2.setNull(20, java.sql.Types.TIMESTAMP);
}

                    if(rejet_struc.date_depot != null) {
pstmt_tDBOutput_2.setTimestamp(21, new java.sql.Timestamp(rejet_struc.date_depot.getTime()));
} else {
pstmt_tDBOutput_2.setNull(21, java.sql.Types.TIMESTAMP);
}

                    if(rejet_struc.errorCode == null) {
pstmt_tDBOutput_2.setNull(22, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(22, rejet_struc.errorCode);
}

                    if(rejet_struc.errorMessage == null) {
pstmt_tDBOutput_2.setNull(23, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(23, rejet_struc.errorMessage);
}

			
    		pstmt_tDBOutput_2.addBatch();
    		nb_line_tDBOutput_2++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_2++;
    		  
            if(!whetherReject_tDBOutput_2) {
                            row10 = new row10Struct();
                                row10.code_assure = rejet_struc.code_assure;
                                row10.code_assureur = rejet_struc.code_assureur;
                                row10.qualite = rejet_struc.qualite;
                                row10.nom = rejet_struc.nom;
                                row10.prenom = rejet_struc.prenom;
                                row10.profession = rejet_struc.profession;
                                row10.date_naissance = rejet_struc.date_naissance;
                                row10.num_contribuable = rejet_struc.num_contribuable;
                                row10.ville = rejet_struc.ville;
                                row10.rue = rejet_struc.rue;
                                row10.boite_postale = rejet_struc.boite_postale;
                                row10.telephone = rejet_struc.telephone;
                                row10.email = rejet_struc.email;
                                row10.numero_permis = rejet_struc.numero_permis;
                                row10.categorie_permis = rejet_struc.categorie_permis;
                                row10.date_delivrance = rejet_struc.date_delivrance;
                                row10.permis_delivre_par = rejet_struc.permis_delivre_par;
                                row10.nom_prenom_conducteur = rejet_struc.nom_prenom_conducteur;
                                row10.date_naissance_conducteur = rejet_struc.date_naissance_conducteur;
                                row10.date_extraction = rejet_struc.date_extraction;
                                row10.date_depot = rejet_struc.date_depot;
                                row10.errorCode = rejet_struc.errorCode;
                                row10.errorMessage = rejet_struc.errorMessage;
            }
    			if ((batchSize_tDBOutput_2 > 0) && (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2)) {
                try {
						int countSum_tDBOutput_2 = 0;
						    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
				    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
            	    	batchSizeCounter_tDBOutput_2 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
				    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
				    	String errormessage_tDBOutput_2;
						if (ne_tDBOutput_2 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
							errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
						}else{
							errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
						}
				    	
				    	int countSum_tDBOutput_2 = 0;
						for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
						rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    	System.err.println(errormessage_tDBOutput_2);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_2++;
                if(commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {
                if ((batchSize_tDBOutput_2 > 0) && (batchSizeCounter_tDBOutput_2 > 0)) {
                try {
                		int countSum_tDBOutput_2 = 0;
                		    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
            	    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
            	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
                batchSizeCounter_tDBOutput_2 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
			    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
			    	String errormessage_tDBOutput_2;
					if (ne_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
						errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
					}else{
						errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
					}
			    	
			    	int countSum_tDBOutput_2 = 0;
					for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
					
			    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
			    	
			    	System.err.println(errormessage_tDBOutput_2);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    }
                    conn_tDBOutput_2.commit();
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_2 = 0;
                    }
                    commitCounter_tDBOutput_2=0;
                }

 


	tos_count_tDBOutput_2++;

/**
 * [tDBOutput_2 main ] stop
 */
	
	/**
	 * [tDBOutput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_begin ] stop
 */
// Start of branch "row10"
if(row10 != null) { 



	
	/**
	 * [tFileOutputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row10"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_1 = new StringBuilder();
                            if(row10.code_assure != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.code_assureur != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.qualite != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.qualite
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.nom != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.nom
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.prenom != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.prenom
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.profession != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.profession
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.date_naissance != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row10.date_naissance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.num_contribuable != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.num_contribuable
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.ville != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.ville
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.rue != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.rue
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.boite_postale != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.boite_postale
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.telephone != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.telephone
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.email != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.email
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.numero_permis != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.numero_permis
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.categorie_permis != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.categorie_permis
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.date_delivrance != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row10.date_delivrance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.permis_delivre_par != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.permis_delivre_par
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.nom_prenom_conducteur != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.nom_prenom_conducteur
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.date_naissance_conducteur != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row10.date_naissance_conducteur, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.date_extraction != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row10.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.date_depot != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row10.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.errorCode != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row10.errorMessage != null) {
                        sb_tFileOutputDelimited_1.append(
                            row10.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_1.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);


                    nb_line_tFileOutputDelimited_1++;
                    resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

                        outtFileOutputDelimited_1.write(sb_tFileOutputDelimited_1.toString());




 


	tos_count_tFileOutputDelimited_1++;

/**
 * [tFileOutputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_end ] stop
 */

} // End of branch "row10"




	
	/**
	 * [tDBOutput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_end ] stop
 */

} // End of branch "rejet_struc"




	
	/**
	 * [tMap_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_5 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 process_data_end ] stop
 */

} // End of branch "row8"




	
	/**
	 * [tFileInputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_end ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	


				nb_line_tFileInputDelimited_1++;
			}
			
			}finally{
    			if(!(filename_tFileInputDelimited_1 instanceof java.io.InputStream)){
    				if(csvReadertFileInputDelimited_1!=null){
    					csvReadertFileInputDelimited_1.close();
    				}
    			}
    			if(csvReadertFileInputDelimited_1!=null){
    				globalMap.put("tFileInputDelimited_1_NB_LINE",nb_line_tFileInputDelimited_1);
    			}
				
			}
						  

 

ok_Hash.put("tFileInputDelimited_1", true);
end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());




/**
 * [tFileInputDelimited_1 end ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row1");
			  	}
			  	
 

ok_Hash.put("tSchemaComplianceCheck_1", true);
end_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());




/**
 * [tSchemaComplianceCheck_1 end ] stop
 */

	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row2");
			  	}
			  	
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());




/**
 * [tLogRow_1 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row4");
			  	}
			  	
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tLogRow_3 end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_3 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_3 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_3 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_3);
                    }
                    
                    consoleOut_tLogRow_3.println(util_tLogRow_3.format().toString());
                    consoleOut_tLogRow_3.flush();
//////
globalMap.put("tLogRow_3_NB_LINE",nb_line_tLogRow_3);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"valides");
			  	}
			  	
 

ok_Hash.put("tLogRow_3", true);
end_Hash.put("tLogRow_3", System.currentTimeMillis());




/**
 * [tLogRow_3 end ] stop
 */

	
	/**
	 * [tDBOutput_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



        if(pstmtUpdate_tDBOutput_1 != null){
            pstmtUpdate_tDBOutput_1.close();
            resourceMap.remove("pstmtUpdate_tDBOutput_1");
        }
        if(pstmtInsert_tDBOutput_1 != null){
            pstmtInsert_tDBOutput_1.close();
            resourceMap.remove("pstmtInsert_tDBOutput_1");
        }
        if(pstmt_tDBOutput_1 != null) {
            pstmt_tDBOutput_1.close();
            resourceMap.remove("pstmt_tDBOutput_1");
        }
    resourceMap.put("statementClosed_tDBOutput_1", true);
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
			}
			conn_tDBOutput_1.commit();
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
				rowsToCommitCount_tDBOutput_1 = 0;
			}
			commitCounter_tDBOutput_1 = 0;
		
    	conn_tDBOutput_1 .close();
    	
    	resourceMap.put("finish_tDBOutput_1", true);
    	

	nb_line_deleted_tDBOutput_1=nb_line_deleted_tDBOutput_1+ deletedCount_tDBOutput_1;
	nb_line_update_tDBOutput_1=nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
	nb_line_inserted_tDBOutput_1=nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
	nb_line_rejected_tDBOutput_1=nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;
	
        globalMap.put("tDBOutput_1_NB_LINE",nb_line_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_UPDATED",nb_line_update_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_DELETED",nb_line_deleted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row5");
			  	}
			  	
 

ok_Hash.put("tDBOutput_1", true);
end_Hash.put("tDBOutput_1", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tFileCopy_1Process(globalMap);



/**
 * [tDBOutput_1 end ] stop
 */







	
	/**
	 * [tLogRow_4 end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_4 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_4 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_4 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_4);
                    }
                    
                    consoleOut_tLogRow_4.println(util_tLogRow_4.format().toString());
                    consoleOut_tLogRow_4.flush();
//////
globalMap.put("tLogRow_4_NB_LINE",nb_line_tLogRow_4);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"rejets");
			  	}
			  	
 

ok_Hash.put("tLogRow_4", true);
end_Hash.put("tLogRow_4", System.currentTimeMillis());




/**
 * [tLogRow_4 end ] stop
 */

	
	/**
	 * [tDBOutput_3 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



	    try {
				int countSum_tDBOutput_3 = 0;
				if (pstmt_tDBOutput_3 != null && batchSizeCounter_tDBOutput_3 > 0) {
						
					for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				}
		    	
		    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
	    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
	    	String errormessage_tDBOutput_3;
			if (ne_tDBOutput_3 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
				errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
			}else{
				errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
			}
	    	
	    	int countSum_tDBOutput_3 = 0;
			for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
				countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
			}
			rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
			
	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
	    	
	    	System.err.println(errormessage_tDBOutput_3);
	    	
		}
	    
        if(pstmt_tDBOutput_3 != null) {
        		
            pstmt_tDBOutput_3.close();
            resourceMap.remove("pstmt_tDBOutput_3");
        }
    resourceMap.put("statementClosed_tDBOutput_3", true);
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
			}
			conn_tDBOutput_3.commit();
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
				rowsToCommitCount_tDBOutput_3 = 0;
			}
			commitCounter_tDBOutput_3 = 0;
		
    	conn_tDBOutput_3 .close();
    	
    	resourceMap.put("finish_tDBOutput_3", true);
    	

	nb_line_deleted_tDBOutput_3=nb_line_deleted_tDBOutput_3+ deletedCount_tDBOutput_3;
	nb_line_update_tDBOutput_3=nb_line_update_tDBOutput_3 + updatedCount_tDBOutput_3;
	nb_line_inserted_tDBOutput_3=nb_line_inserted_tDBOutput_3 + insertedCount_tDBOutput_3;
	nb_line_rejected_tDBOutput_3=nb_line_rejected_tDBOutput_3 + rejectedCount_tDBOutput_3;
	
        globalMap.put("tDBOutput_3_NB_LINE",nb_line_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_UPDATED",nb_line_update_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_DELETED",nb_line_deleted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_3);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row6");
			  	}
			  	
 

ok_Hash.put("tDBOutput_3", true);
end_Hash.put("tDBOutput_3", System.currentTimeMillis());




/**
 * [tDBOutput_3 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	



		
			
					if(outtFileOutputDelimited_3!=null) {
						outtFileOutputDelimited_3.flush();
						outtFileOutputDelimited_3.close();
					}
				
				globalMap.put("tFileOutputDelimited_3_NB_LINE",nb_line_tFileOutputDelimited_3);
				globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_3", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row11");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_3", true);
end_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_3 end ] stop
 */
















	
	/**
	 * [tLogRow_2 end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_2 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_2 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_2 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_2);
                    }
                    
                    consoleOut_tLogRow_2.println(util_tLogRow_2.format().toString());
                    consoleOut_tLogRow_2.flush();
//////
globalMap.put("tLogRow_2_NB_LINE",nb_line_tLogRow_2);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row3");
			  	}
			  	
 

ok_Hash.put("tLogRow_2", true);
end_Hash.put("tLogRow_2", System.currentTimeMillis());




/**
 * [tLogRow_2 end ] stop
 */

	
	/**
	 * [tMap_3 end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row7");
			  	}
			  	
 

ok_Hash.put("tMap_3", true);
end_Hash.put("tMap_3", System.currentTimeMillis());




/**
 * [tMap_3 end ] stop
 */

	
	/**
	 * [tDBOutput_4 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



	    try {
				int countSum_tDBOutput_4 = 0;
				if (pstmt_tDBOutput_4 != null && batchSizeCounter_tDBOutput_4 > 0) {
						
					for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				}
		    	
		    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
	    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
	    	String errormessage_tDBOutput_4;
			if (ne_tDBOutput_4 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
				errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
			}else{
				errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
			}
	    	
	    	int countSum_tDBOutput_4 = 0;
			for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
				countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
			}
			rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
			
	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
	    	
	    	System.err.println(errormessage_tDBOutput_4);
	    	
		}
	    
        if(pstmt_tDBOutput_4 != null) {
        		
            pstmt_tDBOutput_4.close();
            resourceMap.remove("pstmt_tDBOutput_4");
        }
    resourceMap.put("statementClosed_tDBOutput_4", true);
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
			}
			conn_tDBOutput_4.commit();
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
				rowsToCommitCount_tDBOutput_4 = 0;
			}
			commitCounter_tDBOutput_4 = 0;
		
    	conn_tDBOutput_4 .close();
    	
    	resourceMap.put("finish_tDBOutput_4", true);
    	

	nb_line_deleted_tDBOutput_4=nb_line_deleted_tDBOutput_4+ deletedCount_tDBOutput_4;
	nb_line_update_tDBOutput_4=nb_line_update_tDBOutput_4 + updatedCount_tDBOutput_4;
	nb_line_inserted_tDBOutput_4=nb_line_inserted_tDBOutput_4 + insertedCount_tDBOutput_4;
	nb_line_rejected_tDBOutput_4=nb_line_rejected_tDBOutput_4 + rejectedCount_tDBOutput_4;
	
        globalMap.put("tDBOutput_4_NB_LINE",nb_line_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_UPDATED",nb_line_update_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_DELETED",nb_line_deleted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_4);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"rejet_Sch");
			  	}
			  	
 

ok_Hash.put("tDBOutput_4", true);
end_Hash.put("tDBOutput_4", System.currentTimeMillis());




/**
 * [tDBOutput_4 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_4 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_4";

	



		
			
					if(outtFileOutputDelimited_4!=null) {
						outtFileOutputDelimited_4.flush();
						outtFileOutputDelimited_4.close();
					}
				
				globalMap.put("tFileOutputDelimited_4_NB_LINE",nb_line_tFileOutputDelimited_4);
				globalMap.put("tFileOutputDelimited_4_FILE_NAME",fileName_tFileOutputDelimited_4);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_4", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row12");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_4", true);
end_Hash.put("tFileOutputDelimited_4", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_4 end ] stop
 */
















	
	/**
	 * [tLogRow_5 end ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_5 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_5 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_5 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_5);
                    }
                    
                    consoleOut_tLogRow_5.println(util_tLogRow_5.format().toString());
                    consoleOut_tLogRow_5.flush();
//////
globalMap.put("tLogRow_5_NB_LINE",nb_line_tLogRow_5);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row8");
			  	}
			  	
 

ok_Hash.put("tLogRow_5", true);
end_Hash.put("tLogRow_5", System.currentTimeMillis());




/**
 * [tLogRow_5 end ] stop
 */

	
	/**
	 * [tMap_2 end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row9");
			  	}
			  	
 

ok_Hash.put("tMap_2", true);
end_Hash.put("tMap_2", System.currentTimeMillis());




/**
 * [tMap_2 end ] stop
 */

	
	/**
	 * [tDBOutput_2 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



	    try {
				int countSum_tDBOutput_2 = 0;
				if (pstmt_tDBOutput_2 != null && batchSizeCounter_tDBOutput_2 > 0) {
						
					for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				}
		    	
		    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
	    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
	    	String errormessage_tDBOutput_2;
			if (ne_tDBOutput_2 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
				errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
			}else{
				errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
			}
	    	
	    	int countSum_tDBOutput_2 = 0;
			for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
				countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
			}
			rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
			
	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
	    	
	    	System.err.println(errormessage_tDBOutput_2);
	    	
		}
	    
        if(pstmt_tDBOutput_2 != null) {
        		
            pstmt_tDBOutput_2.close();
            resourceMap.remove("pstmt_tDBOutput_2");
        }
    resourceMap.put("statementClosed_tDBOutput_2", true);
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
			}
			conn_tDBOutput_2.commit();
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
				rowsToCommitCount_tDBOutput_2 = 0;
			}
			commitCounter_tDBOutput_2 = 0;
		
    	conn_tDBOutput_2 .close();
    	
    	resourceMap.put("finish_tDBOutput_2", true);
    	

	nb_line_deleted_tDBOutput_2=nb_line_deleted_tDBOutput_2+ deletedCount_tDBOutput_2;
	nb_line_update_tDBOutput_2=nb_line_update_tDBOutput_2 + updatedCount_tDBOutput_2;
	nb_line_inserted_tDBOutput_2=nb_line_inserted_tDBOutput_2 + insertedCount_tDBOutput_2;
	nb_line_rejected_tDBOutput_2=nb_line_rejected_tDBOutput_2 + rejectedCount_tDBOutput_2;
	
        globalMap.put("tDBOutput_2_NB_LINE",nb_line_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_UPDATED",nb_line_update_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_DELETED",nb_line_deleted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_2);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"rejet_struc");
			  	}
			  	
 

ok_Hash.put("tDBOutput_2", true);
end_Hash.put("tDBOutput_2", System.currentTimeMillis());




/**
 * [tDBOutput_2 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	



		
			
					if(outtFileOutputDelimited_1!=null) {
						outtFileOutputDelimited_1.flush();
						outtFileOutputDelimited_1.close();
					}
				
				globalMap.put("tFileOutputDelimited_1_NB_LINE",nb_line_tFileOutputDelimited_1);
				globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_1", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row10");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_1", true);
end_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_1 end ] stop
 */












						if(execStat){
							runStat.updateStatOnConnection("iterate1", 2, "exec" + NB_ITERATE_tFileInputDelimited_1);
						}				
					




	
	/**
	 * [tFileList_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_end ] stop
 */
	
	/**
	 * [tFileList_1 end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

  
    }
  globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
  

  
 

 

ok_Hash.put("tFileList_1", true);
end_Hash.put("tFileList_1", System.currentTimeMillis());




/**
 * [tFileList_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileList_1 finally ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 finally ] stop
 */

	
	/**
	 * [tFileInputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 finally ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 finally ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 finally ] stop
 */

	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tLogRow_3 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 finally ] stop
 */

	
	/**
	 * [tDBOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_1") == null) {
                java.sql.PreparedStatement pstmtUpdateToClose_tDBOutput_1 = null;
                if ((pstmtUpdateToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtUpdate_tDBOutput_1")) != null) {
                    pstmtUpdateToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtInsertToClose_tDBOutput_1 = null;
                if ((pstmtInsertToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtInsert_tDBOutput_1")) != null) {
                    pstmtInsertToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtToClose_tDBOutput_1 = null;
                if ((pstmtToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_1")) != null) {
                    pstmtToClose_tDBOutput_1.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_1") == null){
            java.sql.Connection ctn_tDBOutput_1 = null;
            if((ctn_tDBOutput_1 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_1")) != null){
                try {
                    ctn_tDBOutput_1.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_1) {
                    String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :" + sqlEx_tDBOutput_1.getMessage();
                    System.err.println(errorMessage_tDBOutput_1);
                }
            }
        }
    }
 



/**
 * [tDBOutput_1 finally ] stop
 */







	
	/**
	 * [tLogRow_4 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 finally ] stop
 */

	
	/**
	 * [tDBOutput_3 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_3") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_3 = null;
                if ((pstmtToClose_tDBOutput_3 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_3")) != null) {
                    pstmtToClose_tDBOutput_3.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_3") == null){
            java.sql.Connection ctn_tDBOutput_3 = null;
            if((ctn_tDBOutput_3 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_3")) != null){
                try {
                    ctn_tDBOutput_3.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_3) {
                    String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :" + sqlEx_tDBOutput_3.getMessage();
                    System.err.println(errorMessage_tDBOutput_3);
                }
            }
        }
    }
 



/**
 * [tDBOutput_3 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	


		if(resourceMap.get("finish_tFileOutputDelimited_3") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_3 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_3");
						if(outtFileOutputDelimited_3!=null) {
							outtFileOutputDelimited_3.flush();
							outtFileOutputDelimited_3.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_3 finally ] stop
 */
















	
	/**
	 * [tLogRow_2 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 finally ] stop
 */

	
	/**
	 * [tMap_3 finally ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 finally ] stop
 */

	
	/**
	 * [tDBOutput_4 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_4") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_4 = null;
                if ((pstmtToClose_tDBOutput_4 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_4")) != null) {
                    pstmtToClose_tDBOutput_4.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_4") == null){
            java.sql.Connection ctn_tDBOutput_4 = null;
            if((ctn_tDBOutput_4 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_4")) != null){
                try {
                    ctn_tDBOutput_4.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_4) {
                    String errorMessage_tDBOutput_4 = "failed to close the connection in tDBOutput_4 :" + sqlEx_tDBOutput_4.getMessage();
                    System.err.println(errorMessage_tDBOutput_4);
                }
            }
        }
    }
 



/**
 * [tDBOutput_4 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_4 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_4";

	


		if(resourceMap.get("finish_tFileOutputDelimited_4") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_4 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_4");
						if(outtFileOutputDelimited_4!=null) {
							outtFileOutputDelimited_4.flush();
							outtFileOutputDelimited_4.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_4 finally ] stop
 */
















	
	/**
	 * [tLogRow_5 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_5";

	

 



/**
 * [tLogRow_5 finally ] stop
 */

	
	/**
	 * [tMap_2 finally ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 finally ] stop
 */

	
	/**
	 * [tDBOutput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_2") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_2 = null;
                if ((pstmtToClose_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_2")) != null) {
                    pstmtToClose_tDBOutput_2.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_2") == null){
            java.sql.Connection ctn_tDBOutput_2 = null;
            if((ctn_tDBOutput_2 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_2")) != null){
                try {
                    ctn_tDBOutput_2.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_2) {
                    String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :" + sqlEx_tDBOutput_2.getMessage();
                    System.err.println(errorMessage_tDBOutput_2);
                }
            }
        }
    }
 



/**
 * [tDBOutput_2 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	


		if(resourceMap.get("finish_tFileOutputDelimited_1") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_1 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_1");
						if(outtFileOutputDelimited_1!=null) {
							outtFileOutputDelimited_1.flush();
							outtFileOutputDelimited_1.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_1 finally ] stop
 */















				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileList_1_SUBPROCESS_STATE", 1);
	}
	

public void tFileCopy_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [tFileCopy_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileCopy_1", false);
		start_Hash.put("tFileCopy_1", System.currentTimeMillis());
		
	
	currentComponent="tFileCopy_1";

	
		int tos_count_tFileCopy_1 = 0;
		

 



/**
 * [tFileCopy_1 begin ] stop
 */
	
	/**
	 * [tFileCopy_1 main ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 


        String srcFileName_tFileCopy_1 = ((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"));

		java.io.File srcFile_tFileCopy_1 = new java.io.File(srcFileName_tFileCopy_1);

		// here need check first, before mkdirs().
		if (!srcFile_tFileCopy_1.exists() || !srcFile_tFileCopy_1.isFile()) {
			String errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1 = String.format("The source File \"%s\" does not exist or is not a file.", srcFileName_tFileCopy_1);
				System.err.println(errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
				globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
		}
        String desDirName_tFileCopy_1 = "/var/hubasac/archives/assure";

		String desFileName_tFileCopy_1 =  srcFile_tFileCopy_1.getName() ;

		if (desFileName_tFileCopy_1 != null && ("").equals(desFileName_tFileCopy_1.trim())){
			desFileName_tFileCopy_1 = "NewName.temp";
		}

		java.io.File desFile_tFileCopy_1 = new java.io.File(desDirName_tFileCopy_1, desFileName_tFileCopy_1);

		if (!srcFile_tFileCopy_1.getPath().equals(desFile_tFileCopy_1.getPath())  ) {
				java.io.File parentFile_tFileCopy_1 = desFile_tFileCopy_1.getParentFile();

				if (parentFile_tFileCopy_1 != null && !parentFile_tFileCopy_1.exists()) {
					parentFile_tFileCopy_1.mkdirs();
				}           
				try {
					org.talend.FileCopy.copyFile(srcFile_tFileCopy_1.getPath(), desFile_tFileCopy_1.getPath(), true, false);
				} catch (Exception e) {
globalMap.put("tFileCopy_1_ERROR_MESSAGE",e.getMessage());
						System.err.println("tFileCopy_1 " + e.getMessage());
				}
				java.io.File isRemoved_tFileCopy_1 = new java.io.File(((String) globalMap.get("tFileList_1_CURRENT_FILEPATH")));
				if(isRemoved_tFileCopy_1.exists()) {
					String errorMessageCouldNotRemoveFile_tFileCopy_1 = String.format("tFileCopy_1 - The source file \"%s\" could not be removed from the folder because it is open or you only have read-only rights.", srcFileName_tFileCopy_1);
						System.err.println(errorMessageCouldNotRemoveFile_tFileCopy_1 + "\n");
						globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageCouldNotRemoveFile_tFileCopy_1);
				} 

		}
		globalMap.put("tFileCopy_1_DESTINATION_FILEPATH",desFile_tFileCopy_1.getPath()); 
		globalMap.put("tFileCopy_1_DESTINATION_FILENAME",desFile_tFileCopy_1.getName()); 

		globalMap.put("tFileCopy_1_SOURCE_DIRECTORY", srcFile_tFileCopy_1.getParent());
		globalMap.put("tFileCopy_1_DESTINATION_DIRECTORY", desFile_tFileCopy_1.getParent());

 


	tos_count_tFileCopy_1++;

/**
 * [tFileCopy_1 main ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_end ] stop
 */
	
	/**
	 * [tFileCopy_1 end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 

ok_Hash.put("tFileCopy_1", true);
end_Hash.put("tFileCopy_1", System.currentTimeMillis());




/**
 * [tFileCopy_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileCopy_1 finally ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "Default";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";
    
    private boolean enableLogStash;

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    protected PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final extract_assure extract_assureClass = new extract_assure();

        int exitCode = extract_assureClass.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";
	   	
        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }
        enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

    	
    	

        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }
        boolean inOSGi = routines.system.BundleUtils.inOSGi();

        if (inOSGi) {
            java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

            if (jobProperties != null && jobProperties.get("context") != null) {
                contextStr = (String)jobProperties.get("context");
            }
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = extract_assure.class.getClassLoader().getResourceAsStream("extracteur_hubasac_backup/extract_assure_1_5/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = extract_assure.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                try {
                    //defaultProps is in order to keep the original context value
                    if(context != null && context.isEmpty()) {
	                defaultProps.load(inContext);
	                context = new ContextProperties(defaultProps);
                    }
                } finally {
                    inContext.close();
                }
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
            class ContextProcessing {
                private void processContext_0() {
                        context.setContextType("assure_FieldSeparator", "id_String");
                        if(context.getStringValue("assure_FieldSeparator") == null) {
                            context.assure_FieldSeparator = null;
                        } else {
                            context.assure_FieldSeparator=(String) context.getProperty("assure_FieldSeparator");
                        }
                        context.setContextType("assure_RowSeparator", "id_String");
                        if(context.getStringValue("assure_RowSeparator") == null) {
                            context.assure_RowSeparator = null;
                        } else {
                            context.assure_RowSeparator=(String) context.getProperty("assure_RowSeparator");
                        }
                        context.setContextType("assure_Encoding", "id_String");
                        if(context.getStringValue("assure_Encoding") == null) {
                            context.assure_Encoding = null;
                        } else {
                            context.assure_Encoding=(String) context.getProperty("assure_Encoding");
                        }
                        context.setContextType("assure_Header", "id_Integer");
                        if(context.getStringValue("assure_Header") == null) {
                            context.assure_Header = null;
                        } else {
                            try{
                                context.assure_Header=routines.system.ParserUtils.parseTo_Integer (context.getProperty("assure_Header"));
                            } catch(NumberFormatException e){
                                System.err.println(String.format("Null value will be used for context parameter %s: %s", "assure_Header", e.getMessage()));
                                context.assure_Header=null;
                            }
                        }
                        context.setContextType("assure_File", "id_File");
                        if(context.getStringValue("assure_File") == null) {
                            context.assure_File = null;
                        } else {
                            context.assure_File=(String) context.getProperty("assure_File");
                        }
                        context.setContextType("pg_connexion_Port", "id_String");
                        if(context.getStringValue("pg_connexion_Port") == null) {
                            context.pg_connexion_Port = null;
                        } else {
                            context.pg_connexion_Port=(String) context.getProperty("pg_connexion_Port");
                        }
                        context.setContextType("pg_connexion_Login", "id_String");
                        if(context.getStringValue("pg_connexion_Login") == null) {
                            context.pg_connexion_Login = null;
                        } else {
                            context.pg_connexion_Login=(String) context.getProperty("pg_connexion_Login");
                        }
                        context.setContextType("pg_connexion_AdditionalParams", "id_String");
                        if(context.getStringValue("pg_connexion_AdditionalParams") == null) {
                            context.pg_connexion_AdditionalParams = null;
                        } else {
                            context.pg_connexion_AdditionalParams=(String) context.getProperty("pg_connexion_AdditionalParams");
                        }
                        context.setContextType("pg_connexion_Schema", "id_String");
                        if(context.getStringValue("pg_connexion_Schema") == null) {
                            context.pg_connexion_Schema = null;
                        } else {
                            context.pg_connexion_Schema=(String) context.getProperty("pg_connexion_Schema");
                        }
                        context.setContextType("pg_connexion_Server", "id_String");
                        if(context.getStringValue("pg_connexion_Server") == null) {
                            context.pg_connexion_Server = null;
                        } else {
                            context.pg_connexion_Server=(String) context.getProperty("pg_connexion_Server");
                        }
                        context.setContextType("pg_connexion_Password", "id_Password");
                        if(context.getStringValue("pg_connexion_Password") == null) {
                            context.pg_connexion_Password = null;
                        } else {
                            String pwd_pg_connexion_Password_value = context.getProperty("pg_connexion_Password");
                            context.pg_connexion_Password = null;
                            if(pwd_pg_connexion_Password_value!=null) {
                                if(context_param.containsKey("pg_connexion_Password")) {//no need to decrypt if it come from program argument or parent job runtime
                                    context.pg_connexion_Password = pwd_pg_connexion_Password_value;
                                } else if (!pwd_pg_connexion_Password_value.isEmpty()) {
                                    try {
                                        context.pg_connexion_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_pg_connexion_Password_value);
                                        context.put("pg_connexion_Password",context.pg_connexion_Password);
                                    } catch (java.lang.RuntimeException e) {
                                        //do nothing
                                    }
                                }
                            }
                        }
                        context.setContextType("pg_connexion_Database", "id_String");
                        if(context.getStringValue("pg_connexion_Database") == null) {
                            context.pg_connexion_Database = null;
                        } else {
                            context.pg_connexion_Database=(String) context.getProperty("pg_connexion_Database");
                        }
                        context.setContextType("assure_rejects_structure_check_FieldSeparator", "id_String");
                        if(context.getStringValue("assure_rejects_structure_check_FieldSeparator") == null) {
                            context.assure_rejects_structure_check_FieldSeparator = null;
                        } else {
                            context.assure_rejects_structure_check_FieldSeparator=(String) context.getProperty("assure_rejects_structure_check_FieldSeparator");
                        }
                        context.setContextType("assure_rejects_structure_check_File", "id_File");
                        if(context.getStringValue("assure_rejects_structure_check_File") == null) {
                            context.assure_rejects_structure_check_File = null;
                        } else {
                            context.assure_rejects_structure_check_File=(String) context.getProperty("assure_rejects_structure_check_File");
                        }
                        context.setContextType("assure_rejects_structure_check_Encoding", "id_String");
                        if(context.getStringValue("assure_rejects_structure_check_Encoding") == null) {
                            context.assure_rejects_structure_check_Encoding = null;
                        } else {
                            context.assure_rejects_structure_check_Encoding=(String) context.getProperty("assure_rejects_structure_check_Encoding");
                        }
                        context.setContextType("assure_rejects_structure_check_RowSeparator", "id_String");
                        if(context.getStringValue("assure_rejects_structure_check_RowSeparator") == null) {
                            context.assure_rejects_structure_check_RowSeparator = null;
                        } else {
                            context.assure_rejects_structure_check_RowSeparator=(String) context.getProperty("assure_rejects_structure_check_RowSeparator");
                        }
                        context.setContextType("assure_rejet_rg_RowSeparator", "id_String");
                        if(context.getStringValue("assure_rejet_rg_RowSeparator") == null) {
                            context.assure_rejet_rg_RowSeparator = null;
                        } else {
                            context.assure_rejet_rg_RowSeparator=(String) context.getProperty("assure_rejet_rg_RowSeparator");
                        }
                        context.setContextType("assure_rejet_rg_File", "id_File");
                        if(context.getStringValue("assure_rejet_rg_File") == null) {
                            context.assure_rejet_rg_File = null;
                        } else {
                            context.assure_rejet_rg_File=(String) context.getProperty("assure_rejet_rg_File");
                        }
                        context.setContextType("assure_rejet_rg_FieldSeparator", "id_String");
                        if(context.getStringValue("assure_rejet_rg_FieldSeparator") == null) {
                            context.assure_rejet_rg_FieldSeparator = null;
                        } else {
                            context.assure_rejet_rg_FieldSeparator=(String) context.getProperty("assure_rejet_rg_FieldSeparator");
                        }
                        context.setContextType("assure_rejet_rg_Encoding", "id_String");
                        if(context.getStringValue("assure_rejet_rg_Encoding") == null) {
                            context.assure_rejet_rg_Encoding = null;
                        } else {
                            context.assure_rejet_rg_Encoding=(String) context.getProperty("assure_rejet_rg_Encoding");
                        }
                        context.setContextType("assure_rejet_schema_File", "id_File");
                        if(context.getStringValue("assure_rejet_schema_File") == null) {
                            context.assure_rejet_schema_File = null;
                        } else {
                            context.assure_rejet_schema_File=(String) context.getProperty("assure_rejet_schema_File");
                        }
                        context.setContextType("assure_rejet_schema_Encoding", "id_String");
                        if(context.getStringValue("assure_rejet_schema_Encoding") == null) {
                            context.assure_rejet_schema_Encoding = null;
                        } else {
                            context.assure_rejet_schema_Encoding=(String) context.getProperty("assure_rejet_schema_Encoding");
                        }
                        context.setContextType("assure_rejet_schema_FieldSeparator", "id_String");
                        if(context.getStringValue("assure_rejet_schema_FieldSeparator") == null) {
                            context.assure_rejet_schema_FieldSeparator = null;
                        } else {
                            context.assure_rejet_schema_FieldSeparator=(String) context.getProperty("assure_rejet_schema_FieldSeparator");
                        }
                        context.setContextType("assure_rejet_schema_RowSeparator", "id_String");
                        if(context.getStringValue("assure_rejet_schema_RowSeparator") == null) {
                            context.assure_rejet_schema_RowSeparator = null;
                        } else {
                            context.assure_rejet_schema_RowSeparator=(String) context.getProperty("assure_rejet_schema_RowSeparator");
                        }
                } 
                public void processAllContext() {
                        processContext_0();
                }
            }

            new ContextProcessing().processAllContext();
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }

        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("assure_FieldSeparator")) {
                context.assure_FieldSeparator = (String) parentContextMap.get("assure_FieldSeparator");
            }if (parentContextMap.containsKey("assure_RowSeparator")) {
                context.assure_RowSeparator = (String) parentContextMap.get("assure_RowSeparator");
            }if (parentContextMap.containsKey("assure_Encoding")) {
                context.assure_Encoding = (String) parentContextMap.get("assure_Encoding");
            }if (parentContextMap.containsKey("assure_Header")) {
                context.assure_Header = (Integer) parentContextMap.get("assure_Header");
            }if (parentContextMap.containsKey("assure_File")) {
                context.assure_File = (String) parentContextMap.get("assure_File");
            }if (parentContextMap.containsKey("pg_connexion_Port")) {
                context.pg_connexion_Port = (String) parentContextMap.get("pg_connexion_Port");
            }if (parentContextMap.containsKey("pg_connexion_Login")) {
                context.pg_connexion_Login = (String) parentContextMap.get("pg_connexion_Login");
            }if (parentContextMap.containsKey("pg_connexion_AdditionalParams")) {
                context.pg_connexion_AdditionalParams = (String) parentContextMap.get("pg_connexion_AdditionalParams");
            }if (parentContextMap.containsKey("pg_connexion_Schema")) {
                context.pg_connexion_Schema = (String) parentContextMap.get("pg_connexion_Schema");
            }if (parentContextMap.containsKey("pg_connexion_Server")) {
                context.pg_connexion_Server = (String) parentContextMap.get("pg_connexion_Server");
            }if (parentContextMap.containsKey("pg_connexion_Password")) {
                context.pg_connexion_Password = (java.lang.String) parentContextMap.get("pg_connexion_Password");
            }if (parentContextMap.containsKey("pg_connexion_Database")) {
                context.pg_connexion_Database = (String) parentContextMap.get("pg_connexion_Database");
            }if (parentContextMap.containsKey("assure_rejects_structure_check_FieldSeparator")) {
                context.assure_rejects_structure_check_FieldSeparator = (String) parentContextMap.get("assure_rejects_structure_check_FieldSeparator");
            }if (parentContextMap.containsKey("assure_rejects_structure_check_File")) {
                context.assure_rejects_structure_check_File = (String) parentContextMap.get("assure_rejects_structure_check_File");
            }if (parentContextMap.containsKey("assure_rejects_structure_check_Encoding")) {
                context.assure_rejects_structure_check_Encoding = (String) parentContextMap.get("assure_rejects_structure_check_Encoding");
            }if (parentContextMap.containsKey("assure_rejects_structure_check_RowSeparator")) {
                context.assure_rejects_structure_check_RowSeparator = (String) parentContextMap.get("assure_rejects_structure_check_RowSeparator");
            }if (parentContextMap.containsKey("assure_rejet_rg_RowSeparator")) {
                context.assure_rejet_rg_RowSeparator = (String) parentContextMap.get("assure_rejet_rg_RowSeparator");
            }if (parentContextMap.containsKey("assure_rejet_rg_File")) {
                context.assure_rejet_rg_File = (String) parentContextMap.get("assure_rejet_rg_File");
            }if (parentContextMap.containsKey("assure_rejet_rg_FieldSeparator")) {
                context.assure_rejet_rg_FieldSeparator = (String) parentContextMap.get("assure_rejet_rg_FieldSeparator");
            }if (parentContextMap.containsKey("assure_rejet_rg_Encoding")) {
                context.assure_rejet_rg_Encoding = (String) parentContextMap.get("assure_rejet_rg_Encoding");
            }if (parentContextMap.containsKey("assure_rejet_schema_File")) {
                context.assure_rejet_schema_File = (String) parentContextMap.get("assure_rejet_schema_File");
            }if (parentContextMap.containsKey("assure_rejet_schema_Encoding")) {
                context.assure_rejet_schema_Encoding = (String) parentContextMap.get("assure_rejet_schema_Encoding");
            }if (parentContextMap.containsKey("assure_rejet_schema_FieldSeparator")) {
                context.assure_rejet_schema_FieldSeparator = (String) parentContextMap.get("assure_rejet_schema_FieldSeparator");
            }if (parentContextMap.containsKey("assure_rejet_schema_RowSeparator")) {
                context.assure_rejet_schema_RowSeparator = (String) parentContextMap.get("assure_rejet_schema_RowSeparator");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("pg_connexion_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();


this.globalResumeTicket = true;//to run tPreJob





this.globalResumeTicket = false;//to run others jobs

try {
errorCode = null;tFileList_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tFileList_1) {
globalMap.put("tFileList_1_SUBPROCESS_STATE", -1);

e_tFileList_1.printStackTrace();

}

this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : extract_assure");
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;


    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {


    }














    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();






        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        } else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {//for trunjob call
		    final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     619725 characters generated by Talend Open Studio for ESB 
 *     on the 9 avril 2023 à 07:34:47 CEST
 ************************************************************************************************/