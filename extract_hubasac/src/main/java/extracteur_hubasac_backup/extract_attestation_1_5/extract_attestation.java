// ============================================================================
//
// Copyright (c) 2006-2015, Talend SA
//
// Ce code source a été automatiquement généré par_Talend Open Studio for ESB
// / Soumis à la Licence Apache, Version 2.0 (la "Licence") ;
// votre utilisation de ce fichier doit respecter les termes de la Licence.
// Vous pouvez obtenir une copie de la Licence sur
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Sauf lorsqu'explicitement prévu par la loi en vigueur ou accepté par écrit, le logiciel
// distribué sous la Licence est distribué "TEL QUEL",
// SANS GARANTIE OU CONDITION D'AUCUNE SORTE, expresse ou implicite.
// Consultez la Licence pour connaître la terminologie spécifique régissant les autorisations et
// les limites prévues par la Licence.


package extracteur_hubasac_backup.extract_attestation_1_5;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 





@SuppressWarnings("unused")

/**
 * Job: extract_attestation Purpose: <br>
 * Description:  <br>
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status DEV
 */
public class extract_attestation implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(attestation_FieldSeparator != null){
				
					this.setProperty("attestation_FieldSeparator", attestation_FieldSeparator.toString());
				
			}
			
			if(attestation_RowSeparator != null){
				
					this.setProperty("attestation_RowSeparator", attestation_RowSeparator.toString());
				
			}
			
			if(attestation_Header != null){
				
					this.setProperty("attestation_Header", attestation_Header.toString());
				
			}
			
			if(attestation_Encoding != null){
				
					this.setProperty("attestation_Encoding", attestation_Encoding.toString());
				
			}
			
			if(attestation_File != null){
				
					this.setProperty("attestation_File", attestation_File.toString());
				
			}
			
			if(pg_connexion_Port != null){
				
					this.setProperty("pg_connexion_Port", pg_connexion_Port.toString());
				
			}
			
			if(pg_connexion_Login != null){
				
					this.setProperty("pg_connexion_Login", pg_connexion_Login.toString());
				
			}
			
			if(pg_connexion_AdditionalParams != null){
				
					this.setProperty("pg_connexion_AdditionalParams", pg_connexion_AdditionalParams.toString());
				
			}
			
			if(pg_connexion_Schema != null){
				
					this.setProperty("pg_connexion_Schema", pg_connexion_Schema.toString());
				
			}
			
			if(pg_connexion_Server != null){
				
					this.setProperty("pg_connexion_Server", pg_connexion_Server.toString());
				
			}
			
			if(pg_connexion_Password != null){
				
					this.setProperty("pg_connexion_Password", pg_connexion_Password.toString());
				
			}
			
			if(pg_connexion_Database != null){
				
					this.setProperty("pg_connexion_Database", pg_connexion_Database.toString());
				
			}
			
		}
		
		//if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if(NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

public String attestation_FieldSeparator;
public String getAttestation_FieldSeparator(){
	return this.attestation_FieldSeparator;
}
public String attestation_RowSeparator;
public String getAttestation_RowSeparator(){
	return this.attestation_RowSeparator;
}
public Integer attestation_Header;
public Integer getAttestation_Header(){
	return this.attestation_Header;
}
public String attestation_Encoding;
public String getAttestation_Encoding(){
	return this.attestation_Encoding;
}
		public String attestation_File;
		public String getAttestation_File(){
			return this.attestation_File;
		}
		
public String pg_connexion_Port;
public String getPg_connexion_Port(){
	return this.pg_connexion_Port;
}
public String pg_connexion_Login;
public String getPg_connexion_Login(){
	return this.pg_connexion_Login;
}
public String pg_connexion_AdditionalParams;
public String getPg_connexion_AdditionalParams(){
	return this.pg_connexion_AdditionalParams;
}
public String pg_connexion_Schema;
public String getPg_connexion_Schema(){
	return this.pg_connexion_Schema;
}
public String pg_connexion_Server;
public String getPg_connexion_Server(){
	return this.pg_connexion_Server;
}
public java.lang.String pg_connexion_Password;
public java.lang.String getPg_connexion_Password(){
	return this.pg_connexion_Password;
}
public String pg_connexion_Database;
public String getPg_connexion_Database(){
	return this.pg_connexion_Database;
}
	}
	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "1.5";
	private final String jobName = "extract_attestation";
	private final String projectName = "EXTRACTEUR_HUBASAC_BACKUP";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}
	
	public void setDataSourceReferences(List serviceReferences) throws Exception{
		
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();
		
		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils.getServices(serviceReferences,  javax.sql.DataSource.class).entrySet()) {
                    dataSources.put(entry.getKey(), entry.getValue());
                    talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}


private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				extract_attestation.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(extract_attestation.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
			}
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void tFileList_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileInputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSchemaComplianceCheck_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileCopy_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileCopy_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileList_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tFileCopy_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
	

	public static class ContextBean {
		static String evaluate(String context, String contextExpression)
				throws IOException, javax.script.ScriptException {
			boolean isExpression = contextExpression.contains("+") || contextExpression.contains("(");
			final String prefix = isExpression ? "\"" : "";
			java.util.Properties defaultProps = new java.util.Properties();
			java.io.InputStream inContext = extract_attestation.class.getClassLoader()
					.getResourceAsStream("extracteur_hubasac_backup/extract_attestation_1_5/contexts/" + context + ".properties");
			if (inContext == null) {
				inContext = extract_attestation.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + context + ".properties");
			}
			try {
			    defaultProps.load(inContext);
			} finally {
			    inContext.close();
			}
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("context.([\\w]+)");
			java.util.regex.Matcher matcher = pattern.matcher(contextExpression);

			while (matcher.find()) {
				contextExpression = contextExpression.replaceAll(matcher.group(0),
						prefix + defaultProps.getProperty(matcher.group(1)) + prefix);
			}
			if (contextExpression.startsWith("/services")) {
				contextExpression = contextExpression.replaceFirst("/services","");
            }
			return isExpression ? evaluateContextExpression(contextExpression) : contextExpression;
		}

		public static String evaluateContextExpression(String expression) throws javax.script.ScriptException {
			javax.script.ScriptEngineManager manager = new javax.script.ScriptEngineManager();
			javax.script.ScriptEngine engine = manager.getEngineByName("nashorn");
			// Add some import for Java
			expression = expression.replaceAll("System.getProperty", "java.lang.System.getProperty");
			return engine.eval(expression).toString();
		}

        public static String getContext(String context, String contextName, String jobName) throws Exception {

            String currentContext = null;
            String jobNameStripped = jobName.substring(jobName.lastIndexOf(".") + 1);

            boolean inOSGi = routines.system.BundleUtils.inOSGi();

            if (inOSGi) {
                java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobNameStripped);

                if (jobProperties != null) {
                    currentContext = (String)jobProperties.get("context");
                }
            }

            return contextName.contains("context.") ? evaluate(currentContext == null ? context : currentContext, contextName) : contextName;
        }
    }








public static class row10Struct implements routines.system.IPersistableRow<row10Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row10Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class attestation_valideStruct implements routines.system.IPersistableRow<attestation_valideStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(attestation_valideStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class rejet_attestationStruct implements routines.system.IPersistableRow<rejet_attestationStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String Error_Message;

				public String getError_Message () {
					return this.Error_Message;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.Error_Message = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.Error_Message,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",Error_Message="+Error_Message);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(rejet_attestationStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row5Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row9Struct implements routines.system.IPersistableRow<row9Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row9Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class attestation_rejects_schemaStruct implements routines.system.IPersistableRow<attestation_rejects_schemaStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(attestation_rejects_schemaStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row6Struct implements routines.system.IPersistableRow<row6Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row6Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row4Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row8Struct implements routines.system.IPersistableRow<row8Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row8Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class attestation_rejects_structureStruct implements routines.system.IPersistableRow<attestation_rejects_structureStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(attestation_rejects_structureStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row7Struct implements routines.system.IPersistableRow<row7Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row7Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String numero_police;

				public String getNumero_police () {
					return this.numero_police;
				}
				
			    public java.util.Date date_emission;

				public java.util.Date getDate_emission () {
					return this.date_emission;
				}
				
			    public java.util.Date date_effet;

				public java.util.Date getDate_effet () {
					return this.date_effet;
				}
				
			    public java.util.Date date_echeance;

				public java.util.Date getDate_echeance () {
					return this.date_echeance;
				}
				
			    public String couleur;

				public String getCouleur () {
					return this.couleur;
				}
				
			    public String statut;

				public String getStatut () {
					return this.statut;
				}
				
			    public String zone_circulation;

				public String getZone_circulation () {
					return this.zone_circulation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String remorque;

				public String getRemorque () {
					return this.remorque;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_attestation) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.numero_police = readString(dis);
					
					this.date_emission = readDate(dis);
					
					this.date_effet = readDate(dis);
					
					this.date_echeance = readDate(dis);
					
					this.couleur = readString(dis);
					
					this.statut = readString(dis);
					
					this.zone_circulation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.remorque = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.numero_police,dos);
					
					// java.util.Date
				
						writeDate(this.date_emission,dos);
					
					// java.util.Date
				
						writeDate(this.date_effet,dos);
					
					// java.util.Date
				
						writeDate(this.date_echeance,dos);
					
					// String
				
						writeString(this.couleur,dos);
					
					// String
				
						writeString(this.statut,dos);
					
					// String
				
						writeString(this.zone_circulation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.remorque,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",numero_police="+numero_police);
		sb.append(",date_emission="+String.valueOf(date_emission));
		sb.append(",date_effet="+String.valueOf(date_effet));
		sb.append(",date_echeance="+String.valueOf(date_echeance));
		sb.append(",couleur="+couleur);
		sb.append(",statut="+statut);
		sb.append(",zone_circulation="+zone_circulation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",remorque="+remorque);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tFileList_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileList_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row1Struct row1 = new row1Struct();
row3Struct row3 = new row3Struct();
row3Struct row5 = row3;
attestation_valideStruct attestation_valide = new attestation_valideStruct();
rejet_attestationStruct rejet_attestation = new rejet_attestationStruct();
row10Struct row10 = new row10Struct();
row4Struct row4 = new row4Struct();
row4Struct row6 = row4;
attestation_rejects_schemaStruct attestation_rejects_schema = new attestation_rejects_schemaStruct();
row9Struct row9 = new row9Struct();
row2Struct row2 = new row2Struct();
row2Struct row7 = row2;
attestation_rejects_structureStruct attestation_rejects_structure = new attestation_rejects_structureStruct();
row8Struct row8 = new row8Struct();



	
	/**
	 * [tFileList_1 begin ] start
	 */

				
			int NB_ITERATE_tFileInputDelimited_1 = 0; //for statistics
			

	
		
		ok_Hash.put("tFileList_1", false);
		start_Hash.put("tFileList_1", System.currentTimeMillis());
		
	
	currentComponent="tFileList_1";

	
		int tos_count_tFileList_1 = 0;
		
	
 
     
    
  String directory_tFileList_1 = "/var/hubasac/inputfile";
  final java.util.List<String> maskList_tFileList_1 = new java.util.ArrayList<String>();
  final java.util.List<java.util.regex.Pattern> patternList_tFileList_1 = new java.util.ArrayList<java.util.regex.Pattern>(); 
    maskList_tFileList_1.add("attestation*.txt");  
  for (final String filemask_tFileList_1 : maskList_tFileList_1) {
	String filemask_compile_tFileList_1 = filemask_tFileList_1;
	
		filemask_compile_tFileList_1 = org.apache.oro.text.GlobCompiler.globToPerl5(filemask_tFileList_1.toCharArray(), org.apache.oro.text.GlobCompiler.DEFAULT_MASK);
	
		java.util.regex.Pattern fileNamePattern_tFileList_1 = java.util.regex.Pattern.compile(filemask_compile_tFileList_1, java.util.regex.Pattern.CASE_INSENSITIVE);
	
	patternList_tFileList_1.add(fileNamePattern_tFileList_1);
  }
  int NB_FILEtFileList_1 = 0;

  final boolean case_sensitive_tFileList_1 = false;
	
	
	
    final java.util.List<java.io.File> list_tFileList_1 = new java.util.ArrayList<java.io.File>();
    final java.util.Set<String> filePath_tFileList_1 = new java.util.HashSet<String>();
	java.io.File file_tFileList_1 = new java.io.File(directory_tFileList_1);
    
		file_tFileList_1.listFiles(new java.io.FilenameFilter() {
			public boolean accept(java.io.File dir, String name) {
				java.io.File file = new java.io.File(dir, name);
				
	                if (!file.isDirectory()) {
						
    	String fileName_tFileList_1 = file.getName();
		for (final java.util.regex.Pattern fileNamePattern_tFileList_1 : patternList_tFileList_1) {
          	if (fileNamePattern_tFileList_1.matcher(fileName_tFileList_1).matches()){
					if(!filePath_tFileList_1.contains(file.getAbsolutePath())) {
			          list_tFileList_1.add(file);
			          filePath_tFileList_1.add(file.getAbsolutePath());
			        }
			}
		}
	                	return true;
	                } else {
	                  file.listFiles(this);
	                }
				
				return false;
			}
		}
		); 
      java.util.Collections.sort(list_tFileList_1);
    
    for (int i_tFileList_1 = 0; i_tFileList_1 < list_tFileList_1.size(); i_tFileList_1++){
      java.io.File files_tFileList_1 = list_tFileList_1.get(i_tFileList_1);
      String fileName_tFileList_1 = files_tFileList_1.getName();
      
      String currentFileName_tFileList_1 = files_tFileList_1.getName(); 
      String currentFilePath_tFileList_1 = files_tFileList_1.getAbsolutePath();
      String currentFileDirectory_tFileList_1 = files_tFileList_1.getParent();
      String currentFileExtension_tFileList_1 = null;
      
      if (files_tFileList_1.getName().contains(".") && files_tFileList_1.isFile()){
        currentFileExtension_tFileList_1 = files_tFileList_1.getName().substring(files_tFileList_1.getName().lastIndexOf(".") + 1);
      } else{
        currentFileExtension_tFileList_1 = "";
      }
      
      NB_FILEtFileList_1 ++;
      globalMap.put("tFileList_1_CURRENT_FILE", currentFileName_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEPATH", currentFilePath_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEDIRECTORY", currentFileDirectory_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEEXTENSION", currentFileExtension_tFileList_1);
      globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
      
 



/**
 * [tFileList_1 begin ] stop
 */
	
	/**
	 * [tFileList_1 main ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 


	tos_count_tFileList_1++;

/**
 * [tFileList_1 main ] stop
 */
	
	/**
	 * [tFileList_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_begin ] stop
 */
	NB_ITERATE_tFileInputDelimited_1++;
	
	
					if(execStat){				
	       				runStat.updateStatOnConnection("attestation_valide", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row10", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row6", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row4", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row3", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row2", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("OnComponentOk1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("rejet_attestation", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row9", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row5", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row7", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row8", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("attestation_rejects_structure", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("attestation_rejects_schema", 3, 0);
					}           			
				
				if(execStat){
					runStat.updateStatOnConnection("iterate1", 1, "exec" + NB_ITERATE_tFileInputDelimited_1);
					//Thread.sleep(1000);
				}				
			





	
	/**
	 * [tDBOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_1", false);
		start_Hash.put("tDBOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"attestation_valide");
					}
				
		int tos_count_tDBOutput_1 = 0;
		





String dbschema_tDBOutput_1 = null;
	dbschema_tDBOutput_1 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_1 = null;
if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
	tableName_tDBOutput_1 = ("attestation");
} else {
	tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "\".\"" + ("attestation");
}

        int updateKeyCount_tDBOutput_1 = 1;
        if(updateKeyCount_tDBOutput_1 < 1) {
            throw new RuntimeException("For update, Schema must have a key");
        } else if (updateKeyCount_tDBOutput_1 == 16 && true) {
                    System.err.println("For update, every Schema column can not be a key");
        }

int nb_line_tDBOutput_1 = 0;
int nb_line_update_tDBOutput_1 = 0;
int nb_line_inserted_tDBOutput_1 = 0;
int nb_line_deleted_tDBOutput_1 = 0;
int nb_line_rejected_tDBOutput_1 = 0;

int deletedCount_tDBOutput_1=0;
int updatedCount_tDBOutput_1=0;
int insertedCount_tDBOutput_1=0;
int rowsToCommitCount_tDBOutput_1=0;
int rejectedCount_tDBOutput_1=0;

boolean whetherReject_tDBOutput_1 = false;

java.sql.Connection conn_tDBOutput_1 = null;
String dbUser_tDBOutput_1 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_1 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_1 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_1 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;

    conn_tDBOutput_1 = java.sql.DriverManager.getConnection(url_tDBOutput_1,dbUser_tDBOutput_1,dbPwd_tDBOutput_1);
	
	resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
        conn_tDBOutput_1.setAutoCommit(false);
        int commitEvery_tDBOutput_1 = 10000;
        int commitCounter_tDBOutput_1 = 0;



int count_tDBOutput_1=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_1 = conn_tDBOutput_1.getMetaData();
                                boolean whetherExist_tDBOutput_1 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_1 = dbMetaData_tDBOutput_1.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_1 = "public";
                                    if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_1 = stmtSchema_tDBOutput_1.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_1.next()){
                                                defaultSchema_tDBOutput_1 = rsSchema_tDBOutput_1.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_1.next()) {
                                        String table_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_NAME");
                                        String schema_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_1.equals(("attestation"))
                                            && (schema_tDBOutput_1.equals(dbschema_tDBOutput_1) || ((dbschema_tDBOutput_1 ==null || dbschema_tDBOutput_1.trim().length() ==0) && defaultSchema_tDBOutput_1.equals(schema_tDBOutput_1)))) {
                                            whetherExist_tDBOutput_1 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_1) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1.createStatement()) {
                                        stmtCreate_tDBOutput_1.execute("CREATE TABLE \"" + tableName_tDBOutput_1 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"numero_police\" VARCHAR(128)  ,\"date_emission\" TIMESTAMP ,\"date_effet\" TIMESTAMP ,\"date_echeance\" TIMESTAMP ,\"couleur\" VARCHAR(128)   not null ,\"statut\" VARCHAR(128)   not null ,\"zone_circulation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"remorque\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"c_status\" INT4 default 1  not null ,\"c_date_mis_a_jour\" TIMESTAMP ,\"c_date_transfer\" TIMESTAMP ,\"commentaires\" VARCHAR(128)  ,primary key(\"numero_attestation\"))");
                                    }
                                }
	    java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement("SELECT COUNT(1) FROM \"" + tableName_tDBOutput_1 + "\" WHERE \"numero_attestation\" = ?");
	    resourceMap.put("pstmt_tDBOutput_1", pstmt_tDBOutput_1);
	    String insert_tDBOutput_1 = "INSERT INTO \"" + tableName_tDBOutput_1 + "\" (\"numero_attestation\",\"numero_police\",\"date_emission\",\"date_effet\",\"date_echeance\",\"couleur\",\"statut\",\"zone_circulation\",\"immatriculation\",\"remorque\",\"code_assure\",\"code_assureur\",\"c_status\",\"c_date_mis_a_jour\",\"c_date_transfer\",\"commentaires\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    java.sql.PreparedStatement pstmtInsert_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(insert_tDBOutput_1);
	    resourceMap.put("pstmtInsert_tDBOutput_1", pstmtInsert_tDBOutput_1);
	    String update_tDBOutput_1 = "UPDATE \"" + tableName_tDBOutput_1 + "\" SET \"numero_police\" = ?,\"date_emission\" = ?,\"date_effet\" = ?,\"date_echeance\" = ?,\"couleur\" = ?,\"statut\" = ?,\"zone_circulation\" = ?,\"immatriculation\" = ?,\"remorque\" = ?,\"code_assure\" = ?,\"code_assureur\" = ?,\"c_status\" = ?,\"c_date_mis_a_jour\" = ?,\"c_date_transfer\" = ?,\"commentaires\" = ? WHERE \"numero_attestation\" = ?";
	    java.sql.PreparedStatement pstmtUpdate_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(update_tDBOutput_1);
	    resourceMap.put("pstmtUpdate_tDBOutput_1", pstmtUpdate_tDBOutput_1);
	    

 



/**
 * [tDBOutput_1 begin ] stop
 */





	
	/**
	 * [tFileOutputDelimited_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_3", false);
		start_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row10");
					}
				
		int tos_count_tFileOutputDelimited_3 = 0;
		

String fileName_tFileOutputDelimited_3 = "";
    fileName_tFileOutputDelimited_3 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/attestation_rejet_rg.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_3 = null;
    String extension_tFileOutputDelimited_3 = null;
    String directory_tFileOutputDelimited_3 = null;
    if((fileName_tFileOutputDelimited_3.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") < fileName_tFileOutputDelimited_3.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_3.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(0, fileName_tFileOutputDelimited_3.lastIndexOf("."));
            extension_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3.substring(fileName_tFileOutputDelimited_3.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_3 = fileName_tFileOutputDelimited_3;
            extension_tFileOutputDelimited_3 = "";
        }
        directory_tFileOutputDelimited_3 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_3 = true;
    java.io.File filetFileOutputDelimited_3 = new java.io.File(fileName_tFileOutputDelimited_3);
    globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
        if(filetFileOutputDelimited_3.exists()){
            isFileGenerated_tFileOutputDelimited_3 = false;
        }
            int nb_line_tFileOutputDelimited_3 = 0;
            int splitedFileNo_tFileOutputDelimited_3 = 0;
            int currentRow_tFileOutputDelimited_3 = 0;

            final String OUT_DELIM_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_3:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_3 = /** Start field tFileOutputDelimited_3:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_3:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_3 != null && directory_tFileOutputDelimited_3.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_3 = new java.io.File(directory_tFileOutputDelimited_3);
                        if(!dir_tFileOutputDelimited_3.exists()) {
                            dir_tFileOutputDelimited_3.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_3 = null;

                        outtFileOutputDelimited_3 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_3, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_3.length()==0){
                                        outtFileOutputDelimited_3.write("numero_attestation");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("numero_police");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_emission");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_effet");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_echeance");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("couleur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("statut");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("zone_circulation");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("immatriculation");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("remorque");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("code_assure");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("code_assureur");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_extraction");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("date_depot");
                                            outtFileOutputDelimited_3.write(OUT_DELIM_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.write("Error_Message");
                                        outtFileOutputDelimited_3.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);
                                        outtFileOutputDelimited_3.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_3", outtFileOutputDelimited_3);
resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

 



/**
 * [tFileOutputDelimited_3 begin ] stop
 */



	
	/**
	 * [tDBOutput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_2", false);
		start_Hash.put("tDBOutput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"rejet_attestation");
					}
				
		int tos_count_tDBOutput_2 = 0;
		





String dbschema_tDBOutput_2 = null;
	dbschema_tDBOutput_2 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_2 = null;
if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
	tableName_tDBOutput_2 = ("attestation_rejects_RG_check");
} else {
	tableName_tDBOutput_2 = dbschema_tDBOutput_2 + "\".\"" + ("attestation_rejects_RG_check");
}


int nb_line_tDBOutput_2 = 0;
int nb_line_update_tDBOutput_2 = 0;
int nb_line_inserted_tDBOutput_2 = 0;
int nb_line_deleted_tDBOutput_2 = 0;
int nb_line_rejected_tDBOutput_2 = 0;

int deletedCount_tDBOutput_2=0;
int updatedCount_tDBOutput_2=0;
int insertedCount_tDBOutput_2=0;
int rowsToCommitCount_tDBOutput_2=0;
int rejectedCount_tDBOutput_2=0;

boolean whetherReject_tDBOutput_2 = false;

java.sql.Connection conn_tDBOutput_2 = null;
String dbUser_tDBOutput_2 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_2 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_2 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_2 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;

    conn_tDBOutput_2 = java.sql.DriverManager.getConnection(url_tDBOutput_2,dbUser_tDBOutput_2,dbPwd_tDBOutput_2);
	
	resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);
        conn_tDBOutput_2.setAutoCommit(false);
        int commitEvery_tDBOutput_2 = 10000;
        int commitCounter_tDBOutput_2 = 0;


   int batchSize_tDBOutput_2 = 10000;
   int batchSizeCounter_tDBOutput_2=0;

int count_tDBOutput_2=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_2 = conn_tDBOutput_2.getMetaData();
                                boolean whetherExist_tDBOutput_2 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_2 = dbMetaData_tDBOutput_2.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_2 = "public";
                                    if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_2 = conn_tDBOutput_2.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_2 = stmtSchema_tDBOutput_2.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_2.next()){
                                                defaultSchema_tDBOutput_2 = rsSchema_tDBOutput_2.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_2.next()) {
                                        String table_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_NAME");
                                        String schema_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_2.equals(("attestation_rejects_RG_check"))
                                            && (schema_tDBOutput_2.equals(dbschema_tDBOutput_2) || ((dbschema_tDBOutput_2 ==null || dbschema_tDBOutput_2.trim().length() ==0) && defaultSchema_tDBOutput_2.equals(schema_tDBOutput_2)))) {
                                            whetherExist_tDBOutput_2 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_2) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_2 = conn_tDBOutput_2.createStatement()) {
                                        stmtCreate_tDBOutput_2.execute("CREATE TABLE \"" + tableName_tDBOutput_2 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"numero_police\" VARCHAR(128)  ,\"date_emission\" TIMESTAMP ,\"date_effet\" TIMESTAMP ,\"date_echeance\" TIMESTAMP ,\"couleur\" VARCHAR(128)   not null ,\"statut\" VARCHAR(128)   not null ,\"zone_circulation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"remorque\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"Error_Message\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_2 = "INSERT INTO \"" + tableName_tDBOutput_2 + "\" (\"numero_attestation\",\"numero_police\",\"date_emission\",\"date_effet\",\"date_echeance\",\"couleur\",\"statut\",\"zone_circulation\",\"immatriculation\",\"remorque\",\"code_assure\",\"code_assureur\",\"date_extraction\",\"date_depot\",\"Error_Message\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2.prepareStatement(insert_tDBOutput_2);
	    resourceMap.put("pstmt_tDBOutput_2", pstmt_tDBOutput_2);
	    

 



/**
 * [tDBOutput_2 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row5");
					}
				
		int tos_count_tMap_1 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
	String Num_police;
	String Date_emission;
	String Date_effet;
	String Date_echeance;
	String Couleur;
	String ZoneCirculation;
	String Statut;
	String CouleurVal;
	String ZCirculationVal;
	String StatutVal;
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
attestation_valideStruct attestation_valide_tmp = new attestation_valideStruct();
rejet_attestationStruct rejet_attestation_tmp = new rejet_attestationStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tLogRow_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_2", false);
		start_Hash.put("tLogRow_2", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row3");
					}
				
		int tos_count_tLogRow_2 = 0;
		

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_tLogRow_2 = "|";
		java.io.PrintStream consoleOut_tLogRow_2 = null;	

 		StringBuilder strBuffer_tLogRow_2 = null;
		int nb_line_tLogRow_2 = 0;
///////////////////////    			



 



/**
 * [tLogRow_2 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_2", false);
		start_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row9");
					}
				
		int tos_count_tFileOutputDelimited_2 = 0;
		

String fileName_tFileOutputDelimited_2 = "";
    fileName_tFileOutputDelimited_2 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/attestation_rejet_schema.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_2 = null;
    String extension_tFileOutputDelimited_2 = null;
    String directory_tFileOutputDelimited_2 = null;
    if((fileName_tFileOutputDelimited_2.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") < fileName_tFileOutputDelimited_2.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        }
        directory_tFileOutputDelimited_2 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_2 = true;
    java.io.File filetFileOutputDelimited_2 = new java.io.File(fileName_tFileOutputDelimited_2);
    globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
        if(filetFileOutputDelimited_2.exists()){
            isFileGenerated_tFileOutputDelimited_2 = false;
        }
            int nb_line_tFileOutputDelimited_2 = 0;
            int splitedFileNo_tFileOutputDelimited_2 = 0;
            int currentRow_tFileOutputDelimited_2 = 0;

            final String OUT_DELIM_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_2:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_2:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_2 != null && directory_tFileOutputDelimited_2.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_2 = new java.io.File(directory_tFileOutputDelimited_2);
                        if(!dir_tFileOutputDelimited_2.exists()) {
                            dir_tFileOutputDelimited_2.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_2 = null;

                        outtFileOutputDelimited_2 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_2, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_2.length()==0){
                                        outtFileOutputDelimited_2.write("numero_attestation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("numero_police");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_emission");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_effet");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_echeance");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("couleur");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("statut");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("zone_circulation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("immatriculation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("remorque");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_assure");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_assureur");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_extraction");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_depot");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorCode");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorMessage");
                                        outtFileOutputDelimited_2.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_2", outtFileOutputDelimited_2);
resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

 



/**
 * [tFileOutputDelimited_2 begin ] stop
 */



	
	/**
	 * [tDBOutput_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_3", false);
		start_Hash.put("tDBOutput_3", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"attestation_rejects_schema");
					}
				
		int tos_count_tDBOutput_3 = 0;
		





String dbschema_tDBOutput_3 = null;
	dbschema_tDBOutput_3 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_3 = null;
if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
	tableName_tDBOutput_3 = ("attestation_rejects_schema_check");
} else {
	tableName_tDBOutput_3 = dbschema_tDBOutput_3 + "\".\"" + ("attestation_rejects_schema_check");
}


int nb_line_tDBOutput_3 = 0;
int nb_line_update_tDBOutput_3 = 0;
int nb_line_inserted_tDBOutput_3 = 0;
int nb_line_deleted_tDBOutput_3 = 0;
int nb_line_rejected_tDBOutput_3 = 0;

int deletedCount_tDBOutput_3=0;
int updatedCount_tDBOutput_3=0;
int insertedCount_tDBOutput_3=0;
int rowsToCommitCount_tDBOutput_3=0;
int rejectedCount_tDBOutput_3=0;

boolean whetherReject_tDBOutput_3 = false;

java.sql.Connection conn_tDBOutput_3 = null;
String dbUser_tDBOutput_3 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_3 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_3 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_3 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;

    conn_tDBOutput_3 = java.sql.DriverManager.getConnection(url_tDBOutput_3,dbUser_tDBOutput_3,dbPwd_tDBOutput_3);
	
	resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);
        conn_tDBOutput_3.setAutoCommit(false);
        int commitEvery_tDBOutput_3 = 10000;
        int commitCounter_tDBOutput_3 = 0;


   int batchSize_tDBOutput_3 = 10000;
   int batchSizeCounter_tDBOutput_3=0;

int count_tDBOutput_3=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_3 = conn_tDBOutput_3.getMetaData();
                                boolean whetherExist_tDBOutput_3 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_3 = dbMetaData_tDBOutput_3.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_3 = "public";
                                    if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_3 = conn_tDBOutput_3.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_3 = stmtSchema_tDBOutput_3.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_3.next()){
                                                defaultSchema_tDBOutput_3 = rsSchema_tDBOutput_3.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_3.next()) {
                                        String table_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_NAME");
                                        String schema_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_3.equals(("attestation_rejects_schema_check"))
                                            && (schema_tDBOutput_3.equals(dbschema_tDBOutput_3) || ((dbschema_tDBOutput_3 ==null || dbschema_tDBOutput_3.trim().length() ==0) && defaultSchema_tDBOutput_3.equals(schema_tDBOutput_3)))) {
                                            whetherExist_tDBOutput_3 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_3) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_3 = conn_tDBOutput_3.createStatement()) {
                                        stmtCreate_tDBOutput_3.execute("CREATE TABLE \"" + tableName_tDBOutput_3 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"numero_police\" VARCHAR(128)  ,\"date_emission\" TIMESTAMP ,\"date_effet\" TIMESTAMP ,\"date_echeance\" TIMESTAMP ,\"couleur\" VARCHAR(128)   not null ,\"statut\" VARCHAR(128)   not null ,\"zone_circulation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"remorque\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_3 = "INSERT INTO \"" + tableName_tDBOutput_3 + "\" (\"numero_attestation\",\"numero_police\",\"date_emission\",\"date_effet\",\"date_echeance\",\"couleur\",\"statut\",\"zone_circulation\",\"immatriculation\",\"remorque\",\"code_assure\",\"code_assureur\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3.prepareStatement(insert_tDBOutput_3);
	    resourceMap.put("pstmt_tDBOutput_3", pstmt_tDBOutput_3);
	    

 



/**
 * [tDBOutput_3 begin ] stop
 */



	
	/**
	 * [tMap_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_2", false);
		start_Hash.put("tMap_2", System.currentTimeMillis());
		
	
	currentComponent="tMap_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row6");
					}
				
		int tos_count_tMap_2 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_2__Struct  {
}
Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
attestation_rejects_schemaStruct attestation_rejects_schema_tmp = new attestation_rejects_schemaStruct();
// ###############################

        
        



        









 



/**
 * [tMap_2 begin ] stop
 */



	
	/**
	 * [tLogRow_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_3", false);
		start_Hash.put("tLogRow_3", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row4");
					}
				
		int tos_count_tLogRow_3 = 0;
		

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_tLogRow_3 = "|";
		java.io.PrintStream consoleOut_tLogRow_3 = null;	

 		StringBuilder strBuffer_tLogRow_3 = null;
		int nb_line_tLogRow_3 = 0;
///////////////////////    			



 



/**
 * [tLogRow_3 begin ] stop
 */



	
	/**
	 * [tSchemaComplianceCheck_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSchemaComplianceCheck_1", false);
		start_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());
		
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row1");
					}
				
		int tos_count_tSchemaComplianceCheck_1 = 0;
		

    class RowSetValueUtil_tSchemaComplianceCheck_1 {

        boolean ifPassedThrough = true;
        int errorCodeThrough = 0;
        String errorMessageThrough = "";
        int resultErrorCodeThrough = 0;
        String resultErrorMessageThrough = "";
        String tmpContentThrough = null;

        boolean ifPassed = true;
        int errorCode = 0;
        String errorMessage = "";

        void handleBigdecimalPrecision(String data, int iPrecision, int maxLength){
            //number of digits before the decimal point(ignoring frontend zeroes)
            int len1 = 0;
            int len2 = 0;
            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
            if(data.startsWith("-")){
                data = data.substring(1);
            }
            data = org.apache.commons.lang.StringUtils.stripStart(data, "0");

            if(data.indexOf(".") >= 0){
                len1 = data.indexOf(".");
                data = org.apache.commons.lang.StringUtils.stripEnd(data, "0");
                len2 = data.length() - (len1 + 1);
            }else{
                len1 = data.length();
            }

            if (iPrecision < len2) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|precision Non-matches";
            } else if (maxLength < len1 + iPrecision) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|invalid Length setting is unsuitable for Precision";
            }
        }

        int handleErrorCode(int errorCode, int resultErrorCode){
            if (errorCode > 0) {
                if (resultErrorCode > 0) {
                    resultErrorCode = 16;
                } else {
                    resultErrorCode = errorCode;
                }
            }
            return resultErrorCode;
        }

        String handleErrorMessage(String errorMessage, String resultErrorMessage, String columnLabel){
            if (errorMessage.length() > 0) {
                if (resultErrorMessage.length() > 0) {
                    resultErrorMessage += ";"+ errorMessage.replaceFirst("\\|", columnLabel);
                } else {
                    resultErrorMessage = errorMessage.replaceFirst("\\|", columnLabel);
                }
            }
            return resultErrorMessage;
        }

        void reset(){
            ifPassedThrough = true;
            errorCodeThrough = 0;
            errorMessageThrough = "";
            resultErrorCodeThrough = 0;
            resultErrorMessageThrough = "";
            tmpContentThrough = null;

            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
        }

        void setRowValue_0(row1Struct row1) {
    // validate nullable (empty as null)
    if ((row1.numero_attestation == null) || ("".equals(row1.numero_attestation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.numero_attestation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.numero_attestation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.numero_attestation != null
    ) {
        if (row1.numero_attestation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"numero_attestation:");
            errorMessageThrough = "";    try {
        if(
        row1.numero_police != null
        && (!"".equals(row1.numero_police))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.numero_police);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.numero_police != null
    && (!"".equals(row1.numero_police))
    ) {
        if (row1.numero_police.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"numero_police:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_emission:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_effet:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"date_echeance:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.couleur == null) || ("".equals(row1.couleur))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.couleur != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.couleur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.couleur != null
    ) {
        if (row1.couleur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"couleur:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.statut == null) || ("".equals(row1.statut))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.statut != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.statut);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.statut != null
    ) {
        if (row1.statut.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"statut:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.zone_circulation == null) || ("".equals(row1.zone_circulation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.zone_circulation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.zone_circulation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.zone_circulation != null
    ) {
        if (row1.zone_circulation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"zone_circulation:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.immatriculation == null) || ("".equals(row1.immatriculation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.immatriculation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.immatriculation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.immatriculation != null
    ) {
        if (row1.immatriculation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"immatriculation:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.remorque == null) || ("".equals(row1.remorque))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.remorque != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.remorque);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.remorque != null
    ) {
        if (row1.remorque.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"remorque:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.code_assure == null) || ("".equals(row1.code_assure))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assure != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assure);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assure != null
    ) {
        if (row1.code_assure.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assure:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.code_assureur == null) || ("".equals(row1.code_assureur))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assureur != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assureur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assureur != null
    ) {
        if (row1.code_assureur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assureur:");
            errorMessageThrough = "";
        }
    }
    RowSetValueUtil_tSchemaComplianceCheck_1 rsvUtil_tSchemaComplianceCheck_1 = new RowSetValueUtil_tSchemaComplianceCheck_1();

 



/**
 * [tSchemaComplianceCheck_1 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_1", false);
		start_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row8");
					}
				
		int tos_count_tFileOutputDelimited_1 = 0;
		

String fileName_tFileOutputDelimited_1 = "";
    fileName_tFileOutputDelimited_1 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/attestation_rejet1_structure.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_1 = null;
    String extension_tFileOutputDelimited_1 = null;
    String directory_tFileOutputDelimited_1 = null;
    if((fileName_tFileOutputDelimited_1.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") < fileName_tFileOutputDelimited_1.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        }
        directory_tFileOutputDelimited_1 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_1 = true;
    java.io.File filetFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
    globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
        if(filetFileOutputDelimited_1.exists()){
            isFileGenerated_tFileOutputDelimited_1 = false;
        }
                String[] headColutFileOutputDelimited_1=new String[16];
            class CSVBasicSet_tFileOutputDelimited_1{
                private char field_Delim;
                private char row_Delim;
                private char escape;
                private char textEnclosure;
                private boolean useCRLFRecordDelimiter;

                public boolean isUseCRLFRecordDelimiter() {
                    return useCRLFRecordDelimiter;
                }

                public void setFieldSeparator(String fieldSep) throws IllegalArgumentException{
                    char field_Delim_tFileOutputDelimited_1[] = null;

                    //support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'.
                    if (fieldSep.length() > 0 ){
                        field_Delim_tFileOutputDelimited_1 = fieldSep.toCharArray();
                    }else {
                        throw new IllegalArgumentException("Field Separator must be assigned a char.");
                    }
                    this.field_Delim = field_Delim_tFileOutputDelimited_1[0];
                }

                public char getFieldDelim(){
                    if(this.field_Delim==0){
                        setFieldSeparator(";");
                    }
                    return this.field_Delim;
                }

                public void setRowSeparator(String rowSep){
                    if("\r\n".equals(rowSep)) {
                        useCRLFRecordDelimiter = true;
                        return;
                    }
                    char row_DelimtFileOutputDelimited_1[] = null;

                    //support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'.
                    if (rowSep.length() > 0 ){
                        row_DelimtFileOutputDelimited_1 = rowSep.toCharArray();
                    }else {
                        throw new IllegalArgumentException("Row Separator must be assigned a char.");
                    }
                    this.row_Delim = row_DelimtFileOutputDelimited_1[0];
                }

                public char getRowDelim(){
                    if(this.row_Delim==0){
                        setRowSeparator("\n");
                    }
                    return this.row_Delim;
                }

                public void setEscapeAndTextEnclosure(String strEscape, String strTextEnclosure) throws IllegalArgumentException{
                    if(strEscape.length() <= 0 ){
                        throw new IllegalArgumentException("Escape Char must be assigned a char.");
                    }

                    if ("".equals(strTextEnclosure)) strTextEnclosure = "\0";
                    char textEnclosure_tFileOutputDelimited_1[] = null;

                    if(strTextEnclosure.length() > 0 ){
                        textEnclosure_tFileOutputDelimited_1 = strTextEnclosure.toCharArray();
                    }else {
                        throw new IllegalArgumentException("Text Enclosure must be assigned a char.");
                    }

                    this.textEnclosure = textEnclosure_tFileOutputDelimited_1[0];

                    if(("\\").equals(strEscape)){
                        this.escape = '\\';
                    }else if(strEscape.equals(strTextEnclosure)){
                        this.escape = this.textEnclosure;
                    } else {
                        //the default escape mode is double escape
                        this.escape = this.textEnclosure;
                    }


                }

                public char getEscapeChar(){
                    return (char)this.escape;
                }

                public char getTextEnclosure(){
                    return this.textEnclosure;
                }
            }

            int nb_line_tFileOutputDelimited_1 = 0;
            int splitedFileNo_tFileOutputDelimited_1 =0;
            int currentRow_tFileOutputDelimited_1 = 0;


            CSVBasicSet_tFileOutputDelimited_1 csvSettings_tFileOutputDelimited_1 = new CSVBasicSet_tFileOutputDelimited_1();
            csvSettings_tFileOutputDelimited_1.setFieldSeparator(";");
            csvSettings_tFileOutputDelimited_1.setRowSeparator("\n");
            csvSettings_tFileOutputDelimited_1.setEscapeAndTextEnclosure("\"","\"");
                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_1 != null && directory_tFileOutputDelimited_1.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_1 = new java.io.File(directory_tFileOutputDelimited_1);
                        if(!dir_tFileOutputDelimited_1.exists()) {
                            dir_tFileOutputDelimited_1.mkdirs();
                        }
                    }
                            com.talend.csv.CSVWriter CsvWritertFileOutputDelimited_1 = null;

                            CsvWritertFileOutputDelimited_1 = new com.talend.csv.CSVWriter(new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                            new java.io.FileOutputStream(fileName_tFileOutputDelimited_1, true), "ISO-8859-15")));
                            CsvWritertFileOutputDelimited_1.setSeparator(csvSettings_tFileOutputDelimited_1.getFieldDelim());
                    if(!csvSettings_tFileOutputDelimited_1.isUseCRLFRecordDelimiter() && csvSettings_tFileOutputDelimited_1.getRowDelim()!='\r' && csvSettings_tFileOutputDelimited_1.getRowDelim()!='\n') {
                        CsvWritertFileOutputDelimited_1.setLineEnd(""+csvSettings_tFileOutputDelimited_1.getRowDelim());
                    }
                        if(filetFileOutputDelimited_1.length()==0){
                                    headColutFileOutputDelimited_1[0]="numero_attestation";
                                    headColutFileOutputDelimited_1[1]="numero_police";
                                    headColutFileOutputDelimited_1[2]="date_emission";
                                    headColutFileOutputDelimited_1[3]="date_effet";
                                    headColutFileOutputDelimited_1[4]="date_echeance";
                                    headColutFileOutputDelimited_1[5]="couleur";
                                    headColutFileOutputDelimited_1[6]="statut";
                                    headColutFileOutputDelimited_1[7]="zone_circulation";
                                    headColutFileOutputDelimited_1[8]="immatriculation";
                                    headColutFileOutputDelimited_1[9]="remorque";
                                    headColutFileOutputDelimited_1[10]="code_assure";
                                    headColutFileOutputDelimited_1[11]="code_assureur";
                                    headColutFileOutputDelimited_1[12]="date_extraction";
                                    headColutFileOutputDelimited_1[13]="date_depot";
                                    headColutFileOutputDelimited_1[14]="errorCode";
                                    headColutFileOutputDelimited_1[15]="errorMessage";
                            CsvWritertFileOutputDelimited_1.writeNext(headColutFileOutputDelimited_1);
                            CsvWritertFileOutputDelimited_1.flush();
                        }
                CsvWritertFileOutputDelimited_1.setEscapeChar(csvSettings_tFileOutputDelimited_1.getEscapeChar());
                CsvWritertFileOutputDelimited_1.setQuoteChar(csvSettings_tFileOutputDelimited_1.getTextEnclosure());
                CsvWritertFileOutputDelimited_1.setQuoteStatus(com.talend.csv.CSVWriter.QuoteStatus.FORCE);



    resourceMap.put("CsvWriter_tFileOutputDelimited_1", CsvWritertFileOutputDelimited_1);
resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

 



/**
 * [tFileOutputDelimited_1 begin ] stop
 */



	
	/**
	 * [tDBOutput_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_4", false);
		start_Hash.put("tDBOutput_4", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"attestation_rejects_structure");
					}
				
		int tos_count_tDBOutput_4 = 0;
		





String dbschema_tDBOutput_4 = null;
	dbschema_tDBOutput_4 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_4 = null;
if(dbschema_tDBOutput_4 == null || dbschema_tDBOutput_4.trim().length() == 0) {
	tableName_tDBOutput_4 = ("attestation_rejects_structure_check");
} else {
	tableName_tDBOutput_4 = dbschema_tDBOutput_4 + "\".\"" + ("attestation_rejects_structure_check");
}


int nb_line_tDBOutput_4 = 0;
int nb_line_update_tDBOutput_4 = 0;
int nb_line_inserted_tDBOutput_4 = 0;
int nb_line_deleted_tDBOutput_4 = 0;
int nb_line_rejected_tDBOutput_4 = 0;

int deletedCount_tDBOutput_4=0;
int updatedCount_tDBOutput_4=0;
int insertedCount_tDBOutput_4=0;
int rowsToCommitCount_tDBOutput_4=0;
int rejectedCount_tDBOutput_4=0;

boolean whetherReject_tDBOutput_4 = false;

java.sql.Connection conn_tDBOutput_4 = null;
String dbUser_tDBOutput_4 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_4 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_4 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_4 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_4 = decryptedPassword_tDBOutput_4;

    conn_tDBOutput_4 = java.sql.DriverManager.getConnection(url_tDBOutput_4,dbUser_tDBOutput_4,dbPwd_tDBOutput_4);
	
	resourceMap.put("conn_tDBOutput_4", conn_tDBOutput_4);
        conn_tDBOutput_4.setAutoCommit(false);
        int commitEvery_tDBOutput_4 = 10000;
        int commitCounter_tDBOutput_4 = 0;


   int batchSize_tDBOutput_4 = 10000;
   int batchSizeCounter_tDBOutput_4=0;

int count_tDBOutput_4=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_4 = conn_tDBOutput_4.getMetaData();
                                boolean whetherExist_tDBOutput_4 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_4 = dbMetaData_tDBOutput_4.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_4 = "public";
                                    if(dbschema_tDBOutput_4 == null || dbschema_tDBOutput_4.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_4 = conn_tDBOutput_4.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_4 = stmtSchema_tDBOutput_4.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_4.next()){
                                                defaultSchema_tDBOutput_4 = rsSchema_tDBOutput_4.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_4.next()) {
                                        String table_tDBOutput_4 = rsTable_tDBOutput_4.getString("TABLE_NAME");
                                        String schema_tDBOutput_4 = rsTable_tDBOutput_4.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_4.equals(("attestation_rejects_structure_check"))
                                            && (schema_tDBOutput_4.equals(dbschema_tDBOutput_4) || ((dbschema_tDBOutput_4 ==null || dbschema_tDBOutput_4.trim().length() ==0) && defaultSchema_tDBOutput_4.equals(schema_tDBOutput_4)))) {
                                            whetherExist_tDBOutput_4 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_4) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_4 = conn_tDBOutput_4.createStatement()) {
                                        stmtCreate_tDBOutput_4.execute("CREATE TABLE \"" + tableName_tDBOutput_4 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"numero_police\" VARCHAR(128)  ,\"date_emission\" TIMESTAMP ,\"date_effet\" TIMESTAMP ,\"date_echeance\" TIMESTAMP ,\"couleur\" VARCHAR(128)   not null ,\"statut\" VARCHAR(128)   not null ,\"zone_circulation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"remorque\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_4 = "INSERT INTO \"" + tableName_tDBOutput_4 + "\" (\"numero_attestation\",\"numero_police\",\"date_emission\",\"date_effet\",\"date_echeance\",\"couleur\",\"statut\",\"zone_circulation\",\"immatriculation\",\"remorque\",\"code_assure\",\"code_assureur\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_4 = conn_tDBOutput_4.prepareStatement(insert_tDBOutput_4);
	    resourceMap.put("pstmt_tDBOutput_4", pstmt_tDBOutput_4);
	    

 



/**
 * [tDBOutput_4 begin ] stop
 */



	
	/**
	 * [tMap_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_3", false);
		start_Hash.put("tMap_3", System.currentTimeMillis());
		
	
	currentComponent="tMap_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row7");
					}
				
		int tos_count_tMap_3 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_3__Struct  {
}
Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
// ###############################

// ###############################
// # Outputs initialization
attestation_rejects_structureStruct attestation_rejects_structure_tmp = new attestation_rejects_structureStruct();
// ###############################

        
        



        









 



/**
 * [tMap_3 begin ] stop
 */



	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row2");
					}
				
		int tos_count_tLogRow_1 = 0;
		

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[14];

        public void addRow(String[] row) {

            for (int i = 0; i < 14; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 13 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 13 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%10$-");
        			        sbformat.append(colLengths[9]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%11$-");
        			        sbformat.append(colLengths[10]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%12$-");
        			        sbformat.append(colLengths[11]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%13$-");
        			        sbformat.append(colLengths[12]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%14$-");
        			        sbformat.append(colLengths[13]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[8] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[9] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[10] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[11] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[12] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[13] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"numero_attestation","numero_police","date_emission","date_effet","date_echeance","couleur","statut","zone_circulation","immatriculation","remorque","code_assure","code_assureur","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */



	
	/**
	 * [tFileInputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileInputDelimited_1", false);
		start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileInputDelimited_1";

	
		int tos_count_tFileInputDelimited_1 = 0;
		
	
	
	
 
	
	
	final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();
	
	
				int nb_line_tFileInputDelimited_1 = 0;
				int footer_tFileInputDelimited_1 = 0;
				int totalLinetFileInputDelimited_1 = 0;
				int limittFileInputDelimited_1 = -1;
				int lastLinetFileInputDelimited_1 = -1;	
				
				char fieldSeparator_tFileInputDelimited_1[] = null;
				
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.attestation_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1 = ((String)context.attestation_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
			
				char rowSeparator_tFileInputDelimited_1[] = null;
			
				//support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'. 
				if ( ((String)context.attestation_RowSeparator).length() > 0 ){
					rowSeparator_tFileInputDelimited_1 = ((String)context.attestation_RowSeparator).toCharArray();
				}else {
					throw new IllegalArgumentException("Row Separator must be assigned a char."); 
				}
			
				Object filename_tFileInputDelimited_1 = /** Start field tFileInputDelimited_1:FILENAME */((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"))/** End field tFileInputDelimited_1:FILENAME */;		
				com.talend.csv.CSVReader csvReadertFileInputDelimited_1 = null;
	
				try{
					
						String[] rowtFileInputDelimited_1=null;
						int currentLinetFileInputDelimited_1 = 0;
	        			int outputLinetFileInputDelimited_1 = 0;
						try {//TD110 begin
							if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
							
			int footer_value_tFileInputDelimited_1 = 0;
			if(footer_value_tFileInputDelimited_1 > 0){
				throw new java.lang.Exception("When the input source is a stream,footer shouldn't be bigger than 0.");
			}
		
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.attestation_Encoding);
							}else{
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.attestation_Encoding);
		        			}
					
					
					csvReadertFileInputDelimited_1.setTrimWhitespace(false);
					if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )
	        			csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
	        				csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	            				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							      
		
			
						if(footer_tFileInputDelimited_1 > 0){
						for(totalLinetFileInputDelimited_1=0;totalLinetFileInputDelimited_1 < context.attestation_Header; totalLinetFileInputDelimited_1++){
							csvReadertFileInputDelimited_1.readNext();
						}
						csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
			            while (csvReadertFileInputDelimited_1.readNext()) {
							
	                
	                		totalLinetFileInputDelimited_1++;
	                
							
	                
			            }
	            		int lastLineTemptFileInputDelimited_1 = totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1   < 0? 0 : totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1 ;
	            		if(lastLinetFileInputDelimited_1 > 0){
	                		lastLinetFileInputDelimited_1 = lastLinetFileInputDelimited_1 < lastLineTemptFileInputDelimited_1 ? lastLinetFileInputDelimited_1 : lastLineTemptFileInputDelimited_1; 
	            		}else {
	                		lastLinetFileInputDelimited_1 = lastLineTemptFileInputDelimited_1;
	            		}
	         
			          	csvReadertFileInputDelimited_1.close();
				        if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.attestation_Encoding);
		        		}else{
							csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.attestation_Encoding);
						}
						csvReadertFileInputDelimited_1.setTrimWhitespace(false);
						if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )	
	        				csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
							csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	        				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							  
	        		}
	        
			        if(limittFileInputDelimited_1 != 0){
			        	for(currentLinetFileInputDelimited_1=0;currentLinetFileInputDelimited_1 < context.attestation_Header;currentLinetFileInputDelimited_1++){
			        		csvReadertFileInputDelimited_1.readNext();
			        	}
			        }
			        csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
	        
	    		} catch(java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
					
						
						System.err.println(e.getMessage());
					
	    		}//TD110 end
	        
			    
	        	while ( limittFileInputDelimited_1 != 0 && csvReadertFileInputDelimited_1!=null && csvReadertFileInputDelimited_1.readNext() ) { 
	        		rowstate_tFileInputDelimited_1.reset();
	        
		        	rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
		        	
					
	        	
	        	
	        		currentLinetFileInputDelimited_1++;
	            
		            if(lastLinetFileInputDelimited_1 > -1 && currentLinetFileInputDelimited_1 > lastLinetFileInputDelimited_1) {
		                break;
	    	        }
	        	    outputLinetFileInputDelimited_1++;
	            	if (limittFileInputDelimited_1 > 0 && outputLinetFileInputDelimited_1 > limittFileInputDelimited_1) {
	                	break;
	            	}  
	                                                                      
					
	    							row1 = null;			
								
	    							row2 = null;			
								
								boolean whetherReject_tFileInputDelimited_1 = false;
								row1 = new row1Struct();
								try {			
									
				char fieldSeparator_tFileInputDelimited_1_ListType[] = null;
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.attestation_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1_ListType = ((String)context.attestation_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
				if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
					
							row1.numero_attestation = null;
					
							row1.numero_police = null;
					
							row1.date_emission = null;
					
							row1.date_effet = null;
					
							row1.date_echeance = null;
					
							row1.couleur = null;
					
							row1.statut = null;
					
							row1.zone_circulation = null;
					
							row1.immatriculation = null;
					
							row1.remorque = null;
					
							row1.code_assure = null;
					
							row1.code_assureur = null;
					
				}else{
					
					for(int i_tFileInputDelimited_1=0;i_tFileInputDelimited_1<rowtFileInputDelimited_1.length;i_tFileInputDelimited_1++){
						rowtFileInputDelimited_1[i_tFileInputDelimited_1]=rowtFileInputDelimited_1[i_tFileInputDelimited_1].trim();
					}
					
	                int columnIndexWithD_tFileInputDelimited_1 = 0; //Column Index 
	                
						columnIndexWithD_tFileInputDelimited_1 = 0;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.numero_attestation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.numero_attestation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 1;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.numero_police = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.numero_police = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 2;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_emission = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_emission", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_emission = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_emission = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 3;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_effet = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_effet", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_effet = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_effet = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 4;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
											row1.date_echeance = ParserUtils.parseTo_Date(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], "dd/MM/yyyy", false);
										
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"date_echeance", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.date_echeance = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.date_echeance = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 5;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.couleur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.couleur = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 6;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.statut = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.statut = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 7;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.zone_circulation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.zone_circulation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 8;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.immatriculation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.immatriculation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 9;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.remorque = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.remorque = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 10;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assure = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assure = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 11;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assureur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assureur = null;
							
						
						}
						
						
					
				}
				
 					int filedsum = rowtFileInputDelimited_1.length;
 					if(filedsum < (12 )){
 						throw new java.lang.Exception("Column(s) missing");
 					} else if(filedsum > (12 )) {
 						throw new RuntimeException("Too many columns");
 					}     
				
									
									if(rowstate_tFileInputDelimited_1.getException()!=null) {
										throw rowstate_tFileInputDelimited_1.getException();
									}
									
									
	    						} catch (java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
							        whetherReject_tFileInputDelimited_1 = true;
        							
						                    row2 = new row2Struct();
                							
    				row2.numero_attestation = row1.numero_attestation;
				
    				row2.numero_police = row1.numero_police;
				
    				row2.date_emission = row1.date_emission;
				
    				row2.date_effet = row1.date_effet;
				
    				row2.date_echeance = row1.date_echeance;
				
    				row2.couleur = row1.couleur;
				
    				row2.statut = row1.statut;
				
    				row2.zone_circulation = row1.zone_circulation;
				
    				row2.immatriculation = row1.immatriculation;
				
    				row2.remorque = row1.remorque;
				
    				row2.code_assure = row1.code_assure;
				
    				row2.code_assureur = row1.code_assureur;
				
			
                							row2.errorMessage = e.getMessage() + " - Line: " + tos_count_tFileInputDelimited_1;
                							row1 = null;
                						
            							globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());
            							
	    						}
	
							

 



/**
 * [tFileInputDelimited_1 begin ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 


	tos_count_tFileInputDelimited_1++;

/**
 * [tFileInputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_begin ] stop
 */
// Start of branch "row1"
if(row1 != null) { 



	
	/**
	 * [tSchemaComplianceCheck_1 main ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row1"
						
						);
					}
					
	row3 = null;	row4 = null;
	rsvUtil_tSchemaComplianceCheck_1.setRowValue_0(row1);
	if (rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row3 = new row3Struct();
		row3.numero_attestation = row1.numero_attestation;
		row3.numero_police = row1.numero_police;
		row3.date_emission = row1.date_emission;
		row3.date_effet = row1.date_effet;
		row3.date_echeance = row1.date_echeance;
		row3.couleur = row1.couleur;
		row3.statut = row1.statut;
		row3.zone_circulation = row1.zone_circulation;
		row3.immatriculation = row1.immatriculation;
		row3.remorque = row1.remorque;
		row3.code_assure = row1.code_assure;
		row3.code_assureur = row1.code_assureur;
	}
	if (!rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row4 = new row4Struct();
		row4.numero_attestation = row1.numero_attestation;
		row4.numero_police = row1.numero_police;
		row4.date_emission = row1.date_emission;
		row4.date_effet = row1.date_effet;
		row4.date_echeance = row1.date_echeance;
		row4.couleur = row1.couleur;
		row4.statut = row1.statut;
		row4.zone_circulation = row1.zone_circulation;
		row4.immatriculation = row1.immatriculation;
		row4.remorque = row1.remorque;
		row4.code_assure = row1.code_assure;
		row4.code_assureur = row1.code_assureur;
		row4.errorCode = String.valueOf(rsvUtil_tSchemaComplianceCheck_1.resultErrorCodeThrough);
		row4.errorMessage = rsvUtil_tSchemaComplianceCheck_1.resultErrorMessageThrough;
	}
	rsvUtil_tSchemaComplianceCheck_1.reset();

 


	tos_count_tSchemaComplianceCheck_1++;

/**
 * [tSchemaComplianceCheck_1 main ] stop
 */
	
	/**
	 * [tSchemaComplianceCheck_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_begin ] stop
 */
// Start of branch "row3"
if(row3 != null) { 



	
	/**
	 * [tLogRow_2 main ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row3"
						
						);
					}
					
///////////////////////		
						



				strBuffer_tLogRow_2 = new StringBuilder();




   				
	    		if(row3.numero_attestation != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.numero_attestation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.numero_police != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.numero_police)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.date_emission != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
								FormatterUtils.format_Date(row3.date_emission, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.date_effet != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
								FormatterUtils.format_Date(row3.date_effet, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.date_echeance != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
								FormatterUtils.format_Date(row3.date_echeance, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.couleur != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.couleur)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.statut != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.statut)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.zone_circulation != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.zone_circulation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.immatriculation != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.immatriculation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.remorque != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.remorque)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.code_assure != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.code_assure)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_2.append("|");
    			


   				
	    		if(row3.code_assureur != null) { //              
                    							
       
				strBuffer_tLogRow_2.append(
				                String.valueOf(row3.code_assureur)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_2 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_2 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_2);
                    }
                    consoleOut_tLogRow_2.println(strBuffer_tLogRow_2.toString());
                    consoleOut_tLogRow_2.flush();
                    nb_line_tLogRow_2++;
//////

//////                    
                    
///////////////////////    			

 
     row5 = row3;


	tos_count_tLogRow_2++;

/**
 * [tLogRow_2 main ] stop
 */
	
	/**
	 * [tLogRow_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row5"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;
Var.Num_police = (StringHandling.DOWNCASE(row5.statut).equals("vendue") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && ("").equals(row5.numero_police))?"Numero_police_vide_avec_statut_vendue_suspendue_RemiseEnVigueur_resiliee":"" ;
Var.Date_emission = (StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_emission==null)?"Date_emission_vide_avec_statut_vendue_suspendue_RemiseEnVigueur_resiliee   ":"vendue_suspendue_remse en vigueur_resiliee".contains(StringHandling.DOWNCASE(row5.statut)) && row5.date_emission!=null && TalendDate.compareDate(row5.date_emission,TalendDate.parseDate("dd/MM/yyyy", "01/05/2021"))==-1 ?"date_emission_ancienne   ":"" ;
Var.Date_effet = (StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_effet==null)?"Date_effet_vide_avec_statut_vendue_suspendue_RemiseEnVigueur_resiliee   ":"vendue_suspendue_remse en vigueur_resiliee".contains(StringHandling.DOWNCASE(row5.statut)) && row5.date_effet!=null && TalendDate.compareDate(row5.date_effet,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 ?"date_effet_pas_conforme   ":"" ;
Var.Date_echeance = (StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_echeance==null)?"Date_echeance_vide_avec_statut_vendue_suspendue_RemiseEnVigueur_resiliee   ":"vendue_suspendue_remse en vigueur_resiliee".contains(StringHandling.DOWNCASE(row5.statut)) && row5.date_echeance!=null && TalendDate.compareDate(row5.date_echeance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 ?"date_echeance_pas_conforme   ":"" ;
Var.Couleur = !"marron_verte_bleue".contains(StringHandling.DOWNCASE(row5.couleur))?"Couleur_attestation_erronee   ":"" ;
Var.ZoneCirculation = !"a_b_c".contains(StringHandling.DOWNCASE(row5.zone_circulation))?"zone_circulation_erronee   ":"" ;
Var.Statut = !"vendue_en stock_mise au rebus_annulee_resiliee_suspendue_remise en vigueur".contains(StringHandling.DOWNCASE(row5.statut))?"statut_invalide   ":"" ;
Var.CouleurVal = "marron_verte_bleue";
Var.ZCirculationVal = "a_b_c";
Var.StatutVal = "vendue_en stock_mise au rebus_annulee_resiliee_suspendue_remise en vigueur";// ###############################
        // ###############################
        // # Output tables

attestation_valide = null;
rejet_attestation = null;

boolean rejected_tMap_1 = true;

// # Output table : 'attestation_valide'
// # Filter conditions 
if( 

!(StringHandling.DOWNCASE(row5.statut).equals("vendue") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && ("").equals(row5.numero_police) || StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_emission==null || StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_effet==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_effet==null ||StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_echeance==null || StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_emission!=null && TalendDate.compareDate(row5.date_emission,TalendDate.parseDate("dd/MM/yyyy", "01/05/2021"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_emission!=null && TalendDate.compareDate(row5.date_emission,TalendDate.parseDate("dd/MM/yyyy", "01/05/2021"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_emission!=null && TalendDate.compareDate(row5.date_emission,TalendDate.parseDate("dd/MM/yyyy", "01/05/2021"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_emission!=null && TalendDate.compareDate(row5.date_emission,TalendDate.parseDate("dd/MM/yyyy", "01/05/2021"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_effet!=null && TalendDate.compareDate(row5.date_effet,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_effet!=null && TalendDate.compareDate(row5.date_effet,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_effet!=null && TalendDate.compareDate(row5.date_effet,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_effet!=null && TalendDate.compareDate(row5.date_effet,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("vendue") && row5.date_echeance!=null && TalendDate.compareDate(row5.date_echeance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("suspendue") && row5.date_echeance!=null && TalendDate.compareDate(row5.date_echeance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("remise en vigueur") && row5.date_echeance!=null && TalendDate.compareDate(row5.date_echeance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || StringHandling.DOWNCASE(row5.statut).equals("resiliee") && row5.date_echeance!=null && TalendDate.compareDate(row5.date_echeance,TalendDate.parseDate("dd/MM/yyyy", "01/01/1922"))==-1 || !Var.StatutVal.contains(StringHandling.DOWNCASE(row5.statut)) || !Var.ZCirculationVal.contains(StringHandling.DOWNCASE(row5.zone_circulation)) || !Var.CouleurVal.contains(StringHandling.DOWNCASE(row5.couleur)))

 ) {
rejected_tMap_1 = false;
attestation_valide_tmp.numero_attestation = row5.numero_attestation ;
attestation_valide_tmp.numero_police = row5.numero_police ;
attestation_valide_tmp.date_emission = row5.date_emission ;
attestation_valide_tmp.date_effet = row5.date_effet ;
attestation_valide_tmp.date_echeance = row5.date_echeance ;
attestation_valide_tmp.couleur = row5.couleur ;
attestation_valide_tmp.statut = row5.statut ;
attestation_valide_tmp.zone_circulation = row5.zone_circulation ;
attestation_valide_tmp.immatriculation = row5.immatriculation ;
attestation_valide_tmp.remorque = row5.remorque ;
attestation_valide_tmp.code_assure = row5.code_assure ;
attestation_valide_tmp.code_assureur = row5.code_assureur ;
attestation_valide_tmp.c_status = 1;
attestation_valide_tmp.c_date_mis_a_jour = null;
attestation_valide_tmp.c_date_transfer = null;
attestation_valide_tmp.commentaires = null;
attestation_valide = attestation_valide_tmp;
} // closing filter/reject
// ###### START REJECTS ##### 

// # Output reject table : 'rejet_attestation'
// # Filter conditions 
if( rejected_tMap_1 ) {
rejet_attestation_tmp.numero_attestation = row5.numero_attestation ;
rejet_attestation_tmp.numero_police = row5.numero_police ;
rejet_attestation_tmp.date_emission = row5.date_emission ;
rejet_attestation_tmp.date_effet = row5.date_effet ;
rejet_attestation_tmp.date_echeance = row5.date_echeance ;
rejet_attestation_tmp.couleur = row5.couleur ;
rejet_attestation_tmp.statut = row5.statut ;
rejet_attestation_tmp.zone_circulation = row5.zone_circulation ;
rejet_attestation_tmp.immatriculation = row5.immatriculation ;
rejet_attestation_tmp.remorque = row5.remorque ;
rejet_attestation_tmp.code_assure = row5.code_assure ;
rejet_attestation_tmp.code_assureur = row5.code_assureur ;
rejet_attestation_tmp.date_extraction = TalendDate.getCurrentDate() ;
rejet_attestation_tmp.date_depot = null;
rejet_attestation_tmp.Error_Message = Var.Num_police+Var.Date_emission+Var.Date_effet+Var.Date_echeance+Var.Couleur+Var.ZoneCirculation+Var.Statut;
rejet_attestation = rejet_attestation_tmp;
} // closing filter/reject
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "attestation_valide"
if(attestation_valide != null) { 



	
	/**
	 * [tDBOutput_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"attestation_valide"
						
						);
					}
					



        whetherReject_tDBOutput_1 = false;
                    if(attestation_valide.numero_attestation == null) {
pstmt_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(1, attestation_valide.numero_attestation);
}

            int checkCount_tDBOutput_1 = -1;
            try (java.sql.ResultSet rs_tDBOutput_1 = pstmt_tDBOutput_1.executeQuery()) {
                while(rs_tDBOutput_1.next()) {
                    checkCount_tDBOutput_1 = rs_tDBOutput_1.getInt(1);
                }
            }
            if(checkCount_tDBOutput_1 > 0) {
                        if(attestation_valide.numero_police == null) {
pstmtUpdate_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(1, attestation_valide.numero_police);
}

                        if(attestation_valide.date_emission != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(2, new java.sql.Timestamp(attestation_valide.date_emission.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(2, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.date_effet != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(3, new java.sql.Timestamp(attestation_valide.date_effet.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(3, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.date_echeance != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(4, new java.sql.Timestamp(attestation_valide.date_echeance.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(4, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.couleur == null) {
pstmtUpdate_tDBOutput_1.setNull(5, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(5, attestation_valide.couleur);
}

                        if(attestation_valide.statut == null) {
pstmtUpdate_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(6, attestation_valide.statut);
}

                        if(attestation_valide.zone_circulation == null) {
pstmtUpdate_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(7, attestation_valide.zone_circulation);
}

                        if(attestation_valide.immatriculation == null) {
pstmtUpdate_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(8, attestation_valide.immatriculation);
}

                        if(attestation_valide.remorque == null) {
pstmtUpdate_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(9, attestation_valide.remorque);
}

                        if(attestation_valide.code_assure == null) {
pstmtUpdate_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(10, attestation_valide.code_assure);
}

                        if(attestation_valide.code_assureur == null) {
pstmtUpdate_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(11, attestation_valide.code_assureur);
}

                        pstmtUpdate_tDBOutput_1.setInt(12, attestation_valide.c_status);

                        if(attestation_valide.c_date_mis_a_jour != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(13, new java.sql.Timestamp(attestation_valide.c_date_mis_a_jour.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(13, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.c_date_transfer != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(14, new java.sql.Timestamp(attestation_valide.c_date_transfer.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(14, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.commentaires == null) {
pstmtUpdate_tDBOutput_1.setNull(15, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(15, attestation_valide.commentaires);
}

                        if(attestation_valide.numero_attestation == null) {
pstmtUpdate_tDBOutput_1.setNull(16 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(16 + count_tDBOutput_1, attestation_valide.numero_attestation);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtUpdate_tDBOutput_1.executeUpdate();
                    updatedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            } else {
                        if(attestation_valide.numero_attestation == null) {
pstmtInsert_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(1, attestation_valide.numero_attestation);
}

                        if(attestation_valide.numero_police == null) {
pstmtInsert_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(2, attestation_valide.numero_police);
}

                        if(attestation_valide.date_emission != null) {
pstmtInsert_tDBOutput_1.setTimestamp(3, new java.sql.Timestamp(attestation_valide.date_emission.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(3, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.date_effet != null) {
pstmtInsert_tDBOutput_1.setTimestamp(4, new java.sql.Timestamp(attestation_valide.date_effet.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(4, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.date_echeance != null) {
pstmtInsert_tDBOutput_1.setTimestamp(5, new java.sql.Timestamp(attestation_valide.date_echeance.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(5, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.couleur == null) {
pstmtInsert_tDBOutput_1.setNull(6, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(6, attestation_valide.couleur);
}

                        if(attestation_valide.statut == null) {
pstmtInsert_tDBOutput_1.setNull(7, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(7, attestation_valide.statut);
}

                        if(attestation_valide.zone_circulation == null) {
pstmtInsert_tDBOutput_1.setNull(8, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(8, attestation_valide.zone_circulation);
}

                        if(attestation_valide.immatriculation == null) {
pstmtInsert_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(9, attestation_valide.immatriculation);
}

                        if(attestation_valide.remorque == null) {
pstmtInsert_tDBOutput_1.setNull(10, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(10, attestation_valide.remorque);
}

                        if(attestation_valide.code_assure == null) {
pstmtInsert_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(11, attestation_valide.code_assure);
}

                        if(attestation_valide.code_assureur == null) {
pstmtInsert_tDBOutput_1.setNull(12, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(12, attestation_valide.code_assureur);
}

                        pstmtInsert_tDBOutput_1.setInt(13, attestation_valide.c_status);

                        if(attestation_valide.c_date_mis_a_jour != null) {
pstmtInsert_tDBOutput_1.setTimestamp(14, new java.sql.Timestamp(attestation_valide.c_date_mis_a_jour.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(14, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.c_date_transfer != null) {
pstmtInsert_tDBOutput_1.setTimestamp(15, new java.sql.Timestamp(attestation_valide.c_date_transfer.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(15, java.sql.Types.TIMESTAMP);
}

                        if(attestation_valide.commentaires == null) {
pstmtInsert_tDBOutput_1.setNull(16, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(16, attestation_valide.commentaires);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtInsert_tDBOutput_1.executeUpdate();
                    insertedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            }
            if(!whetherReject_tDBOutput_1) {
            }
    		    commitCounter_tDBOutput_1++;
                if(commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    }
                    conn_tDBOutput_1.commit();
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_1 = 0;
                    }
                    commitCounter_tDBOutput_1=0;
                }

 


	tos_count_tDBOutput_1++;

/**
 * [tDBOutput_1 main ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_end ] stop
 */

} // End of branch "attestation_valide"




// Start of branch "rejet_attestation"
if(rejet_attestation != null) { 



	
	/**
	 * [tDBOutput_2 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"rejet_attestation"
						
						);
					}
					



            row10 = null;
        whetherReject_tDBOutput_2 = false;
                    if(rejet_attestation.numero_attestation == null) {
pstmt_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(1, rejet_attestation.numero_attestation);
}

                    if(rejet_attestation.numero_police == null) {
pstmt_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(2, rejet_attestation.numero_police);
}

                    if(rejet_attestation.date_emission != null) {
pstmt_tDBOutput_2.setTimestamp(3, new java.sql.Timestamp(rejet_attestation.date_emission.getTime()));
} else {
pstmt_tDBOutput_2.setNull(3, java.sql.Types.TIMESTAMP);
}

                    if(rejet_attestation.date_effet != null) {
pstmt_tDBOutput_2.setTimestamp(4, new java.sql.Timestamp(rejet_attestation.date_effet.getTime()));
} else {
pstmt_tDBOutput_2.setNull(4, java.sql.Types.TIMESTAMP);
}

                    if(rejet_attestation.date_echeance != null) {
pstmt_tDBOutput_2.setTimestamp(5, new java.sql.Timestamp(rejet_attestation.date_echeance.getTime()));
} else {
pstmt_tDBOutput_2.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(rejet_attestation.couleur == null) {
pstmt_tDBOutput_2.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(6, rejet_attestation.couleur);
}

                    if(rejet_attestation.statut == null) {
pstmt_tDBOutput_2.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(7, rejet_attestation.statut);
}

                    if(rejet_attestation.zone_circulation == null) {
pstmt_tDBOutput_2.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(8, rejet_attestation.zone_circulation);
}

                    if(rejet_attestation.immatriculation == null) {
pstmt_tDBOutput_2.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(9, rejet_attestation.immatriculation);
}

                    if(rejet_attestation.remorque == null) {
pstmt_tDBOutput_2.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(10, rejet_attestation.remorque);
}

                    if(rejet_attestation.code_assure == null) {
pstmt_tDBOutput_2.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(11, rejet_attestation.code_assure);
}

                    if(rejet_attestation.code_assureur == null) {
pstmt_tDBOutput_2.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(12, rejet_attestation.code_assureur);
}

                    if(rejet_attestation.date_extraction != null) {
pstmt_tDBOutput_2.setTimestamp(13, new java.sql.Timestamp(rejet_attestation.date_extraction.getTime()));
} else {
pstmt_tDBOutput_2.setNull(13, java.sql.Types.TIMESTAMP);
}

                    if(rejet_attestation.date_depot != null) {
pstmt_tDBOutput_2.setTimestamp(14, new java.sql.Timestamp(rejet_attestation.date_depot.getTime()));
} else {
pstmt_tDBOutput_2.setNull(14, java.sql.Types.TIMESTAMP);
}

                    if(rejet_attestation.Error_Message == null) {
pstmt_tDBOutput_2.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(15, rejet_attestation.Error_Message);
}

			
    		pstmt_tDBOutput_2.addBatch();
    		nb_line_tDBOutput_2++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_2++;
    		  
            if(!whetherReject_tDBOutput_2) {
                            row10 = new row10Struct();
                                row10.numero_attestation = rejet_attestation.numero_attestation;
                                row10.numero_police = rejet_attestation.numero_police;
                                row10.date_emission = rejet_attestation.date_emission;
                                row10.date_effet = rejet_attestation.date_effet;
                                row10.date_echeance = rejet_attestation.date_echeance;
                                row10.couleur = rejet_attestation.couleur;
                                row10.statut = rejet_attestation.statut;
                                row10.zone_circulation = rejet_attestation.zone_circulation;
                                row10.immatriculation = rejet_attestation.immatriculation;
                                row10.remorque = rejet_attestation.remorque;
                                row10.code_assure = rejet_attestation.code_assure;
                                row10.code_assureur = rejet_attestation.code_assureur;
                                row10.date_extraction = rejet_attestation.date_extraction;
                                row10.date_depot = rejet_attestation.date_depot;
                                row10.Error_Message = rejet_attestation.Error_Message;
            }
    			if ((batchSize_tDBOutput_2 > 0) && (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2)) {
                try {
						int countSum_tDBOutput_2 = 0;
						    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
				    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
            	    	batchSizeCounter_tDBOutput_2 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
				    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
				    	String errormessage_tDBOutput_2;
						if (ne_tDBOutput_2 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
							errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
						}else{
							errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
						}
				    	
				    	int countSum_tDBOutput_2 = 0;
						for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
						rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    	System.err.println(errormessage_tDBOutput_2);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_2++;
                if(commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {
                if ((batchSize_tDBOutput_2 > 0) && (batchSizeCounter_tDBOutput_2 > 0)) {
                try {
                		int countSum_tDBOutput_2 = 0;
                		    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
            	    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
            	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
                batchSizeCounter_tDBOutput_2 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
			    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
			    	String errormessage_tDBOutput_2;
					if (ne_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
						errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
					}else{
						errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
					}
			    	
			    	int countSum_tDBOutput_2 = 0;
					for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
					
			    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
			    	
			    	System.err.println(errormessage_tDBOutput_2);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    }
                    conn_tDBOutput_2.commit();
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_2 = 0;
                    }
                    commitCounter_tDBOutput_2=0;
                }

 


	tos_count_tDBOutput_2++;

/**
 * [tDBOutput_2 main ] stop
 */
	
	/**
	 * [tDBOutput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_begin ] stop
 */
// Start of branch "row10"
if(row10 != null) { 



	
	/**
	 * [tFileOutputDelimited_3 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row10"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_3 = new StringBuilder();
                            if(row10.numero_attestation != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.numero_attestation
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.numero_police != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.numero_police
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.date_emission != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row10.date_emission, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.date_effet != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row10.date_effet, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.date_echeance != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row10.date_echeance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.couleur != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.couleur
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.statut != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.statut
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.zone_circulation != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.zone_circulation
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.immatriculation != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.remorque != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.remorque
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.code_assure != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.code_assureur != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.date_extraction != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row10.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.date_depot != null) {
                        sb_tFileOutputDelimited_3.append(
                            FormatterUtils.format_Date(row10.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_3.append(OUT_DELIM_tFileOutputDelimited_3);
                            if(row10.Error_Message != null) {
                        sb_tFileOutputDelimited_3.append(
                            row10.Error_Message
                        );
                            }
                    sb_tFileOutputDelimited_3.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_3);


                    nb_line_tFileOutputDelimited_3++;
                    resourceMap.put("nb_line_tFileOutputDelimited_3", nb_line_tFileOutputDelimited_3);

                        outtFileOutputDelimited_3.write(sb_tFileOutputDelimited_3.toString());




 


	tos_count_tFileOutputDelimited_3++;

/**
 * [tFileOutputDelimited_3 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	

 



/**
 * [tFileOutputDelimited_3 process_data_end ] stop
 */

} // End of branch "row10"




	
	/**
	 * [tDBOutput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_end ] stop
 */

} // End of branch "rejet_attestation"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_end ] stop
 */

} // End of branch "row3"




// Start of branch "row4"
if(row4 != null) { 



	
	/**
	 * [tLogRow_3 main ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row4"
						
						);
					}
					
///////////////////////		
						



				strBuffer_tLogRow_3 = new StringBuilder();




   				
	    		if(row4.numero_attestation != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.numero_attestation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.numero_police != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.numero_police)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.date_emission != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
								FormatterUtils.format_Date(row4.date_emission, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.date_effet != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
								FormatterUtils.format_Date(row4.date_effet, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.date_echeance != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
								FormatterUtils.format_Date(row4.date_echeance, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.couleur != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.couleur)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.statut != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.statut)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.zone_circulation != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.zone_circulation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.immatriculation != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.immatriculation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.remorque != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.remorque)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.code_assure != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.code_assure)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.code_assureur != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.code_assureur)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.errorCode != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.errorCode)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_3.append("|");
    			


   				
	    		if(row4.errorMessage != null) { //              
                    							
       
				strBuffer_tLogRow_3.append(
				                String.valueOf(row4.errorMessage)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_3 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_3 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_3);
                    }
                    consoleOut_tLogRow_3.println(strBuffer_tLogRow_3.toString());
                    consoleOut_tLogRow_3.flush();
                    nb_line_tLogRow_3++;
//////

//////                    
                    
///////////////////////    			

 
     row6 = row4;


	tos_count_tLogRow_3++;

/**
 * [tLogRow_3 main ] stop
 */
	
	/**
	 * [tLogRow_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_begin ] stop
 */

	
	/**
	 * [tMap_2 main ] start
	 */

	

	
	
	currentComponent="tMap_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row6"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_2 = false;
		  boolean mainRowRejected_tMap_2 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
        // ###############################
        // # Output tables

attestation_rejects_schema = null;


// # Output table : 'attestation_rejects_schema'
attestation_rejects_schema_tmp.numero_attestation = row6.numero_attestation ;
attestation_rejects_schema_tmp.numero_police = row6.numero_police ;
attestation_rejects_schema_tmp.date_emission = row6.date_emission ;
attestation_rejects_schema_tmp.date_effet = row6.date_effet ;
attestation_rejects_schema_tmp.date_echeance = row6.date_echeance ;
attestation_rejects_schema_tmp.couleur = row6.couleur ;
attestation_rejects_schema_tmp.statut = row6.statut ;
attestation_rejects_schema_tmp.zone_circulation = row6.zone_circulation ;
attestation_rejects_schema_tmp.immatriculation = row6.immatriculation ;
attestation_rejects_schema_tmp.remorque = row6.remorque ;
attestation_rejects_schema_tmp.code_assure = row6.code_assure ;
attestation_rejects_schema_tmp.code_assureur = row6.code_assureur ;
attestation_rejects_schema_tmp.date_extraction = TalendDate.getCurrentDate() ;
attestation_rejects_schema_tmp.date_depot = null;
attestation_rejects_schema_tmp.errorCode = row6.errorCode ;
attestation_rejects_schema_tmp.errorMessage = row6.errorMessage ;
attestation_rejects_schema = attestation_rejects_schema_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_2 = false;










 


	tos_count_tMap_2++;

/**
 * [tMap_2 main ] stop
 */
	
	/**
	 * [tMap_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_begin ] stop
 */
// Start of branch "attestation_rejects_schema"
if(attestation_rejects_schema != null) { 



	
	/**
	 * [tDBOutput_3 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"attestation_rejects_schema"
						
						);
					}
					



            row9 = null;
        whetherReject_tDBOutput_3 = false;
                    if(attestation_rejects_schema.numero_attestation == null) {
pstmt_tDBOutput_3.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(1, attestation_rejects_schema.numero_attestation);
}

                    if(attestation_rejects_schema.numero_police == null) {
pstmt_tDBOutput_3.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(2, attestation_rejects_schema.numero_police);
}

                    if(attestation_rejects_schema.date_emission != null) {
pstmt_tDBOutput_3.setTimestamp(3, new java.sql.Timestamp(attestation_rejects_schema.date_emission.getTime()));
} else {
pstmt_tDBOutput_3.setNull(3, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_schema.date_effet != null) {
pstmt_tDBOutput_3.setTimestamp(4, new java.sql.Timestamp(attestation_rejects_schema.date_effet.getTime()));
} else {
pstmt_tDBOutput_3.setNull(4, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_schema.date_echeance != null) {
pstmt_tDBOutput_3.setTimestamp(5, new java.sql.Timestamp(attestation_rejects_schema.date_echeance.getTime()));
} else {
pstmt_tDBOutput_3.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_schema.couleur == null) {
pstmt_tDBOutput_3.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(6, attestation_rejects_schema.couleur);
}

                    if(attestation_rejects_schema.statut == null) {
pstmt_tDBOutput_3.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(7, attestation_rejects_schema.statut);
}

                    if(attestation_rejects_schema.zone_circulation == null) {
pstmt_tDBOutput_3.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(8, attestation_rejects_schema.zone_circulation);
}

                    if(attestation_rejects_schema.immatriculation == null) {
pstmt_tDBOutput_3.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(9, attestation_rejects_schema.immatriculation);
}

                    if(attestation_rejects_schema.remorque == null) {
pstmt_tDBOutput_3.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(10, attestation_rejects_schema.remorque);
}

                    if(attestation_rejects_schema.code_assure == null) {
pstmt_tDBOutput_3.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(11, attestation_rejects_schema.code_assure);
}

                    if(attestation_rejects_schema.code_assureur == null) {
pstmt_tDBOutput_3.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(12, attestation_rejects_schema.code_assureur);
}

                    if(attestation_rejects_schema.date_extraction != null) {
pstmt_tDBOutput_3.setTimestamp(13, new java.sql.Timestamp(attestation_rejects_schema.date_extraction.getTime()));
} else {
pstmt_tDBOutput_3.setNull(13, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_schema.date_depot != null) {
pstmt_tDBOutput_3.setTimestamp(14, new java.sql.Timestamp(attestation_rejects_schema.date_depot.getTime()));
} else {
pstmt_tDBOutput_3.setNull(14, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_schema.errorCode == null) {
pstmt_tDBOutput_3.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(15, attestation_rejects_schema.errorCode);
}

                    if(attestation_rejects_schema.errorMessage == null) {
pstmt_tDBOutput_3.setNull(16, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(16, attestation_rejects_schema.errorMessage);
}

			
    		pstmt_tDBOutput_3.addBatch();
    		nb_line_tDBOutput_3++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_3++;
    		  
            if(!whetherReject_tDBOutput_3) {
                            row9 = new row9Struct();
                                row9.numero_attestation = attestation_rejects_schema.numero_attestation;
                                row9.numero_police = attestation_rejects_schema.numero_police;
                                row9.date_emission = attestation_rejects_schema.date_emission;
                                row9.date_effet = attestation_rejects_schema.date_effet;
                                row9.date_echeance = attestation_rejects_schema.date_echeance;
                                row9.couleur = attestation_rejects_schema.couleur;
                                row9.statut = attestation_rejects_schema.statut;
                                row9.zone_circulation = attestation_rejects_schema.zone_circulation;
                                row9.immatriculation = attestation_rejects_schema.immatriculation;
                                row9.remorque = attestation_rejects_schema.remorque;
                                row9.code_assure = attestation_rejects_schema.code_assure;
                                row9.code_assureur = attestation_rejects_schema.code_assureur;
                                row9.date_extraction = attestation_rejects_schema.date_extraction;
                                row9.date_depot = attestation_rejects_schema.date_depot;
                                row9.errorCode = attestation_rejects_schema.errorCode;
                                row9.errorMessage = attestation_rejects_schema.errorMessage;
            }
    			if ((batchSize_tDBOutput_3 > 0) && (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3)) {
                try {
						int countSum_tDBOutput_3 = 0;
						    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
				    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
            	    	batchSizeCounter_tDBOutput_3 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
				    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
				    	String errormessage_tDBOutput_3;
						if (ne_tDBOutput_3 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
							errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
						}else{
							errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
						}
				    	
				    	int countSum_tDBOutput_3 = 0;
						for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
						rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    	System.err.println(errormessage_tDBOutput_3);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_3++;
                if(commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {
                if ((batchSize_tDBOutput_3 > 0) && (batchSizeCounter_tDBOutput_3 > 0)) {
                try {
                		int countSum_tDBOutput_3 = 0;
                		    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
            	    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
            	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
                batchSizeCounter_tDBOutput_3 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
			    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
			    	String errormessage_tDBOutput_3;
					if (ne_tDBOutput_3 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
						errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
					}else{
						errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
					}
			    	
			    	int countSum_tDBOutput_3 = 0;
					for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
					
			    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
			    	
			    	System.err.println(errormessage_tDBOutput_3);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    }
                    conn_tDBOutput_3.commit();
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_3 = 0;
                    }
                    commitCounter_tDBOutput_3=0;
                }

 


	tos_count_tDBOutput_3++;

/**
 * [tDBOutput_3 main ] stop
 */
	
	/**
	 * [tDBOutput_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_begin ] stop
 */
// Start of branch "row9"
if(row9 != null) { 



	
	/**
	 * [tFileOutputDelimited_2 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row9"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_2 = new StringBuilder();
                            if(row9.numero_attestation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.numero_attestation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.numero_police != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.numero_police
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.date_emission != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row9.date_emission, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.date_effet != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row9.date_effet, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.date_echeance != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row9.date_echeance, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.couleur != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.couleur
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.statut != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.statut
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.zone_circulation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.zone_circulation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.immatriculation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.remorque != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.remorque
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.code_assure != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.code_assureur != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.date_extraction != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row9.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.date_depot != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row9.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.errorCode != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row9.errorMessage != null) {
                        sb_tFileOutputDelimited_2.append(
                            row9.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_2.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);


                    nb_line_tFileOutputDelimited_2++;
                    resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

                        outtFileOutputDelimited_2.write(sb_tFileOutputDelimited_2.toString());




 


	tos_count_tFileOutputDelimited_2++;

/**
 * [tFileOutputDelimited_2 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_end ] stop
 */

} // End of branch "row9"




	
	/**
	 * [tDBOutput_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_end ] stop
 */

} // End of branch "attestation_rejects_schema"




	
	/**
	 * [tMap_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_end ] stop
 */

} // End of branch "row4"




	
	/**
	 * [tSchemaComplianceCheck_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_end ] stop
 */

} // End of branch "row1"




// Start of branch "row2"
if(row2 != null) { 



	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row2"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[14];
   				
	    		if(row2.numero_attestation != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row2.numero_attestation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.numero_police != null) { //              
                 row_tLogRow_1[1]=    						    
				                String.valueOf(row2.numero_police)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_emission != null) { //              
                 row_tLogRow_1[2]=    						
								FormatterUtils.format_Date(row2.date_emission, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_effet != null) { //              
                 row_tLogRow_1[3]=    						
								FormatterUtils.format_Date(row2.date_effet, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row2.date_echeance != null) { //              
                 row_tLogRow_1[4]=    						
								FormatterUtils.format_Date(row2.date_echeance, "dd/MM/yyyy")
					          ;	
							
	    		} //			
    			   				
	    		if(row2.couleur != null) { //              
                 row_tLogRow_1[5]=    						    
				                String.valueOf(row2.couleur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.statut != null) { //              
                 row_tLogRow_1[6]=    						    
				                String.valueOf(row2.statut)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.zone_circulation != null) { //              
                 row_tLogRow_1[7]=    						    
				                String.valueOf(row2.zone_circulation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.immatriculation != null) { //              
                 row_tLogRow_1[8]=    						    
				                String.valueOf(row2.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.remorque != null) { //              
                 row_tLogRow_1[9]=    						    
				                String.valueOf(row2.remorque)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_assure != null) { //              
                 row_tLogRow_1[10]=    						    
				                String.valueOf(row2.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_assureur != null) { //              
                 row_tLogRow_1[11]=    						    
				                String.valueOf(row2.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.errorCode != null) { //              
                 row_tLogRow_1[12]=    						    
				                String.valueOf(row2.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.errorMessage != null) { //              
                 row_tLogRow_1[13]=    						    
				                String.valueOf(row2.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 
     row7 = row2;


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */

	
	/**
	 * [tMap_3 main ] start
	 */

	

	
	
	currentComponent="tMap_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row7"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_3 = false;
		  boolean mainRowRejected_tMap_3 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
        // ###############################
        // # Output tables

attestation_rejects_structure = null;


// # Output table : 'attestation_rejects_structure'
attestation_rejects_structure_tmp.numero_attestation = row7.numero_attestation ;
attestation_rejects_structure_tmp.numero_police = row7.numero_police ;
attestation_rejects_structure_tmp.date_emission = row7.date_emission ;
attestation_rejects_structure_tmp.date_effet = row7.date_effet ;
attestation_rejects_structure_tmp.date_echeance = row7.date_echeance ;
attestation_rejects_structure_tmp.couleur = row7.couleur ;
attestation_rejects_structure_tmp.statut = row7.statut ;
attestation_rejects_structure_tmp.zone_circulation = row7.zone_circulation ;
attestation_rejects_structure_tmp.immatriculation = row7.immatriculation ;
attestation_rejects_structure_tmp.remorque = row7.remorque ;
attestation_rejects_structure_tmp.code_assure = row7.code_assure ;
attestation_rejects_structure_tmp.code_assureur = row7.code_assureur ;
attestation_rejects_structure_tmp.date_extraction = TalendDate.getCurrentDate() ;
attestation_rejects_structure_tmp.date_depot = null;
attestation_rejects_structure_tmp.errorCode = row7.errorCode ;
attestation_rejects_structure_tmp.errorMessage = row7.errorMessage ;
attestation_rejects_structure = attestation_rejects_structure_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_3 = false;










 


	tos_count_tMap_3++;

/**
 * [tMap_3 main ] stop
 */
	
	/**
	 * [tMap_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_begin ] stop
 */
// Start of branch "attestation_rejects_structure"
if(attestation_rejects_structure != null) { 



	
	/**
	 * [tDBOutput_4 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"attestation_rejects_structure"
						
						);
					}
					



            row8 = null;
        whetherReject_tDBOutput_4 = false;
                    if(attestation_rejects_structure.numero_attestation == null) {
pstmt_tDBOutput_4.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(1, attestation_rejects_structure.numero_attestation);
}

                    if(attestation_rejects_structure.numero_police == null) {
pstmt_tDBOutput_4.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(2, attestation_rejects_structure.numero_police);
}

                    if(attestation_rejects_structure.date_emission != null) {
pstmt_tDBOutput_4.setTimestamp(3, new java.sql.Timestamp(attestation_rejects_structure.date_emission.getTime()));
} else {
pstmt_tDBOutput_4.setNull(3, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_structure.date_effet != null) {
pstmt_tDBOutput_4.setTimestamp(4, new java.sql.Timestamp(attestation_rejects_structure.date_effet.getTime()));
} else {
pstmt_tDBOutput_4.setNull(4, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_structure.date_echeance != null) {
pstmt_tDBOutput_4.setTimestamp(5, new java.sql.Timestamp(attestation_rejects_structure.date_echeance.getTime()));
} else {
pstmt_tDBOutput_4.setNull(5, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_structure.couleur == null) {
pstmt_tDBOutput_4.setNull(6, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(6, attestation_rejects_structure.couleur);
}

                    if(attestation_rejects_structure.statut == null) {
pstmt_tDBOutput_4.setNull(7, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(7, attestation_rejects_structure.statut);
}

                    if(attestation_rejects_structure.zone_circulation == null) {
pstmt_tDBOutput_4.setNull(8, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(8, attestation_rejects_structure.zone_circulation);
}

                    if(attestation_rejects_structure.immatriculation == null) {
pstmt_tDBOutput_4.setNull(9, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(9, attestation_rejects_structure.immatriculation);
}

                    if(attestation_rejects_structure.remorque == null) {
pstmt_tDBOutput_4.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(10, attestation_rejects_structure.remorque);
}

                    if(attestation_rejects_structure.code_assure == null) {
pstmt_tDBOutput_4.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(11, attestation_rejects_structure.code_assure);
}

                    if(attestation_rejects_structure.code_assureur == null) {
pstmt_tDBOutput_4.setNull(12, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(12, attestation_rejects_structure.code_assureur);
}

                    if(attestation_rejects_structure.date_extraction != null) {
pstmt_tDBOutput_4.setTimestamp(13, new java.sql.Timestamp(attestation_rejects_structure.date_extraction.getTime()));
} else {
pstmt_tDBOutput_4.setNull(13, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_structure.date_depot != null) {
pstmt_tDBOutput_4.setTimestamp(14, new java.sql.Timestamp(attestation_rejects_structure.date_depot.getTime()));
} else {
pstmt_tDBOutput_4.setNull(14, java.sql.Types.TIMESTAMP);
}

                    if(attestation_rejects_structure.errorCode == null) {
pstmt_tDBOutput_4.setNull(15, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(15, attestation_rejects_structure.errorCode);
}

                    if(attestation_rejects_structure.errorMessage == null) {
pstmt_tDBOutput_4.setNull(16, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_4.setString(16, attestation_rejects_structure.errorMessage);
}

			
    		pstmt_tDBOutput_4.addBatch();
    		nb_line_tDBOutput_4++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_4++;
    		  
            if(!whetherReject_tDBOutput_4) {
                            row8 = new row8Struct();
                                row8.numero_attestation = attestation_rejects_structure.numero_attestation;
                                row8.numero_police = attestation_rejects_structure.numero_police;
                                row8.date_emission = attestation_rejects_structure.date_emission;
                                row8.date_effet = attestation_rejects_structure.date_effet;
                                row8.date_echeance = attestation_rejects_structure.date_echeance;
                                row8.couleur = attestation_rejects_structure.couleur;
                                row8.statut = attestation_rejects_structure.statut;
                                row8.zone_circulation = attestation_rejects_structure.zone_circulation;
                                row8.immatriculation = attestation_rejects_structure.immatriculation;
                                row8.remorque = attestation_rejects_structure.remorque;
                                row8.code_assure = attestation_rejects_structure.code_assure;
                                row8.code_assureur = attestation_rejects_structure.code_assureur;
                                row8.date_extraction = attestation_rejects_structure.date_extraction;
                                row8.date_depot = attestation_rejects_structure.date_depot;
                                row8.errorCode = attestation_rejects_structure.errorCode;
                                row8.errorMessage = attestation_rejects_structure.errorMessage;
            }
    			if ((batchSize_tDBOutput_4 > 0) && (batchSize_tDBOutput_4 <= batchSizeCounter_tDBOutput_4)) {
                try {
						int countSum_tDBOutput_4 = 0;
						    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
				    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
            	    	batchSizeCounter_tDBOutput_4 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
				    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
				    	String errormessage_tDBOutput_4;
						if (ne_tDBOutput_4 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
							errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
						}else{
							errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
						}
				    	
				    	int countSum_tDBOutput_4 = 0;
						for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
						rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
				    	
				    	System.err.println(errormessage_tDBOutput_4);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_4++;
                if(commitEvery_tDBOutput_4 <= commitCounter_tDBOutput_4) {
                if ((batchSize_tDBOutput_4 > 0) && (batchSizeCounter_tDBOutput_4 > 0)) {
                try {
                		int countSum_tDBOutput_4 = 0;
                		    
						for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
						}
            	    	rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
            	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
            	    	
                batchSizeCounter_tDBOutput_4 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
			    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
			    	String errormessage_tDBOutput_4;
					if (ne_tDBOutput_4 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
						errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
					}else{
						errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
					}
			    	
			    	int countSum_tDBOutput_4 = 0;
					for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
					
			    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
			    	
			    	System.err.println(errormessage_tDBOutput_4);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    }
                    conn_tDBOutput_4.commit();
                    if(rowsToCommitCount_tDBOutput_4 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_4 = 0;
                    }
                    commitCounter_tDBOutput_4=0;
                }

 


	tos_count_tDBOutput_4++;

/**
 * [tDBOutput_4 main ] stop
 */
	
	/**
	 * [tDBOutput_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_begin ] stop
 */
// Start of branch "row8"
if(row8 != null) { 



	
	/**
	 * [tFileOutputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row8"
						
						);
					}
					


                        String[] rowtFileOutputDelimited_1=new String[16];
                            rowtFileOutputDelimited_1[0]=row8.numero_attestation == null ? null : row8.numero_attestation;
                            rowtFileOutputDelimited_1[1]=row8.numero_police == null ? null : row8.numero_police;
                            rowtFileOutputDelimited_1[2]=row8.date_emission == null ? null : FormatterUtils.format_Date(row8.date_emission, "dd/MM/yyyy");
                            rowtFileOutputDelimited_1[3]=row8.date_effet == null ? null : FormatterUtils.format_Date(row8.date_effet, "dd/MM/yyyy");
                            rowtFileOutputDelimited_1[4]=row8.date_echeance == null ? null : FormatterUtils.format_Date(row8.date_echeance, "dd/MM/yyyy");
                            rowtFileOutputDelimited_1[5]=row8.couleur == null ? null : row8.couleur;
                            rowtFileOutputDelimited_1[6]=row8.statut == null ? null : row8.statut;
                            rowtFileOutputDelimited_1[7]=row8.zone_circulation == null ? null : row8.zone_circulation;
                            rowtFileOutputDelimited_1[8]=row8.immatriculation == null ? null : row8.immatriculation;
                            rowtFileOutputDelimited_1[9]=row8.remorque == null ? null : row8.remorque;
                            rowtFileOutputDelimited_1[10]=row8.code_assure == null ? null : row8.code_assure;
                            rowtFileOutputDelimited_1[11]=row8.code_assureur == null ? null : row8.code_assureur;
                            rowtFileOutputDelimited_1[12]=row8.date_extraction == null ? null : FormatterUtils.format_Date(row8.date_extraction, "dd/MM/yyyy");
                            rowtFileOutputDelimited_1[13]=row8.date_depot == null ? null : FormatterUtils.format_Date(row8.date_depot, "dd/MM/yyyy");
                            rowtFileOutputDelimited_1[14]=row8.errorCode == null ? null : row8.errorCode;
                            rowtFileOutputDelimited_1[15]=row8.errorMessage == null ? null : row8.errorMessage;
                nb_line_tFileOutputDelimited_1++;
                resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);
                                       CsvWritertFileOutputDelimited_1.writeNext(rowtFileOutputDelimited_1);




 


	tos_count_tFileOutputDelimited_1++;

/**
 * [tFileOutputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_end ] stop
 */

} // End of branch "row8"




	
	/**
	 * [tDBOutput_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	

 



/**
 * [tDBOutput_4 process_data_end ] stop
 */

} // End of branch "attestation_rejects_structure"




	
	/**
	 * [tMap_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */

} // End of branch "row2"




	
	/**
	 * [tFileInputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_end ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	


				nb_line_tFileInputDelimited_1++;
			}
			
			}finally{
    			if(!(filename_tFileInputDelimited_1 instanceof java.io.InputStream)){
    				if(csvReadertFileInputDelimited_1!=null){
    					csvReadertFileInputDelimited_1.close();
    				}
    			}
    			if(csvReadertFileInputDelimited_1!=null){
    				globalMap.put("tFileInputDelimited_1_NB_LINE",nb_line_tFileInputDelimited_1);
    			}
				
			}
						  

 

ok_Hash.put("tFileInputDelimited_1", true);
end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());




/**
 * [tFileInputDelimited_1 end ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row1");
			  	}
			  	
 

ok_Hash.put("tSchemaComplianceCheck_1", true);
end_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());




/**
 * [tSchemaComplianceCheck_1 end ] stop
 */

	
	/**
	 * [tLogRow_2 end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	


//////
//////
globalMap.put("tLogRow_2_NB_LINE",nb_line_tLogRow_2);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row3");
			  	}
			  	
 

ok_Hash.put("tLogRow_2", true);
end_Hash.put("tLogRow_2", System.currentTimeMillis());




/**
 * [tLogRow_2 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row5");
			  	}
			  	
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tDBOutput_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



        if(pstmtUpdate_tDBOutput_1 != null){
            pstmtUpdate_tDBOutput_1.close();
            resourceMap.remove("pstmtUpdate_tDBOutput_1");
        }
        if(pstmtInsert_tDBOutput_1 != null){
            pstmtInsert_tDBOutput_1.close();
            resourceMap.remove("pstmtInsert_tDBOutput_1");
        }
        if(pstmt_tDBOutput_1 != null) {
            pstmt_tDBOutput_1.close();
            resourceMap.remove("pstmt_tDBOutput_1");
        }
    resourceMap.put("statementClosed_tDBOutput_1", true);
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
			}
			conn_tDBOutput_1.commit();
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
				rowsToCommitCount_tDBOutput_1 = 0;
			}
			commitCounter_tDBOutput_1 = 0;
		
    	conn_tDBOutput_1 .close();
    	
    	resourceMap.put("finish_tDBOutput_1", true);
    	

	nb_line_deleted_tDBOutput_1=nb_line_deleted_tDBOutput_1+ deletedCount_tDBOutput_1;
	nb_line_update_tDBOutput_1=nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
	nb_line_inserted_tDBOutput_1=nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
	nb_line_rejected_tDBOutput_1=nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;
	
        globalMap.put("tDBOutput_1_NB_LINE",nb_line_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_UPDATED",nb_line_update_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_DELETED",nb_line_deleted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"attestation_valide");
			  	}
			  	
 

ok_Hash.put("tDBOutput_1", true);
end_Hash.put("tDBOutput_1", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tFileCopy_1Process(globalMap);



/**
 * [tDBOutput_1 end ] stop
 */




	
	/**
	 * [tDBOutput_2 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



	    try {
				int countSum_tDBOutput_2 = 0;
				if (pstmt_tDBOutput_2 != null && batchSizeCounter_tDBOutput_2 > 0) {
						
					for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				}
		    	
		    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
	    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
	    	String errormessage_tDBOutput_2;
			if (ne_tDBOutput_2 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
				errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
			}else{
				errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
			}
	    	
	    	int countSum_tDBOutput_2 = 0;
			for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
				countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
			}
			rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
			
	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
	    	
	    	System.err.println(errormessage_tDBOutput_2);
	    	
		}
	    
        if(pstmt_tDBOutput_2 != null) {
        		
            pstmt_tDBOutput_2.close();
            resourceMap.remove("pstmt_tDBOutput_2");
        }
    resourceMap.put("statementClosed_tDBOutput_2", true);
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
			}
			conn_tDBOutput_2.commit();
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
				rowsToCommitCount_tDBOutput_2 = 0;
			}
			commitCounter_tDBOutput_2 = 0;
		
    	conn_tDBOutput_2 .close();
    	
    	resourceMap.put("finish_tDBOutput_2", true);
    	

	nb_line_deleted_tDBOutput_2=nb_line_deleted_tDBOutput_2+ deletedCount_tDBOutput_2;
	nb_line_update_tDBOutput_2=nb_line_update_tDBOutput_2 + updatedCount_tDBOutput_2;
	nb_line_inserted_tDBOutput_2=nb_line_inserted_tDBOutput_2 + insertedCount_tDBOutput_2;
	nb_line_rejected_tDBOutput_2=nb_line_rejected_tDBOutput_2 + rejectedCount_tDBOutput_2;
	
        globalMap.put("tDBOutput_2_NB_LINE",nb_line_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_UPDATED",nb_line_update_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_DELETED",nb_line_deleted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_2);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"rejet_attestation");
			  	}
			  	
 

ok_Hash.put("tDBOutput_2", true);
end_Hash.put("tDBOutput_2", System.currentTimeMillis());




/**
 * [tDBOutput_2 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	



		
			
					if(outtFileOutputDelimited_3!=null) {
						outtFileOutputDelimited_3.flush();
						outtFileOutputDelimited_3.close();
					}
				
				globalMap.put("tFileOutputDelimited_3_NB_LINE",nb_line_tFileOutputDelimited_3);
				globalMap.put("tFileOutputDelimited_3_FILE_NAME",fileName_tFileOutputDelimited_3);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_3", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row10");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_3", true);
end_Hash.put("tFileOutputDelimited_3", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_3 end ] stop
 */













	
	/**
	 * [tLogRow_3 end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	


//////
//////
globalMap.put("tLogRow_3_NB_LINE",nb_line_tLogRow_3);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row4");
			  	}
			  	
 

ok_Hash.put("tLogRow_3", true);
end_Hash.put("tLogRow_3", System.currentTimeMillis());




/**
 * [tLogRow_3 end ] stop
 */

	
	/**
	 * [tMap_2 end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row6");
			  	}
			  	
 

ok_Hash.put("tMap_2", true);
end_Hash.put("tMap_2", System.currentTimeMillis());




/**
 * [tMap_2 end ] stop
 */

	
	/**
	 * [tDBOutput_3 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



	    try {
				int countSum_tDBOutput_3 = 0;
				if (pstmt_tDBOutput_3 != null && batchSizeCounter_tDBOutput_3 > 0) {
						
					for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				}
		    	
		    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
	    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
	    	String errormessage_tDBOutput_3;
			if (ne_tDBOutput_3 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
				errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
			}else{
				errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
			}
	    	
	    	int countSum_tDBOutput_3 = 0;
			for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
				countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
			}
			rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
			
	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
	    	
	    	System.err.println(errormessage_tDBOutput_3);
	    	
		}
	    
        if(pstmt_tDBOutput_3 != null) {
        		
            pstmt_tDBOutput_3.close();
            resourceMap.remove("pstmt_tDBOutput_3");
        }
    resourceMap.put("statementClosed_tDBOutput_3", true);
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
			}
			conn_tDBOutput_3.commit();
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
				rowsToCommitCount_tDBOutput_3 = 0;
			}
			commitCounter_tDBOutput_3 = 0;
		
    	conn_tDBOutput_3 .close();
    	
    	resourceMap.put("finish_tDBOutput_3", true);
    	

	nb_line_deleted_tDBOutput_3=nb_line_deleted_tDBOutput_3+ deletedCount_tDBOutput_3;
	nb_line_update_tDBOutput_3=nb_line_update_tDBOutput_3 + updatedCount_tDBOutput_3;
	nb_line_inserted_tDBOutput_3=nb_line_inserted_tDBOutput_3 + insertedCount_tDBOutput_3;
	nb_line_rejected_tDBOutput_3=nb_line_rejected_tDBOutput_3 + rejectedCount_tDBOutput_3;
	
        globalMap.put("tDBOutput_3_NB_LINE",nb_line_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_UPDATED",nb_line_update_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_DELETED",nb_line_deleted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_3);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"attestation_rejects_schema");
			  	}
			  	
 

ok_Hash.put("tDBOutput_3", true);
end_Hash.put("tDBOutput_3", System.currentTimeMillis());




/**
 * [tDBOutput_3 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	



		
			
					if(outtFileOutputDelimited_2!=null) {
						outtFileOutputDelimited_2.flush();
						outtFileOutputDelimited_2.close();
					}
				
				globalMap.put("tFileOutputDelimited_2_NB_LINE",nb_line_tFileOutputDelimited_2);
				globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_2", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row9");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_2", true);
end_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_2 end ] stop
 */
















	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row2");
			  	}
			  	
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());




/**
 * [tLogRow_1 end ] stop
 */

	
	/**
	 * [tMap_3 end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row7");
			  	}
			  	
 

ok_Hash.put("tMap_3", true);
end_Hash.put("tMap_3", System.currentTimeMillis());




/**
 * [tMap_3 end ] stop
 */

	
	/**
	 * [tDBOutput_4 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



	    try {
				int countSum_tDBOutput_4 = 0;
				if (pstmt_tDBOutput_4 != null && batchSizeCounter_tDBOutput_4 > 0) {
						
					for(int countEach_tDBOutput_4: pstmt_tDBOutput_4.executeBatch()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
					}
					rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
						
				}
		    	
		    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_4){
globalMap.put("tDBOutput_4_ERROR_MESSAGE",e_tDBOutput_4.getMessage());
	    	java.sql.SQLException ne_tDBOutput_4 = e_tDBOutput_4.getNextException(),sqle_tDBOutput_4=null;
	    	String errormessage_tDBOutput_4;
			if (ne_tDBOutput_4 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_4 = new java.sql.SQLException(e_tDBOutput_4.getMessage() + "\ncaused by: " + ne_tDBOutput_4.getMessage(), ne_tDBOutput_4.getSQLState(), ne_tDBOutput_4.getErrorCode(), ne_tDBOutput_4);
				errormessage_tDBOutput_4 = sqle_tDBOutput_4.getMessage();
			}else{
				errormessage_tDBOutput_4 = e_tDBOutput_4.getMessage();
			}
	    	
	    	int countSum_tDBOutput_4 = 0;
			for(int countEach_tDBOutput_4: e_tDBOutput_4.getUpdateCounts()) {
				countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0 : countEach_tDBOutput_4);
			}
			rowsToCommitCount_tDBOutput_4 += countSum_tDBOutput_4;
			
	    		insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
	    	
	    	System.err.println(errormessage_tDBOutput_4);
	    	
		}
	    
        if(pstmt_tDBOutput_4 != null) {
        		
            pstmt_tDBOutput_4.close();
            resourceMap.remove("pstmt_tDBOutput_4");
        }
    resourceMap.put("statementClosed_tDBOutput_4", true);
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
			}
			conn_tDBOutput_4.commit();
			if(rowsToCommitCount_tDBOutput_4 != 0){
				
				rowsToCommitCount_tDBOutput_4 = 0;
			}
			commitCounter_tDBOutput_4 = 0;
		
    	conn_tDBOutput_4 .close();
    	
    	resourceMap.put("finish_tDBOutput_4", true);
    	

	nb_line_deleted_tDBOutput_4=nb_line_deleted_tDBOutput_4+ deletedCount_tDBOutput_4;
	nb_line_update_tDBOutput_4=nb_line_update_tDBOutput_4 + updatedCount_tDBOutput_4;
	nb_line_inserted_tDBOutput_4=nb_line_inserted_tDBOutput_4 + insertedCount_tDBOutput_4;
	nb_line_rejected_tDBOutput_4=nb_line_rejected_tDBOutput_4 + rejectedCount_tDBOutput_4;
	
        globalMap.put("tDBOutput_4_NB_LINE",nb_line_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_UPDATED",nb_line_update_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_DELETED",nb_line_deleted_tDBOutput_4);
        globalMap.put("tDBOutput_4_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_4);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"attestation_rejects_structure");
			  	}
			  	
 

ok_Hash.put("tDBOutput_4", true);
end_Hash.put("tDBOutput_4", System.currentTimeMillis());




/**
 * [tDBOutput_4 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	



		
			
		
				
					if(CsvWritertFileOutputDelimited_1!=null) {
				    	CsvWritertFileOutputDelimited_1.close();
				    }
					
		    	globalMap.put("tFileOutputDelimited_1_NB_LINE",nb_line_tFileOutputDelimited_1);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_1", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row8");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_1", true);
end_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_1 end ] stop
 */












						if(execStat){
							runStat.updateStatOnConnection("iterate1", 2, "exec" + NB_ITERATE_tFileInputDelimited_1);
						}				
					




	
	/**
	 * [tFileList_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_end ] stop
 */
	
	/**
	 * [tFileList_1 end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

  
    }
  globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
  

  
 

 

ok_Hash.put("tFileList_1", true);
end_Hash.put("tFileList_1", System.currentTimeMillis());




/**
 * [tFileList_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileList_1 finally ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 finally ] stop
 */

	
	/**
	 * [tFileInputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 finally ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 finally ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 finally ] stop
 */

	
	/**
	 * [tLogRow_2 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tDBOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_1") == null) {
                java.sql.PreparedStatement pstmtUpdateToClose_tDBOutput_1 = null;
                if ((pstmtUpdateToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtUpdate_tDBOutput_1")) != null) {
                    pstmtUpdateToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtInsertToClose_tDBOutput_1 = null;
                if ((pstmtInsertToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtInsert_tDBOutput_1")) != null) {
                    pstmtInsertToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtToClose_tDBOutput_1 = null;
                if ((pstmtToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_1")) != null) {
                    pstmtToClose_tDBOutput_1.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_1") == null){
            java.sql.Connection ctn_tDBOutput_1 = null;
            if((ctn_tDBOutput_1 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_1")) != null){
                try {
                    ctn_tDBOutput_1.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_1) {
                    String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :" + sqlEx_tDBOutput_1.getMessage();
                    System.err.println(errorMessage_tDBOutput_1);
                }
            }
        }
    }
 



/**
 * [tDBOutput_1 finally ] stop
 */




	
	/**
	 * [tDBOutput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_2") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_2 = null;
                if ((pstmtToClose_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_2")) != null) {
                    pstmtToClose_tDBOutput_2.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_2") == null){
            java.sql.Connection ctn_tDBOutput_2 = null;
            if((ctn_tDBOutput_2 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_2")) != null){
                try {
                    ctn_tDBOutput_2.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_2) {
                    String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :" + sqlEx_tDBOutput_2.getMessage();
                    System.err.println(errorMessage_tDBOutput_2);
                }
            }
        }
    }
 



/**
 * [tDBOutput_2 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_3 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_3";

	


		if(resourceMap.get("finish_tFileOutputDelimited_3") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_3 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_3");
						if(outtFileOutputDelimited_3!=null) {
							outtFileOutputDelimited_3.flush();
							outtFileOutputDelimited_3.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_3 finally ] stop
 */













	
	/**
	 * [tLogRow_3 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 finally ] stop
 */

	
	/**
	 * [tMap_2 finally ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 finally ] stop
 */

	
	/**
	 * [tDBOutput_3 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_3") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_3 = null;
                if ((pstmtToClose_tDBOutput_3 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_3")) != null) {
                    pstmtToClose_tDBOutput_3.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_3") == null){
            java.sql.Connection ctn_tDBOutput_3 = null;
            if((ctn_tDBOutput_3 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_3")) != null){
                try {
                    ctn_tDBOutput_3.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_3) {
                    String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :" + sqlEx_tDBOutput_3.getMessage();
                    System.err.println(errorMessage_tDBOutput_3);
                }
            }
        }
    }
 



/**
 * [tDBOutput_3 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	


		if(resourceMap.get("finish_tFileOutputDelimited_2") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_2 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_2");
						if(outtFileOutputDelimited_2!=null) {
							outtFileOutputDelimited_2.flush();
							outtFileOutputDelimited_2.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_2 finally ] stop
 */
















	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */

	
	/**
	 * [tMap_3 finally ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 finally ] stop
 */

	
	/**
	 * [tDBOutput_4 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_4";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_4") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_4 = null;
                if ((pstmtToClose_tDBOutput_4 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_4")) != null) {
                    pstmtToClose_tDBOutput_4.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_4") == null){
            java.sql.Connection ctn_tDBOutput_4 = null;
            if((ctn_tDBOutput_4 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_4")) != null){
                try {
                    ctn_tDBOutput_4.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_4) {
                    String errorMessage_tDBOutput_4 = "failed to close the connection in tDBOutput_4 :" + sqlEx_tDBOutput_4.getMessage();
                    System.err.println(errorMessage_tDBOutput_4);
                }
            }
        }
    }
 



/**
 * [tDBOutput_4 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	


		if(resourceMap.get("finish_tFileOutputDelimited_1") == null){ 
			
				
			
					com.talend.csv.CSVWriter CsvWritertFileOutputDelimited_1 = (com.talend.csv.CSVWriter)resourceMap.get("CsvWriter_tFileOutputDelimited_1");
					
						if(CsvWritertFileOutputDelimited_1!=null) {
					    	CsvWritertFileOutputDelimited_1.close();
					    }
						
			
		}
	

 



/**
 * [tFileOutputDelimited_1 finally ] stop
 */















				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileList_1_SUBPROCESS_STATE", 1);
	}
	

public void tFileCopy_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [tFileCopy_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileCopy_1", false);
		start_Hash.put("tFileCopy_1", System.currentTimeMillis());
		
	
	currentComponent="tFileCopy_1";

	
		int tos_count_tFileCopy_1 = 0;
		

 



/**
 * [tFileCopy_1 begin ] stop
 */
	
	/**
	 * [tFileCopy_1 main ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 


        String srcFileName_tFileCopy_1 = ((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"));

		java.io.File srcFile_tFileCopy_1 = new java.io.File(srcFileName_tFileCopy_1);

		// here need check first, before mkdirs().
		if (!srcFile_tFileCopy_1.exists() || !srcFile_tFileCopy_1.isFile()) {
			String errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1 = String.format("The source File \"%s\" does not exist or is not a file.", srcFileName_tFileCopy_1);
				System.err.println(errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
				globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
		}
        String desDirName_tFileCopy_1 = "/var/hubasac/archives/attestation";

		String desFileName_tFileCopy_1 =  srcFile_tFileCopy_1.getName() ;

		if (desFileName_tFileCopy_1 != null && ("").equals(desFileName_tFileCopy_1.trim())){
			desFileName_tFileCopy_1 = "NewName.temp";
		}

		java.io.File desFile_tFileCopy_1 = new java.io.File(desDirName_tFileCopy_1, desFileName_tFileCopy_1);

		if (!srcFile_tFileCopy_1.getPath().equals(desFile_tFileCopy_1.getPath())  ) {
				java.io.File parentFile_tFileCopy_1 = desFile_tFileCopy_1.getParentFile();

				if (parentFile_tFileCopy_1 != null && !parentFile_tFileCopy_1.exists()) {
					parentFile_tFileCopy_1.mkdirs();
				}           
				try {
					org.talend.FileCopy.copyFile(srcFile_tFileCopy_1.getPath(), desFile_tFileCopy_1.getPath(), true, false);
				} catch (Exception e) {
globalMap.put("tFileCopy_1_ERROR_MESSAGE",e.getMessage());
						System.err.println("tFileCopy_1 " + e.getMessage());
				}
				java.io.File isRemoved_tFileCopy_1 = new java.io.File(((String) globalMap.get("tFileList_1_CURRENT_FILEPATH")));
				if(isRemoved_tFileCopy_1.exists()) {
					String errorMessageCouldNotRemoveFile_tFileCopy_1 = String.format("tFileCopy_1 - The source file \"%s\" could not be removed from the folder because it is open or you only have read-only rights.", srcFileName_tFileCopy_1);
						System.err.println(errorMessageCouldNotRemoveFile_tFileCopy_1 + "\n");
						globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageCouldNotRemoveFile_tFileCopy_1);
				} 

		}
		globalMap.put("tFileCopy_1_DESTINATION_FILEPATH",desFile_tFileCopy_1.getPath()); 
		globalMap.put("tFileCopy_1_DESTINATION_FILENAME",desFile_tFileCopy_1.getName()); 

		globalMap.put("tFileCopy_1_SOURCE_DIRECTORY", srcFile_tFileCopy_1.getParent());
		globalMap.put("tFileCopy_1_DESTINATION_DIRECTORY", desFile_tFileCopy_1.getParent());

 


	tos_count_tFileCopy_1++;

/**
 * [tFileCopy_1 main ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_end ] stop
 */
	
	/**
	 * [tFileCopy_1 end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 

ok_Hash.put("tFileCopy_1", true);
end_Hash.put("tFileCopy_1", System.currentTimeMillis());




/**
 * [tFileCopy_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileCopy_1 finally ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "Default";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";
    
    private boolean enableLogStash;

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    protected PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final extract_attestation extract_attestationClass = new extract_attestation();

        int exitCode = extract_attestationClass.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";
	   	
        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }
        enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

    	
    	

        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }
        boolean inOSGi = routines.system.BundleUtils.inOSGi();

        if (inOSGi) {
            java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

            if (jobProperties != null && jobProperties.get("context") != null) {
                contextStr = (String)jobProperties.get("context");
            }
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = extract_attestation.class.getClassLoader().getResourceAsStream("extracteur_hubasac_backup/extract_attestation_1_5/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = extract_attestation.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                try {
                    //defaultProps is in order to keep the original context value
                    if(context != null && context.isEmpty()) {
	                defaultProps.load(inContext);
	                context = new ContextProperties(defaultProps);
                    }
                } finally {
                    inContext.close();
                }
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
            class ContextProcessing {
                private void processContext_0() {
                        context.setContextType("attestation_FieldSeparator", "id_String");
                        if(context.getStringValue("attestation_FieldSeparator") == null) {
                            context.attestation_FieldSeparator = null;
                        } else {
                            context.attestation_FieldSeparator=(String) context.getProperty("attestation_FieldSeparator");
                        }
                        context.setContextType("attestation_RowSeparator", "id_String");
                        if(context.getStringValue("attestation_RowSeparator") == null) {
                            context.attestation_RowSeparator = null;
                        } else {
                            context.attestation_RowSeparator=(String) context.getProperty("attestation_RowSeparator");
                        }
                        context.setContextType("attestation_Header", "id_Integer");
                        if(context.getStringValue("attestation_Header") == null) {
                            context.attestation_Header = null;
                        } else {
                            try{
                                context.attestation_Header=routines.system.ParserUtils.parseTo_Integer (context.getProperty("attestation_Header"));
                            } catch(NumberFormatException e){
                                System.err.println(String.format("Null value will be used for context parameter %s: %s", "attestation_Header", e.getMessage()));
                                context.attestation_Header=null;
                            }
                        }
                        context.setContextType("attestation_Encoding", "id_String");
                        if(context.getStringValue("attestation_Encoding") == null) {
                            context.attestation_Encoding = null;
                        } else {
                            context.attestation_Encoding=(String) context.getProperty("attestation_Encoding");
                        }
                        context.setContextType("attestation_File", "id_File");
                        if(context.getStringValue("attestation_File") == null) {
                            context.attestation_File = null;
                        } else {
                            context.attestation_File=(String) context.getProperty("attestation_File");
                        }
                        context.setContextType("pg_connexion_Port", "id_String");
                        if(context.getStringValue("pg_connexion_Port") == null) {
                            context.pg_connexion_Port = null;
                        } else {
                            context.pg_connexion_Port=(String) context.getProperty("pg_connexion_Port");
                        }
                        context.setContextType("pg_connexion_Login", "id_String");
                        if(context.getStringValue("pg_connexion_Login") == null) {
                            context.pg_connexion_Login = null;
                        } else {
                            context.pg_connexion_Login=(String) context.getProperty("pg_connexion_Login");
                        }
                        context.setContextType("pg_connexion_AdditionalParams", "id_String");
                        if(context.getStringValue("pg_connexion_AdditionalParams") == null) {
                            context.pg_connexion_AdditionalParams = null;
                        } else {
                            context.pg_connexion_AdditionalParams=(String) context.getProperty("pg_connexion_AdditionalParams");
                        }
                        context.setContextType("pg_connexion_Schema", "id_String");
                        if(context.getStringValue("pg_connexion_Schema") == null) {
                            context.pg_connexion_Schema = null;
                        } else {
                            context.pg_connexion_Schema=(String) context.getProperty("pg_connexion_Schema");
                        }
                        context.setContextType("pg_connexion_Server", "id_String");
                        if(context.getStringValue("pg_connexion_Server") == null) {
                            context.pg_connexion_Server = null;
                        } else {
                            context.pg_connexion_Server=(String) context.getProperty("pg_connexion_Server");
                        }
                        context.setContextType("pg_connexion_Password", "id_Password");
                        if(context.getStringValue("pg_connexion_Password") == null) {
                            context.pg_connexion_Password = null;
                        } else {
                            String pwd_pg_connexion_Password_value = context.getProperty("pg_connexion_Password");
                            context.pg_connexion_Password = null;
                            if(pwd_pg_connexion_Password_value!=null) {
                                if(context_param.containsKey("pg_connexion_Password")) {//no need to decrypt if it come from program argument or parent job runtime
                                    context.pg_connexion_Password = pwd_pg_connexion_Password_value;
                                } else if (!pwd_pg_connexion_Password_value.isEmpty()) {
                                    try {
                                        context.pg_connexion_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_pg_connexion_Password_value);
                                        context.put("pg_connexion_Password",context.pg_connexion_Password);
                                    } catch (java.lang.RuntimeException e) {
                                        //do nothing
                                    }
                                }
                            }
                        }
                        context.setContextType("pg_connexion_Database", "id_String");
                        if(context.getStringValue("pg_connexion_Database") == null) {
                            context.pg_connexion_Database = null;
                        } else {
                            context.pg_connexion_Database=(String) context.getProperty("pg_connexion_Database");
                        }
                } 
                public void processAllContext() {
                        processContext_0();
                }
            }

            new ContextProcessing().processAllContext();
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }

        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("attestation_FieldSeparator")) {
                context.attestation_FieldSeparator = (String) parentContextMap.get("attestation_FieldSeparator");
            }if (parentContextMap.containsKey("attestation_RowSeparator")) {
                context.attestation_RowSeparator = (String) parentContextMap.get("attestation_RowSeparator");
            }if (parentContextMap.containsKey("attestation_Header")) {
                context.attestation_Header = (Integer) parentContextMap.get("attestation_Header");
            }if (parentContextMap.containsKey("attestation_Encoding")) {
                context.attestation_Encoding = (String) parentContextMap.get("attestation_Encoding");
            }if (parentContextMap.containsKey("attestation_File")) {
                context.attestation_File = (String) parentContextMap.get("attestation_File");
            }if (parentContextMap.containsKey("pg_connexion_Port")) {
                context.pg_connexion_Port = (String) parentContextMap.get("pg_connexion_Port");
            }if (parentContextMap.containsKey("pg_connexion_Login")) {
                context.pg_connexion_Login = (String) parentContextMap.get("pg_connexion_Login");
            }if (parentContextMap.containsKey("pg_connexion_AdditionalParams")) {
                context.pg_connexion_AdditionalParams = (String) parentContextMap.get("pg_connexion_AdditionalParams");
            }if (parentContextMap.containsKey("pg_connexion_Schema")) {
                context.pg_connexion_Schema = (String) parentContextMap.get("pg_connexion_Schema");
            }if (parentContextMap.containsKey("pg_connexion_Server")) {
                context.pg_connexion_Server = (String) parentContextMap.get("pg_connexion_Server");
            }if (parentContextMap.containsKey("pg_connexion_Password")) {
                context.pg_connexion_Password = (java.lang.String) parentContextMap.get("pg_connexion_Password");
            }if (parentContextMap.containsKey("pg_connexion_Database")) {
                context.pg_connexion_Database = (String) parentContextMap.get("pg_connexion_Database");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("pg_connexion_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();


this.globalResumeTicket = true;//to run tPreJob





this.globalResumeTicket = false;//to run others jobs

try {
errorCode = null;tFileList_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tFileList_1) {
globalMap.put("tFileList_1_SUBPROCESS_STATE", -1);

e_tFileList_1.printStackTrace();

}

this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : extract_attestation");
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;


    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {


    }














    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();






        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        } else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {//for trunjob call
		    final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     441960 characters generated by Talend Open Studio for ESB 
 *     on the 9 avril 2023 à 07:34:47 CEST
 ************************************************************************************************/