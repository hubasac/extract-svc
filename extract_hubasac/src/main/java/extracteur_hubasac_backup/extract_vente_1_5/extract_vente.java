// ============================================================================
//
// Copyright (c) 2006-2015, Talend SA
//
// Ce code source a été automatiquement généré par_Talend Open Studio for ESB
// / Soumis à la Licence Apache, Version 2.0 (la "Licence") ;
// votre utilisation de ce fichier doit respecter les termes de la Licence.
// Vous pouvez obtenir une copie de la Licence sur
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Sauf lorsqu'explicitement prévu par la loi en vigueur ou accepté par écrit, le logiciel
// distribué sous la Licence est distribué "TEL QUEL",
// SANS GARANTIE OU CONDITION D'AUCUNE SORTE, expresse ou implicite.
// Consultez la Licence pour connaître la terminologie spécifique régissant les autorisations et
// les limites prévues par la Licence.


package extracteur_hubasac_backup.extract_vente_1_5;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 





@SuppressWarnings("unused")

/**
 * Job: extract_vente Purpose: <br>
 * Description:  <br>
 * @author user@talend.com
 * @version 8.0.1.20211109_1610
 * @status DEV
 */
public class extract_vente implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
			if(vente_FieldSeparator != null){
				
					this.setProperty("vente_FieldSeparator", vente_FieldSeparator.toString());
				
			}
			
			if(vente_Header != null){
				
					this.setProperty("vente_Header", vente_Header.toString());
				
			}
			
			if(vente_Encoding != null){
				
					this.setProperty("vente_Encoding", vente_Encoding.toString());
				
			}
			
			if(vente_File != null){
				
					this.setProperty("vente_File", vente_File.toString());
				
			}
			
			if(vente_RowSeparator != null){
				
					this.setProperty("vente_RowSeparator", vente_RowSeparator.toString());
				
			}
			
			if(pg_connexion_Port != null){
				
					this.setProperty("pg_connexion_Port", pg_connexion_Port.toString());
				
			}
			
			if(pg_connexion_Login != null){
				
					this.setProperty("pg_connexion_Login", pg_connexion_Login.toString());
				
			}
			
			if(pg_connexion_AdditionalParams != null){
				
					this.setProperty("pg_connexion_AdditionalParams", pg_connexion_AdditionalParams.toString());
				
			}
			
			if(pg_connexion_Schema != null){
				
					this.setProperty("pg_connexion_Schema", pg_connexion_Schema.toString());
				
			}
			
			if(pg_connexion_Server != null){
				
					this.setProperty("pg_connexion_Server", pg_connexion_Server.toString());
				
			}
			
			if(pg_connexion_Password != null){
				
					this.setProperty("pg_connexion_Password", pg_connexion_Password.toString());
				
			}
			
			if(pg_connexion_Database != null){
				
					this.setProperty("pg_connexion_Database", pg_connexion_Database.toString());
				
			}
			
		}
		
		//if the stored or passed value is "<TALEND_NULL>" string, it mean null
		public String getStringValue(String key) {
			String origin_value = this.getProperty(key);
			if(NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY.equals(origin_value)) {
				return null;
			}
			return origin_value;
		}

public String vente_FieldSeparator;
public String getVente_FieldSeparator(){
	return this.vente_FieldSeparator;
}
public Integer vente_Header;
public Integer getVente_Header(){
	return this.vente_Header;
}
public String vente_Encoding;
public String getVente_Encoding(){
	return this.vente_Encoding;
}
		public String vente_File;
		public String getVente_File(){
			return this.vente_File;
		}
		
public String vente_RowSeparator;
public String getVente_RowSeparator(){
	return this.vente_RowSeparator;
}
public String pg_connexion_Port;
public String getPg_connexion_Port(){
	return this.pg_connexion_Port;
}
public String pg_connexion_Login;
public String getPg_connexion_Login(){
	return this.pg_connexion_Login;
}
public String pg_connexion_AdditionalParams;
public String getPg_connexion_AdditionalParams(){
	return this.pg_connexion_AdditionalParams;
}
public String pg_connexion_Schema;
public String getPg_connexion_Schema(){
	return this.pg_connexion_Schema;
}
public String pg_connexion_Server;
public String getPg_connexion_Server(){
	return this.pg_connexion_Server;
}
public java.lang.String pg_connexion_Password;
public java.lang.String getPg_connexion_Password(){
	return this.pg_connexion_Password;
}
public String pg_connexion_Database;
public String getPg_connexion_Database(){
	return this.pg_connexion_Database;
}
	}
	protected ContextProperties context = new ContextProperties(); // will be instanciated by MS.
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "1.5";
	private final String jobName = "extract_vente";
	private final String projectName = "EXTRACTEUR_HUBASAC_BACKUP";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}
	
	public void setDataSourceReferences(List serviceReferences) throws Exception{
		
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		java.util.Map<String, javax.sql.DataSource> dataSources = new java.util.HashMap<String, javax.sql.DataSource>();
		
		for (java.util.Map.Entry<String, javax.sql.DataSource> entry : BundleUtils.getServices(serviceReferences,  javax.sql.DataSource.class).entrySet()) {
                    dataSources.put(entry.getKey(), entry.getValue());
                    talendDataSources.put(entry.getKey(), new routines.system.TalendDataSource(entry.getValue()));
		}

		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}


private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				extract_vente.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(extract_vente.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
			}
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void tFileList_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileInputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tSchemaComplianceCheck_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_4_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileCopy_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileCopy_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tLogRow_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBOutput_3_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputDelimited_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tFileList_1_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileList_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void tFileCopy_1_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
	

	public static class ContextBean {
		static String evaluate(String context, String contextExpression)
				throws IOException, javax.script.ScriptException {
			boolean isExpression = contextExpression.contains("+") || contextExpression.contains("(");
			final String prefix = isExpression ? "\"" : "";
			java.util.Properties defaultProps = new java.util.Properties();
			java.io.InputStream inContext = extract_vente.class.getClassLoader()
					.getResourceAsStream("extracteur_hubasac_backup/extract_vente_1_5/contexts/" + context + ".properties");
			if (inContext == null) {
				inContext = extract_vente.class.getClassLoader()
						.getResourceAsStream("config/contexts/" + context + ".properties");
			}
			try {
			    defaultProps.load(inContext);
			} finally {
			    inContext.close();
			}
			java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("context.([\\w]+)");
			java.util.regex.Matcher matcher = pattern.matcher(contextExpression);

			while (matcher.find()) {
				contextExpression = contextExpression.replaceAll(matcher.group(0),
						prefix + defaultProps.getProperty(matcher.group(1)) + prefix);
			}
			if (contextExpression.startsWith("/services")) {
				contextExpression = contextExpression.replaceFirst("/services","");
            }
			return isExpression ? evaluateContextExpression(contextExpression) : contextExpression;
		}

		public static String evaluateContextExpression(String expression) throws javax.script.ScriptException {
			javax.script.ScriptEngineManager manager = new javax.script.ScriptEngineManager();
			javax.script.ScriptEngine engine = manager.getEngineByName("nashorn");
			// Add some import for Java
			expression = expression.replaceAll("System.getProperty", "java.lang.System.getProperty");
			return engine.eval(expression).toString();
		}

        public static String getContext(String context, String contextName, String jobName) throws Exception {

            String currentContext = null;
            String jobNameStripped = jobName.substring(jobName.lastIndexOf(".") + 1);

            boolean inOSGi = routines.system.BundleUtils.inOSGi();

            if (inOSGi) {
                java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobNameStripped);

                if (jobProperties != null) {
                    currentContext = (String)jobProperties.get("context");
                }
            }

            return contextName.contains("context.") ? evaluate(currentContext == null ? context : currentContext, contextName) : contextName;
        }
    }








public static class row6Struct implements routines.system.IPersistableRow<row6Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row6Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class Vente_valideStruct implements routines.system.IPersistableRow<Vente_valideStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public int c_status;

				public int getC_status () {
					return this.c_status;
				}
				
			    public java.util.Date c_date_mis_a_jour;

				public java.util.Date getC_date_mis_a_jour () {
					return this.c_date_mis_a_jour;
				}
				
			    public java.util.Date c_date_transfer;

				public java.util.Date getC_date_transfer () {
					return this.c_date_transfer;
				}
				
			    public String commentaires;

				public String getCommentaires () {
					return this.commentaires;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
			        this.c_status = dis.readInt();
					
					this.c_date_mis_a_jour = readDate(dis);
					
					this.c_date_transfer = readDate(dis);
					
					this.commentaires = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// int
				
		            	dos.writeInt(this.c_status);
					
					// java.util.Date
				
						writeDate(this.c_date_mis_a_jour,dos);
					
					// java.util.Date
				
						writeDate(this.c_date_transfer,dos);
					
					// String
				
						writeString(this.commentaires,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",c_status="+String.valueOf(c_status));
		sb.append(",c_date_mis_a_jour="+String.valueOf(c_date_mis_a_jour));
		sb.append(",c_date_transfer="+String.valueOf(c_date_transfer));
		sb.append(",commentaires="+commentaires);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(Vente_valideStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row5Struct implements routines.system.IPersistableRow<row5Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row5Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row10Struct implements routines.system.IPersistableRow<row10Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row10Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vente_rejects_schemaStruct implements routines.system.IPersistableRow<vente_rejects_schemaStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vente_rejects_schemaStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row7Struct implements routines.system.IPersistableRow<row7Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row7Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row3Struct implements routines.system.IPersistableRow<row3Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row3Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row4Struct implements routines.system.IPersistableRow<row4Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row4Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row9Struct implements routines.system.IPersistableRow<row9Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public Integer prime_nette_rc;

				public Integer getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
						this.prime_nette_rc = readInteger(dis);
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
						this.prime_nette_rc = readInteger(dis);
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// Integer
				
						writeInteger(this.prime_nette_rc,dos);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// Integer
				
						writeInteger(this.prime_nette_rc,dos);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row9Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class vente_rejects_structureStruct implements routines.system.IPersistableRow<vente_rejects_structureStruct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public Integer prime_nette_rc;

				public Integer getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public java.util.Date date_extraction;

				public java.util.Date getDate_extraction () {
					return this.date_extraction;
				}
				
			    public java.util.Date date_depot;

				public java.util.Date getDate_depot () {
					return this.date_depot;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}
	
	private java.util.Date readDate(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = unmarshaller.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(unmarshaller.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }
    
    private void writeDate(java.util.Date date1, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(date1 == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeLong(date1.getTime());
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
						this.prime_nette_rc = readInteger(dis);
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
						this.prime_nette_rc = readInteger(dis);
					
						this.dta = readInteger(dis);
					
					this.date_extraction = readDate(dis);
					
					this.date_depot = readDate(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// Integer
				
						writeInteger(this.prime_nette_rc,dos);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// Integer
				
						writeInteger(this.prime_nette_rc,dos);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// java.util.Date
				
						writeDate(this.date_extraction,dos);
					
					// java.util.Date
				
						writeDate(this.date_depot,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",date_extraction="+String.valueOf(date_extraction));
		sb.append(",date_depot="+String.valueOf(date_depot));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(vente_rejects_structureStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row8Struct implements routines.system.IPersistableRow<row8Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row8Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row2Struct implements routines.system.IPersistableRow<row2Struct> {
    final static byte[] commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];
    static byte[] commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[0];

	
			    public String numero_attestation;

				public String getNumero_attestation () {
					return this.numero_attestation;
				}
				
			    public String immatriculation;

				public String getImmatriculation () {
					return this.immatriculation;
				}
				
			    public String code_assure;

				public String getCode_assure () {
					return this.code_assure;
				}
				
			    public String code_assureur;

				public String getCode_assureur () {
					return this.code_assureur;
				}
				
			    public String code_intermediaire_dna;

				public String getCode_intermediaire_dna () {
					return this.code_intermediaire_dna;
				}
				
			    public int prime_nette_rc;

				public int getPrime_nette_rc () {
					return this.prime_nette_rc;
				}
				
			    public Integer dta;

				public Integer getDta () {
					return this.dta;
				}
				
			    public String errorCode;

				public String getErrorCode () {
					return this.errorCode;
				}
				
			    public String errorMessage;

				public String getErrorMessage () {
					return this.errorMessage;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}
	
	private String readString(org.jboss.marshalling.Unmarshaller unmarshaller) throws IOException{
		String strReturn = null;
		int length = 0;
        length = unmarshaller.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length) {
				if(length < 1024 && commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente.length == 0) {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[1024];
				} else {
   					commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente = new byte[2 * length];
   				}
			}
			unmarshaller.readFully(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length);
			strReturn = new String(commonByteArray_EXTRACTEUR_HUBASAC_BACKUP_extract_vente, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
    
    private void writeString(String str, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(str == null) {
			marshaller.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
            marshaller.writeInt(byteArray.length);
            marshaller.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}
	
	private Integer readInteger(org.jboss.marshalling.Unmarshaller dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}
	
	private void writeInteger(Integer intNum, org.jboss.marshalling.Marshaller marshaller) throws IOException{
		if(intNum == null) {
			marshaller.writeByte(-1);
		} else {
			marshaller.writeByte(0);
			marshaller.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }
    
    public void readData(org.jboss.marshalling.Unmarshaller dis) {

		synchronized(commonByteArrayLock_EXTRACTEUR_HUBASAC_BACKUP_extract_vente) {

        	try {

        		int length = 0;
		
					this.numero_attestation = readString(dis);
					
					this.immatriculation = readString(dis);
					
					this.code_assure = readString(dis);
					
					this.code_assureur = readString(dis);
					
					this.code_intermediaire_dna = readString(dis);
					
			        this.prime_nette_rc = dis.readInt();
					
						this.dta = readInteger(dis);
					
					this.errorCode = readString(dis);
					
					this.errorMessage = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }
    
    public void writeData(org.jboss.marshalling.Marshaller dos) {
        try {

		
					// String
				
						writeString(this.numero_attestation,dos);
					
					// String
				
						writeString(this.immatriculation,dos);
					
					// String
				
						writeString(this.code_assure,dos);
					
					// String
				
						writeString(this.code_assureur,dos);
					
					// String
				
						writeString(this.code_intermediaire_dna,dos);
					
					// int
				
		            	dos.writeInt(this.prime_nette_rc);
					
					// Integer
				
						writeInteger(this.dta,dos);
					
					// String
				
						writeString(this.errorCode,dos);
					
					// String
				
						writeString(this.errorMessage,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("numero_attestation="+numero_attestation);
		sb.append(",immatriculation="+immatriculation);
		sb.append(",code_assure="+code_assure);
		sb.append(",code_assureur="+code_assureur);
		sb.append(",code_intermediaire_dna="+code_intermediaire_dna);
		sb.append(",prime_nette_rc="+String.valueOf(prime_nette_rc));
		sb.append(",dta="+String.valueOf(dta));
		sb.append(",errorCode="+errorCode);
		sb.append(",errorMessage="+errorMessage);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row2Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tFileList_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileList_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row1Struct row1 = new row1Struct();
row3Struct row3 = new row3Struct();
row3Struct row5 = row3;
Vente_valideStruct Vente_valide = new Vente_valideStruct();
Vente_valideStruct row6 = Vente_valide;
row4Struct row4 = new row4Struct();
row4Struct row7 = row4;
vente_rejects_schemaStruct vente_rejects_schema = new vente_rejects_schemaStruct();
row10Struct row10 = new row10Struct();
row2Struct row2 = new row2Struct();
row2Struct row8 = row2;
vente_rejects_structureStruct vente_rejects_structure = new vente_rejects_structureStruct();
row9Struct row9 = new row9Struct();



	
	/**
	 * [tFileList_1 begin ] start
	 */

				
			int NB_ITERATE_tFileInputDelimited_1 = 0; //for statistics
			

	
		
		ok_Hash.put("tFileList_1", false);
		start_Hash.put("tFileList_1", System.currentTimeMillis());
		
	
	currentComponent="tFileList_1";

	
		int tos_count_tFileList_1 = 0;
		
	
 
     
    
  String directory_tFileList_1 = "/var/hubasac/inputfile";
  final java.util.List<String> maskList_tFileList_1 = new java.util.ArrayList<String>();
  final java.util.List<java.util.regex.Pattern> patternList_tFileList_1 = new java.util.ArrayList<java.util.regex.Pattern>(); 
    maskList_tFileList_1.add("vente*.txt");  
  for (final String filemask_tFileList_1 : maskList_tFileList_1) {
	String filemask_compile_tFileList_1 = filemask_tFileList_1;
	
		filemask_compile_tFileList_1 = org.apache.oro.text.GlobCompiler.globToPerl5(filemask_tFileList_1.toCharArray(), org.apache.oro.text.GlobCompiler.DEFAULT_MASK);
	
		java.util.regex.Pattern fileNamePattern_tFileList_1 = java.util.regex.Pattern.compile(filemask_compile_tFileList_1, java.util.regex.Pattern.CASE_INSENSITIVE);
	
	patternList_tFileList_1.add(fileNamePattern_tFileList_1);
  }
  int NB_FILEtFileList_1 = 0;

  final boolean case_sensitive_tFileList_1 = false;
	
	
	
    final java.util.List<java.io.File> list_tFileList_1 = new java.util.ArrayList<java.io.File>();
    final java.util.Set<String> filePath_tFileList_1 = new java.util.HashSet<String>();
	java.io.File file_tFileList_1 = new java.io.File(directory_tFileList_1);
    
		file_tFileList_1.listFiles(new java.io.FilenameFilter() {
			public boolean accept(java.io.File dir, String name) {
				java.io.File file = new java.io.File(dir, name);
				
	                if (!file.isDirectory()) {
						
    	String fileName_tFileList_1 = file.getName();
		for (final java.util.regex.Pattern fileNamePattern_tFileList_1 : patternList_tFileList_1) {
          	if (fileNamePattern_tFileList_1.matcher(fileName_tFileList_1).matches()){
					if(!filePath_tFileList_1.contains(file.getAbsolutePath())) {
			          list_tFileList_1.add(file);
			          filePath_tFileList_1.add(file.getAbsolutePath());
			        }
			}
		}
	                	return true;
	                } else {
	                  file.listFiles(this);
	                }
				
				return false;
			}
		}
		); 
      java.util.Collections.sort(list_tFileList_1);
    
    for (int i_tFileList_1 = 0; i_tFileList_1 < list_tFileList_1.size(); i_tFileList_1++){
      java.io.File files_tFileList_1 = list_tFileList_1.get(i_tFileList_1);
      String fileName_tFileList_1 = files_tFileList_1.getName();
      
      String currentFileName_tFileList_1 = files_tFileList_1.getName(); 
      String currentFilePath_tFileList_1 = files_tFileList_1.getAbsolutePath();
      String currentFileDirectory_tFileList_1 = files_tFileList_1.getParent();
      String currentFileExtension_tFileList_1 = null;
      
      if (files_tFileList_1.getName().contains(".") && files_tFileList_1.isFile()){
        currentFileExtension_tFileList_1 = files_tFileList_1.getName().substring(files_tFileList_1.getName().lastIndexOf(".") + 1);
      } else{
        currentFileExtension_tFileList_1 = "";
      }
      
      NB_FILEtFileList_1 ++;
      globalMap.put("tFileList_1_CURRENT_FILE", currentFileName_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEPATH", currentFilePath_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEDIRECTORY", currentFileDirectory_tFileList_1);
      globalMap.put("tFileList_1_CURRENT_FILEEXTENSION", currentFileExtension_tFileList_1);
      globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
      
 



/**
 * [tFileList_1 begin ] stop
 */
	
	/**
	 * [tFileList_1 main ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 


	tos_count_tFileList_1++;

/**
 * [tFileList_1 main ] stop
 */
	
	/**
	 * [tFileList_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_begin ] stop
 */
	NB_ITERATE_tFileInputDelimited_1++;
	
	
					if(execStat){				
	       				runStat.updateStatOnConnection("vente_rejects_structure", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row10", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("Vente_valide", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row6", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row7", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row2", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row9", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("vente_rejects_schema", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row4", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("OnComponentOk1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row5", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row8", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row1", 3, 0);
					}           			
				
					if(execStat){				
	       				runStat.updateStatOnConnection("row3", 3, 0);
					}           			
				
				if(execStat){
					runStat.updateStatOnConnection("iterate1", 1, "exec" + NB_ITERATE_tFileInputDelimited_1);
					//Thread.sleep(1000);
				}				
			






	
	/**
	 * [tDBOutput_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_1", false);
		start_Hash.put("tDBOutput_1", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row6");
					}
				
		int tos_count_tDBOutput_1 = 0;
		





String dbschema_tDBOutput_1 = null;
	dbschema_tDBOutput_1 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_1 = null;
if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
	tableName_tDBOutput_1 = ("vente");
} else {
	tableName_tDBOutput_1 = dbschema_tDBOutput_1 + "\".\"" + ("vente");
}

        int updateKeyCount_tDBOutput_1 = 2;
        if(updateKeyCount_tDBOutput_1 < 1) {
            throw new RuntimeException("For update, Schema must have a key");
        } else if (updateKeyCount_tDBOutput_1 == 11 && true) {
                    System.err.println("For update, every Schema column can not be a key");
        }

int nb_line_tDBOutput_1 = 0;
int nb_line_update_tDBOutput_1 = 0;
int nb_line_inserted_tDBOutput_1 = 0;
int nb_line_deleted_tDBOutput_1 = 0;
int nb_line_rejected_tDBOutput_1 = 0;

int deletedCount_tDBOutput_1=0;
int updatedCount_tDBOutput_1=0;
int insertedCount_tDBOutput_1=0;
int rowsToCommitCount_tDBOutput_1=0;
int rejectedCount_tDBOutput_1=0;

boolean whetherReject_tDBOutput_1 = false;

java.sql.Connection conn_tDBOutput_1 = null;
String dbUser_tDBOutput_1 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_1 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_1 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_1 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;

    conn_tDBOutput_1 = java.sql.DriverManager.getConnection(url_tDBOutput_1,dbUser_tDBOutput_1,dbPwd_tDBOutput_1);
	
	resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
        conn_tDBOutput_1.setAutoCommit(false);
        int commitEvery_tDBOutput_1 = 10000;
        int commitCounter_tDBOutput_1 = 0;



int count_tDBOutput_1=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_1 = conn_tDBOutput_1.getMetaData();
                                boolean whetherExist_tDBOutput_1 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_1 = dbMetaData_tDBOutput_1.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_1 = "public";
                                    if(dbschema_tDBOutput_1 == null || dbschema_tDBOutput_1.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_1 = conn_tDBOutput_1.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_1 = stmtSchema_tDBOutput_1.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_1.next()){
                                                defaultSchema_tDBOutput_1 = rsSchema_tDBOutput_1.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_1.next()) {
                                        String table_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_NAME");
                                        String schema_tDBOutput_1 = rsTable_tDBOutput_1.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_1.equals(("vente"))
                                            && (schema_tDBOutput_1.equals(dbschema_tDBOutput_1) || ((dbschema_tDBOutput_1 ==null || dbschema_tDBOutput_1.trim().length() ==0) && defaultSchema_tDBOutput_1.equals(schema_tDBOutput_1)))) {
                                            whetherExist_tDBOutput_1 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_1) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1.createStatement()) {
                                        stmtCreate_tDBOutput_1.execute("CREATE TABLE \"" + tableName_tDBOutput_1 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"code_intermediaire_dna\" VARCHAR(128)  ,\"prime_nette_rc\" INT4  not null ,\"dta\" INT4 ,\"c_status\" INT4 default 1  not null ,\"c_date_mis_a_jour\" TIMESTAMP ,\"c_date_transfer\" TIMESTAMP ,\"commentaires\" VARCHAR(128)  ,primary key(\"numero_attestation\",\"immatriculation\"))");
                                    }
                                }
	    java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1.prepareStatement("SELECT COUNT(1) FROM \"" + tableName_tDBOutput_1 + "\" WHERE \"numero_attestation\" = ? AND \"immatriculation\" = ?");
	    resourceMap.put("pstmt_tDBOutput_1", pstmt_tDBOutput_1);
	    String insert_tDBOutput_1 = "INSERT INTO \"" + tableName_tDBOutput_1 + "\" (\"numero_attestation\",\"immatriculation\",\"code_assure\",\"code_assureur\",\"code_intermediaire_dna\",\"prime_nette_rc\",\"dta\",\"c_status\",\"c_date_mis_a_jour\",\"c_date_transfer\",\"commentaires\") VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	    java.sql.PreparedStatement pstmtInsert_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(insert_tDBOutput_1);
	    resourceMap.put("pstmtInsert_tDBOutput_1", pstmtInsert_tDBOutput_1);
	    String update_tDBOutput_1 = "UPDATE \"" + tableName_tDBOutput_1 + "\" SET \"code_assure\" = ?,\"code_assureur\" = ?,\"code_intermediaire_dna\" = ?,\"prime_nette_rc\" = ?,\"dta\" = ?,\"c_status\" = ?,\"c_date_mis_a_jour\" = ?,\"c_date_transfer\" = ?,\"commentaires\" = ? WHERE \"numero_attestation\" = ? AND \"immatriculation\" = ?";
	    java.sql.PreparedStatement pstmtUpdate_tDBOutput_1 = conn_tDBOutput_1.prepareStatement(update_tDBOutput_1);
	    resourceMap.put("pstmtUpdate_tDBOutput_1", pstmtUpdate_tDBOutput_1);
	    

 



/**
 * [tDBOutput_1 begin ] stop
 */



	
	/**
	 * [tLogRow_4 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_4", false);
		start_Hash.put("tLogRow_4", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_4";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"Vente_valide");
					}
				
		int tos_count_tLogRow_4 = 0;
		

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_tLogRow_4 = "|";
		java.io.PrintStream consoleOut_tLogRow_4 = null;	

 		StringBuilder strBuffer_tLogRow_4 = null;
		int nb_line_tLogRow_4 = 0;
///////////////////////    			



 



/**
 * [tLogRow_4 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row5");
					}
				
		int tos_count_tMap_1 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
Vente_valideStruct Vente_valide_tmp = new Vente_valideStruct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tLogRow_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_2", false);
		start_Hash.put("tLogRow_2", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row3");
					}
				
		int tos_count_tLogRow_2 = 0;
		

	///////////////////////
	
         class Util_tLogRow_2 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[7];

        public void addRow(String[] row) {

            for (int i = 0; i < 7; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 6 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 6 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[6] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_2 util_tLogRow_2 = new Util_tLogRow_2();
        util_tLogRow_2.setTableName("tLogRow_2");
        util_tLogRow_2.addRow(new String[]{"numero_attestation","immatriculation","code_assure","code_assureur","code_intermediaire_dna","prime_nette_rc","dta",});        
 		StringBuilder strBuffer_tLogRow_2 = null;
		int nb_line_tLogRow_2 = 0;
///////////////////////    			



 



/**
 * [tLogRow_2 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_2", false);
		start_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row10");
					}
				
		int tos_count_tFileOutputDelimited_2 = 0;
		

String fileName_tFileOutputDelimited_2 = "";
    fileName_tFileOutputDelimited_2 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/vente_rejet_schema.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_2 = null;
    String extension_tFileOutputDelimited_2 = null;
    String directory_tFileOutputDelimited_2 = null;
    if((fileName_tFileOutputDelimited_2.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") < fileName_tFileOutputDelimited_2.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_2.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(0, fileName_tFileOutputDelimited_2.lastIndexOf("."));
            extension_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2.substring(fileName_tFileOutputDelimited_2.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_2 = fileName_tFileOutputDelimited_2;
            extension_tFileOutputDelimited_2 = "";
        }
        directory_tFileOutputDelimited_2 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_2 = true;
    java.io.File filetFileOutputDelimited_2 = new java.io.File(fileName_tFileOutputDelimited_2);
    globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
        if(filetFileOutputDelimited_2.exists()){
            isFileGenerated_tFileOutputDelimited_2 = false;
        }
            int nb_line_tFileOutputDelimited_2 = 0;
            int splitedFileNo_tFileOutputDelimited_2 = 0;
            int currentRow_tFileOutputDelimited_2 = 0;

            final String OUT_DELIM_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_2:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_2 = /** Start field tFileOutputDelimited_2:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_2:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_2 != null && directory_tFileOutputDelimited_2.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_2 = new java.io.File(directory_tFileOutputDelimited_2);
                        if(!dir_tFileOutputDelimited_2.exists()) {
                            dir_tFileOutputDelimited_2.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_2 = null;

                        outtFileOutputDelimited_2 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_2, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_2.length()==0){
                                        outtFileOutputDelimited_2.write("numero_attestation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("immatriculation");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_assure");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_assureur");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("code_intermediaire_dna");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("prime_nette_rc");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("dta");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_extraction");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("date_depot");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorCode");
                                            outtFileOutputDelimited_2.write(OUT_DELIM_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.write("errorMessage");
                                        outtFileOutputDelimited_2.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);
                                        outtFileOutputDelimited_2.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_2", outtFileOutputDelimited_2);
resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

 



/**
 * [tFileOutputDelimited_2 begin ] stop
 */



	
	/**
	 * [tDBOutput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_2", false);
		start_Hash.put("tDBOutput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vente_rejects_schema");
					}
				
		int tos_count_tDBOutput_2 = 0;
		





String dbschema_tDBOutput_2 = null;
	dbschema_tDBOutput_2 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_2 = null;
if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
	tableName_tDBOutput_2 = ("vente_rejects_schema_check");
} else {
	tableName_tDBOutput_2 = dbschema_tDBOutput_2 + "\".\"" + ("vente_rejects_schema_check");
}


int nb_line_tDBOutput_2 = 0;
int nb_line_update_tDBOutput_2 = 0;
int nb_line_inserted_tDBOutput_2 = 0;
int nb_line_deleted_tDBOutput_2 = 0;
int nb_line_rejected_tDBOutput_2 = 0;

int deletedCount_tDBOutput_2=0;
int updatedCount_tDBOutput_2=0;
int insertedCount_tDBOutput_2=0;
int rowsToCommitCount_tDBOutput_2=0;
int rejectedCount_tDBOutput_2=0;

boolean whetherReject_tDBOutput_2 = false;

java.sql.Connection conn_tDBOutput_2 = null;
String dbUser_tDBOutput_2 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_2 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_2 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_2 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;

    conn_tDBOutput_2 = java.sql.DriverManager.getConnection(url_tDBOutput_2,dbUser_tDBOutput_2,dbPwd_tDBOutput_2);
	
	resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);
        conn_tDBOutput_2.setAutoCommit(false);
        int commitEvery_tDBOutput_2 = 10000;
        int commitCounter_tDBOutput_2 = 0;


   int batchSize_tDBOutput_2 = 10000;
   int batchSizeCounter_tDBOutput_2=0;

int count_tDBOutput_2=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_2 = conn_tDBOutput_2.getMetaData();
                                boolean whetherExist_tDBOutput_2 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_2 = dbMetaData_tDBOutput_2.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_2 = "public";
                                    if(dbschema_tDBOutput_2 == null || dbschema_tDBOutput_2.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_2 = conn_tDBOutput_2.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_2 = stmtSchema_tDBOutput_2.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_2.next()){
                                                defaultSchema_tDBOutput_2 = rsSchema_tDBOutput_2.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_2.next()) {
                                        String table_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_NAME");
                                        String schema_tDBOutput_2 = rsTable_tDBOutput_2.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_2.equals(("vente_rejects_schema_check"))
                                            && (schema_tDBOutput_2.equals(dbschema_tDBOutput_2) || ((dbschema_tDBOutput_2 ==null || dbschema_tDBOutput_2.trim().length() ==0) && defaultSchema_tDBOutput_2.equals(schema_tDBOutput_2)))) {
                                            whetherExist_tDBOutput_2 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_2) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_2 = conn_tDBOutput_2.createStatement()) {
                                        stmtCreate_tDBOutput_2.execute("CREATE TABLE \"" + tableName_tDBOutput_2 + "\"(\"numero_attestation\" VARCHAR(128)   not null ,\"immatriculation\" VARCHAR(128)   not null ,\"code_assure\" VARCHAR(128)   not null ,\"code_assureur\" VARCHAR(128)   not null ,\"code_intermediaire_dna\" VARCHAR(128)  ,\"prime_nette_rc\" INT4  not null ,\"dta\" INT4 ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_2 = "INSERT INTO \"" + tableName_tDBOutput_2 + "\" (\"numero_attestation\",\"immatriculation\",\"code_assure\",\"code_assureur\",\"code_intermediaire_dna\",\"prime_nette_rc\",\"dta\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2.prepareStatement(insert_tDBOutput_2);
	    resourceMap.put("pstmt_tDBOutput_2", pstmt_tDBOutput_2);
	    

 



/**
 * [tDBOutput_2 begin ] stop
 */



	
	/**
	 * [tMap_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_2", false);
		start_Hash.put("tMap_2", System.currentTimeMillis());
		
	
	currentComponent="tMap_2";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row7");
					}
				
		int tos_count_tMap_2 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_2__Struct  {
}
Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
// ###############################

// ###############################
// # Outputs initialization
vente_rejects_schemaStruct vente_rejects_schema_tmp = new vente_rejects_schemaStruct();
// ###############################

        
        



        









 



/**
 * [tMap_2 begin ] stop
 */



	
	/**
	 * [tLogRow_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_3", false);
		start_Hash.put("tLogRow_3", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row4");
					}
				
		int tos_count_tLogRow_3 = 0;
		

	///////////////////////
	
         class Util_tLogRow_3 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[9];

        public void addRow(String[] row) {

            for (int i = 0; i < 9; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 8 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 8 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[8] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_3 util_tLogRow_3 = new Util_tLogRow_3();
        util_tLogRow_3.setTableName("tLogRow_3");
        util_tLogRow_3.addRow(new String[]{"numero_attestation","immatriculation","code_assure","code_assureur","code_intermediaire_dna","prime_nette_rc","dta","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_3 = null;
		int nb_line_tLogRow_3 = 0;
///////////////////////    			



 



/**
 * [tLogRow_3 begin ] stop
 */



	
	/**
	 * [tSchemaComplianceCheck_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tSchemaComplianceCheck_1", false);
		start_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());
		
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row1");
					}
				
		int tos_count_tSchemaComplianceCheck_1 = 0;
		

    class RowSetValueUtil_tSchemaComplianceCheck_1 {

        boolean ifPassedThrough = true;
        int errorCodeThrough = 0;
        String errorMessageThrough = "";
        int resultErrorCodeThrough = 0;
        String resultErrorMessageThrough = "";
        String tmpContentThrough = null;

        boolean ifPassed = true;
        int errorCode = 0;
        String errorMessage = "";

        void handleBigdecimalPrecision(String data, int iPrecision, int maxLength){
            //number of digits before the decimal point(ignoring frontend zeroes)
            int len1 = 0;
            int len2 = 0;
            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
            if(data.startsWith("-")){
                data = data.substring(1);
            }
            data = org.apache.commons.lang.StringUtils.stripStart(data, "0");

            if(data.indexOf(".") >= 0){
                len1 = data.indexOf(".");
                data = org.apache.commons.lang.StringUtils.stripEnd(data, "0");
                len2 = data.length() - (len1 + 1);
            }else{
                len1 = data.length();
            }

            if (iPrecision < len2) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|precision Non-matches";
            } else if (maxLength < len1 + iPrecision) {
                ifPassed = false;
                errorCode += 8;
                errorMessage += "|invalid Length setting is unsuitable for Precision";
            }
        }

        int handleErrorCode(int errorCode, int resultErrorCode){
            if (errorCode > 0) {
                if (resultErrorCode > 0) {
                    resultErrorCode = 16;
                } else {
                    resultErrorCode = errorCode;
                }
            }
            return resultErrorCode;
        }

        String handleErrorMessage(String errorMessage, String resultErrorMessage, String columnLabel){
            if (errorMessage.length() > 0) {
                if (resultErrorMessage.length() > 0) {
                    resultErrorMessage += ";"+ errorMessage.replaceFirst("\\|", columnLabel);
                } else {
                    resultErrorMessage = errorMessage.replaceFirst("\\|", columnLabel);
                }
            }
            return resultErrorMessage;
        }

        void reset(){
            ifPassedThrough = true;
            errorCodeThrough = 0;
            errorMessageThrough = "";
            resultErrorCodeThrough = 0;
            resultErrorMessageThrough = "";
            tmpContentThrough = null;

            ifPassed = true;
            errorCode = 0;
            errorMessage = "";
        }

        void setRowValue_0(row1Struct row1) {
    // validate nullable (empty as null)
    if ((row1.numero_attestation == null) || ("".equals(row1.numero_attestation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.numero_attestation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.numero_attestation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.numero_attestation != null
    ) {
        if (row1.numero_attestation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"numero_attestation:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.immatriculation == null) || ("".equals(row1.immatriculation))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.immatriculation != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.immatriculation);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.immatriculation != null
    ) {
        if (row1.immatriculation.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"immatriculation:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.code_assure == null) || ("".equals(row1.code_assure))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assure != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assure);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assure != null
    ) {
        if (row1.code_assure.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assure:");
            errorMessageThrough = "";
    // validate nullable (empty as null)
    if ((row1.code_assureur == null) || ("".equals(row1.code_assureur))) {
        ifPassedThrough = false;
        errorCodeThrough += 4;
        errorMessageThrough += "|empty or null";
    }    try {
        if(
        row1.code_assureur != null
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_assureur);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_assureur != null
    ) {
        if (row1.code_assureur.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_assureur:");
            errorMessageThrough = "";    try {
        if(
        row1.code_intermediaire_dna != null
        && (!"".equals(row1.code_intermediaire_dna))
        ) {
            String tester_tSchemaComplianceCheck_1 = String.valueOf(row1.code_intermediaire_dna);
        }
    } catch(java.lang.Exception e) {
globalMap.put("tSchemaComplianceCheck_1_ERROR_MESSAGE",e.getMessage());
        ifPassedThrough = false;
        errorCodeThrough += 2;
        errorMessageThrough += "|wrong type";
    }
    if (
    row1.code_intermediaire_dna != null
    && (!"".equals(row1.code_intermediaire_dna))
    ) {
        if (row1.code_intermediaire_dna.length() > 128) {
            ifPassedThrough = false;
            errorCodeThrough += 8;
            errorMessageThrough += "|exceed max length";
        }
    }
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"code_intermediaire_dna:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"prime_nette_rc:");
            errorMessageThrough = "";
            resultErrorCodeThrough = handleErrorCode(errorCodeThrough,resultErrorCodeThrough);
            errorCodeThrough = 0;
            resultErrorMessageThrough = handleErrorMessage(errorMessageThrough,resultErrorMessageThrough,"dta:");
            errorMessageThrough = "";
        }
    }
    RowSetValueUtil_tSchemaComplianceCheck_1 rsvUtil_tSchemaComplianceCheck_1 = new RowSetValueUtil_tSchemaComplianceCheck_1();

 



/**
 * [tSchemaComplianceCheck_1 begin ] stop
 */







	
	/**
	 * [tFileOutputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputDelimited_1", false);
		start_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row9");
					}
				
		int tos_count_tFileOutputDelimited_1 = 0;
		

String fileName_tFileOutputDelimited_1 = "";
    fileName_tFileOutputDelimited_1 = (new java.io.File("D:/Projet MOE/Tests/Data/rejets/vente_rejet_structure.txt")).getAbsolutePath().replace("\\","/");
    String fullName_tFileOutputDelimited_1 = null;
    String extension_tFileOutputDelimited_1 = null;
    String directory_tFileOutputDelimited_1 = null;
    if((fileName_tFileOutputDelimited_1.indexOf("/") != -1)) {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") < fileName_tFileOutputDelimited_1.lastIndexOf("/")) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        }
        directory_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("/"));
    } else {
        if(fileName_tFileOutputDelimited_1.lastIndexOf(".") != -1) {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(0, fileName_tFileOutputDelimited_1.lastIndexOf("."));
            extension_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1.substring(fileName_tFileOutputDelimited_1.lastIndexOf("."));
        } else {
            fullName_tFileOutputDelimited_1 = fileName_tFileOutputDelimited_1;
            extension_tFileOutputDelimited_1 = "";
        }
        directory_tFileOutputDelimited_1 = "";
    }
    boolean isFileGenerated_tFileOutputDelimited_1 = true;
    java.io.File filetFileOutputDelimited_1 = new java.io.File(fileName_tFileOutputDelimited_1);
    globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
        if(filetFileOutputDelimited_1.exists()){
            isFileGenerated_tFileOutputDelimited_1 = false;
        }
            int nb_line_tFileOutputDelimited_1 = 0;
            int splitedFileNo_tFileOutputDelimited_1 = 0;
            int currentRow_tFileOutputDelimited_1 = 0;

            final String OUT_DELIM_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:FIELDSEPARATOR */";"/** End field tFileOutputDelimited_1:FIELDSEPARATOR */;

            final String OUT_DELIM_ROWSEP_tFileOutputDelimited_1 = /** Start field tFileOutputDelimited_1:ROWSEPARATOR */"\n"/** End field tFileOutputDelimited_1:ROWSEPARATOR */;

                    //create directory only if not exists
                    if(directory_tFileOutputDelimited_1 != null && directory_tFileOutputDelimited_1.trim().length() != 0) {
                        java.io.File dir_tFileOutputDelimited_1 = new java.io.File(directory_tFileOutputDelimited_1);
                        if(!dir_tFileOutputDelimited_1.exists()) {
                            dir_tFileOutputDelimited_1.mkdirs();
                        }
                    }

                        //routines.system.Row
                        java.io.Writer outtFileOutputDelimited_1 = null;

                        outtFileOutputDelimited_1 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                        new java.io.FileOutputStream(fileName_tFileOutputDelimited_1, true),"ISO-8859-15"));
                                    if(filetFileOutputDelimited_1.length()==0){
                                        outtFileOutputDelimited_1.write("numero_attestation");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("immatriculation");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("code_assure");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("code_assureur");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("code_intermediaire_dna");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("prime_nette_rc");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("dta");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_extraction");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("date_depot");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorCode");
                                            outtFileOutputDelimited_1.write(OUT_DELIM_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.write("errorMessage");
                                        outtFileOutputDelimited_1.write(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);
                                        outtFileOutputDelimited_1.flush();
                                    }


        resourceMap.put("out_tFileOutputDelimited_1", outtFileOutputDelimited_1);
resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

 



/**
 * [tFileOutputDelimited_1 begin ] stop
 */



	
	/**
	 * [tDBOutput_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBOutput_3", false);
		start_Hash.put("tDBOutput_3", System.currentTimeMillis());
		
	
	currentComponent="tDBOutput_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"vente_rejects_structure");
					}
				
		int tos_count_tDBOutput_3 = 0;
		





String dbschema_tDBOutput_3 = null;
	dbschema_tDBOutput_3 = context.pg_connexion_Schema;
	

String tableName_tDBOutput_3 = null;
if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
	tableName_tDBOutput_3 = ("vente_rejects_structure_check");
} else {
	tableName_tDBOutput_3 = dbschema_tDBOutput_3 + "\".\"" + ("vente_rejects_structure_check");
}


int nb_line_tDBOutput_3 = 0;
int nb_line_update_tDBOutput_3 = 0;
int nb_line_inserted_tDBOutput_3 = 0;
int nb_line_deleted_tDBOutput_3 = 0;
int nb_line_rejected_tDBOutput_3 = 0;

int deletedCount_tDBOutput_3=0;
int updatedCount_tDBOutput_3=0;
int insertedCount_tDBOutput_3=0;
int rowsToCommitCount_tDBOutput_3=0;
int rejectedCount_tDBOutput_3=0;

boolean whetherReject_tDBOutput_3 = false;

java.sql.Connection conn_tDBOutput_3 = null;
String dbUser_tDBOutput_3 = null;

	
    java.lang.Class.forName("org.postgresql.Driver");
    
        String url_tDBOutput_3 = "jdbc:postgresql://"+context.pg_connexion_Server+":"+context.pg_connexion_Port+"/"+context.pg_connexion_Database + "?" + context.pg_connexion_AdditionalParams;
    dbUser_tDBOutput_3 = context.pg_connexion_Login;

	final String decryptedPassword_tDBOutput_3 = context.pg_connexion_Password; 

    String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;

    conn_tDBOutput_3 = java.sql.DriverManager.getConnection(url_tDBOutput_3,dbUser_tDBOutput_3,dbPwd_tDBOutput_3);
	
	resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);
        conn_tDBOutput_3.setAutoCommit(false);
        int commitEvery_tDBOutput_3 = 10000;
        int commitCounter_tDBOutput_3 = 0;


   int batchSize_tDBOutput_3 = 10000;
   int batchSizeCounter_tDBOutput_3=0;

int count_tDBOutput_3=0;
                                java.sql.DatabaseMetaData dbMetaData_tDBOutput_3 = conn_tDBOutput_3.getMetaData();
                                boolean whetherExist_tDBOutput_3 = false;
                                try (java.sql.ResultSet rsTable_tDBOutput_3 = dbMetaData_tDBOutput_3.getTables(null, null, null, new String[]{"TABLE"})) {
                                    String defaultSchema_tDBOutput_3 = "public";
                                    if(dbschema_tDBOutput_3 == null || dbschema_tDBOutput_3.trim().length() == 0) {
                                        try(java.sql.Statement stmtSchema_tDBOutput_3 = conn_tDBOutput_3.createStatement();
                                            java.sql.ResultSet rsSchema_tDBOutput_3 = stmtSchema_tDBOutput_3.executeQuery("select current_schema() ")) {
                                            while(rsSchema_tDBOutput_3.next()){
                                                defaultSchema_tDBOutput_3 = rsSchema_tDBOutput_3.getString("current_schema");
                                            }
                                        }
                                    }
                                    while(rsTable_tDBOutput_3.next()) {
                                        String table_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_NAME");
                                        String schema_tDBOutput_3 = rsTable_tDBOutput_3.getString("TABLE_SCHEM");
                                        if(table_tDBOutput_3.equals(("vente_rejects_structure_check"))
                                            && (schema_tDBOutput_3.equals(dbschema_tDBOutput_3) || ((dbschema_tDBOutput_3 ==null || dbschema_tDBOutput_3.trim().length() ==0) && defaultSchema_tDBOutput_3.equals(schema_tDBOutput_3)))) {
                                            whetherExist_tDBOutput_3 = true;
                                            break;
                                        }
                                    }
                                }
                                if(!whetherExist_tDBOutput_3) {
                                    try (java.sql.Statement stmtCreate_tDBOutput_3 = conn_tDBOutput_3.createStatement()) {
                                        stmtCreate_tDBOutput_3.execute("CREATE TABLE \"" + tableName_tDBOutput_3 + "\"(\"numero_attestation\" VARCHAR(128)  ,\"immatriculation\" VARCHAR(128)  ,\"code_assure\" VARCHAR(128)  ,\"code_assureur\" VARCHAR(128)  ,\"code_intermediaire_dna\" VARCHAR(128)  ,\"prime_nette_rc\" INT4 ,\"dta\" INT4 ,\"date_extraction\" TIMESTAMP ,\"date_depot\" TIMESTAMP ,\"errorCode\" VARCHAR(255)  ,\"errorMessage\" VARCHAR(255)  )");
                                    }
                                }
	    String insert_tDBOutput_3 = "INSERT INTO \"" + tableName_tDBOutput_3 + "\" (\"numero_attestation\",\"immatriculation\",\"code_assure\",\"code_assureur\",\"code_intermediaire_dna\",\"prime_nette_rc\",\"dta\",\"date_extraction\",\"date_depot\",\"errorCode\",\"errorMessage\") VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	    
	    java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3.prepareStatement(insert_tDBOutput_3);
	    resourceMap.put("pstmt_tDBOutput_3", pstmt_tDBOutput_3);
	    

 



/**
 * [tDBOutput_3 begin ] stop
 */



	
	/**
	 * [tMap_3 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_3", false);
		start_Hash.put("tMap_3", System.currentTimeMillis());
		
	
	currentComponent="tMap_3";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row8");
					}
				
		int tos_count_tMap_3 = 0;
		




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_3__Struct  {
}
Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
// ###############################

// ###############################
// # Outputs initialization
vente_rejects_structureStruct vente_rejects_structure_tmp = new vente_rejects_structureStruct();
// ###############################

        
        



        









 



/**
 * [tMap_3 begin ] stop
 */



	
	/**
	 * [tLogRow_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tLogRow_1", false);
		start_Hash.put("tLogRow_1", System.currentTimeMillis());
		
	
	currentComponent="tLogRow_1";

	
					if(execStat) {
						runStat.updateStatOnConnection(resourceMap,iterateId,0,0,"row2");
					}
				
		int tos_count_tLogRow_1 = 0;
		

	///////////////////////
	
         class Util_tLogRow_1 {

        String[] des_top = { ".", ".", "-", "+" };

        String[] des_head = { "|=", "=|", "-", "+" };

        String[] des_bottom = { "'", "'", "-", "+" };

        String name="";

        java.util.List<String[]> list = new java.util.ArrayList<String[]>();

        int[] colLengths = new int[9];

        public void addRow(String[] row) {

            for (int i = 0; i < 9; i++) {
                if (row[i]!=null) {
                  colLengths[i] = Math.max(colLengths[i], row[i].length());
                }
            }
            list.add(row);
        }

        public void setTableName(String name) {

            this.name = name;
        }

            public StringBuilder format() {
            
                StringBuilder sb = new StringBuilder();
  
            
                    sb.append(print(des_top));
    
                    int totals = 0;
                    for (int i = 0; i < colLengths.length; i++) {
                        totals = totals + colLengths[i];
                    }
    
                    // name
                    sb.append("|");
                    int k = 0;
                    for (k = 0; k < (totals + 8 - name.length()) / 2; k++) {
                        sb.append(' ');
                    }
                    sb.append(name);
                    for (int i = 0; i < totals + 8 - name.length() - k; i++) {
                        sb.append(' ');
                    }
                    sb.append("|\n");

                    // head and rows
                    sb.append(print(des_head));
                    for (int i = 0; i < list.size(); i++) {
    
                        String[] row = list.get(i);
    
                        java.util.Formatter formatter = new java.util.Formatter(new StringBuilder());
                        
                        StringBuilder sbformat = new StringBuilder();                                             
        			        sbformat.append("|%1$-");
        			        sbformat.append(colLengths[0]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%2$-");
        			        sbformat.append(colLengths[1]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%3$-");
        			        sbformat.append(colLengths[2]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%4$-");
        			        sbformat.append(colLengths[3]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%5$-");
        			        sbformat.append(colLengths[4]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%6$-");
        			        sbformat.append(colLengths[5]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%7$-");
        			        sbformat.append(colLengths[6]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%8$-");
        			        sbformat.append(colLengths[7]);
        			        sbformat.append("s");
        			              
        			        sbformat.append("|%9$-");
        			        sbformat.append(colLengths[8]);
        			        sbformat.append("s");
        			                      
                        sbformat.append("|\n");                    
       
                        formatter.format(sbformat.toString(), (Object[])row);	
                                
                        sb.append(formatter.toString());
                        if (i == 0)
                            sb.append(print(des_head)); // print the head
                    }
    
                    // end
                    sb.append(print(des_bottom));
                    return sb;
                }
            

            private StringBuilder print(String[] fillChars) {
                StringBuilder sb = new StringBuilder();
                //first column
                sb.append(fillChars[0]);                
                    for (int i = 0; i < colLengths[0] - fillChars[0].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);	                

                    for (int i = 0; i < colLengths[1] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[2] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[3] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[4] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[5] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[6] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                    for (int i = 0; i < colLengths[7] - fillChars[3].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }
                    sb.append(fillChars[3]);
                
                    //last column
                    for (int i = 0; i < colLengths[8] - fillChars[1].length() + 1; i++) {
                        sb.append(fillChars[2]);
                    }         
                sb.append(fillChars[1]);
                sb.append("\n");               
                return sb;
            }
            
            public boolean isTableEmpty(){
            	if (list.size() > 1)
            		return false;
            	return true;
            }
        }
        Util_tLogRow_1 util_tLogRow_1 = new Util_tLogRow_1();
        util_tLogRow_1.setTableName("tLogRow_1");
        util_tLogRow_1.addRow(new String[]{"numero_attestation","immatriculation","code_assure","code_assureur","code_intermediaire_dna","prime_nette_rc","dta","errorCode","errorMessage",});        
 		StringBuilder strBuffer_tLogRow_1 = null;
		int nb_line_tLogRow_1 = 0;
///////////////////////    			



 



/**
 * [tLogRow_1 begin ] stop
 */



	
	/**
	 * [tFileInputDelimited_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileInputDelimited_1", false);
		start_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());
		
	
	currentComponent="tFileInputDelimited_1";

	
		int tos_count_tFileInputDelimited_1 = 0;
		
	
	
	
 
	
	
	final routines.system.RowState rowstate_tFileInputDelimited_1 = new routines.system.RowState();
	
	
				int nb_line_tFileInputDelimited_1 = 0;
				int footer_tFileInputDelimited_1 = 0;
				int totalLinetFileInputDelimited_1 = 0;
				int limittFileInputDelimited_1 = -1;
				int lastLinetFileInputDelimited_1 = -1;	
				
				char fieldSeparator_tFileInputDelimited_1[] = null;
				
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.vente_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1 = ((String)context.vente_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
			
				char rowSeparator_tFileInputDelimited_1[] = null;
			
				//support passing value (property: Row Separator) by 'context.rs' or 'globalMap.get("rs")'. 
				if ( ((String)context.vente_RowSeparator).length() > 0 ){
					rowSeparator_tFileInputDelimited_1 = ((String)context.vente_RowSeparator).toCharArray();
				}else {
					throw new IllegalArgumentException("Row Separator must be assigned a char."); 
				}
			
				Object filename_tFileInputDelimited_1 = /** Start field tFileInputDelimited_1:FILENAME */((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"))/** End field tFileInputDelimited_1:FILENAME */;		
				com.talend.csv.CSVReader csvReadertFileInputDelimited_1 = null;
	
				try{
					
						String[] rowtFileInputDelimited_1=null;
						int currentLinetFileInputDelimited_1 = 0;
	        			int outputLinetFileInputDelimited_1 = 0;
						try {//TD110 begin
							if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
							
			int footer_value_tFileInputDelimited_1 = 0;
			if(footer_value_tFileInputDelimited_1 > 0){
				throw new java.lang.Exception("When the input source is a stream,footer shouldn't be bigger than 0.");
			}
		
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.vente_Encoding);
							}else{
								csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.vente_Encoding);
		        			}
					
					
					csvReadertFileInputDelimited_1.setTrimWhitespace(false);
					if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )
	        			csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
	        				csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	            				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							      
		
			
						if(footer_tFileInputDelimited_1 > 0){
						for(totalLinetFileInputDelimited_1=0;totalLinetFileInputDelimited_1 < context.vente_Header; totalLinetFileInputDelimited_1++){
							csvReadertFileInputDelimited_1.readNext();
						}
						csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
			            while (csvReadertFileInputDelimited_1.readNext()) {
							
	                
	                		totalLinetFileInputDelimited_1++;
	                
							
	                
			            }
	            		int lastLineTemptFileInputDelimited_1 = totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1   < 0? 0 : totalLinetFileInputDelimited_1 - footer_tFileInputDelimited_1 ;
	            		if(lastLinetFileInputDelimited_1 > 0){
	                		lastLinetFileInputDelimited_1 = lastLinetFileInputDelimited_1 < lastLineTemptFileInputDelimited_1 ? lastLinetFileInputDelimited_1 : lastLineTemptFileInputDelimited_1; 
	            		}else {
	                		lastLinetFileInputDelimited_1 = lastLineTemptFileInputDelimited_1;
	            		}
	         
			          	csvReadertFileInputDelimited_1.close();
				        if(filename_tFileInputDelimited_1 instanceof java.io.InputStream){
				 			csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader((java.io.InputStream)filename_tFileInputDelimited_1, fieldSeparator_tFileInputDelimited_1[0], context.vente_Encoding);
		        		}else{
							csvReadertFileInputDelimited_1=new com.talend.csv.CSVReader(String.valueOf(filename_tFileInputDelimited_1),fieldSeparator_tFileInputDelimited_1[0], context.vente_Encoding);
						}
						csvReadertFileInputDelimited_1.setTrimWhitespace(false);
						if ( (rowSeparator_tFileInputDelimited_1[0] != '\n') && (rowSeparator_tFileInputDelimited_1[0] != '\r') )	
	        				csvReadertFileInputDelimited_1.setLineEnd(""+rowSeparator_tFileInputDelimited_1[0]);
						
							csvReadertFileInputDelimited_1.setQuoteChar('"');
						
	        				csvReadertFileInputDelimited_1.setEscapeChar(csvReadertFileInputDelimited_1.getQuoteChar());
							  
	        		}
	        
			        if(limittFileInputDelimited_1 != 0){
			        	for(currentLinetFileInputDelimited_1=0;currentLinetFileInputDelimited_1 < context.vente_Header;currentLinetFileInputDelimited_1++){
			        		csvReadertFileInputDelimited_1.readNext();
			        	}
			        }
			        csvReadertFileInputDelimited_1.setSkipEmptyRecords(false);
	        
	    		} catch(java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
					
						
						System.err.println(e.getMessage());
					
	    		}//TD110 end
	        
			    
	        	while ( limittFileInputDelimited_1 != 0 && csvReadertFileInputDelimited_1!=null && csvReadertFileInputDelimited_1.readNext() ) { 
	        		rowstate_tFileInputDelimited_1.reset();
	        
		        	rowtFileInputDelimited_1=csvReadertFileInputDelimited_1.getValues();
		        	
					
	        	
	        	
	        		currentLinetFileInputDelimited_1++;
	            
		            if(lastLinetFileInputDelimited_1 > -1 && currentLinetFileInputDelimited_1 > lastLinetFileInputDelimited_1) {
		                break;
	    	        }
	        	    outputLinetFileInputDelimited_1++;
	            	if (limittFileInputDelimited_1 > 0 && outputLinetFileInputDelimited_1 > limittFileInputDelimited_1) {
	                	break;
	            	}  
	                                                                      
					
	    							row1 = null;			
								
	    							row2 = null;			
								
								boolean whetherReject_tFileInputDelimited_1 = false;
								row1 = new row1Struct();
								try {			
									
				char fieldSeparator_tFileInputDelimited_1_ListType[] = null;
				//support passing value (property: Field Separator) by 'context.fs' or 'globalMap.get("fs")'. 
				if ( ((String)context.vente_FieldSeparator).length() > 0 ){
					fieldSeparator_tFileInputDelimited_1_ListType = ((String)context.vente_FieldSeparator).toCharArray();
				}else {			
					throw new IllegalArgumentException("Field Separator must be assigned a char."); 
				}
				if(rowtFileInputDelimited_1.length == 1 && ("\015").equals(rowtFileInputDelimited_1[0])){//empty line when row separator is '\n'
					
							row1.numero_attestation = null;
					
							row1.immatriculation = null;
					
							row1.code_assure = null;
					
							row1.code_assureur = null;
					
							row1.code_intermediaire_dna = null;
					
							row1.prime_nette_rc = 0;
					
							row1.dta = null;
					
				}else{
					
					for(int i_tFileInputDelimited_1=0;i_tFileInputDelimited_1<rowtFileInputDelimited_1.length;i_tFileInputDelimited_1++){
						rowtFileInputDelimited_1[i_tFileInputDelimited_1]=rowtFileInputDelimited_1[i_tFileInputDelimited_1].trim();
					}
					
	                int columnIndexWithD_tFileInputDelimited_1 = 0; //Column Index 
	                
						columnIndexWithD_tFileInputDelimited_1 = 0;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.numero_attestation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.numero_attestation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 1;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.immatriculation = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.immatriculation = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 2;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assure = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assure = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 3;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_assureur = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_assureur = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 4;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
									row1.code_intermediaire_dna = rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1];
									
							
						
						}else{
						
							
								row1.code_intermediaire_dna = null;
							
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 5;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row1.prime_nette_rc = ParserUtils.parseTo_int(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"prime_nette_rc", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
    										rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'prime_nette_rc' in 'row1' connection, value is invalid or this column should be nullable or have a default value."));
    									
    								}
									
									
							
						
						}else{
						
							rowstate_tFileInputDelimited_1.setException(new RuntimeException("Value is empty for column : 'prime_nette_rc' in 'row1' connection, value is invalid or this column should be nullable or have a default value."));
						
						}
						
						
					
						columnIndexWithD_tFileInputDelimited_1 = 6;
						
						
						
						if(columnIndexWithD_tFileInputDelimited_1 < rowtFileInputDelimited_1.length){
						
						
							
								
									if(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1].length() > 0) {
										try {
									
										row1.dta = ParserUtils.parseTo_Integer(rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1]);
									
									
										} catch(java.lang.Exception ex_tFileInputDelimited_1) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",ex_tFileInputDelimited_1.getMessage());
											rowstate_tFileInputDelimited_1.setException(new RuntimeException(String.format("Couldn't parse value for column '%s' in '%s', value is '%s'. Details: %s",
												"dta", "row1", rowtFileInputDelimited_1[columnIndexWithD_tFileInputDelimited_1], ex_tFileInputDelimited_1), ex_tFileInputDelimited_1));
										}
    								}else{
    									
											
												row1.dta = null;
											
    									
    								}
									
									
							
						
						}else{
						
							
								row1.dta = null;
							
						
						}
						
						
					
				}
				
 					int filedsum = rowtFileInputDelimited_1.length;
 					if(filedsum < (7 )){
 						throw new java.lang.Exception("Column(s) missing");
 					} else if(filedsum > (7 )) {
 						throw new RuntimeException("Too many columns");
 					}     
				
									
									if(rowstate_tFileInputDelimited_1.getException()!=null) {
										throw rowstate_tFileInputDelimited_1.getException();
									}
									
									
	    						} catch (java.lang.Exception e) {
globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE",e.getMessage());
							        whetherReject_tFileInputDelimited_1 = true;
        							
						                    row2 = new row2Struct();
                							
    				row2.numero_attestation = row1.numero_attestation;
				
    				row2.immatriculation = row1.immatriculation;
				
    				row2.code_assure = row1.code_assure;
				
    				row2.code_assureur = row1.code_assureur;
				
    				row2.code_intermediaire_dna = row1.code_intermediaire_dna;
				
    				row2.prime_nette_rc = row1.prime_nette_rc;
				
    				row2.dta = row1.dta;
				
			
                							row2.errorMessage = e.getMessage() + " - Line: " + tos_count_tFileInputDelimited_1;
                							row1 = null;
                						
            							globalMap.put("tFileInputDelimited_1_ERROR_MESSAGE", e.getMessage());
            							
	    						}
	
							

 



/**
 * [tFileInputDelimited_1 begin ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 


	tos_count_tFileInputDelimited_1++;

/**
 * [tFileInputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_begin ] stop
 */
// Start of branch "row1"
if(row1 != null) { 



	
	/**
	 * [tSchemaComplianceCheck_1 main ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row1"
						
						);
					}
					
	row3 = null;	row4 = null;
	rsvUtil_tSchemaComplianceCheck_1.setRowValue_0(row1);
	if (rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row3 = new row3Struct();
		row3.numero_attestation = row1.numero_attestation;
		row3.immatriculation = row1.immatriculation;
		row3.code_assure = row1.code_assure;
		row3.code_assureur = row1.code_assureur;
		row3.code_intermediaire_dna = row1.code_intermediaire_dna;
		row3.prime_nette_rc = row1.prime_nette_rc;
		row3.dta = row1.dta;
	}
	if (!rsvUtil_tSchemaComplianceCheck_1.ifPassedThrough) {
		row4 = new row4Struct();
		row4.numero_attestation = row1.numero_attestation;
		row4.immatriculation = row1.immatriculation;
		row4.code_assure = row1.code_assure;
		row4.code_assureur = row1.code_assureur;
		row4.code_intermediaire_dna = row1.code_intermediaire_dna;
		row4.prime_nette_rc = row1.prime_nette_rc;
		row4.dta = row1.dta;
		row4.errorCode = String.valueOf(rsvUtil_tSchemaComplianceCheck_1.resultErrorCodeThrough);
		row4.errorMessage = rsvUtil_tSchemaComplianceCheck_1.resultErrorMessageThrough;
	}
	rsvUtil_tSchemaComplianceCheck_1.reset();

 


	tos_count_tSchemaComplianceCheck_1++;

/**
 * [tSchemaComplianceCheck_1 main ] stop
 */
	
	/**
	 * [tSchemaComplianceCheck_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_begin ] stop
 */
// Start of branch "row3"
if(row3 != null) { 



	
	/**
	 * [tLogRow_2 main ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row3"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_2 = new String[7];
   				
	    		if(row3.numero_attestation != null) { //              
                 row_tLogRow_2[0]=    						    
				                String.valueOf(row3.numero_attestation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.immatriculation != null) { //              
                 row_tLogRow_2[1]=    						    
				                String.valueOf(row3.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.code_assure != null) { //              
                 row_tLogRow_2[2]=    						    
				                String.valueOf(row3.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.code_assureur != null) { //              
                 row_tLogRow_2[3]=    						    
				                String.valueOf(row3.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row3.code_intermediaire_dna != null) { //              
                 row_tLogRow_2[4]=    						    
				                String.valueOf(row3.code_intermediaire_dna)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_2[5]=    						    
				                String.valueOf(row3.prime_nette_rc)			
					          ;	
										
    			   				
	    		if(row3.dta != null) { //              
                 row_tLogRow_2[6]=    						    
				                String.valueOf(row3.dta)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_2.addRow(row_tLogRow_2);	
				nb_line_tLogRow_2++;
//////

//////                    
                    
///////////////////////    			

 
     row5 = row3;


	tos_count_tLogRow_2++;

/**
 * [tLogRow_2 main ] stop
 */
	
	/**
	 * [tLogRow_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row5"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
        // ###############################
        // # Output tables

Vente_valide = null;


// # Output table : 'Vente_valide'
Vente_valide_tmp.numero_attestation = row5.numero_attestation ;
Vente_valide_tmp.immatriculation = row5.immatriculation ;
Vente_valide_tmp.code_assure = row5.code_assure ;
Vente_valide_tmp.code_assureur = row5.code_assureur ;
Vente_valide_tmp.code_intermediaire_dna = row5.code_intermediaire_dna ;
Vente_valide_tmp.prime_nette_rc = row5.prime_nette_rc;
Vente_valide_tmp.dta = row5.dta ;
Vente_valide_tmp.c_status = 1;
Vente_valide_tmp.c_date_mis_a_jour = TalendDate.getCurrentDate() ;
Vente_valide_tmp.c_date_transfer = null;
Vente_valide_tmp.commentaires = null;
Vente_valide = Vente_valide_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "Vente_valide"
if(Vente_valide != null) { 



	
	/**
	 * [tLogRow_4 main ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"Vente_valide"
						
						);
					}
					
///////////////////////		
						



				strBuffer_tLogRow_4 = new StringBuilder();




   				
	    		if(Vente_valide.numero_attestation != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.numero_attestation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.immatriculation != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.immatriculation)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.code_assure != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.code_assure)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.code_assureur != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.code_assureur)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.code_intermediaire_dna != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.code_intermediaire_dna)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.prime_nette_rc)							
				);


							  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.dta != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.dta)							
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.c_status)							
				);


							  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.c_date_mis_a_jour != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
								FormatterUtils.format_Date(Vente_valide.c_date_mis_a_jour, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.c_date_transfer != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
								FormatterUtils.format_Date(Vente_valide.c_date_transfer, "dd/MM/yyyy")				
				);


							
	    		} //  			

    			strBuffer_tLogRow_4.append("|");
    			


   				
	    		if(Vente_valide.commentaires != null) { //              
                    							
       
				strBuffer_tLogRow_4.append(
				                String.valueOf(Vente_valide.commentaires)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_4 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_4 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_4);
                    }
                    consoleOut_tLogRow_4.println(strBuffer_tLogRow_4.toString());
                    consoleOut_tLogRow_4.flush();
                    nb_line_tLogRow_4++;
//////

//////                    
                    
///////////////////////    			

 
     row6 = Vente_valide;


	tos_count_tLogRow_4++;

/**
 * [tLogRow_4 main ] stop
 */
	
	/**
	 * [tLogRow_4 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_begin ] stop
 */

	
	/**
	 * [tDBOutput_1 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row6"
						
						);
					}
					



        whetherReject_tDBOutput_1 = false;
                    if(row6.numero_attestation == null) {
pstmt_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(1, row6.numero_attestation);
}

                    if(row6.immatriculation == null) {
pstmt_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_1.setString(2, row6.immatriculation);
}

            int checkCount_tDBOutput_1 = -1;
            try (java.sql.ResultSet rs_tDBOutput_1 = pstmt_tDBOutput_1.executeQuery()) {
                while(rs_tDBOutput_1.next()) {
                    checkCount_tDBOutput_1 = rs_tDBOutput_1.getInt(1);
                }
            }
            if(checkCount_tDBOutput_1 > 0) {
                        if(row6.code_assure == null) {
pstmtUpdate_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(1, row6.code_assure);
}

                        if(row6.code_assureur == null) {
pstmtUpdate_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(2, row6.code_assureur);
}

                        if(row6.code_intermediaire_dna == null) {
pstmtUpdate_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(3, row6.code_intermediaire_dna);
}

                        pstmtUpdate_tDBOutput_1.setInt(4, row6.prime_nette_rc);

                        if(row6.dta == null) {
pstmtUpdate_tDBOutput_1.setNull(5, java.sql.Types.INTEGER);
} else {pstmtUpdate_tDBOutput_1.setInt(5, row6.dta);
}

                        pstmtUpdate_tDBOutput_1.setInt(6, row6.c_status);

                        if(row6.c_date_mis_a_jour != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(7, new java.sql.Timestamp(row6.c_date_mis_a_jour.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(7, java.sql.Types.TIMESTAMP);
}

                        if(row6.c_date_transfer != null) {
pstmtUpdate_tDBOutput_1.setTimestamp(8, new java.sql.Timestamp(row6.c_date_transfer.getTime()));
} else {
pstmtUpdate_tDBOutput_1.setNull(8, java.sql.Types.TIMESTAMP);
}

                        if(row6.commentaires == null) {
pstmtUpdate_tDBOutput_1.setNull(9, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(9, row6.commentaires);
}

                        if(row6.numero_attestation == null) {
pstmtUpdate_tDBOutput_1.setNull(10 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(10 + count_tDBOutput_1, row6.numero_attestation);
}

                        if(row6.immatriculation == null) {
pstmtUpdate_tDBOutput_1.setNull(11 + count_tDBOutput_1, java.sql.Types.VARCHAR);
} else {pstmtUpdate_tDBOutput_1.setString(11 + count_tDBOutput_1, row6.immatriculation);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtUpdate_tDBOutput_1.executeUpdate();
                    updatedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            } else {
                        if(row6.numero_attestation == null) {
pstmtInsert_tDBOutput_1.setNull(1, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(1, row6.numero_attestation);
}

                        if(row6.immatriculation == null) {
pstmtInsert_tDBOutput_1.setNull(2, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(2, row6.immatriculation);
}

                        if(row6.code_assure == null) {
pstmtInsert_tDBOutput_1.setNull(3, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(3, row6.code_assure);
}

                        if(row6.code_assureur == null) {
pstmtInsert_tDBOutput_1.setNull(4, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(4, row6.code_assureur);
}

                        if(row6.code_intermediaire_dna == null) {
pstmtInsert_tDBOutput_1.setNull(5, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(5, row6.code_intermediaire_dna);
}

                        pstmtInsert_tDBOutput_1.setInt(6, row6.prime_nette_rc);

                        if(row6.dta == null) {
pstmtInsert_tDBOutput_1.setNull(7, java.sql.Types.INTEGER);
} else {pstmtInsert_tDBOutput_1.setInt(7, row6.dta);
}

                        pstmtInsert_tDBOutput_1.setInt(8, row6.c_status);

                        if(row6.c_date_mis_a_jour != null) {
pstmtInsert_tDBOutput_1.setTimestamp(9, new java.sql.Timestamp(row6.c_date_mis_a_jour.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(9, java.sql.Types.TIMESTAMP);
}

                        if(row6.c_date_transfer != null) {
pstmtInsert_tDBOutput_1.setTimestamp(10, new java.sql.Timestamp(row6.c_date_transfer.getTime()));
} else {
pstmtInsert_tDBOutput_1.setNull(10, java.sql.Types.TIMESTAMP);
}

                        if(row6.commentaires == null) {
pstmtInsert_tDBOutput_1.setNull(11, java.sql.Types.VARCHAR);
} else {pstmtInsert_tDBOutput_1.setString(11, row6.commentaires);
}

                try {
					
                    int processedCount_tDBOutput_1 = pstmtInsert_tDBOutput_1.executeUpdate();
                    insertedCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    rowsToCommitCount_tDBOutput_1 += processedCount_tDBOutput_1;
                    nb_line_tDBOutput_1++;
					
                } catch(java.lang.Exception e) {
globalMap.put("tDBOutput_1_ERROR_MESSAGE",e.getMessage());
					
                    whetherReject_tDBOutput_1 = true;
                        nb_line_tDBOutput_1++;
                            System.err.print(e.getMessage());
                }
            }
            if(!whetherReject_tDBOutput_1) {
            }
    		    commitCounter_tDBOutput_1++;
                if(commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    }
                    conn_tDBOutput_1.commit();
                    if(rowsToCommitCount_tDBOutput_1 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_1 = 0;
                    }
                    commitCounter_tDBOutput_1=0;
                }

 


	tos_count_tDBOutput_1++;

/**
 * [tDBOutput_1 main ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_begin ] stop
 */
	
	/**
	 * [tDBOutput_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	

 



/**
 * [tDBOutput_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_4 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 process_data_end ] stop
 */

} // End of branch "Vente_valide"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 process_data_end ] stop
 */

} // End of branch "row3"




// Start of branch "row4"
if(row4 != null) { 



	
	/**
	 * [tLogRow_3 main ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row4"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_3 = new String[9];
   				
	    		if(row4.numero_attestation != null) { //              
                 row_tLogRow_3[0]=    						    
				                String.valueOf(row4.numero_attestation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.immatriculation != null) { //              
                 row_tLogRow_3[1]=    						    
				                String.valueOf(row4.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.code_assure != null) { //              
                 row_tLogRow_3[2]=    						    
				                String.valueOf(row4.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.code_assureur != null) { //              
                 row_tLogRow_3[3]=    						    
				                String.valueOf(row4.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.code_intermediaire_dna != null) { //              
                 row_tLogRow_3[4]=    						    
				                String.valueOf(row4.code_intermediaire_dna)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_3[5]=    						    
				                String.valueOf(row4.prime_nette_rc)			
					          ;	
										
    			   				
	    		if(row4.dta != null) { //              
                 row_tLogRow_3[6]=    						    
				                String.valueOf(row4.dta)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.errorCode != null) { //              
                 row_tLogRow_3[7]=    						    
				                String.valueOf(row4.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row4.errorMessage != null) { //              
                 row_tLogRow_3[8]=    						    
				                String.valueOf(row4.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_3.addRow(row_tLogRow_3);	
				nb_line_tLogRow_3++;
//////

//////                    
                    
///////////////////////    			

 
     row7 = row4;


	tos_count_tLogRow_3++;

/**
 * [tLogRow_3 main ] stop
 */
	
	/**
	 * [tLogRow_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_begin ] stop
 */

	
	/**
	 * [tMap_2 main ] start
	 */

	

	
	
	currentComponent="tMap_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row7"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_2 = false;
		  boolean mainRowRejected_tMap_2 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
        // ###############################
        // # Output tables

vente_rejects_schema = null;


// # Output table : 'vente_rejects_schema'
vente_rejects_schema_tmp.numero_attestation = row7.numero_attestation ;
vente_rejects_schema_tmp.immatriculation = row7.immatriculation ;
vente_rejects_schema_tmp.code_assure = row7.code_assure ;
vente_rejects_schema_tmp.code_assureur = row7.code_assureur ;
vente_rejects_schema_tmp.code_intermediaire_dna = row7.code_intermediaire_dna ;
vente_rejects_schema_tmp.prime_nette_rc = row7.prime_nette_rc;
vente_rejects_schema_tmp.dta = row7.dta ;
vente_rejects_schema_tmp.date_extraction = TalendDate.getCurrentDate() ;
vente_rejects_schema_tmp.date_depot = null;
vente_rejects_schema_tmp.errorCode = row7.errorCode ;
vente_rejects_schema_tmp.errorMessage = row7.errorMessage ;
vente_rejects_schema = vente_rejects_schema_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_2 = false;










 


	tos_count_tMap_2++;

/**
 * [tMap_2 main ] stop
 */
	
	/**
	 * [tMap_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_begin ] stop
 */
// Start of branch "vente_rejects_schema"
if(vente_rejects_schema != null) { 



	
	/**
	 * [tDBOutput_2 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vente_rejects_schema"
						
						);
					}
					



            row10 = null;
        whetherReject_tDBOutput_2 = false;
                    if(vente_rejects_schema.numero_attestation == null) {
pstmt_tDBOutput_2.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(1, vente_rejects_schema.numero_attestation);
}

                    if(vente_rejects_schema.immatriculation == null) {
pstmt_tDBOutput_2.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(2, vente_rejects_schema.immatriculation);
}

                    if(vente_rejects_schema.code_assure == null) {
pstmt_tDBOutput_2.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(3, vente_rejects_schema.code_assure);
}

                    if(vente_rejects_schema.code_assureur == null) {
pstmt_tDBOutput_2.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(4, vente_rejects_schema.code_assureur);
}

                    if(vente_rejects_schema.code_intermediaire_dna == null) {
pstmt_tDBOutput_2.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(5, vente_rejects_schema.code_intermediaire_dna);
}

                    pstmt_tDBOutput_2.setInt(6, vente_rejects_schema.prime_nette_rc);

                    if(vente_rejects_schema.dta == null) {
pstmt_tDBOutput_2.setNull(7, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_2.setInt(7, vente_rejects_schema.dta);
}

                    if(vente_rejects_schema.date_extraction != null) {
pstmt_tDBOutput_2.setTimestamp(8, new java.sql.Timestamp(vente_rejects_schema.date_extraction.getTime()));
} else {
pstmt_tDBOutput_2.setNull(8, java.sql.Types.TIMESTAMP);
}

                    if(vente_rejects_schema.date_depot != null) {
pstmt_tDBOutput_2.setTimestamp(9, new java.sql.Timestamp(vente_rejects_schema.date_depot.getTime()));
} else {
pstmt_tDBOutput_2.setNull(9, java.sql.Types.TIMESTAMP);
}

                    if(vente_rejects_schema.errorCode == null) {
pstmt_tDBOutput_2.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(10, vente_rejects_schema.errorCode);
}

                    if(vente_rejects_schema.errorMessage == null) {
pstmt_tDBOutput_2.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_2.setString(11, vente_rejects_schema.errorMessage);
}

			
    		pstmt_tDBOutput_2.addBatch();
    		nb_line_tDBOutput_2++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_2++;
    		  
            if(!whetherReject_tDBOutput_2) {
                            row10 = new row10Struct();
                                row10.numero_attestation = vente_rejects_schema.numero_attestation;
                                row10.immatriculation = vente_rejects_schema.immatriculation;
                                row10.code_assure = vente_rejects_schema.code_assure;
                                row10.code_assureur = vente_rejects_schema.code_assureur;
                                row10.code_intermediaire_dna = vente_rejects_schema.code_intermediaire_dna;
                                row10.prime_nette_rc = vente_rejects_schema.prime_nette_rc;
                                row10.dta = vente_rejects_schema.dta;
                                row10.date_extraction = vente_rejects_schema.date_extraction;
                                row10.date_depot = vente_rejects_schema.date_depot;
                                row10.errorCode = vente_rejects_schema.errorCode;
                                row10.errorMessage = vente_rejects_schema.errorMessage;
            }
    			if ((batchSize_tDBOutput_2 > 0) && (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2)) {
                try {
						int countSum_tDBOutput_2 = 0;
						    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
				    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
            	    	batchSizeCounter_tDBOutput_2 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
				    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
				    	String errormessage_tDBOutput_2;
						if (ne_tDBOutput_2 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
							errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
						}else{
							errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
						}
				    	
				    	int countSum_tDBOutput_2 = 0;
						for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
						rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
				    	
				    	System.err.println(errormessage_tDBOutput_2);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_2++;
                if(commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {
                if ((batchSize_tDBOutput_2 > 0) && (batchSizeCounter_tDBOutput_2 > 0)) {
                try {
                		int countSum_tDBOutput_2 = 0;
                		    
						for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
						}
            	    	rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
            	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
            	    	
                batchSizeCounter_tDBOutput_2 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
			    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
			    	String errormessage_tDBOutput_2;
					if (ne_tDBOutput_2 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
						errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
					}else{
						errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
					}
			    	
			    	int countSum_tDBOutput_2 = 0;
					for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
					
			    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
			    	
			    	System.err.println(errormessage_tDBOutput_2);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    }
                    conn_tDBOutput_2.commit();
                    if(rowsToCommitCount_tDBOutput_2 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_2 = 0;
                    }
                    commitCounter_tDBOutput_2=0;
                }

 


	tos_count_tDBOutput_2++;

/**
 * [tDBOutput_2 main ] stop
 */
	
	/**
	 * [tDBOutput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_begin ] stop
 */
// Start of branch "row10"
if(row10 != null) { 



	
	/**
	 * [tFileOutputDelimited_2 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row10"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_2 = new StringBuilder();
                            if(row10.numero_attestation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.numero_attestation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.immatriculation != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.code_assure != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.code_assureur != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.code_intermediaire_dna != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.code_intermediaire_dna
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                        sb_tFileOutputDelimited_2.append(
                            row10.prime_nette_rc
                        );
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.dta != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.dta
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.date_extraction != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row10.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.date_depot != null) {
                        sb_tFileOutputDelimited_2.append(
                            FormatterUtils.format_Date(row10.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.errorCode != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_2.append(OUT_DELIM_tFileOutputDelimited_2);
                            if(row10.errorMessage != null) {
                        sb_tFileOutputDelimited_2.append(
                            row10.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_2.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_2);


                    nb_line_tFileOutputDelimited_2++;
                    resourceMap.put("nb_line_tFileOutputDelimited_2", nb_line_tFileOutputDelimited_2);

                        outtFileOutputDelimited_2.write(sb_tFileOutputDelimited_2.toString());




 


	tos_count_tFileOutputDelimited_2++;

/**
 * [tFileOutputDelimited_2 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	

 



/**
 * [tFileOutputDelimited_2 process_data_end ] stop
 */

} // End of branch "row10"




	
	/**
	 * [tDBOutput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	

 



/**
 * [tDBOutput_2 process_data_end ] stop
 */

} // End of branch "vente_rejects_schema"




	
	/**
	 * [tMap_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 process_data_end ] stop
 */

} // End of branch "row4"




	
	/**
	 * [tSchemaComplianceCheck_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 process_data_end ] stop
 */

} // End of branch "row1"




// Start of branch "row2"
if(row2 != null) { 



	
	/**
	 * [tLogRow_1 main ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row2"
						
						);
					}
					
///////////////////////		
						

				
				String[] row_tLogRow_1 = new String[9];
   				
	    		if(row2.numero_attestation != null) { //              
                 row_tLogRow_1[0]=    						    
				                String.valueOf(row2.numero_attestation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.immatriculation != null) { //              
                 row_tLogRow_1[1]=    						    
				                String.valueOf(row2.immatriculation)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_assure != null) { //              
                 row_tLogRow_1[2]=    						    
				                String.valueOf(row2.code_assure)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_assureur != null) { //              
                 row_tLogRow_1[3]=    						    
				                String.valueOf(row2.code_assureur)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.code_intermediaire_dna != null) { //              
                 row_tLogRow_1[4]=    						    
				                String.valueOf(row2.code_intermediaire_dna)			
					          ;	
							
	    		} //			
    			              
                 row_tLogRow_1[5]=    						    
				                String.valueOf(row2.prime_nette_rc)			
					          ;	
										
    			   				
	    		if(row2.dta != null) { //              
                 row_tLogRow_1[6]=    						    
				                String.valueOf(row2.dta)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.errorCode != null) { //              
                 row_tLogRow_1[7]=    						    
				                String.valueOf(row2.errorCode)			
					          ;	
							
	    		} //			
    			   				
	    		if(row2.errorMessage != null) { //              
                 row_tLogRow_1[8]=    						    
				                String.valueOf(row2.errorMessage)			
					          ;	
							
	    		} //			
    			 

				util_tLogRow_1.addRow(row_tLogRow_1);	
				nb_line_tLogRow_1++;
//////

//////                    
                    
///////////////////////    			

 
     row8 = row2;


	tos_count_tLogRow_1++;

/**
 * [tLogRow_1 main ] stop
 */
	
	/**
	 * [tLogRow_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_begin ] stop
 */

	
	/**
	 * [tMap_3 main ] start
	 */

	

	
	
	currentComponent="tMap_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row8"
						
						);
					}
					

		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;
		

        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_3 = false;
		  boolean mainRowRejected_tMap_3 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
        // ###############################
        // # Output tables

vente_rejects_structure = null;


// # Output table : 'vente_rejects_structure'
vente_rejects_structure_tmp.numero_attestation = row8.numero_attestation ;
vente_rejects_structure_tmp.immatriculation = row8.immatriculation ;
vente_rejects_structure_tmp.code_assure = row8.code_assure ;
vente_rejects_structure_tmp.code_assureur = row8.code_assureur ;
vente_rejects_structure_tmp.code_intermediaire_dna = row8.code_intermediaire_dna ;
vente_rejects_structure_tmp.prime_nette_rc = row8.prime_nette_rc;
vente_rejects_structure_tmp.dta = row8.dta ;
vente_rejects_structure_tmp.date_extraction = TalendDate.getCurrentDate() ;
vente_rejects_structure_tmp.date_depot = null;
vente_rejects_structure_tmp.errorCode = row8.errorCode ;
vente_rejects_structure_tmp.errorMessage = row8.errorMessage ;
vente_rejects_structure = vente_rejects_structure_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_3 = false;










 


	tos_count_tMap_3++;

/**
 * [tMap_3 main ] stop
 */
	
	/**
	 * [tMap_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_begin ] stop
 */
// Start of branch "vente_rejects_structure"
if(vente_rejects_structure != null) { 



	
	/**
	 * [tDBOutput_3 main ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"vente_rejects_structure"
						
						);
					}
					



            row9 = null;
        whetherReject_tDBOutput_3 = false;
                    if(vente_rejects_structure.numero_attestation == null) {
pstmt_tDBOutput_3.setNull(1, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(1, vente_rejects_structure.numero_attestation);
}

                    if(vente_rejects_structure.immatriculation == null) {
pstmt_tDBOutput_3.setNull(2, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(2, vente_rejects_structure.immatriculation);
}

                    if(vente_rejects_structure.code_assure == null) {
pstmt_tDBOutput_3.setNull(3, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(3, vente_rejects_structure.code_assure);
}

                    if(vente_rejects_structure.code_assureur == null) {
pstmt_tDBOutput_3.setNull(4, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(4, vente_rejects_structure.code_assureur);
}

                    if(vente_rejects_structure.code_intermediaire_dna == null) {
pstmt_tDBOutput_3.setNull(5, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(5, vente_rejects_structure.code_intermediaire_dna);
}

                    if(vente_rejects_structure.prime_nette_rc == null) {
pstmt_tDBOutput_3.setNull(6, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(6, vente_rejects_structure.prime_nette_rc);
}

                    if(vente_rejects_structure.dta == null) {
pstmt_tDBOutput_3.setNull(7, java.sql.Types.INTEGER);
} else {pstmt_tDBOutput_3.setInt(7, vente_rejects_structure.dta);
}

                    if(vente_rejects_structure.date_extraction != null) {
pstmt_tDBOutput_3.setTimestamp(8, new java.sql.Timestamp(vente_rejects_structure.date_extraction.getTime()));
} else {
pstmt_tDBOutput_3.setNull(8, java.sql.Types.TIMESTAMP);
}

                    if(vente_rejects_structure.date_depot != null) {
pstmt_tDBOutput_3.setTimestamp(9, new java.sql.Timestamp(vente_rejects_structure.date_depot.getTime()));
} else {
pstmt_tDBOutput_3.setNull(9, java.sql.Types.TIMESTAMP);
}

                    if(vente_rejects_structure.errorCode == null) {
pstmt_tDBOutput_3.setNull(10, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(10, vente_rejects_structure.errorCode);
}

                    if(vente_rejects_structure.errorMessage == null) {
pstmt_tDBOutput_3.setNull(11, java.sql.Types.VARCHAR);
} else {pstmt_tDBOutput_3.setString(11, vente_rejects_structure.errorMessage);
}

			
    		pstmt_tDBOutput_3.addBatch();
    		nb_line_tDBOutput_3++;
    		  
    		  
    		  batchSizeCounter_tDBOutput_3++;
    		  
            if(!whetherReject_tDBOutput_3) {
                            row9 = new row9Struct();
                                row9.numero_attestation = vente_rejects_structure.numero_attestation;
                                row9.immatriculation = vente_rejects_structure.immatriculation;
                                row9.code_assure = vente_rejects_structure.code_assure;
                                row9.code_assureur = vente_rejects_structure.code_assureur;
                                row9.code_intermediaire_dna = vente_rejects_structure.code_intermediaire_dna;
                                row9.prime_nette_rc = vente_rejects_structure.prime_nette_rc;
                                row9.dta = vente_rejects_structure.dta;
                                row9.date_extraction = vente_rejects_structure.date_extraction;
                                row9.date_depot = vente_rejects_structure.date_depot;
                                row9.errorCode = vente_rejects_structure.errorCode;
                                row9.errorMessage = vente_rejects_structure.errorMessage;
            }
    			if ((batchSize_tDBOutput_3 > 0) && (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3)) {
                try {
						int countSum_tDBOutput_3 = 0;
						    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
				    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
            	    	batchSizeCounter_tDBOutput_3 = 0;
                }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
				    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
				    	String errormessage_tDBOutput_3;
						if (ne_tDBOutput_3 != null) {
							// build new exception to provide the original cause
							sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
							errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
						}else{
							errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
						}
				    	
				    	int countSum_tDBOutput_3 = 0;
						for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
						rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
				    	
				    	System.err.println(errormessage_tDBOutput_3);
				    	
					}
    			}
    		
    		    commitCounter_tDBOutput_3++;
                if(commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {
                if ((batchSize_tDBOutput_3 > 0) && (batchSizeCounter_tDBOutput_3 > 0)) {
                try {
                		int countSum_tDBOutput_3 = 0;
                		    
						for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
						}
            	    	rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
            	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
            	    	
                batchSizeCounter_tDBOutput_3 = 0;
               }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
			    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
			    	String errormessage_tDBOutput_3;
					if (ne_tDBOutput_3 != null) {
						// build new exception to provide the original cause
						sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
						errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
					}else{
						errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
					}
			    	
			    	int countSum_tDBOutput_3 = 0;
					for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
					
			    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
			    	
			    	System.err.println(errormessage_tDBOutput_3);
			    	
				}
            }
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    }
                    conn_tDBOutput_3.commit();
                    if(rowsToCommitCount_tDBOutput_3 != 0){
                    	
                    	rowsToCommitCount_tDBOutput_3 = 0;
                    }
                    commitCounter_tDBOutput_3=0;
                }

 


	tos_count_tDBOutput_3++;

/**
 * [tDBOutput_3 main ] stop
 */
	
	/**
	 * [tDBOutput_3 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_begin ] stop
 */
// Start of branch "row9"
if(row9 != null) { 



	
	/**
	 * [tFileOutputDelimited_1 main ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	
					if(execStat){
						runStat.updateStatOnConnection(iterateId,1,1
						
							,"row9"
						
						);
					}
					


                    StringBuilder sb_tFileOutputDelimited_1 = new StringBuilder();
                            if(row9.numero_attestation != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.numero_attestation
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.immatriculation != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.immatriculation
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.code_assure != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.code_assure
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.code_assureur != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.code_assureur
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.code_intermediaire_dna != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.code_intermediaire_dna
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.prime_nette_rc != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.prime_nette_rc
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.dta != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.dta
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.date_extraction != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row9.date_extraction, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.date_depot != null) {
                        sb_tFileOutputDelimited_1.append(
                            FormatterUtils.format_Date(row9.date_depot, "dd/MM/yyyy")
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.errorCode != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.errorCode
                        );
                            }
                            sb_tFileOutputDelimited_1.append(OUT_DELIM_tFileOutputDelimited_1);
                            if(row9.errorMessage != null) {
                        sb_tFileOutputDelimited_1.append(
                            row9.errorMessage
                        );
                            }
                    sb_tFileOutputDelimited_1.append(OUT_DELIM_ROWSEP_tFileOutputDelimited_1);


                    nb_line_tFileOutputDelimited_1++;
                    resourceMap.put("nb_line_tFileOutputDelimited_1", nb_line_tFileOutputDelimited_1);

                        outtFileOutputDelimited_1.write(sb_tFileOutputDelimited_1.toString());




 


	tos_count_tFileOutputDelimited_1++;

/**
 * [tFileOutputDelimited_1 main ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	

 



/**
 * [tFileOutputDelimited_1 process_data_end ] stop
 */

} // End of branch "row9"




	
	/**
	 * [tDBOutput_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	

 



/**
 * [tDBOutput_3 process_data_end ] stop
 */

} // End of branch "vente_rejects_structure"




	
	/**
	 * [tMap_3 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 process_data_end ] stop
 */



	
	/**
	 * [tLogRow_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 process_data_end ] stop
 */

} // End of branch "row2"




	
	/**
	 * [tFileInputDelimited_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 process_data_end ] stop
 */
	
	/**
	 * [tFileInputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	


				nb_line_tFileInputDelimited_1++;
			}
			
			}finally{
    			if(!(filename_tFileInputDelimited_1 instanceof java.io.InputStream)){
    				if(csvReadertFileInputDelimited_1!=null){
    					csvReadertFileInputDelimited_1.close();
    				}
    			}
    			if(csvReadertFileInputDelimited_1!=null){
    				globalMap.put("tFileInputDelimited_1_NB_LINE",nb_line_tFileInputDelimited_1);
    			}
				
			}
						  

 

ok_Hash.put("tFileInputDelimited_1", true);
end_Hash.put("tFileInputDelimited_1", System.currentTimeMillis());




/**
 * [tFileInputDelimited_1 end ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 end ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row1");
			  	}
			  	
 

ok_Hash.put("tSchemaComplianceCheck_1", true);
end_Hash.put("tSchemaComplianceCheck_1", System.currentTimeMillis());




/**
 * [tSchemaComplianceCheck_1 end ] stop
 */

	
	/**
	 * [tLogRow_2 end ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_2 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_2 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_2 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_2);
                    }
                    
                    consoleOut_tLogRow_2.println(util_tLogRow_2.format().toString());
                    consoleOut_tLogRow_2.flush();
//////
globalMap.put("tLogRow_2_NB_LINE",nb_line_tLogRow_2);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row3");
			  	}
			  	
 

ok_Hash.put("tLogRow_2", true);
end_Hash.put("tLogRow_2", System.currentTimeMillis());




/**
 * [tLogRow_2 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row5");
			  	}
			  	
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tLogRow_4 end ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	


//////
//////
globalMap.put("tLogRow_4_NB_LINE",nb_line_tLogRow_4);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"Vente_valide");
			  	}
			  	
 

ok_Hash.put("tLogRow_4", true);
end_Hash.put("tLogRow_4", System.currentTimeMillis());




/**
 * [tLogRow_4 end ] stop
 */

	
	/**
	 * [tDBOutput_1 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



        if(pstmtUpdate_tDBOutput_1 != null){
            pstmtUpdate_tDBOutput_1.close();
            resourceMap.remove("pstmtUpdate_tDBOutput_1");
        }
        if(pstmtInsert_tDBOutput_1 != null){
            pstmtInsert_tDBOutput_1.close();
            resourceMap.remove("pstmtInsert_tDBOutput_1");
        }
        if(pstmt_tDBOutput_1 != null) {
            pstmt_tDBOutput_1.close();
            resourceMap.remove("pstmt_tDBOutput_1");
        }
    resourceMap.put("statementClosed_tDBOutput_1", true);
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
			}
			conn_tDBOutput_1.commit();
			if(rowsToCommitCount_tDBOutput_1 != 0){
				
				rowsToCommitCount_tDBOutput_1 = 0;
			}
			commitCounter_tDBOutput_1 = 0;
		
    	conn_tDBOutput_1 .close();
    	
    	resourceMap.put("finish_tDBOutput_1", true);
    	

	nb_line_deleted_tDBOutput_1=nb_line_deleted_tDBOutput_1+ deletedCount_tDBOutput_1;
	nb_line_update_tDBOutput_1=nb_line_update_tDBOutput_1 + updatedCount_tDBOutput_1;
	nb_line_inserted_tDBOutput_1=nb_line_inserted_tDBOutput_1 + insertedCount_tDBOutput_1;
	nb_line_rejected_tDBOutput_1=nb_line_rejected_tDBOutput_1 + rejectedCount_tDBOutput_1;
	
        globalMap.put("tDBOutput_1_NB_LINE",nb_line_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_UPDATED",nb_line_update_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_DELETED",nb_line_deleted_tDBOutput_1);
        globalMap.put("tDBOutput_1_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_1);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row6");
			  	}
			  	
 

ok_Hash.put("tDBOutput_1", true);
end_Hash.put("tDBOutput_1", System.currentTimeMillis());

				if(execStat){   
   	 				runStat.updateStatOnConnection("OnComponentOk1", 0, "ok");
				}
				tFileCopy_1Process(globalMap);



/**
 * [tDBOutput_1 end ] stop
 */













	
	/**
	 * [tLogRow_3 end ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_3 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_3 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_3 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_3);
                    }
                    
                    consoleOut_tLogRow_3.println(util_tLogRow_3.format().toString());
                    consoleOut_tLogRow_3.flush();
//////
globalMap.put("tLogRow_3_NB_LINE",nb_line_tLogRow_3);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row4");
			  	}
			  	
 

ok_Hash.put("tLogRow_3", true);
end_Hash.put("tLogRow_3", System.currentTimeMillis());




/**
 * [tLogRow_3 end ] stop
 */

	
	/**
	 * [tMap_2 end ] start
	 */

	

	
	
	currentComponent="tMap_2";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row7");
			  	}
			  	
 

ok_Hash.put("tMap_2", true);
end_Hash.put("tMap_2", System.currentTimeMillis());




/**
 * [tMap_2 end ] stop
 */

	
	/**
	 * [tDBOutput_2 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



	    try {
				int countSum_tDBOutput_2 = 0;
				if (pstmt_tDBOutput_2 != null && batchSizeCounter_tDBOutput_2 > 0) {
						
					for(int countEach_tDBOutput_2: pstmt_tDBOutput_2.executeBatch()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
					}
					rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
						
				}
		    	
		    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_2){
globalMap.put("tDBOutput_2_ERROR_MESSAGE",e_tDBOutput_2.getMessage());
	    	java.sql.SQLException ne_tDBOutput_2 = e_tDBOutput_2.getNextException(),sqle_tDBOutput_2=null;
	    	String errormessage_tDBOutput_2;
			if (ne_tDBOutput_2 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_2 = new java.sql.SQLException(e_tDBOutput_2.getMessage() + "\ncaused by: " + ne_tDBOutput_2.getMessage(), ne_tDBOutput_2.getSQLState(), ne_tDBOutput_2.getErrorCode(), ne_tDBOutput_2);
				errormessage_tDBOutput_2 = sqle_tDBOutput_2.getMessage();
			}else{
				errormessage_tDBOutput_2 = e_tDBOutput_2.getMessage();
			}
	    	
	    	int countSum_tDBOutput_2 = 0;
			for(int countEach_tDBOutput_2: e_tDBOutput_2.getUpdateCounts()) {
				countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0 : countEach_tDBOutput_2);
			}
			rowsToCommitCount_tDBOutput_2 += countSum_tDBOutput_2;
			
	    		insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
	    	
	    	System.err.println(errormessage_tDBOutput_2);
	    	
		}
	    
        if(pstmt_tDBOutput_2 != null) {
        		
            pstmt_tDBOutput_2.close();
            resourceMap.remove("pstmt_tDBOutput_2");
        }
    resourceMap.put("statementClosed_tDBOutput_2", true);
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
			}
			conn_tDBOutput_2.commit();
			if(rowsToCommitCount_tDBOutput_2 != 0){
				
				rowsToCommitCount_tDBOutput_2 = 0;
			}
			commitCounter_tDBOutput_2 = 0;
		
    	conn_tDBOutput_2 .close();
    	
    	resourceMap.put("finish_tDBOutput_2", true);
    	

	nb_line_deleted_tDBOutput_2=nb_line_deleted_tDBOutput_2+ deletedCount_tDBOutput_2;
	nb_line_update_tDBOutput_2=nb_line_update_tDBOutput_2 + updatedCount_tDBOutput_2;
	nb_line_inserted_tDBOutput_2=nb_line_inserted_tDBOutput_2 + insertedCount_tDBOutput_2;
	nb_line_rejected_tDBOutput_2=nb_line_rejected_tDBOutput_2 + rejectedCount_tDBOutput_2;
	
        globalMap.put("tDBOutput_2_NB_LINE",nb_line_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_UPDATED",nb_line_update_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_DELETED",nb_line_deleted_tDBOutput_2);
        globalMap.put("tDBOutput_2_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_2);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vente_rejects_schema");
			  	}
			  	
 

ok_Hash.put("tDBOutput_2", true);
end_Hash.put("tDBOutput_2", System.currentTimeMillis());




/**
 * [tDBOutput_2 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	



		
			
					if(outtFileOutputDelimited_2!=null) {
						outtFileOutputDelimited_2.flush();
						outtFileOutputDelimited_2.close();
					}
				
				globalMap.put("tFileOutputDelimited_2_NB_LINE",nb_line_tFileOutputDelimited_2);
				globalMap.put("tFileOutputDelimited_2_FILE_NAME",fileName_tFileOutputDelimited_2);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_2", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row10");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_2", true);
end_Hash.put("tFileOutputDelimited_2", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_2 end ] stop
 */
















	
	/**
	 * [tLogRow_1 end ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	


//////

                    
                    java.io.PrintStream consoleOut_tLogRow_1 = null;
                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_tLogRow_1 = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_tLogRow_1 = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_tLogRow_1);
                    }
                    
                    consoleOut_tLogRow_1.println(util_tLogRow_1.format().toString());
                    consoleOut_tLogRow_1.flush();
//////
globalMap.put("tLogRow_1_NB_LINE",nb_line_tLogRow_1);

///////////////////////    			

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row2");
			  	}
			  	
 

ok_Hash.put("tLogRow_1", true);
end_Hash.put("tLogRow_1", System.currentTimeMillis());




/**
 * [tLogRow_1 end ] stop
 */

	
	/**
	 * [tMap_3 end ] start
	 */

	

	
	
	currentComponent="tMap_3";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row8");
			  	}
			  	
 

ok_Hash.put("tMap_3", true);
end_Hash.put("tMap_3", System.currentTimeMillis());




/**
 * [tMap_3 end ] stop
 */

	
	/**
	 * [tDBOutput_3 end ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



	    try {
				int countSum_tDBOutput_3 = 0;
				if (pstmt_tDBOutput_3 != null && batchSizeCounter_tDBOutput_3 > 0) {
						
					for(int countEach_tDBOutput_3: pstmt_tDBOutput_3.executeBatch()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
					}
					rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
						
				}
		    	
		    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
		    	
	    }catch (java.sql.BatchUpdateException e_tDBOutput_3){
globalMap.put("tDBOutput_3_ERROR_MESSAGE",e_tDBOutput_3.getMessage());
	    	java.sql.SQLException ne_tDBOutput_3 = e_tDBOutput_3.getNextException(),sqle_tDBOutput_3=null;
	    	String errormessage_tDBOutput_3;
			if (ne_tDBOutput_3 != null) {
				// build new exception to provide the original cause
				sqle_tDBOutput_3 = new java.sql.SQLException(e_tDBOutput_3.getMessage() + "\ncaused by: " + ne_tDBOutput_3.getMessage(), ne_tDBOutput_3.getSQLState(), ne_tDBOutput_3.getErrorCode(), ne_tDBOutput_3);
				errormessage_tDBOutput_3 = sqle_tDBOutput_3.getMessage();
			}else{
				errormessage_tDBOutput_3 = e_tDBOutput_3.getMessage();
			}
	    	
	    	int countSum_tDBOutput_3 = 0;
			for(int countEach_tDBOutput_3: e_tDBOutput_3.getUpdateCounts()) {
				countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0 : countEach_tDBOutput_3);
			}
			rowsToCommitCount_tDBOutput_3 += countSum_tDBOutput_3;
			
	    		insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
	    	
	    	System.err.println(errormessage_tDBOutput_3);
	    	
		}
	    
        if(pstmt_tDBOutput_3 != null) {
        		
            pstmt_tDBOutput_3.close();
            resourceMap.remove("pstmt_tDBOutput_3");
        }
    resourceMap.put("statementClosed_tDBOutput_3", true);
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
			}
			conn_tDBOutput_3.commit();
			if(rowsToCommitCount_tDBOutput_3 != 0){
				
				rowsToCommitCount_tDBOutput_3 = 0;
			}
			commitCounter_tDBOutput_3 = 0;
		
    	conn_tDBOutput_3 .close();
    	
    	resourceMap.put("finish_tDBOutput_3", true);
    	

	nb_line_deleted_tDBOutput_3=nb_line_deleted_tDBOutput_3+ deletedCount_tDBOutput_3;
	nb_line_update_tDBOutput_3=nb_line_update_tDBOutput_3 + updatedCount_tDBOutput_3;
	nb_line_inserted_tDBOutput_3=nb_line_inserted_tDBOutput_3 + insertedCount_tDBOutput_3;
	nb_line_rejected_tDBOutput_3=nb_line_rejected_tDBOutput_3 + rejectedCount_tDBOutput_3;
	
        globalMap.put("tDBOutput_3_NB_LINE",nb_line_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_UPDATED",nb_line_update_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_INSERTED",nb_line_inserted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_DELETED",nb_line_deleted_tDBOutput_3);
        globalMap.put("tDBOutput_3_NB_LINE_REJECTED", nb_line_rejected_tDBOutput_3);
    

	


				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"vente_rejects_structure");
			  	}
			  	
 

ok_Hash.put("tDBOutput_3", true);
end_Hash.put("tDBOutput_3", System.currentTimeMillis());




/**
 * [tDBOutput_3 end ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 end ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	



		
			
					if(outtFileOutputDelimited_1!=null) {
						outtFileOutputDelimited_1.flush();
						outtFileOutputDelimited_1.close();
					}
				
				globalMap.put("tFileOutputDelimited_1_NB_LINE",nb_line_tFileOutputDelimited_1);
				globalMap.put("tFileOutputDelimited_1_FILE_NAME",fileName_tFileOutputDelimited_1);
			
		
		
		resourceMap.put("finish_tFileOutputDelimited_1", true);
	

				if(execStat){
			  		runStat.updateStat(resourceMap,iterateId,2,0,"row9");
			  	}
			  	
 

ok_Hash.put("tFileOutputDelimited_1", true);
end_Hash.put("tFileOutputDelimited_1", System.currentTimeMillis());




/**
 * [tFileOutputDelimited_1 end ] stop
 */












						if(execStat){
							runStat.updateStatOnConnection("iterate1", 2, "exec" + NB_ITERATE_tFileInputDelimited_1);
						}				
					




	
	/**
	 * [tFileList_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 process_data_end ] stop
 */
	
	/**
	 * [tFileList_1 end ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

  
    }
  globalMap.put("tFileList_1_NB_FILE", NB_FILEtFileList_1);
  

  
 

 

ok_Hash.put("tFileList_1", true);
end_Hash.put("tFileList_1", System.currentTimeMillis());




/**
 * [tFileList_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileList_1 finally ] start
	 */

	

	
	
	currentComponent="tFileList_1";

	

 



/**
 * [tFileList_1 finally ] stop
 */

	
	/**
	 * [tFileInputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileInputDelimited_1";

	

 



/**
 * [tFileInputDelimited_1 finally ] stop
 */

	
	/**
	 * [tSchemaComplianceCheck_1 finally ] start
	 */

	

	
	
	currentComponent="tSchemaComplianceCheck_1";

	

 



/**
 * [tSchemaComplianceCheck_1 finally ] stop
 */

	
	/**
	 * [tLogRow_2 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_2";

	

 



/**
 * [tLogRow_2 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tLogRow_4 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_4";

	

 



/**
 * [tLogRow_4 finally ] stop
 */

	
	/**
	 * [tDBOutput_1 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_1";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_1") == null) {
                java.sql.PreparedStatement pstmtUpdateToClose_tDBOutput_1 = null;
                if ((pstmtUpdateToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtUpdate_tDBOutput_1")) != null) {
                    pstmtUpdateToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtInsertToClose_tDBOutput_1 = null;
                if ((pstmtInsertToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmtInsert_tDBOutput_1")) != null) {
                    pstmtInsertToClose_tDBOutput_1.close();
                }
                java.sql.PreparedStatement pstmtToClose_tDBOutput_1 = null;
                if ((pstmtToClose_tDBOutput_1 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_1")) != null) {
                    pstmtToClose_tDBOutput_1.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_1") == null){
            java.sql.Connection ctn_tDBOutput_1 = null;
            if((ctn_tDBOutput_1 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_1")) != null){
                try {
                    ctn_tDBOutput_1.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_1) {
                    String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :" + sqlEx_tDBOutput_1.getMessage();
                    System.err.println(errorMessage_tDBOutput_1);
                }
            }
        }
    }
 



/**
 * [tDBOutput_1 finally ] stop
 */













	
	/**
	 * [tLogRow_3 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_3";

	

 



/**
 * [tLogRow_3 finally ] stop
 */

	
	/**
	 * [tMap_2 finally ] start
	 */

	

	
	
	currentComponent="tMap_2";

	

 



/**
 * [tMap_2 finally ] stop
 */

	
	/**
	 * [tDBOutput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_2";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_2") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_2 = null;
                if ((pstmtToClose_tDBOutput_2 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_2")) != null) {
                    pstmtToClose_tDBOutput_2.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_2") == null){
            java.sql.Connection ctn_tDBOutput_2 = null;
            if((ctn_tDBOutput_2 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_2")) != null){
                try {
                    ctn_tDBOutput_2.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_2) {
                    String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :" + sqlEx_tDBOutput_2.getMessage();
                    System.err.println(errorMessage_tDBOutput_2);
                }
            }
        }
    }
 



/**
 * [tDBOutput_2 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_2 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_2";

	


		if(resourceMap.get("finish_tFileOutputDelimited_2") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_2 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_2");
						if(outtFileOutputDelimited_2!=null) {
							outtFileOutputDelimited_2.flush();
							outtFileOutputDelimited_2.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_2 finally ] stop
 */
















	
	/**
	 * [tLogRow_1 finally ] start
	 */

	

	
	
	currentComponent="tLogRow_1";

	

 



/**
 * [tLogRow_1 finally ] stop
 */

	
	/**
	 * [tMap_3 finally ] start
	 */

	

	
	
	currentComponent="tMap_3";

	

 



/**
 * [tMap_3 finally ] stop
 */

	
	/**
	 * [tDBOutput_3 finally ] start
	 */

	

	
	
	currentComponent="tDBOutput_3";

	



    try {
    if (resourceMap.get("statementClosed_tDBOutput_3") == null) {
                java.sql.PreparedStatement pstmtToClose_tDBOutput_3 = null;
                if ((pstmtToClose_tDBOutput_3 = (java.sql.PreparedStatement) resourceMap.remove("pstmt_tDBOutput_3")) != null) {
                    pstmtToClose_tDBOutput_3.close();
                }
    }
    } finally {
        if(resourceMap.get("finish_tDBOutput_3") == null){
            java.sql.Connection ctn_tDBOutput_3 = null;
            if((ctn_tDBOutput_3 = (java.sql.Connection)resourceMap.get("conn_tDBOutput_3")) != null){
                try {
                    ctn_tDBOutput_3.close();
                } catch (java.sql.SQLException sqlEx_tDBOutput_3) {
                    String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :" + sqlEx_tDBOutput_3.getMessage();
                    System.err.println(errorMessage_tDBOutput_3);
                }
            }
        }
    }
 



/**
 * [tDBOutput_3 finally ] stop
 */

	
	/**
	 * [tFileOutputDelimited_1 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputDelimited_1";

	


		if(resourceMap.get("finish_tFileOutputDelimited_1") == null){ 
			
				
						java.io.Writer outtFileOutputDelimited_1 = (java.io.Writer)resourceMap.get("out_tFileOutputDelimited_1");
						if(outtFileOutputDelimited_1!=null) {
							outtFileOutputDelimited_1.flush();
							outtFileOutputDelimited_1.close();
						}
					
				
			
		}
	

 



/**
 * [tFileOutputDelimited_1 finally ] stop
 */















				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileList_1_SUBPROCESS_STATE", 1);
	}
	

public void tFileCopy_1Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;





	
	/**
	 * [tFileCopy_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileCopy_1", false);
		start_Hash.put("tFileCopy_1", System.currentTimeMillis());
		
	
	currentComponent="tFileCopy_1";

	
		int tos_count_tFileCopy_1 = 0;
		

 



/**
 * [tFileCopy_1 begin ] stop
 */
	
	/**
	 * [tFileCopy_1 main ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 


        String srcFileName_tFileCopy_1 = ((String) globalMap.get("tFileList_1_CURRENT_FILEPATH"));

		java.io.File srcFile_tFileCopy_1 = new java.io.File(srcFileName_tFileCopy_1);

		// here need check first, before mkdirs().
		if (!srcFile_tFileCopy_1.exists() || !srcFile_tFileCopy_1.isFile()) {
			String errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1 = String.format("The source File \"%s\" does not exist or is not a file.", srcFileName_tFileCopy_1);
				System.err.println(errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
				globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageFileDoesnotExistsOrIsNotAFile_tFileCopy_1);
		}
        String desDirName_tFileCopy_1 = "/var/hubasac/archives/vente";

		String desFileName_tFileCopy_1 =  srcFile_tFileCopy_1.getName() ;

		if (desFileName_tFileCopy_1 != null && ("").equals(desFileName_tFileCopy_1.trim())){
			desFileName_tFileCopy_1 = "NewName.temp";
		}

		java.io.File desFile_tFileCopy_1 = new java.io.File(desDirName_tFileCopy_1, desFileName_tFileCopy_1);

		if (!srcFile_tFileCopy_1.getPath().equals(desFile_tFileCopy_1.getPath())  ) {
				java.io.File parentFile_tFileCopy_1 = desFile_tFileCopy_1.getParentFile();

				if (parentFile_tFileCopy_1 != null && !parentFile_tFileCopy_1.exists()) {
					parentFile_tFileCopy_1.mkdirs();
				}           
				try {
					org.talend.FileCopy.copyFile(srcFile_tFileCopy_1.getPath(), desFile_tFileCopy_1.getPath(), true, false);
				} catch (Exception e) {
globalMap.put("tFileCopy_1_ERROR_MESSAGE",e.getMessage());
						System.err.println("tFileCopy_1 " + e.getMessage());
				}
				java.io.File isRemoved_tFileCopy_1 = new java.io.File(((String) globalMap.get("tFileList_1_CURRENT_FILEPATH")));
				if(isRemoved_tFileCopy_1.exists()) {
					String errorMessageCouldNotRemoveFile_tFileCopy_1 = String.format("tFileCopy_1 - The source file \"%s\" could not be removed from the folder because it is open or you only have read-only rights.", srcFileName_tFileCopy_1);
						System.err.println(errorMessageCouldNotRemoveFile_tFileCopy_1 + "\n");
						globalMap.put("tFileCopy_1_ERROR_MESSAGE", errorMessageCouldNotRemoveFile_tFileCopy_1);
				} 

		}
		globalMap.put("tFileCopy_1_DESTINATION_FILEPATH",desFile_tFileCopy_1.getPath()); 
		globalMap.put("tFileCopy_1_DESTINATION_FILENAME",desFile_tFileCopy_1.getName()); 

		globalMap.put("tFileCopy_1_SOURCE_DIRECTORY", srcFile_tFileCopy_1.getParent());
		globalMap.put("tFileCopy_1_DESTINATION_DIRECTORY", desFile_tFileCopy_1.getParent());

 


	tos_count_tFileCopy_1++;

/**
 * [tFileCopy_1 main ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileCopy_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 process_data_end ] stop
 */
	
	/**
	 * [tFileCopy_1 end ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 

ok_Hash.put("tFileCopy_1", true);
end_Hash.put("tFileCopy_1", System.currentTimeMillis());




/**
 * [tFileCopy_1 end ] stop
 */
				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tFileCopy_1 finally ] start
	 */

	

	
	
	currentComponent="tFileCopy_1";

	

 



/**
 * [tFileCopy_1 finally ] stop
 */
				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tFileCopy_1_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "Default";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";
    
    private boolean enableLogStash;

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    protected PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final extract_vente extract_venteClass = new extract_vente();

        int exitCode = extract_venteClass.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";
	   	
        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }
        enableLogStash = "true".equalsIgnoreCase(System.getProperty("audit.enabled"));

    	
    	

        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }
        boolean inOSGi = routines.system.BundleUtils.inOSGi();

        if (inOSGi) {
            java.util.Dictionary<String, Object> jobProperties = routines.system.BundleUtils.getJobProperties(jobName);

            if (jobProperties != null && jobProperties.get("context") != null) {
                contextStr = (String)jobProperties.get("context");
            }
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = extract_vente.class.getClassLoader().getResourceAsStream("extracteur_hubasac_backup/extract_vente_1_5/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = extract_vente.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                try {
                    //defaultProps is in order to keep the original context value
                    if(context != null && context.isEmpty()) {
	                defaultProps.load(inContext);
	                context = new ContextProperties(defaultProps);
                    }
                } finally {
                    inContext.close();
                }
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
            class ContextProcessing {
                private void processContext_0() {
                        context.setContextType("vente_FieldSeparator", "id_String");
                        if(context.getStringValue("vente_FieldSeparator") == null) {
                            context.vente_FieldSeparator = null;
                        } else {
                            context.vente_FieldSeparator=(String) context.getProperty("vente_FieldSeparator");
                        }
                        context.setContextType("vente_Header", "id_Integer");
                        if(context.getStringValue("vente_Header") == null) {
                            context.vente_Header = null;
                        } else {
                            try{
                                context.vente_Header=routines.system.ParserUtils.parseTo_Integer (context.getProperty("vente_Header"));
                            } catch(NumberFormatException e){
                                System.err.println(String.format("Null value will be used for context parameter %s: %s", "vente_Header", e.getMessage()));
                                context.vente_Header=null;
                            }
                        }
                        context.setContextType("vente_Encoding", "id_String");
                        if(context.getStringValue("vente_Encoding") == null) {
                            context.vente_Encoding = null;
                        } else {
                            context.vente_Encoding=(String) context.getProperty("vente_Encoding");
                        }
                        context.setContextType("vente_File", "id_File");
                        if(context.getStringValue("vente_File") == null) {
                            context.vente_File = null;
                        } else {
                            context.vente_File=(String) context.getProperty("vente_File");
                        }
                        context.setContextType("vente_RowSeparator", "id_String");
                        if(context.getStringValue("vente_RowSeparator") == null) {
                            context.vente_RowSeparator = null;
                        } else {
                            context.vente_RowSeparator=(String) context.getProperty("vente_RowSeparator");
                        }
                        context.setContextType("pg_connexion_Port", "id_String");
                        if(context.getStringValue("pg_connexion_Port") == null) {
                            context.pg_connexion_Port = null;
                        } else {
                            context.pg_connexion_Port=(String) context.getProperty("pg_connexion_Port");
                        }
                        context.setContextType("pg_connexion_Login", "id_String");
                        if(context.getStringValue("pg_connexion_Login") == null) {
                            context.pg_connexion_Login = null;
                        } else {
                            context.pg_connexion_Login=(String) context.getProperty("pg_connexion_Login");
                        }
                        context.setContextType("pg_connexion_AdditionalParams", "id_String");
                        if(context.getStringValue("pg_connexion_AdditionalParams") == null) {
                            context.pg_connexion_AdditionalParams = null;
                        } else {
                            context.pg_connexion_AdditionalParams=(String) context.getProperty("pg_connexion_AdditionalParams");
                        }
                        context.setContextType("pg_connexion_Schema", "id_String");
                        if(context.getStringValue("pg_connexion_Schema") == null) {
                            context.pg_connexion_Schema = null;
                        } else {
                            context.pg_connexion_Schema=(String) context.getProperty("pg_connexion_Schema");
                        }
                        context.setContextType("pg_connexion_Server", "id_String");
                        if(context.getStringValue("pg_connexion_Server") == null) {
                            context.pg_connexion_Server = null;
                        } else {
                            context.pg_connexion_Server=(String) context.getProperty("pg_connexion_Server");
                        }
                        context.setContextType("pg_connexion_Password", "id_Password");
                        if(context.getStringValue("pg_connexion_Password") == null) {
                            context.pg_connexion_Password = null;
                        } else {
                            String pwd_pg_connexion_Password_value = context.getProperty("pg_connexion_Password");
                            context.pg_connexion_Password = null;
                            if(pwd_pg_connexion_Password_value!=null) {
                                if(context_param.containsKey("pg_connexion_Password")) {//no need to decrypt if it come from program argument or parent job runtime
                                    context.pg_connexion_Password = pwd_pg_connexion_Password_value;
                                } else if (!pwd_pg_connexion_Password_value.isEmpty()) {
                                    try {
                                        context.pg_connexion_Password = routines.system.PasswordEncryptUtil.decryptPassword(pwd_pg_connexion_Password_value);
                                        context.put("pg_connexion_Password",context.pg_connexion_Password);
                                    } catch (java.lang.RuntimeException e) {
                                        //do nothing
                                    }
                                }
                            }
                        }
                        context.setContextType("pg_connexion_Database", "id_String");
                        if(context.getStringValue("pg_connexion_Database") == null) {
                            context.pg_connexion_Database = null;
                        } else {
                            context.pg_connexion_Database=(String) context.getProperty("pg_connexion_Database");
                        }
                } 
                public void processAllContext() {
                        processContext_0();
                }
            }

            new ContextProcessing().processAllContext();
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }

        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {if (parentContextMap.containsKey("vente_FieldSeparator")) {
                context.vente_FieldSeparator = (String) parentContextMap.get("vente_FieldSeparator");
            }if (parentContextMap.containsKey("vente_Header")) {
                context.vente_Header = (Integer) parentContextMap.get("vente_Header");
            }if (parentContextMap.containsKey("vente_Encoding")) {
                context.vente_Encoding = (String) parentContextMap.get("vente_Encoding");
            }if (parentContextMap.containsKey("vente_File")) {
                context.vente_File = (String) parentContextMap.get("vente_File");
            }if (parentContextMap.containsKey("vente_RowSeparator")) {
                context.vente_RowSeparator = (String) parentContextMap.get("vente_RowSeparator");
            }if (parentContextMap.containsKey("pg_connexion_Port")) {
                context.pg_connexion_Port = (String) parentContextMap.get("pg_connexion_Port");
            }if (parentContextMap.containsKey("pg_connexion_Login")) {
                context.pg_connexion_Login = (String) parentContextMap.get("pg_connexion_Login");
            }if (parentContextMap.containsKey("pg_connexion_AdditionalParams")) {
                context.pg_connexion_AdditionalParams = (String) parentContextMap.get("pg_connexion_AdditionalParams");
            }if (parentContextMap.containsKey("pg_connexion_Schema")) {
                context.pg_connexion_Schema = (String) parentContextMap.get("pg_connexion_Schema");
            }if (parentContextMap.containsKey("pg_connexion_Server")) {
                context.pg_connexion_Server = (String) parentContextMap.get("pg_connexion_Server");
            }if (parentContextMap.containsKey("pg_connexion_Password")) {
                context.pg_connexion_Password = (java.lang.String) parentContextMap.get("pg_connexion_Password");
            }if (parentContextMap.containsKey("pg_connexion_Database")) {
                context.pg_connexion_Database = (String) parentContextMap.get("pg_connexion_Database");
            }
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
			parametersToEncrypt.add("pg_connexion_Password");
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();


this.globalResumeTicket = true;//to run tPreJob





this.globalResumeTicket = false;//to run others jobs

try {
errorCode = null;tFileList_1Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tFileList_1) {
globalMap.put("tFileList_1_SUBPROCESS_STATE", -1);

e_tFileList_1.printStackTrace();

}

this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : extract_vente");
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;


    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {


    }














    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();






        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        } else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		} else if (arg.startsWith("--audit.enabled") && arg.contains("=")) {//for trunjob call
		    final int equal = arg.indexOf('=');
			final String key = arg.substring("--".length(), equal);
			System.setProperty(key, arg.substring(equal + 1));
		}
    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     352411 characters generated by Talend Open Studio for ESB 
 *     on the 9 avril 2023 à 07:34:48 CEST
 ************************************************************************************************/