#! /bin/bash
echo ""
echo "Welcome to the Talent cron job for Asac Hub Extractor"
echo ""
jobdir="/srv/asac/app/hubasac/extract-svc/extract_hubasac"
logdir="/var/log/hubasac"
jobname="extract_hubasac_run"
echo ""
#--------please comment the next 20 lines ou if you intend to pass external parameters ------------
#
# jobdir=${1}
# jobname=${2}
# logdir=${3}
#
# if [ ${#} -ne 3 ]; then
#        echo "runTalendJob: usage: project_folder job_name log_folder"
#        exit 2
# fi
#----------End of commented lines

echo "`date`: Starting Job $jobname" 1>> ${logdir}/$jobname.log
#$jobdir/$jobname.sh 1>> $logdir/$jobname.log 2>&1
cd $jobdir
/bin/sh $jobname.sh 1 | tee $logdir/$jobname.log 2>&1
JobExitStatus=${?}

if [ ${JobExitStatus} -eq 0 ]; then
        echo "`date`: Job $jobdir.$jobname ended successfully" 1>> $logdir/$jobname.log
else
        echo "`date`: Job $jobdir.$jobname ended in error" 1>> $logdir/$jobname.log
fi
exit ${JobExitStatus}
